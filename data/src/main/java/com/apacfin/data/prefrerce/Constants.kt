package com.apacfin.data.prefrerce


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

object Constants {

    var Contact1 = "022 - 66668169"
    var Contact2 = "1800-313-205-205"
    var ContactUsEmail = "contactus@apacfin.com"
    var CustomercareEmail = "customercarehf@apacfin.com"
    var FACEBOOK_PAGE_ID = "108383701519888"

    val dateFormat = "dd-MMM-yyyy"
    const val CUSTOMER = "CUST/" //--Realtime
    const val FCM = "FCM/"
    var Token = "token"
    var Ucic = "ucic"
    var Name = "name"
    var MobileNo = "mobileNo"
    var Fname = "fName"
    var Lname = "lName"
    var Pincode = "pincode"
    var DOB = "DOB"
    var RefNo = "refNo"
    var LoanID = "loanID"
    var DocID = "docID"
    var FromView = "fromview"
    var FromDate = "fromdate"
    var ToDate = "todate"
    var UserNew = 0
    var UserExisting = 1
    var Language = 2
    var fromRegistation = 1
    var fromProfile = 2
    var IsLogin = "IsLogin"
    var IsRegistred = "IsRegistred"
    var SetLanguage = "SetLanguage"
    var IsLanguageSet = "IsLanguageSet"
    var Latitude = "latitude"
    var Longitude = "longitude"
    var AppVersion = "appVersion"
    var SimMobNo = "simMobNo"
    var ImeiNo = "imei"
    var FirebaseToken = "firebaseToken"
    var gratitudeFlag = "gratitudeFlag"
    var isDayFirstLogin = "isDayFirstLogin"
    var LoginDate = "LoginDate"
    var VerifyOTPFrg = "VerifyOTPFrg"
    var ResetMPINFrag = "ResetMPINFrag"
    var LoginMPINFrag = "LoginMPINFrag"

    var GOTOREGFrag = "GoToRegFrag"
    var GenerateVCard = "GenerateVCard"
    var BranchConnect = "BranchConnect"
    var PayEMI = "PayEMI"
    var ReferandEarn = "ReferandEarn"
    var LoanTracker = "LoanTracker"
    var Documents = "Documents"
    var LoanViewDetails = "LoanViewDetails"
    var Type = "type"

    var InAppReview = "InAppReview"
    var InAppReviewDone = "InAppReviewDone"

}
