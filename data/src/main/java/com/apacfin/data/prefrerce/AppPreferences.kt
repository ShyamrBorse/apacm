package com.apacfin.data.prefrerce

import android.content.Context
import android.content.SharedPreferences


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class AppPreferences (var context: Context) {
    var sharedPreferences: SharedPreferences

    companion object {
        var appPreferences: AppPreferences? = null
        fun getAppPreferences(context: Context): AppPreferences? {
            if (appPreferences != null) {
                return appPreferences
            }
            appPreferences = AppPreferences(context)
            return appPreferences
        }
    }

    init {
        /*val res = context.resources
        val preferencesName = res.getString(R.string.app_name)*/
        sharedPreferences = context.getSharedPreferences("ApacM", Context.MODE_PRIVATE)
    }


    fun putBoolean(key: String?, value: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getBoolean(key: String?, defaultValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    fun putString(key: String?, value: String?) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getString(key: String?, defaultValue: String?): String? {
        return sharedPreferences.getString(key, defaultValue)
    }

    fun putInt(key: String?, value: Int): Int {
        val editor = sharedPreferences.edit()
        editor.putInt(key, value)
        editor.commit()
        return value
    }

    fun getInt(key: String?, defaultValue: Int): Int {
        return sharedPreferences.getInt(key, defaultValue)
    }


}