package com.apacfin.data


import com.apacfin.data.model.LoginModel
import com.apacfin.data.model.SliderResponce
import com.apacfin.data.model.alert.AlertModelResponse
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.data.model.businesscard.BusinessCardResponse
import com.apacfin.data.model.document.DocViewModelResponse
import com.apacfin.data.model.document.DocumentModelResponse
import com.apacfin.data.model.esign.EsignLoanDetailsModelResponse
import com.apacfin.data.model.esign.ListOfEsignLinksModelResponse
import com.apacfin.data.model.home.*
import com.apacfin.data.model.language.LanguageModelResponse
import com.apacfin.data.model.listofchaarges.ListOfChargesModelResponse
import com.apacfin.data.model.myloan.AccountStatementModelResponse
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.payment.PaymentApiKeyResponse
import com.apacfin.data.model.payment.TransactionListResponse
import com.apacfin.data.model.profile.AppDetailModel
import com.apacfin.data.model.referal.LoanTypeModelResponse
import com.apacfin.data.model.registration.GetDetailsFromUCIC
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.Constants.CUSTOMER
import com.apacfin.data.prefrerce.Constants.FCM
import retrofit2.http.*


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : ApiService.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */


interface ApiService {

    @POST(CUSTOMER + "registration")
    suspend fun getRegistration(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + "mobileVerification")
    suspend fun mobileVerification(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + "setMpin")
    suspend fun setMpin(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + "mPinLogin")
    suspend fun mPinLogin(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + "newUser")
    suspend fun newUser(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + "setLangForUser")
    suspend fun setLangForUser(@Body body: MutableMap<String, String>): RegistrationModel

    @GET(CUSTOMER + "getSlider")
    suspend fun sliderList(): SliderModel

    @GET(CUSTOMER + "getBanner")
    suspend fun getBanner(): BannerModelResponse

    @GET(CUSTOMER + "getLoanDetails")
    suspend fun getLoanDetails(): LoanDetailsModelResponse

    @POST(CUSTOMER + "getAccountStatement/{id}")
    suspend fun getAccountStatement(
        @Path("id") id: String,
        @Body body: MutableMap<String, String>
    ): AccountStatementModelResponse

    @GET(CUSTOMER + "getPaymentShedule/{id}")
    suspend fun getPaymentShedule(
        @Path("id") id: String
    ): AccountStatementModelResponse

    @GET(CUSTOMER + "getDocument/{id}/{loanId}")
    suspend fun getDocument(
        @Path("id") d: String, @Path("loanId") loanID: String
    ): DocViewModelResponse

    @GET(CUSTOMER + "getSlider")
    suspend fun sliderData(): SliderResponce

    @GET(CUSTOMER + "getListOfCharges")
    suspend fun getListOfCharges(@Query("data") d: String): ListOfChargesModelResponse

    @GET(CUSTOMER + "getAlertList")
    suspend fun getAlertList(): AlertModelResponse

    @GET(CUSTOMER + "getListLang")
    suspend fun getListLang(): LanguageModelResponse

    @GET(CUSTOMER + "getSignatureDocument")
    suspend fun getSignatureDocument(): DocumentModelResponse

    @GET(CUSTOMER + "getAllDocument")
    suspend fun getAllDocument(): DocumentModelResponse

    @GET(CUSTOMER + "logout")
    suspend fun logout(): RegistrationModel

    @POST(CUSTOMER + "getAppDetails")
    suspend fun getAppDetails(@Body body: MutableMap<String, String>): AppDetailModel

    @POST(CUSTOMER + "saveServiceComplaint")
    suspend fun saveServiceComplaint(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + "saveAccountStatementRequest")
    suspend fun saveAccountStatementRequest(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + "saveReferral")
    suspend fun saveReferral(@Body body: MutableMap<String, String>): RegistrationModel

    @POST(CUSTOMER + FCM + "updateUserToken")
    suspend fun updateUserToken(@Body body: MutableMap<String, String>): RegistrationModel

    @GET(CUSTOMER + "getLoanType")
    suspend fun getLoanType(): LoanTypeModelResponse

    //------

    @POST("apacMlogin")
    suspend fun login(@Body body: MutableMap<String, String>): LoginModel

    @GET(CUSTOMER + "getDetailsFromUCIC/{id}")
    suspend fun getDetailsFromUCIC(@Path("id") d: String): GetDetailsFromUCIC

    @GET(CUSTOMER + "getUCIC/{id}")
    suspend fun getUCIC(@Path("id") d: String): GetDetailsFromUCIC

    @POST(CUSTOMER + "getOTP/{id}")
    suspend fun getOTP(@Path("id") d: String): GetDetailsFromUCIC

    @POST(CUSTOMER + "validateOTP")
    suspend fun validateOTP(@Body body: MutableMap<String, String>): GetDetailsFromUCIC

    @POST(CUSTOMER + "validateOTPnSendUCIC")
    suspend fun validateOTPnSendUCIC(@Body body: MutableMap<String, String>): GetDetailsFromUCIC

    @POST(CUSTOMER + "getMyLoans")
    suspend fun getMyLoans(): LoanDetailsModelResponse

    @POST(CUSTOMER + "getTestimonials")
    suspend fun getTestimonials(): TestimonialModelResponse

    //------eSign Api

    @GET(CUSTOMER + "getLoanDetailsEsign")
    suspend fun getLoanDetailsForEsign(): EsignLoanDetailsModelResponse

    @POST(CUSTOMER + "getListOfEsignLinks/{id}/{type}")
    suspend fun getListOfEsignLinks(@Path("id") d: String,@Path("type") d1: Int?): ListOfEsignLinksModelResponse

    @POST(CUSTOMER + "initiateEsign/{id}/{type}")
    suspend fun initiateEsign(@Path("id") d: String,@Path("type") d1: Int): RegistrationModel

    @POST(CUSTOMER + "esignInitialise")
    suspend fun esignInitialise(@Body body: MutableMap<String, String>): RegistrationModel

    //------Paymentgateway Api

    @GET(CUSTOMER + "getPaymentApiKey")
    suspend fun getPaymentApiKey(): PaymentApiKeyResponse

    @POST(CUSTOMER + "createOrderId")
    suspend fun createOrderId(@Body body: MutableMap<String, String>): PaymentApiKeyResponse

    @GET(CUSTOMER + "getEmiOptionsAmt/{lanID}")
    suspend fun getEmiOptionsAmt(@Path("lanID") d: String): PaymentApiKeyResponse

    @POST(CUSTOMER + "getAndUpdatePaymentStatus")
    suspend fun updatePaymentStatus(@Body body: MutableMap<String, String>): PaymentApiKeyResponse

    @GET(CUSTOMER + "getListOfTranctions")
    suspend fun paymentTransactionList(): TransactionListResponse


    //------Bussinesscard Api
    @GET(CUSTOMER + "getTemplateList")
    suspend fun getTemplateList(): SliderModel

    @GET(CUSTOMER + "getBusinessDetailsList")
    suspend fun getBusinessDetailsList(): BusinessCardResponse

    /*@GET(CUSTOMER + "getBusinessCardList")
    suspend fun getBusinessCardList(): BusinessCardResponse*/

    @POST(CUSTOMER + "saveUpdateBusinessDetailsList")
    suspend fun saveUpdateBusinessDetailsList(@Body body: BusinessCardModel): BusinessCardResponse

    //------Gratitude Api
    @POST(CUSTOMER + "getGratitudeImageList")
    suspend fun getGratitude(@Body body: MutableMap<String, String>): SliderModel

    //------Gratitude Api
    @POST(CUSTOMER + "getAlphaLoanStageDetails/{lanID}")
    suspend fun getAlphaLoanStageDetails(@Path("lanID") d: String): RegistrationModel

    //------emiPaymentHistoryList Api
    @POST(CUSTOMER + "getEMIHeatMapDataByLanId/{lanID}")
    suspend fun getemiPaymentHistoryList(@Path("lanID") d: String): PaymentHistoryResponse

    //------getBranchDetails Api
    @POST(CUSTOMER + "getBranchDetailsByLoanId/{lanID}")
    suspend fun getBranchDetailsByLoanId(@Path("lanID") d: String): BranchDetailsModel

    @GET("CUSTOMER/getKnowledgeSeries/{type}")
    suspend fun getKnowledgeSeries(@Path("type") type: String): SliderModel


}