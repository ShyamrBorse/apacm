package com.apacfin.data.model.alert

data class AlertModel(
    val date: String,
    val title: String,
    val message: String
)