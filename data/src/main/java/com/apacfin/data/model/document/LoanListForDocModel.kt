package com.apacfin.data.model.document

import com.apacfin.data.model.myloan.LoanDetailModel

data class LoanListForDocModel(
    val documents: List<DocumentListModel>,
    val loanDetails: LoanDetailModel
)