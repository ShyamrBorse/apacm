package com.apacfin.data.model.home

data class PaymentHistoryResponse(
    var emiDetailList : ArrayList<EmiPayHistoryDetailModel> = ArrayList<EmiPayHistoryDetailModel>(),
    var status : Boolean? = null,
    var msg : String? = null
)
