package com.apacfin.data.model.myloan

data class StatementModel(
    val paidAmount: Int,
    val paidOn: String,
    val paymentMode: String
)