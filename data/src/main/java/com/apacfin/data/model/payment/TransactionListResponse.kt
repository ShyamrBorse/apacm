package com.apacfin.data.model.payment

data class TransactionListResponse(
    val msg: String,
    val status: Boolean,
    val transactionList: List<TransactionModel>
)