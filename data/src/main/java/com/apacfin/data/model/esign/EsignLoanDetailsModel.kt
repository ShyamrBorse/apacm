package com.apacfin.data.model.esign


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : EsignLoanDetailsModel.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

data class EsignLoanDetailsModel(
    val loanID: String,
    val applicantName: String,
    val sanctionAmount: String,
    val intiStampStatus: String,
    val refId: String
)
