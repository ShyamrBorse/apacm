package com.apacfin.data.model.esign

data class EsignLoanDetailsModelResponse(
    val loanDetails: List<EsignLoanDetailsModel>,
    val msg: String,
    val status: Boolean,
    val type : Int?
)