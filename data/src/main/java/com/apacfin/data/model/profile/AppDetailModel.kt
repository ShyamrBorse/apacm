package com.apacfin.data.model.profile

data class AppDetailModel(
    val forceUpdate: Boolean,
    val version: String,
    val msg: String,
    val status: Boolean
)