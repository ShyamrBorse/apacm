package com.apacfin.data.model.myloan

data class LoanDetailsModelResponse(
    val loanDetails: List<LoanDetailModel>,
    val msg: String,
    val status: Boolean
)