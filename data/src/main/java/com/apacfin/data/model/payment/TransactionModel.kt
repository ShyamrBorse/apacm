package com.apacfin.data.model.payment

data class TransactionModel(
    val apacStatus: String,
    val createdOn: String,
    val lan: String,
    val mobileNUmber: String,
    val orderAmount: String,
    val orderId: String,
    val transactionId: String,
    val paymentMethod: String,
    val paymentNote: String
)