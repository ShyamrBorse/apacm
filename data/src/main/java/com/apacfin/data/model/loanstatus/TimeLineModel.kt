package com.apacfin.data.model.loanstatus

import android.os.Parcel
import android.os.Parcelable


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */


class TimeLineModel(
    var title: String,
    var stageStatus: Boolean,
    var seq: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readByte() != 0.toByte(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeByte(if (stageStatus) 1 else 0)
        parcel.writeInt(seq)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TimeLineModel> {
        override fun createFromParcel(parcel: Parcel): TimeLineModel {
            return TimeLineModel(parcel)
        }

        override fun newArray(size: Int): Array<TimeLineModel?> {
            return arrayOfNulls(size)
        }
    }
    override fun toString(): String {
        return "TimeLineModel(title='$title', stageStatus=$stageStatus, seq=$seq)"
    }

}