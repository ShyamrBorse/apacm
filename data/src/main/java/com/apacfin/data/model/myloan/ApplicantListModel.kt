package com.apacfin.data.model.myloan

data class ApplicantListModel(
    val applicantName: String,
    val applicantType: String
)