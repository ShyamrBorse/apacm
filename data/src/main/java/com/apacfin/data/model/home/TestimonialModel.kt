package com.apacfin.data.model.home

data class TestimonialModel(
    var userName: String,
    val comment: String,
    val imageUrl: String
)