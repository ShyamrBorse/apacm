package com.apacfin.data.model.home

data class TestimonialModelResponse(
    val testimonialData: List<TestimonialModel>,
    val msg: String,
    val status: Boolean
)