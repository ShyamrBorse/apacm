package com.apacfin.data.model.home

data class BranchDetailsModel(
    var branchName: String,
    var branchAddress: String,
    var activeManagerName: String,
    var managerMobileNumber: String,
    var msg: String,
    var status: Boolean
)
