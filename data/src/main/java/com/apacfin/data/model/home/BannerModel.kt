package com.apacfin.data.model.home

data class BannerModel(
    val subTitle: String,
    val title: String,
    val loanId: String,
    val action: String
)