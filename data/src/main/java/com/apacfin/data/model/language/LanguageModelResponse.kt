package com.apacfin.data.model.language

data class LanguageModelResponse(
    val languages: List<LanguageModel>,
    val msg: String,
    val status: Boolean
)