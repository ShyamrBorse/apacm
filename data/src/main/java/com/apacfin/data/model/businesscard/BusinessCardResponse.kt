package com.apacfin.data.model.businesscard

data class BusinessCardResponse(
    val msg: String,
    val status: Boolean,
    val sliderList: BusinessCardModel
)