package com.apacfin.data.model.esign

data class ListOfEsignLinksModelResponse(
    val applicantsList: List<ListOfEsignLinksModel>,
    val msg: String,
    val status: Boolean
)