package com.apacfin.data.model.referal

data class LoanTypeModel(
    val code: String,
    val codeValueName: String
)