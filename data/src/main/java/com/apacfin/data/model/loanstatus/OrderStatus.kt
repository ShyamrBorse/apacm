package com.apacfin.data.model.loanstatus

enum class OrderStatus {
    COMPLETED,
    ACTIVE,
    INACTIVE
}