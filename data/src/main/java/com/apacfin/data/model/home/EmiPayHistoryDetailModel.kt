package com.apacfin.data.model.home

data class EmiPayHistoryDetailModel(
    var emiDate : String?,
    var emiAmount : String?,
    var customerPaidAmount : String?,
    var isBounce : Boolean?
)
