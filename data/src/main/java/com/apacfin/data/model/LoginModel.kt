package com.apacfin.data.model

data class LoginModel(
    val branchId: String,
    val branchName: String,
    val businessType: String,
    val employeeId: String,
    val employeeName: String,
    val errors: List<Any>,
    val latitude: String,
    val loginDateTime: String,
    val longitude: String,
    val msg: String,
    val status: Boolean,
    val token: String,
    val version: String
)