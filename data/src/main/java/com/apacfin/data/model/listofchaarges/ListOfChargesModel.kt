package com.apacfin.data.model.listofchaarges

data class ListOfChargesModel(
    val name: String,
    val particulars: String
)