package com.apacfin.data.model.home

data class SliderModel(
    val msg: String,
    val sliderList: List<String>,
    val status: Boolean
)