package com.apacfin.data.model.esign

data class ListOfEsignLinksModel(
    val applicantId: String,
    val applicantName: String,
    val applicantType: String,
    val esignLisnk: String,
    val esignStatus: String,
    val mobileNo: String
)