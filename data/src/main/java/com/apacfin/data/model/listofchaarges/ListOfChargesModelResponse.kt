package com.apacfin.data.model.listofchaarges

data class ListOfChargesModelResponse(
    val charges: List<ListOfChargesModel>,
    val msg: String,
    val status: Boolean
)