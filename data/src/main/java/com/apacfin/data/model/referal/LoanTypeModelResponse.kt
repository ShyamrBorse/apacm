package com.apacfin.data.model.referal

data class LoanTypeModelResponse(
    val loanType: List<LoanTypeModel>,
    val msg: String,
    val status: Boolean
)