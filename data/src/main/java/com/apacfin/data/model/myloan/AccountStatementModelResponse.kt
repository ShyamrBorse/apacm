package com.apacfin.data.model.myloan

data class AccountStatementModelResponse(
    val loanDetails: LoanDetailModel,
    val msg: String,
    val statements: List<StatementModel>,
    val status: Boolean
)