package com.apacfin.data.model.language

data class LanguageModel(
    val code: String,
    val codeValueName: String,
    val codeValue: String
)