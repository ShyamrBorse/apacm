package com.apacfin.data.model.document

data class DocumentModelResponse(
    val data: List<LoanListForDocModel>,
    val msg: String,
    val status: Boolean
)