package com.apacfin.data.model.document

data class DocViewModelResponse(
    val document: DocumentsPojo,
    val msg: String,
    val status: Boolean
)