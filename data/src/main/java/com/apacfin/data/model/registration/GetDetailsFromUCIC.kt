package com.apacfin.data.model.registration

data class GetDetailsFromUCIC(
    val refNo: String,
    val mobileNo: String,
    val name: String,
    val newUsrFlg: String,
    val msg: String,
    val status: Boolean
)