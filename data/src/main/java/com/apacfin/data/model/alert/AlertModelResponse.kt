package com.apacfin.data.model.alert

data class AlertModelResponse(
    val alerts: List<AlertModel>,
    val msg: String,
    val status: Boolean
)