package com.apacfin.data.model.registration

import com.apacfin.data.model.loanstatus.TimeLineModel

data class RegistrationModel(
    val refNo: String,
    val name: String,
    val ucic: String,
    val lang: String,
    val token: String,
    val newUserFlag: Boolean,
    val gratitudeFlag: String,
    val loanStageData: ArrayList<TimeLineModel>,
    val msg: String,
    val status: Boolean
)