package com.apacfin.data.model.payment

data class VpaDetailsModel(
    val accountNo: String,
    val ifscCode: String,
    val branchName: String,
    val accountHolderName: String,
)
