package com.apacfin.data.model.document

data class DocumentListModel(
    val file: String,
    val id: Int,
    val name: String,
    val status: String
)