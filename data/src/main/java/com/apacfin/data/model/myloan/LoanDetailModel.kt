package com.apacfin.data.model.myloan

data class LoanDetailModel(
    val applicant: String,
    val balanceTenure: Int,
    val disbursedAmount: Int,
    val dueDate: String,
    val emiAmount: Int,
    val loanId: String,
    val loanStatus: String,
    val loanType: String,
    val principleAmount: Int,
    val totalAmount: Int,
    val lastEMIDate: String,
    var isLoanTypeSelectedForHistory: Boolean?=false,
    val applicantDetails: List<ApplicantListModel>
)