package com.apacfin.data.model.payment


data class PaymentApiKeyResponse(
    val keyId: String,
    val keySecret: String,
    val refId: String,
    val orderId: String,
    val emiAmount: String,
    val emiStatic: String,
    val emiWithCharges: String,
    val charges: String,
    val msg: String,
    val status: Boolean,
    val vpaAccountDetails: VpaDetailsModel
)