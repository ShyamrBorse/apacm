package com.apacfin.data.model.businesscard

data class BusinessCardModel(
    var id: String?,
    var lan: String,
    var applicantName: String,
    var mobile: String,
    var dob: String,
    var businessName: String,
    var address1: String,
    var address2: String,
    var city: String,
    var pincode: String,
    var image: String,
    var updatedOn: String?,
    var createdOn: String?
)