package com.apacfin.data.model.home

data class BannerModelResponse(
    val banners: List<BannerModel>,
    val msg: String,
    val status: Boolean
)