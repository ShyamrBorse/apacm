package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class RegistrationRepository @Inject constructor(
    private val api: ApiService,

    ) : SafeApiCall {

    suspend fun getRegistration(params: MutableMap<String, String>) = safeApiCall {
        api.getRegistration(params)
    }

    suspend fun mobileVerification(params: MutableMap<String, String>) = safeApiCall {
        api.mobileVerification(params)
    }

    suspend fun setMpin(params: MutableMap<String, String>) = safeApiCall {
        api.setMpin(params)
    }

    suspend fun getDetailsFromUCIC(params: String) = safeApiCall {
        api.getDetailsFromUCIC(params)
    }

    suspend fun getUCIC(params: String) = safeApiCall {
        api.getUCIC(params)
    }

    suspend fun getOTP(params: String) = safeApiCall {
        api.getOTP(params)
    }

    suspend fun validateOTP(params: MutableMap<String, String>) = safeApiCall {
        api.validateOTP(params)
    }

    suspend fun validateOTPnSendUCIC(params: MutableMap<String, String>) = safeApiCall {
        api.validateOTPnSendUCIC(params)
    }

    suspend fun mPinLogin(params: MutableMap<String, String>) = safeApiCall {
        api.mPinLogin(params)
    }

    suspend fun newUser(params: MutableMap<String, String>) = safeApiCall {
        api.newUser(params)
    }

    suspend fun getAppDetails(params: MutableMap<String, String>) = safeApiCall {
        api.getAppDetails(params)
    }

    suspend fun  updateUserToken(params: MutableMap<String, String>) = safeApiCall {
        api.updateUserToken(params)
    }

}