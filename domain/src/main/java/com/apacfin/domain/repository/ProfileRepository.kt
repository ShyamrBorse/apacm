package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : LanguageRepository.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class ProfileRepository @Inject constructor(
    private val api: ApiService,

    ) : SafeApiCall {


    suspend fun setLangForUser(params: MutableMap<String, String>) = safeApiCall {
        api.setLangForUser(params)
    }

    suspend fun getListLang() = safeApiCall {
        api.getListLang()
    }

    suspend fun logout() = safeApiCall {
        api.logout()
    }

    suspend fun saveServiceComplaint(params: MutableMap<String, String>) = safeApiCall {
        api.saveServiceComplaint(params)
    }

    suspend fun saveReferral(params: MutableMap<String, String>) = safeApiCall {
        api.saveReferral(params)
    }

    suspend fun getLoanType() = safeApiCall {
        api.getLoanType()
    }

    suspend fun getMyLoans() = safeApiCall {
        api.getMyLoans()
    }

    suspend fun getTemplateList() = safeApiCall {
        api.getTemplateList()
    }

    suspend fun getBusinessDetailsList() = safeApiCall {
        api.getBusinessDetailsList()
    }

    /*suspend fun getBusinessCardList() = safeApiCall {
        api.getBusinessCardList()
    }*/

    suspend fun saveUpdateBusinessDetailsList(params:  BusinessCardModel ) = safeApiCall {
        api.saveUpdateBusinessDetailsList(params)
    }

    suspend fun getBranchDetails(lanID: String) = safeApiCall {
        api.getBranchDetailsByLoanId(lanID)
    }
}