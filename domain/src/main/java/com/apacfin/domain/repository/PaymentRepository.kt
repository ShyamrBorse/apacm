package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : PaymentRepository.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class PaymentRepository @Inject constructor(
    private val api: ApiService,

    ) : SafeApiCall {


    suspend fun getPaymentApiKey() = safeApiCall {
        api.getPaymentApiKey()
    }

    suspend fun createOrderId(params: MutableMap<String, String>) = safeApiCall {
        api.createOrderId(params)
    }

    suspend fun getEmiOptionsAmt(id: String) = safeApiCall {
        api.getEmiOptionsAmt(id)
    }

    suspend fun updatePaymentStatus(params: MutableMap<String, String>) = safeApiCall {
        api.updatePaymentStatus(params)
    }

    suspend fun paymentTransactionList() = safeApiCall {
        api.paymentTransactionList()
    }


}