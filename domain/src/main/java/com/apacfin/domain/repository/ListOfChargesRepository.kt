package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : ListOfChargesRepository.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class ListOfChargesRepository @Inject constructor(private val api: ApiService) : SafeApiCall {
    suspend fun getListOfCharges(data:String) = safeApiCall {
        api.getListOfCharges(data)
    }
}