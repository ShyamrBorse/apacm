package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : DocumentRepository.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class DocumentRepository @Inject constructor (
    private val api: ApiService,

    ) : SafeApiCall {

    suspend fun getSignatureDocument() = safeApiCall {
        api.getSignatureDocument()
    }
    suspend fun getLoanDetailsForEsign() = safeApiCall {
        api.getLoanDetailsForEsign()
    }

    suspend fun getListOfEsignLinks(refId: String,type:Int?) = safeApiCall {
        api.getListOfEsignLinks(refId,type)
    }

    suspend fun initiateEsign(loanID: String,type:Int) = safeApiCall {
        api.initiateEsign(loanID,type)
    }

    suspend fun esignInitialise(params: MutableMap<String, String>) = safeApiCall {
        api.esignInitialise(params)
    }

    suspend fun getAllDocument() = safeApiCall {
        api.getAllDocument()
    }




}