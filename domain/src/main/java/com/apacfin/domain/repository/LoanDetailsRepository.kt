package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class LoanDetailsRepository @Inject constructor(
    private val api: ApiService,

    ) : SafeApiCall {

    suspend fun getLoanDetails() = safeApiCall {
        api.getLoanDetails()
    }

    suspend fun getAccountStatement(id: String, params: MutableMap<String, String>) = safeApiCall {
        api.getAccountStatement(id, params)
    }

     suspend fun saveAccountStatementRequest(params: MutableMap<String, String>) = safeApiCall {
        api.saveAccountStatementRequest(params)
    }

    suspend fun getPaymentShedule(id: String) = safeApiCall {
        api.getPaymentShedule(id)
    }

    suspend fun getDocument(id: String,loanID: String) = safeApiCall {
        api.getDocument(id,loanID)
    }


}