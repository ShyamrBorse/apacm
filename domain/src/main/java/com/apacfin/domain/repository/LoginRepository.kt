package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class LoginRepository @Inject constructor (
    private val api: ApiService,

    ) : SafeApiCall {

    suspend fun login(params: MutableMap<String, String>) = safeApiCall {
        api.login(params)
    }

    /* suspend fun homePageFlags(id:Int) = safeApiCall {
        api.homePageFlags(id)
    }
    suspend fun registerLiveEvent(body: RequestBody)= safeApiCall {
        api.registerLiveEvent(body)
    }*/
}