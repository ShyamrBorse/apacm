package com.apacfin.domain.repository

import com.apacfin.data.ApiService
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.domain.SafeApiCall
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class HomeRepository @Inject constructor(
    private val api: ApiService,

    ) : SafeApiCall {

    suspend fun sliderData() = safeApiCall {
        api.sliderData()
    }

    suspend fun sliderList() = safeApiCall {
        api.sliderList()
    }

    suspend fun getBanner() = safeApiCall {
        api.getBanner()
    }

    suspend fun getMyLoans() = safeApiCall {
        api.getMyLoans()
    }

    suspend fun getTestimonials() = safeApiCall {
        api.getTestimonials()
    }

    suspend fun getGratitude(param: MutableMap<String, String>) = safeApiCall {
        api.getGratitude(param)
    }

    suspend fun getAlphaLoanStageDetails(lanID: String) = safeApiCall {
        api.getAlphaLoanStageDetails(lanID)
    }

    suspend fun getBusinessDetailsList() = safeApiCall {
        api.getBusinessDetailsList()
    }

    suspend fun getTemplateList() = safeApiCall {
        api.getTemplateList()
    }

    suspend fun saveUpdateBusinessDetailsList(params: BusinessCardModel) = safeApiCall {
        api.saveUpdateBusinessDetailsList(params)
    }

    suspend fun emiPaymentHistoryList(lanID: String) = safeApiCall {
        api.getemiPaymentHistoryList(lanID)
    }

    suspend fun getBranchDetails(lanID: String) = safeApiCall {
        api.getBranchDetailsByLoanId(lanID)
    }
    suspend fun getKnowledgeSeries(type: String) = safeApiCall {
        api.getKnowledgeSeries(type)
    }
}