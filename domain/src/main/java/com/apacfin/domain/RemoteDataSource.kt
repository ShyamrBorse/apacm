package com.apacfin.domain

import android.content.Context
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants.AppVersion
import com.apacfin.data.prefrerce.Constants.ImeiNo
import com.apacfin.data.prefrerce.Constants.Latitude
import com.apacfin.data.prefrerce.Constants.Longitude
import com.apacfin.data.prefrerce.Constants.Token
import com.google.gson.GsonBuilder
import okhttp3.*

import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */


class RemoteDataSource @Inject constructor() {

    companion object {

        private lateinit var interceptor: HttpLoggingInterceptor
        //private lateinit var okHttpClient: OkHttpClient
    }

    fun <Api> buildApi(
        api: Class<Api>,
        context: Context
    ): Api {
        val appPreferences: AppPreferences = AppPreferences.getAppPreferences(context)!!
        interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        /*okHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                     .addHeader("Authorization", appPreferences.getString(Token, "")!!)
                    .addHeader("imei", appPreferences.getString(ImeiNo, "")!!)
                    .addHeader("lat", appPreferences.getString(Latitude, "")!!)
                    .addHeader("long", appPreferences.getString(Longitude, "")!!)
                    .addHeader("appVersion", appPreferences.getString(AppVersion, "")!!)
                   // .addHeader("appVersion", appPreferences.getString(SimMobNo, "")!!)
                    //.addHeader("Source", "M")
                    //                                .addHeader("X-APAC-Key", "KEY-MOB-#23")
                    //                                .addHeader("ReqDT", new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime()))
                    .build()
                chain.proceed(request)
            }
            //.addInterceptor(BasicAuthInterceptorVimeo())
            .connectionSpecs(
                Arrays.asList(
                    ConnectionSpec.MODERN_TLS,
                    ConnectionSpec.COMPATIBLE_TLS,
                    ConnectionSpec.CLEARTEXT))
            .followRedirects(true)
            .followSslRedirects(true)
            .retryOnConnectionFailure(true)
            .connectTimeout(40, TimeUnit.SECONDS)
            .readTimeout(40, TimeUnit.SECONDS)
            .writeTimeout(40, TimeUnit.SECONDS)
            .cache(null)
            .build()*/

        /*   val okkHttpclient = OkHttpClient.Builder()
               .addInterceptor(networkConnectionInterceptor)
               .build()*/

        val url: HttpUrl = HttpUrl.Builder()
            .scheme("https")
            .host(BuildConfig.BASE_URL)
            .port(Integer.parseInt(BuildConfig.Port))
            .build()

        val gson = GsonBuilder().create()
        return Retrofit.Builder()
            //.client(okHttpClient)
            .client(getUnsafeOkHttpClient(context))
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(api)
    }



    private fun getUnsafeOkHttpClient(context: Context): OkHttpClient {

        val appPreferences = AppPreferences.getAppPreferences(context)
        return try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts =
                arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate> {
                            return arrayOf()
                        }
                    }
                )

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .retryOnConnectionFailure(true)
                .addInterceptor(Interceptor { chain ->
                    val request = chain.request()
                        .newBuilder() //                                    .addHeader("Authorization", EncryptionUtils.decrypt(context, appPreferences.getString(Constants.Token,""))+"")
                        .addHeader("Authorization", appPreferences?.getString(Token, "")!!)
                        .addHeader("imei", appPreferences.getString(ImeiNo, "")!!)
                        .addHeader("lat", appPreferences.getString(Latitude, "")!!)
                        .addHeader("long", appPreferences.getString(Longitude, "")!!)
                        .addHeader("appVersion", appPreferences.getString(AppVersion, "")!!)
                        .build()
                    chain.proceed(request)
                })
                .connectionSpecs(
                    Arrays.asList(
                        ConnectionSpec.MODERN_TLS,
                        ConnectionSpec.COMPATIBLE_TLS,
                        ConnectionSpec.CLEARTEXT))
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .writeTimeout(40, TimeUnit.SECONDS)
                .cache(null)
            builder.hostnameVerifier { hostname, session -> true }
            builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}