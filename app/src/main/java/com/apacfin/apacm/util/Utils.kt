package com.apacfin.apacm.util

import android.Manifest.permission
import android.app.Activity
import android.content.*
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.TextUtils
import android.util.Base64
import android.util.Base64.encodeToString
import android.util.TypedValue
import android.view.*
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.apacfin.apacm.BaseApplication
import com.apacfin.apacm.BuildConfig.Secret
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.profile.IMAGES_FOLDER_NAME
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.apacfin.data.prefrerce.Constants.FACEBOOK_PAGE_ID
import com.apacfin.data.prefrerce.Constants.ImeiNo
import com.apacfin.data.prefrerce.Constants.Latitude
import com.apacfin.data.prefrerce.Constants.Longitude
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.play.core.review.ReviewManagerFactory
import com.google.firebase.messaging.FirebaseMessaging
import com.jackandphantom.carouselrecyclerview.CarouselRecyclerview
import timber.log.Timber
import java.io.*
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : Utils.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

var secretKey: SecretKeySpec? = null
var key: ByteArray = byteArrayOf()
//var secret = "YXBhY3ZpcnVz" //this key will be provided by APAC team


private val keys = "A^a~32B!t&asC@deS*cr*tKn@wnK*y24"
private val iv = "0024087000120012".toByteArray()

fun encryptPassword(plaintext: String): String {
    var encryption_pass: String? = null

    try {
        val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        val secretKeySpec = SecretKeySpec(keys.toByteArray(), "AES")
        val ivParameterSpec = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec)
        val encryptedByteValue = cipher.doFinal(plaintext.toByteArray(StandardCharsets.UTF_8))
        encryption_pass = Base64.encodeToString(encryptedByteValue, Base64.DEFAULT)
        Timber.d("---------------------->encryptPassword: $encryption_pass")
    } catch (e: java.lang.Exception) {
        println("Error while encrypting: $e")
    }
    return encryption_pass.toString()
}

/*fun encryptPassword(password: String): String {
    var encryption_pass: String? = null
    try {
        setKey(Secret)
        val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        val encryptedByteValue = cipher.doFinal(password.toByteArray(StandardCharsets.UTF_8))
        encryption_pass = encodeToString(encryptedByteValue, Base64.DEFAULT)
        Timber.d("encryptPassword: $encryption_pass")
    } catch (e: java.lang.Exception) {
        println("Error while encrypting: $e")
    }
    return encryption_pass.toString()
}*/

fun setKey(myKey: String) {
    val sha: MessageDigest?
    try {
        key = myKey.toByteArray(StandardCharsets.UTF_8)
        sha = MessageDigest.getInstance("SHA-1")
        key = sha.digest(key)
        key = Arrays.copyOf(key, 16)
        secretKey = SecretKeySpec(key, "AES")
    } catch (e: NoSuchAlgorithmException) {
        //  e.printStackTrace()
    }
}

fun showProgressDialog(mContext: Activity): BeautifulProgressDialog {
    val progressDialog =
        BeautifulProgressDialog(
            mContext,
            BeautifulProgressDialog.withLottie,
            mContext.getString(R.string.loading)
        )
    Timber.e("showProgressDialog ")
    /*progressDialog!!.setImageLocation(resources.getDrawable(R.drawable.logo_new_removebg))
       progressDialog!!.setLayoutColor(resources.getColor(R.color.Orange))*/

    /*  val myUri: Uri = Uri.fromFile(File("//android_asset/progress.gif"))
      progressDialog!!.setGifLocation(myUri)*/
    progressDialog.setLottieLocation("progress.json")
    progressDialog.setLottieLoop(true)
    progressDialog.setLayoutColor(mContext.resources.getColor(R.color.white))
    progressDialog.setMessageColor(mContext.resources.getColor(R.color.Orange))
    /*if (isShow==true){
        Log.e("TAG", "showProgressDialog true" + isShow )
        progressDialog!!.show()
    }
    if (isShow==false){
        Log.e("TAG", "showProgressDialog false" + isShow )
        progressDialog!!.dismiss()
        //Handler().postDelayed({  }, 5000)
    }*/

    return progressDialog

}

fun isEmpty(text: EditText): Boolean {
    val str: CharSequence = text.text.toString()
    return TextUtils.isEmpty(str)
}

fun isValidPhoneNumber(target: EditText): Boolean {
    val str: CharSequence = target.text.toString()
    return if (str.length != 10) {
        false
    } else {
        val p: Pattern = Pattern.compile("(1-9)?[6-9][0-9]{9}")

        val m: Matcher = p.matcher(str)
        return (m.find() && m.group().equals(str))

    }
}

fun getLastNCharsOfString(str: String, n: Int): String {
    var lastnChars = str
    if (lastnChars.length > n) {
        lastnChars = lastnChars.substring(lastnChars.length - n, lastnChars.length)
    }
    return lastnChars
}

fun showToast(context: Context?, toasts: String?) {
    val toast = Toast.makeText(context, toasts, Toast.LENGTH_SHORT)
    toast.show()
    val handler = Handler(Looper.getMainLooper())
    handler.postDelayed({ toast.cancel() }, 4000)
}


fun isNetworkAvailable(context: Context): Boolean {
    var status = false
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    status = true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    status = true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    status = true
                }
            }
        }
    } else {
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
            status = true
        }
    }
    Timber.e("isNetworkAvailable status : " + status)
    if (!status) {
        showInternetConnectionLost(
            context,
            context.resources.getString(R.string.no_internet_msg),
            context.resources.getString(R.string.ok)
        )
        return false
    }
    return true
    // return false
}

fun showInternetConnectionLost(context: Context, msg: String, btn: String) {

    try {
        val dialog = MaterialAlertDialogBuilder(context, R.style.MyMaterialAlertDialog).create()
        dialog.setTitle(" ")
        dialog.setIcon(R.drawable.ic_new_logo_foreground)
        dialog.setMessage(msg)
        dialog.setCancelable(false)
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, btn) { _, _ -> dialog.dismiss() }

        /* dialog.setButton(
             DialogInterface.BUTTON_NEGATIVE,
             "Retry",
             DialogInterface.OnClickListener { _, _ ->

             })*/
        dialog.show()
    } catch (e: Exception) {
        Timber.e("classTag", e.toString())
    }
}

fun showAlertMessageSingleClick(
    context: Context, positive: String?, message: String?,
    okListener: DialogInterface.OnClickListener?
) {
    MaterialAlertDialogBuilder(context, R.style.MyMaterialAlertDialog)
        .setMessage(message)
        .setTitle(" ")
        .setPositiveButton(positive, okListener)
        .setIcon(R.drawable.ic_new_logo_foreground)
        .setCancelable(false)
        .create()
        .show()
}

fun getCurrentLatLong(context: Context) {
    val appPreferences = AppPreferences(context)
    requestSingleUpdate(context,
        object : LocationCallback {

            override fun onNewLocationAvailable(location: GPSCoordinates?) {
                val latitudeToSet: Float = location!!.latitude.toFloat()
                val longitudeToSet: Float = location!!.longitude.toFloat()
                appPreferences.putString(Latitude, latitudeToSet.toString())
                appPreferences.putString(Longitude, longitudeToSet.toString())
                /*appPreferences.putString(
                    GeoAddress, getLocationAdress(
                        context,
                        latitudeToSet,
                        longitudeToSet
                    )
                )*/
            }
        })
}

fun getDeviceId(context: Context): String? {
    val appPreferences = AppPreferences(context)
    val deviceId: String
    deviceId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        )
    } else {
        val mTelephony = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        if (mTelephony.deviceId != null) {
            mTelephony.deviceId
        } else {
            Settings.Secure.getString(
                context.contentResolver,
                Settings.Secure.ANDROID_ID
            )
        }
    }
    Timber.e("deviceId: " + deviceId)
    appPreferences.putString(ImeiNo, deviceId)
    return deviceId
}

fun getCurrentDayLogin(context: Context): Boolean? {
    var isboolean = false
    val appPreferences = AppPreferences(context)
    /* val df: DateFormat = SimpleDateFormat(Constants.dateFormat, Locale.US)
     df.timeZone = TimeZone.getTimeZone("en_IE.UTF-8")*/
    val currentDate = getCurrentDate()
    Timber.e("getCurrentDayLogincurrentDate: " + currentDate)
    Timber.e("getCurrentDayLogin: " + appPreferences.getString(Constants.LoginDate, ""))
    if (currentDate.equals(appPreferences.getString(Constants.LoginDate, ""))) {
        appPreferences?.putBoolean(Constants.isDayFirstLogin, true)
    } else {
        appPreferences?.putBoolean(Constants.isDayFirstLogin, false)
    }

    return isboolean
}

fun showSnackBar(
    activity: Activity,
    message: String,
    action: String? = null,
    actionListener: View.OnClickListener? = null,
    duration: Int = Snackbar.LENGTH_SHORT
) {
    val snackBar = Snackbar.make(activity.findViewById(android.R.id.content), message, duration)
        .setTextColor(Color.WHITE)
    if (action != null && actionListener != null) {
        snackBar.setAction(action, actionListener)
    }
    snackBar.show()
}

fun changeTabsFont(tabs: TabLayout) {
    Timber.e("changeTabsFont: called")
    val vg = tabs.getChildAt(0) as ViewGroup
    val tabsCount = vg.childCount
    for (j in 0 until tabsCount) {
        val vgTab = vg.getChildAt(j) as ViewGroup
        val tabChildCount = vgTab.childCount
        for (i in 0 until tabChildCount) {
            val tabViewChild = vgTab.getChildAt(i)
            if (tabViewChild is TextView) {
                tabViewChild.textSize = 15f
                tabViewChild.setTypeface(null, Typeface.NORMAL)
            }
        }
    }
}

/**
 *
 * @return current Date from Calendar in dd/MMM/yyyy format
 * adding 1 into month because Calendar month starts from zero
 */
fun getDate(monthAgo: Int): String? {
    val sdf = SimpleDateFormat(Constants.dateFormat, Locale.US)
    val cal = Calendar.getInstance()
    cal.timeZone = TimeZone.getTimeZone("GMT")
    System.out.println("current date: " + sdf.format(cal.getTime()))
    //subtracting months from Date
    cal.add(Calendar.MONTH, -monthAgo);

    return sdf.format(cal.getTime())
}

/**
 *
 * @return current Date from Calendar in dd/MMM/yyyy format
 *
 */
fun getCurrentDate(): String? {
    val sdf = SimpleDateFormat(Constants.dateFormat, Locale.US)
    val cal = Calendar.getInstance()
    cal.timeZone = TimeZone.getTimeZone("GMT")
    System.out.println("current date: " + sdf.format(cal.getTime()))


    return sdf.format(cal.getTime())
}

/**
 *
 * @return compare 2 dates result
 *
 */
fun getDateDiff(fromDate: String, toDate: String): Boolean {
    var result = false

    val sdf = SimpleDateFormat(Constants.dateFormat, Locale.US)
    val date1 = sdf.parse(fromDate)
    val date2 = sdf.parse(toDate)
    val sdf1 = SimpleDateFormat("yyyy-MM-dd")
    println("date1 : " + sdf1.format(date1))
    println("date2 : " + sdf1.format(date2))

    /*if (date1.equals(date2)) {
        println("Date1 is equal Date2")
    }
    if (date1.after(date2)) {
        println("Date1 is after Date2")
    }*/
    if (date1.before(date2)) {
        println("Date1 is before Date2")
        result = true
    }
    return result
}

fun getFirebaseToken(context: Context) {
    val appPreferences = AppPreferences(context)
    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
        if (!task.isSuccessful) {
            //Log.w(ContentValues.TAG, "Fetching FCM registration token failed", task.exception)
            return@OnCompleteListener
        }
        // Get new FCM registration token
        val token = task.result
        Timber.e(ContentValues.TAG + "getFirebaseToken: " + token)
        appPreferences?.putString(Constants.FirebaseToken, token)
    })
}

fun isSIMInserted(context: Context): Boolean {
    return TelephonyManager.SIM_STATE_ABSENT != (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).simState
}

fun callDialer(context: Context, callNo: String) {
    val callIntent = Intent(Intent.ACTION_CALL)
    callIntent.data = Uri.parse("tel:" + callNo) //change the number

    context.startActivity(callIntent)
}

fun checkPermission(context: Context?): Boolean {
    //val readPerm = ContextCompat.checkSelfPermission(context!!, permission.READ_EXTERNAL_STORAGE)
    // val writePerm = ContextCompat.checkSelfPermission(context, permission.WRITE_EXTERNAL_STORAGE)
    val callPerm = ContextCompat.checkSelfPermission(context!!, permission.CALL_PHONE)
    //val managaePerm = ContextCompat.checkSelfPermission(context, permission.MANAGE_EXTERNAL_STORAGE)
    val coreLocationPerm =
        ContextCompat.checkSelfPermission(context, permission.ACCESS_COARSE_LOCATION)
    val fineLocationPerm =
        ContextCompat.checkSelfPermission(context, permission.ACCESS_FINE_LOCATION)
    Timber.e("checkPermission " +/*readPerm+","+*/callPerm + "," + "," + coreLocationPerm + "," + fineLocationPerm)
    return /*readPerm == PackageManager.PERMISSION_GRANTED  &&*/ callPerm == PackageManager.PERMISSION_GRANTED && coreLocationPerm == PackageManager.PERMISSION_GRANTED && fineLocationPerm == PackageManager.PERMISSION_GRANTED
}


//method to get the right URL to use in the intent for faccebook
fun openFacebookUrl(context: Context) {

    val pageUrl = "https://www.facebook.com/$FACEBOOK_PAGE_ID"

    try {
        val applicationInfo: ApplicationInfo =
            context.getPackageManager().getApplicationInfo("com.facebook.katana", 0)
        if (applicationInfo.enabled) {
            val versionCode: Int =
                context.getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode
            val url: String
            url = if (versionCode >= 3002850) {
                "fb://facewebmodal/f?href=$pageUrl"
            } else {
                "fb://page/$FACEBOOK_PAGE_ID"
            }
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } else {
            throw java.lang.Exception("Facebook is disabled")
        }
    } catch (e: java.lang.Exception) {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(pageUrl)))
    }
}

//method to get the right URL to use in the intent for linkedin
fun openLinkedInUrl(context: Context) {

    context.startActivity(
        Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://in.linkedin.com/company/apac-financial-services")
        )
    )

}

fun amountFormater(amout: Double): String {
    //if (amout){

    var returnFormat = NumberFormat.getCurrencyInstance(Locale("en", "IN"))
    returnFormat.maximumFractionDigits = 0
    returnFormat.format(amout)

    return returnFormat.format(amout)
/*}
    return ""*/
}

fun commaSepratedValueWithnumberToText(
    s: String,
    editText: EditText
) {
    try {
        var givenstring = s
        if (givenstring.contains(",")) {
            givenstring = givenstring.replace(",".toRegex(), "")
        }
        val number: Long = givenstring.toLong()
        val formatter = DecimalFormat("##,##,##0")
        editText.setText(formatter.format(number))
        editText.setSelection(editText.text.length)

    } catch (nfe: NumberFormatException) {
        nfe.printStackTrace()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun playSound(context: Context) {
    var mediaPlayer: MediaPlayer = MediaPlayer.create(context, R.raw.transaction)
    mediaPlayer.start()
}

fun disableCopyPasteOperations(editText: EditText) {
    editText.setCustomSelectionActionModeCallback(object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode?, p1: Menu?): Boolean {
            mode?.finish()  // menu.close(); menu.clear();
            return false
        }

        override fun onPrepareActionMode(mode: ActionMode?, p1: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode?, p1: MenuItem?): Boolean {
            return false
        }

        override fun onDestroyActionMode(mode: ActionMode?) {

        }
    })
    editText.isLongClickable = false
    editText.setTextIsSelectable(false)
}

fun getSelectedView(recycler: CarouselRecyclerview, selectedPosition: Int): View {
    Timber.e("getSelectedView ${recycler.getSelectedPosition()}")
    /*val viewHolder: RecyclerView.ViewHolder? =
        recycler.findViewHolderForAdapterPosition(selectedPosition)*/
    val view1 = recycler.getChildAt(selectedPosition)
    Timber.e("getSelectedView1 $view1")
    val viewHolder = recycler.findContainingViewHolder(view1)
    val view = viewHolder?.itemView
    Timber.e("getSelectedView $view")
    val relyview = view!!.findViewById<View>(R.id.layout_view) as RelativeLayout
    return relyview
}

fun getBitmapFromView(view: View): Bitmap? {
    //Define a bitmap with the same size as the view
    val returnedBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
    //Bind a canvas to it
    val canvas = Canvas(returnedBitmap)
    //Get the view's background
    val bgDrawable = view.background
    if (bgDrawable != null) {
        //has background drawable, then draw it on the canvas
        bgDrawable.draw(canvas)
    } else {
        //does not have background drawable, then draw white background on the canvas
        canvas.drawColor(Color.MAGENTA)
    }
    // draw the view on the canvas
    view.draw(canvas)
    //return the bitmap
    return returnedBitmap
}

@Throws(IOException::class)
fun saveImage(context: Context, bitmap: Bitmap, name: String) {
    val saved: Boolean
    val fos: OutputStream?
    fos = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val resolver: ContentResolver = context.getContentResolver()
        val contentValues = ContentValues()
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name)
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/$IMAGES_FOLDER_NAME")
        val imageUri =
            resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
        resolver.openOutputStream(imageUri!!)
    } else {
        val imagesDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DCIM
        ).toString() + File.separator + IMAGES_FOLDER_NAME
        val file = File(imagesDir)
        if (!file.exists()) {
            file.mkdir()
        }
        val image = File(imagesDir, "$name")
        FileOutputStream(image)
    }
    saved = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
    fos!!.flush()
    fos.close()

    //Log.e("saved: ","saved:- "+saved)
    if (saved) {
        showToast(context, "Card saved to gallery")
    }
}

fun convertToBase64(bitmap: Bitmap): String {
    val outputStream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
    return encodeToString(outputStream.toByteArray(), Base64.DEFAULT).replace("\n", "").trim()
}

fun getDrawable(context: Context, drawableResId: Int): Drawable? {
    return VectorDrawableCompat.create(context.resources, drawableResId, context.theme)
}

fun getDrawable(context: Context, drawableResId: Int, colorFilter: Int): Drawable {
    val drawable = getDrawable(context, drawableResId)
    drawable!!.setColorFilter(colorFilter, PorterDuff.Mode.SRC_IN)
    return drawable
}

fun getBitmap(context: Context, drawableId: Int): Bitmap {
    val drawable = getDrawable(context, drawableId)

    val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    drawable.setBounds(0, 0, canvas.width, canvas.height)
    drawable.draw(canvas)

    return bitmap
}

fun dpToPx(dp: Float): Int {
    return dpToPx(dp, BaseApplication.instance.resources)
}

private fun dpToPx(dp: Float, resources: Resources): Int {
    val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
    return px.toInt()
}

fun getColorCompat(resId: Int) = ContextCompat.getColor(BaseApplication.instance, resId)

fun View.setVisible() {
    visibility = View.VISIBLE
}

fun View.setInvisible() {
    visibility = View.INVISIBLE
}

fun View.setGone() {
    visibility = View.GONE
}

fun alertMessageNoGps(context: Context, btn: String) {

    try {
        val dialog = MaterialAlertDialogBuilder(context, R.style.MyMaterialAlertDialog).create()
        dialog.setTitle(" ")
        dialog.setMessage("Your GPS seems to be disabled, do you want to enable it?")
        dialog.setCancelable(false)
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, btn) { _, _ ->
            context.startActivity(
                Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            )
        }

        dialog.show()
    } catch (e: Exception) {
        Timber.e("classTag", e.toString())
    }
}


fun requestInAppReview(context: Context) {
    val reviewManager = ReviewManagerFactory.create(context)

    val request = reviewManager.requestReviewFlow()
    request.addOnFailureListener {

    }
    request.addOnCompleteListener { task ->
        if (task.isSuccessful) {
            val reviewInfo = task.result

            val activity = context as? Activity
            if (activity != null) {
                val flow = reviewManager.launchReviewFlow(activity, reviewInfo)
                flow.addOnCompleteListener { flowTask ->
                    if (flowTask.isSuccessful) {
                        AppPreferences.appPreferences?.putBoolean(Constants.InAppReview, false)
                    } else {
                        AppPreferences.appPreferences?.putBoolean(Constants.InAppReview, true)
                    }
                }
            } else {
                AppPreferences.appPreferences?.putBoolean(Constants.InAppReview, true)
            }
        } else {
            AppPreferences.appPreferences?.putBoolean(Constants.InAppReview, true)
        }
    }
}
