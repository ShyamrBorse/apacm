package com.apacfin.apacm.util

import android.content.Context
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import okhttp3.ConnectionSpec
import okhttp3.Interceptor
import java.security.cert.CertificateException


import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


import okhttp3.OkHttpClient
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.TimeUnit

fun getUnsafeOkHttpClient(context: Context): OkHttpClient {

        val appPreferences = AppPreferences.getAppPreferences(context)
        return try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts =
                arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate> {
                            return arrayOf()
                        }
                    }
                )

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .retryOnConnectionFailure(true)
                .addInterceptor(Interceptor { chain ->
                    val request = chain.request()
                        .newBuilder() //                                    .addHeader("Authorization", EncryptionUtils.decrypt(context, appPreferences.getString(Constants.Token,""))+"")
                        .addHeader("Authorization", appPreferences?.getString(Constants.Token, "")!!)
                        .addHeader("imei", appPreferences.getString(Constants.ImeiNo, "")!!)
                        .addHeader("lat", appPreferences.getString(Constants.Latitude, "")!!)
                        .addHeader("long", appPreferences.getString(Constants.Longitude, "")!!)
                        .addHeader("appVersion", appPreferences.getString(Constants.AppVersion, "")!!)
                        .build()
                    chain.proceed(request)
                })
                .connectionSpecs(
                    Arrays.asList(
                        ConnectionSpec.MODERN_TLS,
                        ConnectionSpec.COMPATIBLE_TLS,
                        ConnectionSpec.CLEARTEXT))
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .writeTimeout(40, TimeUnit.SECONDS)
                .cache(null)
            builder.hostnameVerifier { hostname, session -> true }
            builder.build()

            val okHttpClient = builder.build()

            return okHttpClient

        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

