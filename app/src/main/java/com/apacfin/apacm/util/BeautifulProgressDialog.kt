package com.apacfin.apacm.util


import java.lang.Exception
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.apacfin.apacm.R
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import timber.log.Timber

import java.util.Objects;
/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : BeautifulProgressDialog.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class BeautifulProgressDialog {

companion object{
    var withImage = "withImage"
    var withGIF = "withGIF"
    var withLottie = "withLottie"
}


    // public static final String withProgressBar = "withProgressBar";
    private var viewType: String? = null

    private var gifLocation: Uri? = null

    private var lottieCompactPadding = false
    lateinit var viewAnimationView: LottieAnimationView

    private var mContext: Activity
    private var alertDialog: Dialog
    var dialogView: View? = null

    var parent: CardView? = null

    private  var topPadding = 0
    private  var bottomPadding:Int = 0
    private  var leftPadding:Int = 0
    private  var rightPadding:Int = 0
    private  var overallPadding:Int = 0

    var viewInteriorLayout: LinearLayout? = null
    lateinit var viewImageView: ImageView
    var viewMessage: TextView? = null
    // ProgressBar viewProgressBar;

    // ProgressBar viewProgressBar;
    @SuppressLint("ResourceAsColor")
    constructor(activity: Activity?, type: String?, message: String?){
        mContext = activity!!
        /**
         * final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
         * alertDialog = dialogBuilder.create();
         * alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
         * dialogView = LayoutInflater.from(activity).inflate(R.layout.layout_progress_dialog, null);
         * alertDialog.setView(dialogView);
         * Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
         */
        alertDialog = Dialog(activity!!, R.style.RoundedCornersDialog_Mid)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogView = LayoutInflater.from(activity).inflate(R.layout.layout_progress_dialog, null)
        alertDialog.setContentView(dialogView!!)
        Objects.requireNonNull(alertDialog.getWindow())
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.setOnCancelListener(DialogInterface.OnCancelListener {
            if (viewAnimationView.isAnimating()) {
                viewAnimationView.cancelAnimation()
            }
        })
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        setUpPadding()
        parent = dialogView!!.findViewById(R.id.parent)
        parent!!.elevation = 3f
        parent!!.radius = 15f
        viewInteriorLayout = dialogView!!.findViewById(
            R.id.interior_layout
        )
        viewImageView = dialogView!!.findViewById(R.id.imageView)
        viewMessage = dialogView!!.findViewById(R.id.message)
        viewAnimationView = dialogView!!.findViewById(R.id.lottie)
        //viewProgressBar = dialogView.findViewById(R.id.progressBar);
        viewAnimationView.setRepeatCount(ValueAnimator.INFINITE)
        viewType = type
        when (viewType) {
            withImage, withGIF -> {
                viewImageView.setVisibility(View.VISIBLE)
                viewAnimationView.setVisibility(View.GONE)
            }
            withLottie -> {
                viewImageView.setVisibility(View.GONE)
                viewAnimationView.setVisibility(View.VISIBLE)
                viewAnimationView.setRepeatCount(ValueAnimator.INFINITE)
            }
        }
        if (message != null) {
            viewMessage!!.text = message
            viewMessage!!.visibility = View.VISIBLE
            viewInteriorLayout!!.setPadding(leftPadding, topPadding, rightPadding, bottomPadding)
        } else {
            viewMessage!!.visibility = View.GONE
            viewInteriorLayout!!.setPadding(
                overallPadding,
                overallPadding,
                overallPadding,
                overallPadding
            )
            if (!viewType.equals(withLottie, ignoreCase = true)) {
                viewInteriorLayout!!.setPadding(
                    overallPadding,
                    overallPadding,
                    overallPadding,
                    overallPadding
                )
            } else {
                if (lottieCompactPadding) {
                    viewInteriorLayout!!.setPadding(0, 0, 0, 0)
                } else {
                    viewInteriorLayout!!.setPadding(
                        overallPadding,
                        overallPadding,
                        overallPadding,
                        overallPadding
                    )
                }
            }
        }
    }

    /**
     * Convert padding from dp to px.
     */
    private fun setUpPadding() {
        /*topPadding = dpToPx(15)
        bottomPadding = dpToPx(15)
        leftPadding = dpToPx(30)
        rightPadding = dpToPx(30)
        overallPadding = dpToPx(20)*/
        topPadding = dpToPx(10)
        bottomPadding = dpToPx(10)
        leftPadding = dpToPx(15)
        rightPadding = dpToPx(15)
        overallPadding = dpToPx(15)
    }

    /**
     * Set custom font for the message
     * @param font
     */
    fun setFont(font: String?) {
        try {
            val face: Typeface = Typeface.createFromAsset(
                mContext!!.assets,
                font
            )
            viewMessage!!.setTypeface(face)
        } catch (e: Exception) {
           // e.printStackTrace()
        }
    }

    /**
     * Set view type. Must be one of the defined.
     * @param type
     * @throws Exception
     */
    fun setViewType(type: String?) {
        viewType = type
        when (type) {
            withImage, withGIF -> {
                viewImageView!!.setVisibility(View.VISIBLE)
                viewAnimationView.setVisibility(View.GONE)
            }
            withLottie -> {
                viewImageView!!.setVisibility(View.GONE)
                viewAnimationView.setVisibility(View.VISIBLE)
            }
        }
    }

    /**
     * Get status of Progress Dialog
     * @return
     */
    fun isShowing(): Boolean {
        return alertDialog!!.isShowing()
    }

    /**
     * Set color of the message
     * @param color
     */
    fun setMessageColor(color: Int) {
        viewMessage!!.setTextColor(color)
    }

    /**
     * Set color of the progress bar.
     * @param color
     */
    private fun setProgressBarColor(color: Int) {
        //viewProgressBar.getIndeterminateDrawable().setColorFilter(color, android.graphics.PorterDuff.Mode.SRC_IN);
    }

    /**
     * Set background color of the Layout
     * @param color
     */
    @SuppressLint("ResourceAsColor")
    fun setLayoutColor(color: Int) {
        parent!!.setCardBackgroundColor(color)
    }

    /**
     * Set corner radius of the Layout
     * @param radius
     */
    fun setLayoutRadius(radius: Float) {
        parent!!.radius = radius
    }

    /**
     * Set elevation of the layout
     * @param elevation
     */
    fun setLayoutElevation(elevation: Float) {
        parent!!.elevation = elevation
    }

    /**
     * Set message to be displayed in the progress dialog
     * @param value
     */
    fun showMessage(value: Boolean) {
        if (value) {
            viewMessage!!.visibility = View.VISIBLE
            viewInteriorLayout!!.setPadding(leftPadding, topPadding, rightPadding, bottomPadding)
        } else {
            viewMessage!!.visibility = View.GONE
            if (!viewType.equals(withLottie, ignoreCase = true)) {
                viewInteriorLayout!!.setPadding(
                    overallPadding,
                    overallPadding,
                    overallPadding,
                    overallPadding
                )
            } else {
                if (lottieCompactPadding) {
                    viewInteriorLayout!!.setPadding(0, 0, 0, 0)
                } else {
                    viewInteriorLayout!!.setPadding(
                        overallPadding,
                        overallPadding,
                        overallPadding,
                        overallPadding
                    )
                }
            }
        }
    }

    /**
     * Set message to be displayed in the progress dialog
     * @param text
     */
    fun setMessage(text: String?) {
        viewMessage!!.text = text
        viewMessage!!.visibility = View.VISIBLE
        viewInteriorLayout!!.setPadding(leftPadding, topPadding, rightPadding, bottomPadding)
    }

    /**
     * Remove message to be displayed in the progress dialog
     */
    fun removeMessage() {
        viewMessage!!.visibility = View.GONE
        if (!viewType.equals(withLottie, ignoreCase = true)) {
            viewInteriorLayout!!.setPadding(
                overallPadding,
                overallPadding,
                overallPadding,
                overallPadding
            )
        } else {
            if (lottieCompactPadding) {
                viewInteriorLayout!!.setPadding(0, 0, 0, 0)
            } else {
                viewInteriorLayout!!.setPadding(
                    overallPadding,
                    overallPadding,
                    overallPadding,
                    overallPadding
                )
            }
        }
    }

    /**
     * Set Image Drawable resources. User must set withImage as view type
     * @param drawable
     * @throws Exception
     */
    fun setImageLocation(drawable: Drawable?) {
        viewImageView!!.setImageDrawable(drawable)
    }

    /**
     * Set Image Bitmap resource. User must set withImage as view type
     * @param bitmap
     * @throws Exception
     */
    fun setImageLocation(bitmap: Bitmap?) {
        viewImageView!!.setImageBitmap(bitmap)
    }

    /**
     * Set Image int resource. User must set withImage as view type
     * @param location
     * @throws Exception
     */
    fun setImageLocation(location: Uri?) {
        viewImageView!!.setImageURI(location)
    }

    /**
     * Set GIF int resource. User must set withGIF as view type
     * @param gifLocation
     * @throws Exception
     */
    fun setGifLocation(gifLocation: Uri?) {
        this.gifLocation = gifLocation
    }

    /**
     * Set Lottie string resource. User must set withLottie as view type
     * @param url
     * @throws Exception
     */
    fun setLottieLocation(url: String?) {
        viewAnimationView.setAnimation(url)
    }

    /**
     * Set loop for Lottie animation
     * @param value
     */
    fun setLottieLoop(value: Boolean) {
        viewAnimationView.setRepeatCount(if (value) ValueAnimator.INFINITE else 0)
    }

    fun setLottieCompactPadding(value: Boolean) {
        lottieCompactPadding = value
        if (lottieCompactPadding) {
            viewInteriorLayout!!.setPadding(0, 0, 0, 0)
        } else {
            viewInteriorLayout!!.setPadding(
                overallPadding,
                overallPadding,
                overallPadding,
                overallPadding
            )
        }
    }

    /**
     * Set progress dialog's cancelable feature if user touches outside. default set to false
     * @param value
     */
    fun setCancelableOnTouchOutside(value: Boolean) {
        alertDialog!!.setCanceledOnTouchOutside(value)
    }

    /**
     * Set progress dialog's cancelable feature. default set to false
     * @param value
     */
    fun setCancelable(value: Boolean) {
        alertDialog!!.setCancelable(false)
    }

    /**
     * Show progress dialog. Make sure view type is set and if message is set to yes, then message cannot be null
     * @throws Exception
     */


    /**
     * Show progress dialog. Make sure view type is set and if message is set to yes, then message cannot be null
     * @throws Exception
     */
    /**
     * Show progress dialog
     */
    fun show() {
        if (viewType == null) {
            Timber.e("View Type"+  "Null")
            return
        }
        if (viewType.equals(withLottie, ignoreCase = true) && viewAnimationView != null) {
            viewAnimationView.playAnimation()
        }
        if (viewType.equals(withGIF, ignoreCase = true) && gifLocation != null) {
            Glide.with(mContext!!)
                .load(gifLocation)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(viewImageView!!)
        }
        alertDialog!!.show()
    }

    /**
     * Dismiss progress dialog
     */
    fun dismiss() {
        Timber.e("View Type"+ viewType.toString())
        if (viewType.equals(withLottie, ignoreCase = true) && viewAnimationView != null) {
            Timber.e("View Type if "+ viewType.toString())
            viewAnimationView.cancelAnimation()
        }
        if (viewType.equals(withGIF, ignoreCase = true)) {
            try {
                (viewImageView!!.getDrawable() as GifDrawable).stop()
            } catch (e: Exception) {
               // e.printStackTrace()
            }
        }
        alertDialog.dismiss()
        //alertDialog!!.cancel()
    }

    /**
     * Convert padding from dp to px.
     */
    fun dpToPx(dp: Int): Int {
        return (dp.toFloat() * Resources.getSystem().getDisplayMetrics().density).toInt()
    }

}