package com.apacfin.apacm.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import timber.log.Timber


// calls back to calling thread, note this is for low grain: if you want higher precision, swap the
// contents of the else and if. Also be sure to check gps permission/settings are allowed.
// call usually takes <10ms
fun requestSingleUpdate(context: Context, callback: LocationCallback) {
    if (ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    ) {
        return
    }
    var gps_enabled = false
    var network_enabled = false
    val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    try {
        gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    } catch (ex: Exception) {
    }
    try {
        network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    } catch (ex: Exception) {
    }
    Timber.e("Location Through GPS : $gps_enabled Location Through Network : $network_enabled")
    if (gps_enabled) {
        val criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE
        locationManager.requestSingleUpdate(criteria, object : LocationListener {
            override fun onLocationChanged(location: Location) {
                callback.onNewLocationAvailable(
                    GPSCoordinates(
                        location.latitude,
                        location.longitude
                    )
                )
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
            override fun onProviderEnabled(provider: String) {}
            override fun onProviderDisabled(provider: String) {}
        }, null)
    } else if (network_enabled) {
        val criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_COARSE
        locationManager.requestSingleUpdate(criteria, object : LocationListener {
            override fun onLocationChanged(location: Location) {
                callback.onNewLocationAvailable(
                    GPSCoordinates(
                        location.latitude,
                        location.longitude
                    )
                )
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
            override fun onProviderEnabled(provider: String) {}
            override fun onProviderDisabled(provider: String) {}
        }, null)
    } else {
        callback.onNewLocationAvailable(GPSCoordinates((-1).toDouble(), (-1).toDouble()))
    }
}

interface LocationCallback {
    fun onNewLocationAvailable(location: GPSCoordinates?)
}

// consider returning Location instead of this dummy wrapper class
class GPSCoordinates {
    var longitude = -1f
    var latitude = -1f

    constructor(theLatitude: Float, theLongitude: Float) {
        longitude = theLongitude
        latitude = theLatitude
    }

    constructor(theLatitude: Double, theLongitude: Double) {
        longitude = theLongitude.toFloat()
        latitude = theLatitude.toFloat()
    }
}
