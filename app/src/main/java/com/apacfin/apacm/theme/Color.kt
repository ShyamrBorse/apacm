package com.apacfin.apacm.theme


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */


import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFF00063F)
val Purple500 = Color(0xFF1A237E)
val Purple700 = Color(0xFF1A237E)
val Purple700Trans = Color(0xBF1A237E)
val Teal200 = Color(0xFFFF6F00)
val White = Color(0xFFFFFFFF)
val Black = Color(0xFF000000)
val Orange = Color(0xFFfd7e14)
val OrangeLight = Color(0xBFFD7E14)
val Lavender = Color(0xBF880E4F)
val GrayBg = Color(0xFFAFAEAE)
