package com.apacfin.apacm.presentation.document

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.apacfin.apacm.R
import com.apacfin.data.model.alert.AlertModelResponse
import com.apacfin.data.model.document.DocumentModelResponse
import com.apacfin.data.model.esign.EsignLoanDetailsModelResponse
import com.apacfin.data.model.esign.ListOfEsignLinksModelResponse
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.DocumentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : DocumentViewModel.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@HiltViewModel
class DocumentViewModel @Inject constructor(
    private val repository: DocumentRepository
) : ViewModel() {

    val _getAllDocument: MutableLiveData<DocumentModelResponse> = MutableLiveData()
    val _getSignatureDocument: MutableLiveData<EsignLoanDetailsModelResponse> = MutableLiveData()
    val _getListOfEsignLinks: MutableLiveData<ListOfEsignLinksModelResponse> = MutableLiveData()
    val _initiateEsign: MutableLiveData<RegistrationModel> = MutableLiveData()
    val _esignInitialise: MutableLiveData<RegistrationModel> = MutableLiveData()
    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //_getAllDocument responce
    val getAllDocument: LiveData<DocumentModelResponse> get() = _getAllDocument

    //getSignatureDocument responce
    val getSignatureDocument: LiveData<EsignLoanDetailsModelResponse> get() = _getSignatureDocument

     //_getListOfEsignLinks responce
    val getListOfEsignLinks: LiveData<ListOfEsignLinksModelResponse> get() = _getListOfEsignLinks

    //_initiateEsign responce
    val initiateEsign: LiveData<RegistrationModel> get() = _initiateEsign

    //_esignInitialise responce
    val esignInitialise: LiveData<RegistrationModel> get() = _esignInitialise

    //getSignatureDocument api
    fun getSignatureDocument(context:Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getLoanDetailsForEsign()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("getSignatureDocument  "+ response)
                _getSignatureDocument.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("getSignatureDocument"+ response.errorCode+ response.isNetworkError+ response.errorBody)
                var documentModelResponse =  EsignLoanDetailsModelResponse(
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false, 0)
                _getSignatureDocument.postValue(documentModelResponse)
            }
        }

    }

   //getListOfEsignLinks api
    fun getListOfEsignLinks(context:Context,loanID: String,type:Int?) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getListOfEsignLinks(loanID,type)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("getListOfEsignLinks  " + response)
                _getListOfEsignLinks.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("getSignatureDocument" + response.errorCode+ response.isNetworkError+ response.errorBody)
                var documentModelResponse =  ListOfEsignLinksModelResponse(
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false)
                _getListOfEsignLinks.postValue(documentModelResponse)
            }
        }

    }

    //initiateEsign api
    fun initiateEsign(context:Context,refId: String,type:Int) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.initiateEsign(refId,type)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_initiateEsign" + response.value.msg)
                _initiateEsign.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("getSignatureDocument" + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,"",
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false)
                _initiateEsign.postValue(registrationModel)
            }
        }

    }

    //esignInitialise api
    fun esignInitialise(context:Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.esignInitialise(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_esignInitialise"  + response.value.msg)
                _esignInitialise.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("getSignatureDocument"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf(), context.getString(R.string.tecnical_error),
                    false)
                _esignInitialise.postValue(registrationModel)
            }
        }

    }

    //getAllDocument api
    fun getAllDocument(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getAllDocument()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("getAllDocument  " + response)
                _getAllDocument.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("getAllDocument"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var documentModelResponse =  DocumentModelResponse(
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false)
                _getAllDocument.postValue(documentModelResponse)

            }
        }

    }


}