package com.apacfin.apacm.presentation.alert

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.listofcharges.ChargesAdapter
import com.apacfin.apacm.presentation.listofcharges.ListOfChargesActivity
import com.apacfin.apacm.presentation.listofcharges.ListOfChargesViewModel
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.alert.AlertModel
import com.apacfin.data.model.listofchaarges.ListOfChargesModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_list_of_charges.*
import kotlinx.android.synthetic.main.fragment_alert.*
import kotlinx.android.synthetic.main.no_data_layout.*
import timber.log.Timber
import java.util.ArrayList
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AlertFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class AlertFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    val TAG = AlertFragment::class.java.simpleName
    private val viewModel: AlertViewModel by viewModels()
    var loading = false
    var adapter: AlertAdapter? = null
    var appPreferences: AppPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_alert, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(requireActivity(), {
            loading = it;
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getListOfCharges response
        viewModel.getAlertListResponse.observe(requireActivity(), {

            Timber.e("getListOfCharges " + it)

            if (it.status) {

                if (it.alerts.size > 0) {
                    setAdapter(it.alerts)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }

            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                    requireActivity(),
                    it.msg,
                    getString(R.string.ok)
                )
            }
        })
    }

    private fun setAdapter(list: List<AlertModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        adapter = AlertAdapter(
            requireContext(),
            list
        )
        rcyclerViewAlert!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        rcyclerViewAlert!!.layoutManager = LinearLayoutManager(requireContext())
        setVisibility(View.GONE, View.GONE)
        if (isNetworkAvailable(requireContext())) {
            viewModel.getAlertList(requireActivity())
        }
    }

    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewAlert.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AlertFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AlertFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}