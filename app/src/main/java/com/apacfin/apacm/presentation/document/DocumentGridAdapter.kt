package com.apacfin.apacm.presentation.document

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.gratitude.GratituideFragment
import com.apacfin.data.model.document.DocumentListModel
import com.apacfin.data.prefrerce.Constants
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : DocumentGridAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class DocumentGridAdapter(
    context: Context, loanDetailModel: List<DocumentListModel>, loanId: String
) : RecyclerView.Adapter<DocumentGridAdapter.ViewHolder?>() {
    var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_grid_doc, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val documentListModel:DocumentListModel = loanDetailModelData[position]
     Timber.e(TAG+ " documentListModel  : " + documentListModel)


        holder.tvDocName.text = "" + documentListModel.name
        holder.itemView.setOnClickListener {
            Timber.e(TAG + " loanDetailModel  : " +documentListModel.id+", "+ documentListModel.name+", "+ documentListModel.file)
            if (documentListModel.file.startsWith("http")){

               // val transaction: FragmentTransaction = (it.getContext() as FragmentActivity).supportFragmentManager.beginTransaction()
                val transaction: FragmentTransaction = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                GratituideFragment.newInstance(documentListModel.file, "").apply {
                    show(transaction, tag)
                }
            }else{
                context.startActivity(Intent(context,PdfViewActivity::class.java).putExtra(Constants.DocID,documentListModel.id).putExtra(Constants.LoanID,
                    strLoanId))
            }


        }

    }

    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }




    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var tvDocName: TextView

        override fun onClick(view: View) {

        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            tvDocName = itemView.findViewById(R.id.tvDocName)

            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = DocumentGridAdapter::class.java.simpleName
        lateinit var loanDetailModelData: List<DocumentListModel>
        lateinit var strLoanId: String

    }

    init {
        loanDetailModelData = loanDetailModel
        strLoanId = loanId

        this.context = context
    }
}
