package com.apacfin.apacm.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.animation.OvershootInterpolator
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.LoginActivityNew
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.LoginMpinActivity
import com.apacfin.apacm.presentation.registrationprocess.splash.registration.RegistrationActivity
import com.apacfin.apacm.theme.ApacMTheme
import com.apacfin.apacm.theme.Purple200
import com.apacfin.apacm.theme.Purple700
import com.apacfin.apacm.util.showAlertMessageSingleClick
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.stericson.RootTools.RootTools
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay

@AndroidEntryPoint
class SplashActivity : ComponentActivity() {
    var appPreferences: AppPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appPreferences = AppPreferences.getAppPreferences(this@SplashActivity)
        setContent {
            ApacMTheme {
                // A surface container using the 'background' color from the theme
                Surface() {
                    //Greeting("Android")
                    SplashScreen()
                }
            }
        }
    }

}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun SplashScreen() {

    val context = (LocalLifecycleOwner.current as ComponentActivity)
    var appPreferences: AppPreferences? = AppPreferences.getAppPreferences(context)
    val scale = remember {
        androidx.compose.animation.core.Animatable(0f)
    }

    // Animation
    LaunchedEffect(key1 = true) {
        scale.animateTo(
            targetValue = 0.5f,
            // tween Animation
            animationSpec = tween(
                durationMillis = 1000,
                easing = {
                    OvershootInterpolator(5f).getInterpolation(it)
                })
        )
        // Customize the delay time
        delay(1000L)
        //navController.navigate("main_screen")
        //Condition to check rooted device
        if(RootTools.isRootAvailable()){
            showAlertMessageSingleClick(
                context,
                context.getString(R.string.ok),
                context.getString(R.string.rooted_device)
            ) { alertDialog, which ->
                alertDialog.dismiss()
                context.finish()
            }
        }else {
            if (appPreferences!!.getBoolean(Constants.IsRegistred, false)) {
                context.startActivity(Intent(context, LoginActivityNew::class.java)) //LoginMpinActivity
                context.finish()
            } else {
                context.startActivity(
                    Intent(context, LoginActivityNew::class.java).putExtra( //RegistrationActivity
                        Constants.FromView,
                        0
                    )
                )
                context.finish()
            }
        }
        //context.startActivity(Intent(context, UserTypeActivity::class.java))
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(
                Brush.radialGradient(
                    listOf(Purple700, Purple200),radius = 450f

                )),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment  = Alignment.CenterHorizontally

    ) {
        // Change the logo
        Image(
            painter = painterResource(id = R.drawable.logo_new_removebg),
            contentDescription = "Logo",
            modifier = Modifier.scale(scale.value)
        )
    }

}


