package com.apacfin.apacm.presentation.registrationprocess.mpinlogin

import `in`.aabhasjindal.otptextview.OTPListener
import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.interfaces.LoadFragmentListener
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_otp_dialog.*
import kotlinx.android.synthetic.main.fragment_otp_dialog.btnVerifyOtp
import kotlinx.android.synthetic.main.fragment_otp_dialog.edtOTP
import kotlinx.android.synthetic.main.fragment_otp_dialog.tvOtpMsg
import kotlinx.android.synthetic.main.fragment_otp_dialog.tvResendOtp
import kotlinx.android.synthetic.main.fragment_verify_otp.*
import timber.log.Timber
import java.util.HashMap

@AndroidEntryPoint
class VerifyOtpFragment : Fragment(), View.OnClickListener{

    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    var name = ""
    var mobileNo = ""
    var dob = ""
    var refNo = ""
    var fName = ""
    var lName = ""
    var pincode = ""
    var fromView = false

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_verify_otp, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            initView()

            //-- Handle progressDialog loading
            var progressDialog = showProgressDialog(requireActivity())
            viewModel.loading.observe(viewLifecycleOwner, Observer {
                loading = it
                if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                    Timber.e("SmileyFaceCanvasloading " + loading + " " + it)
                    if (loading) {
                        progressDialog.show()
                    } else {
                        progressDialog.dismiss()
                        // showProgressDialog(this,false)
                        //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
                    }
                }
            })

            //-- Handle getRegistration response
            viewModel.getRegistration.observe(viewLifecycleOwner, {
                registrationModel = it
                if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                    Timber.e("getRegistration " + registrationModel)

                    if (registrationModel.status) {
                        refNo = registrationModel.refNo
                        name = registrationModel.name
                    } else {
                        showInternetConnectionLost(
                            requireContext(),
                            registrationModel.msg,
                            getString(R.string.ok)
                        )

                    }
                }
            })

            //-- Handle mobileVerification response
            viewModel.getmobileVerification.observe(viewLifecycleOwner, {
                registrationModel = it
                if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {

                    Timber.e("validateOTPResponse " + registrationModel)

                    if (registrationModel.status) {
                        //dialog?.dismiss()

                        val bundle = Bundle()
                        bundle.putString(Constants.Name, name)
                        bundle.putString(Constants.MobileNo, mobileNo)
                        bundle.putString(Constants.DOB, dob)

                        bundle.putString(Constants.Fname, fName)
                        bundle.putString(Constants.Lname, lName)
                        bundle.putString(Constants.Pincode, pincode)
                        bundle.putBoolean(Constants.FromView, fromView)

                        val fr: Fragment = SetupMPinFragment()
                        val fc: LoadFragmentListener? = activity as LoadFragmentListener?
                        fc?.loadFragListener(requireActivity(), Constants.ResetMPINFrag, bundle, fr)

//                        val transaction: FragmentTransaction =
//                            requireActivity().supportFragmentManager.beginTransaction()
//                        SetMpinDialogFragment().apply {
//                            show(transaction, tag)
//                            arguments = bundle
//                        }

                    } else {
                        edtOTP.showError()
                        showAlertMessageSingleClick(
                            requireContext(),
                            getString(R.string.ok),
                            registrationModel.msg
                        ) { dialog, which ->
                            edtOTP.resetState()
                            edtOTP.otp = ""
                        }

                    }
                }
            })

            //-- Hand
            edtOTP.setOtpListener(object : OTPListener {
                override fun onInteractionListener() {
                    // fired when user types something in the Otpbox
                }

                override fun onOTPComplete(otp: String) {
                    // fired when user has entered the OTP fully.
                    validation(edtOTP.otp)
                }
            })
        }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun initView() {
        arguments?.let {
            // ucic = getArguments()?.getString(Constants.Ucic, "").toString()
            name = getArguments()?.getString(Constants.Name, "").toString()
            mobileNo = getArguments()?.getString(Constants.MobileNo, "").toString()
            dob = getArguments()?.getString(Constants.DOB, "").toString()
            refNo = getArguments()?.getString(Constants.RefNo, "").toString()
            fName = getArguments()?.getString(Constants.Fname, "").toString()
            lName = getArguments()?.getString(Constants.Lname, "").toString()
            pincode = getArguments()?.getString(Constants.Pincode, "").toString()
            fromView = requireArguments().getBoolean(Constants.FromView, false)

        }
        //Timber.e(   TAG +"  "+name + ", " + mobileNo + ", " + refNo + ", " + dob)

        tvOtpMsg.text =
            resources.getString(R.string.otp_msg) + " " + getLastNCharsOfString(mobileNo, 3)

        txtWelcome.text =  " " + name

        tvResendOtp.setPaintFlags(tvResendOtp.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        //dialog?.setCancelable(false)
        tvResendOtp.setOnClickListener(this)
        //ivCloseDialog.setOnClickListener(this)
        btnVerifyOtp.setOnClickListener(this)
    }

    override fun onClick(view: View?) {

        when (view?.getId()) {
            //R.id.ivCloseDialog -> dialog?.dismiss()
            R.id.tvResendOtp -> {
                validateResendOTP()
            }
            R.id.btnVerifyOtp -> {

                validation(edtOTP.otp)

            }

        }
    }

    private fun validation(otp: String) {
        if (isNetworkAvailable(requireContext())) {
            if (otp.isNullOrBlank()) {
                showInternetConnectionLost(
                    requireContext(),
                    requireContext().resources.getString(R.string.er_enter_otp),
                    requireContext().resources.getString(R.string.ok)
                )
            } else {
                /*if (isGetUCIC) {
                    val params: MutableMap<String, String> = HashMap()
                    params["refNo"] = refNo
                    params["otp"] = otp
                    params["mobileNo"] = mobileNo
                    viewModel.validateOTPnSendUCIC(params)
             } else {*/
                val params: MutableMap<String, String> = HashMap()
                params["otp"] = otp
                params["refNo"] = refNo
                viewModel.mobileVerification(requireActivity(),params)
                // }
            }
        }
    }

    private fun validateResendOTP() {
        if (isNetworkAvailable(requireContext())) {

            edtOTP.resetState()
            edtOTP.otp = ""

            val params: MutableMap<String, String> = HashMap()
            params["mobileNo"] = mobileNo
            params["dob"] = dob
            viewModel.getRegistration(requireActivity(),params)
        }
    }
}