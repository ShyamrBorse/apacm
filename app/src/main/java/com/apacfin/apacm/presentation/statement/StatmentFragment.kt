package com.apacfin.apacm.presentation.statement

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.myloan.*
import com.apacfin.apacm.util.*
import com.apacfin.data.model.myloan.AccountStatementModelResponse
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.StatementModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.apacfin.data.prefrerce.Constants.dateFormat
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_alert.*
import kotlinx.android.synthetic.main.fragment_statment.*
import kotlinx.android.synthetic.main.no_data_layout.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class StatmentFragment : Fragment(),  StatementAdapter.OnClickListener, View.OnClickListener {

    val TAG = StatmentFragment::class.java.simpleName
    private val viewModel: LoanDetailsViewModel by viewModels()
    var loading = false
    lateinit var accountStatementModelResponse: AccountStatementModelResponse
    var appPreferences: AppPreferences? = null
    var adapter: StatementAdapter? = null
    lateinit var loanDetails: LoanDetailModel
    lateinit var statementModelList: List<StatementModel>
    var loanID = ""
    var fromDate = ""
    var toDate = ""
    var cal = Calendar.getInstance()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statment, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // setVisibility(View.VISIBLE,View.GONE)
        initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, {
            loading = it;
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getLoanDetails response
        viewModel.getAccountStatementResponse.observe(viewLifecycleOwner, {
            accountStatementModelResponse = it
            Timber.e("accountStatementModelResponse " + accountStatementModelResponse)

            if (accountStatementModelResponse.status) {
                loanDetails = accountStatementModelResponse.loanDetails
                statementModelList = accountStatementModelResponse.statements
                setDataAndAdapter(loanDetails, statementModelList)

                if (statementModelList.size > 0) {
                    setVisibility(View.VISIBLE, View.GONE)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }
            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                    requireContext(),
                    accountStatementModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })

    }

    private fun setDataAndAdapter(
        loanDetails: LoanDetailModel,
        statementModelList: List<StatementModel>
    ) {

        tvLoanID.text = loanDetails.loanId
        tvEMIAmount.text = "" +amountFormater(loanDetails.emiAmount.toDouble())
        tvLoanType.text = loanDetails.loanType
        tvDueDate.text = loanDetails.dueDate

        adapter = StatementAdapter(
            requireContext(),
            statementModelList,
            2,
            this
        )
        rcyclerViewStatement!!.adapter = adapter
        adapter!!.notifyDataSetChanged()

    }

    private fun initView() {
        setVisibility(View.GONE, View.GONE)
        arguments?.let {
            loanID = it.getString(Constants.LoanID).toString()

        }
        Timber.e(TAG + " loanID: " + loanID)
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        rcyclerViewStatement!!.layoutManager = LinearLayoutManager(requireContext())
        //getStatementList(loanID)
        getSheduleList(loanID)

        txtPaymentShedule.setOnClickListener(this)
        /* tvFromDate.setOnClickListener(this)
         tvToDate.setOnClickListener(this)
         btnProceed.setOnClickListener(this)
         rbtnLastMonth.setOnClickListener(this)
         rbtnLast3Month.setOnClickListener(this)
         rbtnLast6Month.setOnClickListener(this)
         rbtnLast1Year.setOnClickListener(this)*/
    }

    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        cardPaymentShedule.visibility = isVisibleRecy
        rcyclerViewStatement.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData
        no_data_layout.visibility = isVisibleNoData
    }

    private fun getStatementList(loanID: String) {
        if (isNetworkAvailable(requireContext())) {
            val params: MutableMap<String, String> = HashMap()
            /* params["mobileNo"] = appPreferences?.getString(Constants.MobileNo, "").toString()
             params["dob"] = appPreferences?.getString(Constants.DOB, "").toString()*/
            params["fromDate"] = ""
            params["endDate"] = ""

            viewModel.getAccountStatement(loanID, params)

        }
    }
    private fun getSheduleList(loanID: String) {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getPaymentShedule(loanID)

        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {

                when (tab?.position) {
                    0 -> setVisibility(View.VISIBLE, View.GONE)
                    1 -> setVisibility(View.GONE, View.VISIBLE)
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })*/


    }

    /*private fun setVisibility(isLnyLastTrans: Int, isLnyDtlStmt: Int) {
        rcyclerViewStatement.visibility = isLnyLastTrans
        lnyDtlStmt.visibility = isLnyDtlStmt
    }
*/

    override fun onClick(view: View) {
        when (view.id) {
            R.id.txtPaymentShedule -> {
                fragmentManager?.popBackStack()
            }
        }
        /* R.id.btnProceed -> validation()
         R.id.rbtnLastMonth -> {
             rgTwo.clearCheck()
             tvFromDate.text = ""
             tvToDate.text = ""
             System.out.println("date before 1 months : " + getDate(1))

         }

         R.id.rbtnLast3Month -> {
             rgTwo.clearCheck()
             tvFromDate.text = ""
             tvToDate.text = ""
             System.out.println("date before 3 months : " + getDate(3))
         }

         R.id.rbtnLast6Month -> {
             rgOne.clearCheck()
             tvFromDate.text = ""
             tvToDate.text = ""
             System.out.println("date before 6 months : " + getDate(6))
         }

         R.id.rbtnLast1Year -> {
             rgOne.clearCheck()
             tvFromDate.text = ""
             tvToDate.text = ""
             System.out.println("date before 12 months : " + getDate(12))
         }

         R.id.tvFromDate -> {
             fromDate = ""

             rgOne.clearCheck()
             rgTwo.clearCheck()
             val datePickerDialog = DatePickerDialog(
                 requireContext(), R.style.SpinnerDatePickerDialog,
                 fromDateSetListener,
                 // set DatePickerDialog to point to today's date when it loads up
                 cal.get(Calendar.YEAR),
                 cal.get(Calendar.MONTH),
                 cal.get(Calendar.DAY_OF_MONTH)
             )

             //following line to restrict future date selection
             datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis())
             datePickerDialog.show()
         }
         R.id.tvToDate -> {
             toDate = ""
             rgOne.clearCheck()
             rgTwo.clearCheck()
             val datePickerDialog = DatePickerDialog(
                 requireContext(), R.style.SpinnerDatePickerDialog,
                 toDateSetListener,
                 // set DatePickerDialog to point to today's date when it loads up
                 cal.get(Calendar.YEAR),
                 cal.get(Calendar.MONTH),
                 cal.get(Calendar.DAY_OF_MONTH)
             )

             //following line to restrict future date selection
             datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis())
             datePickerDialog.show()
         }
     }*/
    }

    /*private fun validation() {

        if (rbtnLastMonth.isChecked) {
            fromDate = getDate(1).toString()
            toDate = getCurrentDate().toString()
        }
        if (rbtnLast3Month.isChecked) {
            fromDate = getDate(3).toString()
            toDate = getCurrentDate().toString()
        }

        if (rbtnLast6Month.isChecked) {
            fromDate = getDate(6).toString()
            toDate = getCurrentDate().toString()
        }

        if (rbtnLast1Year.isChecked) {
            fromDate = getDate(12).toString()
            toDate = getCurrentDate().toString()
        }
        Timber.e(TAG+"Date : "+fromDate+", "+toDate)
        if (fromDate.isNullOrEmpty() || toDate.isNullOrEmpty()) {
            showToast(requireContext(), resources.getString(R.string.er_statement_date))
        } else {
            if (getDateDiff(fromDate,toDate)){
                startActivity(
                    Intent(
                        activity,
                        StatmentActivity::class.java
                    ).putExtra(Constants.FromView, 1).putExtra(Constants.LoanID, loanID)
                        .putExtra(Constants.FromDate, fromDate).putExtra(Constants.ToDate, toDate)
                ) // 1 for Statement
            }else{
                showInternetConnectionLost(
                    requireContext(),
                    resources.getString(R.string.apac),
                    resources.getString(R.string.er_date_diff),
                    resources.getString(R.string.ok)
                )
            }


        }
    }*/

    /*// create an OnDateSetListener
    var fromDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(
            view: DatePicker, year: Int, monthOfYear: Int,
            dayOfMonth: Int
        ) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //val myFormat = "dd-MMM-yyyy" // mention the format you need
            val sdf = SimpleDateFormat(dateFormat, Locale.US)
            tvFromDate.setText(sdf.format(cal.getTime()))
            fromDate= tvFromDate.text.toString()
        }
    }

    // create an OnDateSetListener
    var toDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(
            view: DatePicker, year: Int, monthOfYear: Int,
            dayOfMonth: Int
        ) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //  val myFormat = "dd-MMM-yyyy" // mention the format you need
            val sdf = SimpleDateFormat(dateFormat, Locale.US)
            tvToDate.setText(sdf.format(cal.getTime()))
            toDate = tvToDate.text.toString()
        }
    }*/

    override fun onLeadClick(position: Int) {

    }


}