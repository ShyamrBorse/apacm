package com.apacfin.apacm.presentation.home

import android.content.Context
import androidx.lifecycle.*
import com.apacfin.apacm.R
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.data.model.businesscard.BusinessCardResponse
import com.apacfin.data.model.home.*
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.HomeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : HomeViewModel.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: HomeRepository
) : ViewModel() {

    val _sliderListResponse: MutableLiveData<SliderModel> = MutableLiveData()
    val _getBannerResponse: MutableLiveData<BannerModelResponse> = MutableLiveData()
    val _getLoanDetailsResponse: MutableLiveData<LoanDetailsModelResponse> = MutableLiveData()
    val _getTestimonialsResponse: MutableLiveData<TestimonialModelResponse> = MutableLiveData()
    val _getGratitudeResponse: MutableLiveData<SliderModel> = MutableLiveData()
    val _getLoanStageDetailsResponse: MutableLiveData<RegistrationModel> = MutableLiveData()
    val _getKnowledgeSeries: MutableLiveData<SliderModel> = MutableLiveData()


    //PAyment History
    val _getPaymentHistoryResponse: MutableLiveData<PaymentHistoryResponse> = MutableLiveData()

    //PAyment History
    val _getBranchDetailsResponse: MutableLiveData<BranchDetailsModel> = MutableLiveData()

    private val _businessDetailsListResponse: MutableLiveData<BusinessCardResponse> =
        MutableLiveData()
    private val _saveUpdateBusinessDetailsResponse: MutableLiveData<BusinessCardResponse> =
        MutableLiveData()

    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //sliderList responce
    val sliderListResponse: LiveData<SliderModel> get() = _sliderListResponse

    //getBanner responce
    val getBannerResponse: LiveData<BannerModelResponse> get() = _getBannerResponse

    //getLoanDetails responce
    val getLoanDetails: LiveData<LoanDetailsModelResponse> get() = _getLoanDetailsResponse

    //getTestimonials responce
    val getTestimonials: LiveData<TestimonialModelResponse> get() = _getTestimonialsResponse

   //getTestimonials responce
    val getGratitude: LiveData<SliderModel> get() = _getGratitudeResponse

    //getLoanStageDetails responce
    val getLoanStageDetails: LiveData<RegistrationModel> get() = _getLoanStageDetailsResponse

    //_businessDetailsList responce
    val businessDetailsListResponse: LiveData<BusinessCardResponse> get() = _businessDetailsListResponse

    //_businessCardList responce
    val saveUpdateBusinessDetailsResponse: LiveData<BusinessCardResponse> get() = _saveUpdateBusinessDetailsResponse

    val getPaymentHistoryResponse: LiveData<PaymentHistoryResponse> get() = _getPaymentHistoryResponse

    val getBranchDetailsResponse: LiveData<BranchDetailsModel> get() = _getBranchDetailsResponse

    val getKnowledgeSeries: LiveData<SliderModel> get() = _getKnowledgeSeries
    //sliderList api
    fun sliderList(context:Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.sliderList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_sliderListResponse"  + response.value.msg)
                _sliderListResponse.postValue(response.value!!)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var sliderModel =  SliderModel(context.getString(R.string.tecnical_error),
                    arrayListOf(),
                    false)
                _sliderListResponse.postValue(sliderModel)
            }
        }

    }

    //getBanner api
    fun getBanner(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getBanner()) {

            is Resource.Success -> {

                _dataLoading.postValue(false)

                Timber.e("_getBannerResponse"  + response.value.msg)
                _getBannerResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var bannerModel =  BannerModelResponse(arrayListOf(),context.getString(R.string.tecnical_error),false)
                _getBannerResponse.postValue(bannerModel)
            }
        }

    }

    //getMyLoan api
    fun getMyLoans(context:Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getMyLoans()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getMyLoanRespons"  + response)
                _getLoanDetailsResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getLoanDetailsRespons"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var loanDetailsModelResponse =  LoanDetailsModelResponse(
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false)
                _getLoanDetailsResponse.postValue(loanDetailsModelResponse)
            }
        }

    }

    //getTestimonials api
    fun getTestimonials(context:Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getTestimonials()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getTestimonialsRespons"  + response)
                _getTestimonialsResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getTestimonialsRespons"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var testimonialModelResponse =  TestimonialModelResponse(
                    arrayListOf(), context.getString(R.string.tecnical_error),
                    false)
                _getTestimonialsResponse.postValue(testimonialModelResponse)
            }
        }

    }

 //getGratitude api
    fun getGratitude(context:Context, param: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getGratitude(param)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getGratitudeResponse"  + response)
                _getGratitudeResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getGratitudeResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var sliderModel =  SliderModel(
                    context.getString(R.string.tecnical_error),arrayListOf(),
                    false)
                _getGratitudeResponse.postValue(sliderModel)
            }
        }

    }

//LoanStageDetails api
    fun getLoanStageDetails(context:Context, lanID:  String ) = viewModelScope.launch {
    Timber.e("getLoanStageDetails"  + lanID)
        _dataLoading.postValue(true)
        when (val response = repository.getAlphaLoanStageDetails(lanID)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getGratitudeResponse"  + response)
                _getLoanStageDetailsResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getGratitudeResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf() ,context.getString(R.string.tecnical_error),
                    false)
                _getLoanStageDetailsResponse.postValue(registrationModel)
            }
        }

    }

    //getBusinessDetailsList api
    fun getBusinessDetailsList(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getBusinessDetailsList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_businessDetailsListResponse" + response.value.msg)
                _businessDetailsListResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = BusinessCardResponse(
                    context.getString(R.string.tecnical_error), false,
                    BusinessCardModel(
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    )
                )
                _businessDetailsListResponse.postValue(sliderModel)
            }
        }

    }


    //getTemplateList api
    fun getTemplateList(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getTemplateList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_sliderListResponse" + response.value.msg)
                _sliderListResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = SliderModel(
                    context.getString(R.string.tecnical_error),
                    arrayListOf(),
                    false
                )
                _sliderListResponse.postValue(sliderModel)
            }
        }

    }

    //saveUpdateBusinessDetailsList api
    fun saveUpdateBusinessDetailsList(context: Context,params:  BusinessCardModel ) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.saveUpdateBusinessDetailsList(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("saveUpdateBusinessDetailsList" + response.value.msg)
                _saveUpdateBusinessDetailsResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = BusinessCardResponse(
                    context.getString(R.string.tecnical_error), false,
                    BusinessCardModel(
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    )
                )
                _saveUpdateBusinessDetailsResponse.postValue(sliderModel)
            }
        }

    }

    //EMI Payment History api
    fun getPaymentHistory(context: Context,lanID:  String ) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.emiPaymentHistoryList(lanID)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getBannerResponse"  + response.value.msg)
                _getPaymentHistoryResponse.postValue(response.value)
            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var bannerModel =  PaymentHistoryResponse(arrayListOf(),false,context.getString(R.string.tecnical_error))
                _getPaymentHistoryResponse.postValue(bannerModel)
            }
        }

    }

    //Branch Connect api
    fun getBranchDetails(context: Context,lanID:  String ) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getBranchDetails(lanID)) {
            is Resource.Success -> {
                _dataLoading.postValue(false)
                //Timber.e("getBranchDetails"  + response.value.msg)
                _getBranchDetailsResponse.postValue(response.value)
            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                //Timber.e("___loginResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var branchDetailsModel =  BranchDetailsModel("","","","",context.getString(R.string.tecnical_error),false)
                _getBranchDetailsResponse.postValue(branchDetailsModel)
            }
        }

    }

    fun getKnowledgeSeries(context: Context,type:String) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getKnowledgeSeries(type)) {
            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getKnowledgeSeries" + response.value.msg)
                _getKnowledgeSeries.postValue(response.value!!)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = SliderModel(
                    context.getString(R.string.tecnical_error),
                    arrayListOf(),
                    false
                )
                _getKnowledgeSeries.postValue(sliderModel)
            }
        }

    }

}