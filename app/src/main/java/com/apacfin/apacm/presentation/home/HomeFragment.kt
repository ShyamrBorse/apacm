package com.apacfin.apacm.presentation.home

import android.R.attr.button
import android.animation.ObjectAnimator
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.BounceInterpolator
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.SnapHelper
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.gratitude.GratituideFragment
import com.apacfin.apacm.presentation.home.adapter.BannerAdapter
import com.apacfin.apacm.presentation.home.adapter.MyLoanAdapterHome
import com.apacfin.apacm.presentation.home.adapter.SliderAdapter
import com.apacfin.apacm.presentation.home.adapter.TestimonialAdapter
import com.apacfin.apacm.presentation.referral.ReferralDialogFragment
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.home.*
import com.apacfin.data.model.loanstatus.TimeLineModel
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.apacfin.data.prefrerce.Constants.isDayFirstLogin
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import timber.log.Timber
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class HomeFragment : Fragment(), MyLoanAdapterHome.OnClickListener,
    MyLoanAdapterHome.OnStatmentClickListener, MyLoanAdapterHome.OnPaymentSheduleClickListener,
    BannerAdapter.OnClickListener {
    val TAG: String = HomeFragment::class.java.simpleName
    private val viewModel: HomeViewModel by viewModels()
    var loading = false
    lateinit var sliderModel: SliderModel
    lateinit var bannerModel: BannerModelResponse
    lateinit var loanDetailsModelResponse: LoanDetailsModelResponse
    lateinit var testimonialModelResponse: TestimonialModelResponse
    lateinit var registrationModel: RegistrationModel
    var adapter: MyLoanAdapterHome? = null
    var adapterTestimonial: TestimonialAdapter? = null
    var adapterBanner: BannerAdapter? = null
    lateinit var loanDetailsList: List<LoanDetailModel>
    lateinit var testimonialList: List<TestimonialModel>
    lateinit var bannerList: List<BannerModel>
    private var mDataList = ArrayList<TimeLineModel>()
    private var loanId:String = ""

    private var param1: String? = null
    private var param2: String? = null
    var currentPageSlider = 0
    var timer: Timer? = null
    val DELAY_MS: Long = 700 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 5000
    val handler = Handler()

    var list = mutableListOf<String>()
    var appPreferences: AppPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, {
            loading = it

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
            }
        })

        //-- Handle sliderList response
        viewModel.sliderListResponse.observe(viewLifecycleOwner, {
            sliderModel = it
            Timber.e("sliderModel " + sliderModel)

            if (sliderModel.status) {

                list.addAll(sliderModel.sliderList)
                setAdapter(list)
            } else {
                showInternetConnectionLost(
                    requireContext(),
                    sliderModel.msg,
                    getString(R.string.ok)
                )
            }
        })

        //-- Handle getBanner response
        viewModel.getBannerResponse.observe(viewLifecycleOwner, {
            bannerModel = it
            Timber.e("getBanner " + bannerModel)

            if (bannerModel.status) {
                if (!bannerModel.banners.isNullOrEmpty()) {
                    bannerList = bannerModel.banners
                    setAdapterBanner(bannerList)
                    rcyclerViewBanner.visibility = View.VISIBLE
                }

            } else {
                rcyclerViewBanner.visibility = View.GONE

                showInternetConnectionLost(
                    requireContext(),
                    bannerModel.msg,
                    getString(R.string.ok)
                )
            }
        })

        //-- Handle getLoanDetails response
        viewModel.getLoanDetails.observe(viewLifecycleOwner, {
            loanDetailsModelResponse = it
            Timber.e("loanDetailsModelResponse " + loanDetailsModelResponse)

            if (loanDetailsModelResponse.status) {
                loanDetailsList = loanDetailsModelResponse.loanDetails
                setAdapterLoan(loanDetailsList)
            } else {
                tvMyLoans.visibility = View.GONE
                showInternetConnectionLost(
                    requireContext(),
                    loanDetailsModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })

        //-- Handle getTestimonials response
        /*viewModel.getTestimonials.observe(viewLifecycleOwner, {
            testimonialModelResponse = it
            Timber.e("testimonialModelResponse " + testimonialModelResponse)

            if (testimonialModelResponse.status) {
                testimonialList = testimonialModelResponse.testimonialData
                setAdapterTestimonial(testimonialList)
            } else {
                tvOurHpyCust.visibility = View.GONE
                showInternetConnectionLost(
                    requireContext(),
                    testimonialModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })*/

        //-- Handle getGratitude response
        viewModel.getGratitude.observe(viewLifecycleOwner, {
            sliderModel = it
            Timber.e("getGratitude " + sliderModel)

            if (sliderModel.status) {
                if (!sliderModel.sliderList.isEmpty()) {
                    val transaction: FragmentTransaction =
                        requireActivity().supportFragmentManager.beginTransaction()
                    GratituideFragment.newInstance(sliderModel.sliderList.get(0), "").apply {
                        show(transaction, tag)
                    }

                    appPreferences?.putBoolean(isDayFirstLogin, true) 
                  //  appPreferences?.putString(LoginDate, getCurrentDate())
                }


            } else {
                showInternetConnectionLost(
                    requireContext(),
                    sliderModel.msg,
                    getString(R.string.ok)
                )
            }
        })

        //-- Handle getLoanStageDetails response
        viewModel.getLoanStageDetails.observe(viewLifecycleOwner, {
            registrationModel = it
            Timber.e("getLoanStageDetails " + registrationModel)

            if (registrationModel.status) {
                if (!registrationModel.loanStageData.isEmpty()) {

                    mDataList = registrationModel.loanStageData
                    //initRecyclerView(mDataList)

                    val transaction: FragmentTransaction =
                        requireActivity().supportFragmentManager.beginTransaction()
                    LoanStatusDialogFragment.newInstance(
                        loanId,
                        mDataList
                    ).apply {
                        show(transaction, tag)

                    }
                }


            } else {
                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
            }
        })
    }


    private fun setAdapter(list: MutableList<String>) {

        val adapter = SliderAdapter(activity, list)
        viewpagerSlider.adapter = adapter
        viewpagerSlider.setClipToPadding(false)
        viewpagerSlider.setPadding(10, 0, 10, 0)
        indicator.attachToPager(viewpagerSlider)
    }

    private fun setAdapterLoan(loanDetailsList: List<LoanDetailModel>) {
        tvMyLoans.visibility = View.VISIBLE
        adapter = MyLoanAdapterHome(
            requireContext(),
            loanDetailsList,
            this, this, this
        )
        rcyclerViewMyLoanHome!!.adapter = adapter
        adapter!!.notifyDataSetChanged()

    }

    /*private fun setAdapterTestimonial(testimonialList: List<TestimonialModel>) {
        tvOurHpyCust.visibility = View.VISIBLE
        adapterTestimonial = TestimonialAdapter(requireContext(), testimonialList)
        rcyclerViewTestimonial!!.adapter = adapterTestimonial
        adapterTestimonial!!.notifyDataSetChanged()

    }*/

    private fun setAdapterBanner(bannerModelList: List<BannerModel>) {
        adapterBanner = BannerAdapter(rcyclerViewBanner, requireContext(), bannerModelList, this)
        rcyclerViewBanner!!.adapter = adapterBanner
        adapterBanner!!.notifyDataSetChanged()

    }

    private fun initView() {
        rcyclerViewMyLoanHome!!.layoutManager = LinearLayoutManager(requireContext())
       // rcyclerViewTestimonial!!.layoutManager = LinearLayoutManager(requireContext())
        rcyclerViewBanner!!.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        val snapHelper: SnapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(rcyclerViewBanner)

        val animY = ObjectAnimator.ofFloat(cardBottonNav, "translationY", -100f, 0f)
        animY.duration = 1000 //1sec

        animY.interpolator = BounceInterpolator()
        animY.repeatCount = 1
        animY.start()

       // cardBottonNav.startAnimation(translateAnimation)

        getSliderList()
       // getTestimonials()
        getLoanList()

        appPreferences = AppPreferences.getAppPreferences(requireContext())
        if (appPreferences?.getString(Constants.gratitudeFlag, "")
                .equals("0") || appPreferences?.getString(Constants.gratitudeFlag, "").equals("1")
        ) {
           //getCurrentDayLogin(requireContext())

            if (!appPreferences?.getBoolean(isDayFirstLogin, false)!!) {
                getGratitude()
           }
        }
        /*After setting the adapter use the timer */
        val Update = Runnable {
            Timber.e("currentPageFlashSale: " + currentPageSlider + " : " + list.size)
            if (currentPageSlider === list.size - 1) {
                currentPageSlider = 0
            }
            viewpagerSlider.setCurrentItem(currentPageSlider++, true)
        }

        timer = Timer() // This will create a new Thread

        timer!!.schedule(object : TimerTask() {
            // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)

        ivReferal.setOnClickListener {
            val transaction: FragmentTransaction =
                requireActivity().supportFragmentManager.beginTransaction()
            ReferralDialogFragment().apply {
                show(transaction, tag)

            }
        }
    }


    private fun getSliderList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.sliderList(requireActivity())
            viewModel.getBanner(requireActivity())
        }

    }

    private fun getLoanList() {
        if (isNetworkAvailable(requireContext())) {

            viewModel.getMyLoans(requireActivity())

        }
    }

    private fun getTestimonials() {
        if (isNetworkAvailable(requireContext())) {

            viewModel.getTestimonials(requireActivity())

        }
    }

    private fun getGratitude() {
        if (isNetworkAvailable(requireContext())) {

            val params: MutableMap<String, String> = HashMap()
            params["appVersion"] = appPreferences?.getString(Constants.gratitudeFlag, "").toString()
            viewModel.getGratitude(requireActivity(), params)

        }
    }
    private fun getLoanStatus(loanId: String) {
        if (isNetworkAvailable(requireContext())) {

            viewModel.getLoanStageDetails(requireActivity(), loanId)

        }
    }
    override fun onPause() {
        super.onPause()
        Timber.e("aa onPause")
        handler.removeCallbacksAndMessages(null)
        timer?.cancel()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onLeadClick(position: Int) {
        //findNavController().navigate(R.id.frgMyLoan)
        loanId = loanDetailsList.get(position).loanId
        getLoanStatus(loanId)

    }

    override fun onBannerClick(position: Int) {
        Timber.e(TAG + "onBannerClick: " + bannerList.size)
        if (bannerList[position].action.equals("ESIGN")) { //ESIGN/EMI
            findNavController().navigate(R.id.frgDocument)
        }
        if (bannerList[position].action.equals("EMI")) {
            findNavController().navigate(R.id.frgMyLoan)
        }
    }

    override fun onStatmentClickListener(position: Int) {
    }

    override fun onPaymentSheduleClickListener(position: Int) {
    }

}