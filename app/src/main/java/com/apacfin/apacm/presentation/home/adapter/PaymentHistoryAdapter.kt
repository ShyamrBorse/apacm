package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.home.DashBoardModel
import com.apacfin.data.model.home.EmiPayHistoryDetailModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PaymentHistoryAdapter(
     context: Context,
    paymentHistoryArrayList: ArrayList<EmiPayHistoryDetailModel>,onPaymentHistoryListener:OnPaymentHistoryListener
) :
    RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder>() {

    var context: Context
    val paymentHistoryArrayList: ArrayList<EmiPayHistoryDetailModel>
    private lateinit var onPaymentHistoryListener:OnPaymentHistoryListener

    init {
        this.onPaymentHistoryListener = onPaymentHistoryListener
        this.context = context
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PaymentHistoryAdapter.ViewHolder {
        // to inflate the layout for each item of recycler view.
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_payment_history_detail, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PaymentHistoryAdapter.ViewHolder, position: Int) {

        var model: EmiPayHistoryDetailModel = paymentHistoryArrayList[position]
        if (model.isBounce == true) {
            holder.tvMarkGreen.setBackgroundResource(R.drawable.badge_circle_red)
        } else {
            holder.tvMarkGreen.setBackgroundResource(R.drawable.badge_circle_green)
        }
        holder.tvMarkGreen.setText(getMonth(model.emiDate.toString()))

        holder.tvMarkGreen.setOnClickListener(View.OnClickListener {
            onPaymentHistoryListener.onPaymentHistoryListener(context,model)
        })

    }


    override fun getItemCount(): Int {
        // this method is used for showing number of card items in recycler view.
        return paymentHistoryArrayList.size
    }

    // View holder class for initializing of your views such as TextView and Imageview.
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvMarkGreen: TextView
        init {
            tvMarkGreen = itemView.findViewById(R.id.tvMarkGreen)
        }
    }


    // Constructor
    init {
        this.paymentHistoryArrayList = paymentHistoryArrayList
    }

    @Throws(ParseException::class)
    private fun getMonth(date: String): String? {
        val d: Date = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).parse(date)
        val cal: Calendar = Calendar.getInstance()
        cal.setTime(d)
        return SimpleDateFormat("MMM").format(cal.getTime())
    }

    interface OnPaymentHistoryListener {
        fun onPaymentHistoryListener(context: Context,model: EmiPayHistoryDetailModel)
    }
}
