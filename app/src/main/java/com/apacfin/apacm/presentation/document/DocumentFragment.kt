package com.apacfin.apacm.presentation.document

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.apacfin.apacm.R
import com.apacfin.apacm.util.ViewPagerAdapter
import com.apacfin.apacm.util.changeTabsFont
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_document.view.*
import kotlinx.android.synthetic.main.view_pager.view.*


class DocumentFragment : Fragment() {
    lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_document, container, false)
        initialization()
        return rootView
    }

    private fun initialization() {
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        setupViewPager(rootView.viewPager)
        rootView.viewPager.offscreenPageLimit = 1
        rootView.viewPager.currentItem = 0
//        Migrated into ViewPager 2
        TabLayoutMediator(rootView.tabs, rootView.viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = resources.getString(R.string.pendingDocument)
                }
                1 -> {
                    tab.text = resources.getString(R.string.signDocument)
                }
            }
        }.attach()
        val params = rootView.tabs?.layoutParams as AppBarLayout.LayoutParams
        params.scrollFlags = 0
        changeTabsFont(rootView.tabs!!)
    }


    private fun setupViewPager(viewpager: ViewPager2?) {
        val adapter = ViewPagerAdapter(requireActivity())
        adapter.addFragment(SignPendingDocFragment(), resources.getString(R.string.pendingDocument))
        adapter.addFragment(AllDocFragment(), resources.getString(R.string.signDocument))

        viewpager?.adapter = adapter
    }

}