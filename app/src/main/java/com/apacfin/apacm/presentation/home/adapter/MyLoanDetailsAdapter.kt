package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.myloan.LoanDetailModel

class MyLoanDetailsAdapter(var onMyLoadViewDetailsLis: OnMyLoadViewDetailsListener,rcyclerViewLoanDetails: RecyclerView,context: Context,var loanDetailsArrayList: List<LoanDetailModel>) :
     RecyclerView.Adapter<MyLoanDetailsAdapter.ViewHolder>() {

        var context: Context
        var onMyLoadViewDetailsListener: OnMyLoadViewDetailsListener



     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
         // to inflate the layout for each item of recycler view.
         val view: View = LayoutInflater.from(parent.context).inflate(R.layout.contain_single_loan_details, parent, false)
         return ViewHolder(view)
     }


     class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         val tvApplicantId: TextView
         val tvApplicantName: TextView
         val tvLoanStatus: TextView
         val tvLoanType: TextView
         var btnViewDetails: Button

         init {
             tvApplicantId = itemView.findViewById(R.id.tvApplicantId)
             tvApplicantName = itemView.findViewById(R.id.tvApplicantName)
             tvLoanStatus = itemView.findViewById(R.id.tvLoanStatus)
             tvLoanType = itemView.findViewById(R.id.tvLoanType)
             btnViewDetails = itemView.findViewById(R.id.btnViewDetails)

             if (Companion.loanDetailsArrayLists.size > 1) {
                 val width: Int = Companion.rcyclerViewLoanDetails.getWidth()
                 val params = itemView.layoutParams
                 params.width = (width * 0.92).toInt()
                 itemView.layoutParams = params
             }
         }
     }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: LoanDetailModel = loanDetailsArrayList[position]
        holder.tvApplicantId.text = "" + loanDetailModel.loanId
        holder.tvApplicantName.text = "" + loanDetailModel.applicant
        holder.tvLoanStatus.text = "" + loanDetailModel.loanStatus
        holder.tvLoanType.text = "" + loanDetailModel.loanType

        holder.btnViewDetails.setOnClickListener {
            onMyLoadViewDetailsListener.onMyLoadViewDetailsListener(context,loanDetailModel)
        }
    }

     companion object {
         private val TAG = MyLoanDetailsAdapter::class.java.simpleName
         lateinit var loanDetailsArrayLists: List<LoanDetailModel>
         lateinit var rcyclerViewLoanDetails: RecyclerView
         var selectedPosition = -1
     }



    override fun getItemCount(): Int {
        return loanDetailsArrayList.size
    }

    init {
        Companion.loanDetailsArrayLists = loanDetailsArrayList
        this.context = context
        this.onMyLoadViewDetailsListener = onMyLoadViewDetailsLis
        Companion.rcyclerViewLoanDetails = rcyclerViewLoanDetails
    }

    interface OnMyLoadViewDetailsListener {
        fun onMyLoadViewDetailsListener(context: Context,loanDetailModel: LoanDetailModel)
    }

}






