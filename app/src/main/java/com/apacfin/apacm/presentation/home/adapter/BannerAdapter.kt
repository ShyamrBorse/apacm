package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.home.BannerModel
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : BannerAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class BannerAdapter(
    rcyclerViewBanner: RecyclerView, context: Context,
    listOfChargesModel: List<BannerModel>,
    onClickListener: OnClickListener
) : RecyclerView.Adapter<BannerAdapter.ViewHolder?>() {
    var context: Context
    private val mOnClickListener: OnClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_banner, parent, false)
        /*val width: Int = rcyclerViewBanner.getWidth()
        val params = view.layoutParams
        params.width = (width * 0.8).toInt()
        view.layoutParams = params*/
        return ViewHolder(view, mOnClickListener)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bannerModel: BannerModel = listOfChargesModels[position]
        holder.txtTitle.text = bannerModel.title

//        val animation1 = AlphaAnimation(0.2f, 1.0f)
//        animation1.duration = 500
//        holder.ivBanner.setAlpha(1f)
//        holder.ivBanner.startAnimation(animation1)
//
//        Handler().postDelayed(Runnable {
//            holder.ivBanner.startAnimation(animation1)
//        }, 1000)

//        Handler().postDelayed(Runnable {
//            val anim = AlphaAnimation(0.0f, 1.0f)
//            anim.duration = 1000
//            holder.ivBanner.startAnimation(anim)
//        }, 1000)

        val fadeIn = AlphaAnimation(0.0f, 1.0f)
        val fadeOut = AlphaAnimation(1.0f, 0.0f)

        holder.cvPayNow.startAnimation(fadeIn)
        holder.cvPayNow.startAnimation(fadeOut)

        holder.cvClickHere.startAnimation(fadeIn)
        holder.cvClickHere.startAnimation(fadeOut)

        holder.cvReferEarn.startAnimation(fadeIn)
        holder.cvReferEarn.startAnimation(fadeOut)

        fadeIn.duration = 200
        fadeOut.duration = 500
        fadeOut.startOffset = 500 + fadeIn.startOffset + 500
        fadeIn.setRepeatCount(Animation.INFINITE)
        fadeOut.setRepeatCount(Animation.INFINITE)

        if (listOfChargesModels[position].action.equals("ESIGN")) {
            holder.ivBanner.setImageResource(R.drawable.e_sign_banner1)
            holder.cvPayNow.visibility = View.GONE
            holder.cvReferEarn.visibility = View.GONE
            holder.cvClickHere.visibility = View.VISIBLE
        }

        if (listOfChargesModels[position].action.equals("EMI")) {
            holder.ivBanner.setImageResource(R.drawable.pay_your_emi_banner_1)
            holder.cvPayNow.visibility = View.VISIBLE
            holder.cvClickHere.visibility = View.GONE
            holder.cvReferEarn.visibility = View.GONE
        }

        if (listOfChargesModels[position].action.equals("ReferAndErn")) {
            holder.ivBanner.setImageResource(R.drawable.referalbanner3)
            holder.cvPayNow.visibility = View.GONE
            holder.cvClickHere.visibility = View.GONE
            holder.cvReferEarn.visibility = View.VISIBLE
        }


        /*if (bannerModel.subTitle.isNullOrEmpty()) {
            holder.txtSubTitle.visibility = View.GONE
        } else {
            holder.txtSubTitle.visibility = View.VISIBLE
            holder.txtSubTitle.text = bannerModel.subTitle
            holder.txtSubTitle.setPaintFlags(holder.txtSubTitle.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        }*/
    }

    override fun getItemCount(): Int {
        return listOfChargesModels.size
    }

    interface OnClickListener {
        fun onBannerClick(position: Int)
    }


    inner class ViewHolder(
        itemView: View, onClickListener: OnClickListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var txtTitle: TextView
        var txtSubTitle: TextView
        var ivBanner : ImageView
        var cvClickHere : CardView
        var cvPayNow:CardView
        var cvReferEarn:CardView

        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {
            Timber.e(TAG + "onClick: " + adapterPosition + "  " + listOfChargesModels[adapterPosition])
            mOnClickListener.onBannerClick(adapterPosition)
        }

        init {
            txtTitle = itemView.findViewById(R.id.txtTitle)
            txtSubTitle = itemView.findViewById(R.id.txtSubTitle)
            ivBanner = itemView.findViewById(R.id.ivBanner)
            cvClickHere = itemView.findViewById(R.id.cvClickHere)
            cvReferEarn = itemView.findViewById(R.id.cvReferEarn)
            cvPayNow = itemView.findViewById(R.id.cvPayNow)

            mOnClickListener = onClickListener
            itemView.setOnClickListener(this)

            if (Companion.listOfChargesModels.size > 1) {
                val width: Int = Companion.rcyclerViewBanner.getWidth()
                val params = itemView.layoutParams
                params.width = (width * 0.85).toInt()
                itemView.layoutParams = params
            }
        }
    }

    companion object {
        private val TAG = BannerAdapter::class.java.simpleName
        lateinit var listOfChargesModels: List<BannerModel>
        lateinit var rcyclerViewBanner: RecyclerView
        var selectedPosition = -1
    }

    init {
        mOnClickListener = onClickListener
        listOfChargesModels = listOfChargesModel
        this.context = context
        Companion.rcyclerViewBanner = rcyclerViewBanner

    }
}