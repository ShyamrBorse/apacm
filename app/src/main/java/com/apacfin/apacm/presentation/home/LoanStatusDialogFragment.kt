package com.apacfin.apacm.presentation.home

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.home.adapter.ExampleTimeLineAdapter
import com.apacfin.data.model.loanstatus.TimeLineModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_loan_status_dialog.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LoanStatusDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class LoanStatusDialogFragment : DialogFragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var loanID: String? = null
    private var mDataList = ArrayList<TimeLineModel>()

    val TAG: String = LoanStatusDialogFragment::class.java.simpleName

    private lateinit var mAdapter: ExampleTimeLineAdapter

    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            loanID = it.getString(ARG_PARAM1)
            mDataList = it.getParcelableArrayList<TimeLineModel>(ARG_PARAM2)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_loan_status_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout(width - 40, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        btnCloseLoanStatusDialog.setOnClickListener(this)
        btnGotoHome.setOnClickListener(this)
        tvLoanID.apply {
            text = ": "+loanID
        }
        initRecyclerView(mDataList)

    }


    private fun initRecyclerView(dataList: ArrayList<TimeLineModel>) {
        mLayoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = mLayoutManager
        mAdapter = ExampleTimeLineAdapter(dataList)
        recyclerView.adapter = mAdapter
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireActivity(), theme) {
            override fun onBackPressed() {
                // On backpress, do your stuff here.
                dialog?.dismiss()
            }
        }
    }
    override fun onClick(view: View?) {

        when (view?.getId()) {
            R.id.btnCloseLoanStatusDialog -> dialog?.dismiss()
            R.id.btnGotoHome -> dialog?.dismiss()


        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LoanStatusDialogFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: java.util.ArrayList<TimeLineModel>) =
            LoanStatusDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putParcelableArrayList(ARG_PARAM2, param2)
                }
            }
    }
}