package com.apacfin.apacm.presentation.payment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.amountFormater
import com.apacfin.data.model.payment.TransactionModel
import com.apacfin.data.prefrerce.Constants
import java.text.SimpleDateFormat
import java.util.*


/**
 *
 * @author SHYAM BORSE
 *
 * © Copyright APAC Financial Services
 *
 * File Name : PaymentTransactionAdapter.java
 *
 * Modification History
 *
 * 16-Oct-2020 Shyam Borse : Initial version
 * 01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class PaymentTransactionAdapter(
    context: Context, loanDetailModel: List<TransactionModel>,
    onClickListener: OnClickListener,
    onPaymentSheduleClickListener: OnPaymentSheduleClickListener
) : RecyclerView.Adapter<PaymentTransactionAdapter.ViewHolder?>() {
    var context: Context

    private val mOnClickListener: OnClickListener
    private var mOnPaymentSheduleClickListener: OnPaymentSheduleClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_list_of_payment_transaction, parent, false)
        return ViewHolder(
            view,
            mOnClickListener,
            mOnPaymentSheduleClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: TransactionModel = loanDetailModelData[position]


        holder.txtLoanID.text = "" + loanDetailModel.lan
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val date1: Date = sdf.parse(loanDetailModel.createdOn)
        val sdf1 = SimpleDateFormat(Constants.dateFormat, Locale.US)
        val output: String = sdf1.format(date1)
        holder.txtTransDate.text = "" + output
        holder.txtTransactionId.text = "" + loanDetailModel.orderId
// val separated: List<String> = loanDetailModel.orderAmount.split(".")

        if(loanDetailModel.paymentNote.trim().equals("") || loanDetailModel.paymentNote.trim().isEmpty()){
            holder.llPaymentNote.visibility = View.GONE
            holder.txtStatus.setTextColor(ContextCompat.getColor(context,R.color.black))
        }else{
            holder.txtPaymentNote.text = loanDetailModel.paymentNote
            holder.llPaymentNote.visibility = View.VISIBLE
            holder.txtStatus.setTextColor(ContextCompat.getColor(context,R.color.red))
        }

        holder.txtAmt.text = "" + amountFormater(loanDetailModel.orderAmount.toDouble())

        if (loanDetailModel.paymentMethod.isNullOrEmpty()) {
            holder.txtPaymentMode.text = "N/A"
        } else{
            holder.txtPaymentMode.text = "" + loanDetailModel.paymentMethod
        }

        if (loanDetailModel.apacStatus.trim().equals("inProgress")){
            holder.txtStatus.text = context.getString(R.string.inprogress)
            holder.btnChkStatus.visibility =View.VISIBLE
        }else if (loanDetailModel.apacStatus.trim().equals("paid")){
            holder.txtStatus.text = context.getString(R.string.paid)
            holder.btnChkStatus.visibility = View.GONE
            if((!loanDetailModel.paymentNote.trim().equals("")) || (!loanDetailModel.paymentNote.trim().isEmpty())){
                holder.txtStatus.setText("Paid *")
            }
        }else if (loanDetailModel.apacStatus.trim().equals("fail")){
            holder.txtStatus.text = context.getString(R.string.fail)
            holder.btnChkStatus.visibility = View.GONE
        }else{
            holder.txtStatus.text = "" + loanDetailModel.apacStatus
            holder.btnChkStatus.visibility = View.GONE
        }
        holder.btnChkStatus.setOnClickListener {
            mOnPaymentSheduleClickListener.onPaymentSheduleClickListener(position)
        }

    }

    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }

    interface OnStatmentClickListener {
        fun onStatmentClickListener(position: Int)
    }

    interface OnPaymentSheduleClickListener {
        fun onPaymentSheduleClickListener(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View,
        onClickListener: OnClickListener,
        onPaymentSheduleClickListener: OnPaymentSheduleClickListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var txtLoanID: TextView
        var txtTransDate: TextView
        var txtTransactionId: TextView
        var txtStatus: TextView
        var txtAmt: TextView
        var txtPaymentMode: TextView
        var btnChkStatus: TextView
        var txtPaymentNote: TextView
        var llPaymentNote:LinearLayout

        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {
// Log.d(TAG, "onClick: " + selectedPosition)
            mOnClickListener.onLeadClick(selectedPosition)
        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            txtLoanID = itemView.findViewById(R.id.txtLoanID)
            txtTransDate = itemView.findViewById(R.id.txtTransDate)
            txtTransactionId = itemView.findViewById(R.id.txtTransactionId)
            txtStatus = itemView.findViewById(R.id.txtStatus)
            txtAmt = itemView.findViewById(R.id.txtAmt)
            txtPaymentMode = itemView.findViewById(R.id.txtPaymentMode)
            btnChkStatus = itemView.findViewById(R.id.btnChkStatus)
            txtPaymentNote = itemView.findViewById(R.id.txtPaymentNote)
            llPaymentNote = itemView.findViewById(R.id.llPaymentNote)

            mOnClickListener = onClickListener
            mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = PaymentTransactionAdapter::class.java.simpleName
        lateinit var loanDetailModelData: List<TransactionModel>
        var selectedPosition = -1
    }

    init {
        loanDetailModelData = loanDetailModel
        mOnClickListener = onClickListener
        mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
        this.context = context
    }
}