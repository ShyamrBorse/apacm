package com.apacfin.apacm.presentation.myloan

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.apacfin.apacm.R
import com.apacfin.data.model.document.DocViewModelResponse
import com.apacfin.data.model.document.DocumentsPojo
import com.apacfin.data.model.listofchaarges.ListOfChargesModelResponse
import com.apacfin.data.model.myloan.AccountStatementModelResponse
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.LoanDetailsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : LanguageViewModel.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@HiltViewModel
class LoanDetailsViewModel @Inject constructor(
    private val repository: LoanDetailsRepository
) : ViewModel() {

    val _getLoanDetailsResponse: MutableLiveData<LoanDetailsModelResponse> = MutableLiveData()
    val _getAccountStatementResponse: MutableLiveData<AccountStatementModelResponse> =
        MutableLiveData()
    val _getPaymentSheduleResponse: MutableLiveData<AccountStatementModelResponse> =
        MutableLiveData()
    val _getDocumentResponse: MutableLiveData<DocViewModelResponse> =
        MutableLiveData()
    val _saveAccountStatementRequestResponse: MutableLiveData<RegistrationModel> =
        MutableLiveData()

    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //getRegistration responce
    val getLoanDetails: LiveData<LoanDetailsModelResponse> get() = _getLoanDetailsResponse

    //getAccountStatement and getPaymentShedule responce
    val getAccountStatementResponse: LiveData<AccountStatementModelResponse> get() = _getAccountStatementResponse

    //getPaymentSheduleResponse responce
    val getPaymentSheduleResponse: LiveData<AccountStatementModelResponse> get() = _getPaymentSheduleResponse

    //getDocumentResponse responce
    val getDocumentResponse: LiveData<DocViewModelResponse> get() = _getDocumentResponse

    //_saveAccountStatementRequestResponse responce
    val saveAccountStatementRequestResponse: LiveData<RegistrationModel> get() = _saveAccountStatementRequestResponse


    //getLoanDetails api
    fun getLoanDetails(context:Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getLoanDetails()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getLoanDetailsRespons"  + response)
                _getLoanDetailsResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getLoanDetailsRespons" + response.errorCode+ response.isNetworkError+ response.errorBody)
                var loanDetailsModelResponse =  LoanDetailsModelResponse(
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false)
                _getLoanDetailsResponse.postValue(loanDetailsModelResponse)
            }
        }

    }

    //getAccountStatement api
    fun getAccountStatement(id: String, params: MutableMap<String, String>) =
        viewModelScope.launch {
            _dataLoading.postValue(true)
            when (val response = repository.getAccountStatement(id, params)) {

                is Resource.Success -> {
                    _dataLoading.postValue(false)
                    Timber.e("_getAccountStatement"  + response.value.msg)
                    _getAccountStatementResponse.postValue(response.value)

                }


                is Resource.Failure -> {
                    _dataLoading.postValue(false)
                    Timber.e("_getAccountStatement" + response.errorBody)

                }
            }

        }

    //getAccountStatement api
    fun saveAccountStatementRequest(context: Context,params: MutableMap<String, String>) =
        viewModelScope.launch {
            _dataLoading.postValue(true)
            when (val response = repository.saveAccountStatementRequest(params)) {

                is Resource.Success -> {
                    _dataLoading.postValue(false)
                    Timber.e("saveAccountStatement"  + response.value.msg)
                    _saveAccountStatementRequestResponse.postValue(response.value)

                }


                is Resource.Failure -> {
                    _dataLoading.postValue(false)
                    Timber.e("saveAccountStatement"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                    var registrationModel =  RegistrationModel("", "", "", "", "", false,
                        "", arrayListOf() ,context.getString(R.string.tecnical_error),
                        false)
                    _saveAccountStatementRequestResponse.postValue(registrationModel)
                }
            }

        }

    //getPaymentShedule api
    fun getPaymentShedule(id: String) =
        viewModelScope.launch {
            _dataLoading.postValue(true)
            when (val response = repository.getPaymentShedule(id)) {

                is Resource.Success -> {
                    _dataLoading.postValue(false)
                    Timber.e("_getPaymentSheduleRespo"+ response.value.msg)
                    _getAccountStatementResponse.postValue(response.value)

                }


                is Resource.Failure -> {
                    _dataLoading.postValue(false)
                    Timber.e("_getPaymentSheduleRespo" + response.errorBody)

                }
            }

        }

    //getDocument api
    fun getDocument(context: Context,id: String,loanID: String) =
        viewModelScope.launch {
            _dataLoading.postValue(true)
            when (val response = repository.getDocument(id,loanID)) {

                is Resource.Success -> {
                    _dataLoading.postValue(false)
                    Timber.e("getDocument"+ response.value.msg)
                    _getDocumentResponse.postValue(response.value)

                }


                is Resource.Failure -> {
                    _dataLoading.postValue(false)
                    Timber.e("getDocument" + response.errorCode+ response.isNetworkError+ response.errorBody)
                    var docViewModelResponse =  DocViewModelResponse(
                        DocumentsPojo("",0,"",""),context.getString(R.string.tecnical_error),
                        false)
                    _getDocumentResponse.postValue(docViewModelResponse)
                }
            }

        }


}