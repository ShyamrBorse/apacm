package com.apacfin.apacm.presentation.servicecomplaint

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.profile.ProfileViewModel
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.util.*
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_service_complaint_dialog.*
import timber.log.Timber

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ServiceComplaintDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ServiceComplaintDialogFragment : DialogFragment(), View.OnClickListener , AdapterView.OnItemSelectedListener,
    LocationListener {
    val TAG = ServiceComplaintDialogFragment::class.java.simpleName
    private val viewModel: ProfileViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    lateinit var loanDetailsModelResponse: LoanDetailsModelResponse
    lateinit var loanDetailsList: List<LoanDetailModel>
    private var param1: String? = null
    private var param2: String? = null
    var appPreferences: AppPreferences? = null
    var loanID = ""
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_service_complaint_dialog, container, false)
    }
    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout(width - 80, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(this, Observer {
            loading = it
            Timber.e( "SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })
        //-- Handle getLoanDetails response
        viewModel.getLoanDetails.observe(viewLifecycleOwner, {
            loanDetailsModelResponse = it
            Timber.e("loanDetailsModelResponse " + loanDetailsModelResponse)

            if (loanDetailsModelResponse.status) {

                loanDetailsList = loanDetailsModelResponse.loanDetails
                setSpinnerData(loanDetailsList)
            } else {
                showInternetConnectionLost(
                    requireContext(),
                    loanDetailsModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })
        //-- Handle saveServiceComplaint response
        viewModel.saveServiceComplaintResponse.observe(this, {
            registrationModel = it
            Timber.e("newUser " + registrationModel)

            if (registrationModel.status) {
                //showToast(requireContext(),  registrationModel.msg)

                showAlertMessageSingleClick(
                    requireContext(),
                    getString(R.string.ok),
                    registrationModel.msg
                ) { alertDialog, which ->
                    alertDialog.dismiss()
                    dialog?.dismiss()
                }
            } else {
                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
                //  showSnackBar(this, getDetailsFromUCIC.msg) }
                //startActivity(Intent(this, MainActivity::class.java))
            }
        })

    }

    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        tvCustCareEmailCompl.text = Constants.ContactUsEmail
        btnCloseServiceDialog.setOnClickListener(this)
        btnSubmitService.setOnClickListener(this)

        getLoanList()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireActivity(), theme) {
            override fun onBackPressed() {
                // On backpress, do your stuff here.
                dialog?.dismiss()
            }
        }
    }
    private fun getLoanList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getMyLoans(requireActivity())
        }
    }
    private fun setSpinnerData(loanDetailsList: List<LoanDetailModel>) {
        if (loanDetailsList.size>0){
            loanID = loanDetailsList[0].loanId
            var lonList : ArrayList<String?> = arrayListOf()
            for(item in loanDetailsList){
                lonList.add(item.loanId)
            }
            spAccount!!.setOnItemSelectedListener(this)

            // Create an ArrayAdapter using a simple spinner layout and languages array
            val aa = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, lonList)
            // Set layout to use when the list of choices appear
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            with(spAccount)
            {
                adapter = aa
                setSelection(0, false)
                onItemSelectedListener = this@ServiceComplaintDialogFragment
                prompt = context.getString(R.string.select_account)
                setPopupBackgroundResource(R.color.material_grey_600)

            }
        }



    }
    private fun validation() {

        if (ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val manager =
                requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                alertMessageNoGps(requireActivity(), "Yes")
            } else {
                var progressDialog = showProgressDialog(requireActivity())


                object : AsyncTaskCoroutine<Int?, Boolean?>() {
                    override fun onPostExecute(@Nullable result: Boolean?) {
                        progressDialog.dismiss()

                        if (isNetworkAvailable(requireContext())) {
                            if (isEmpty(edtComplatint)) {
                                showToast(requireContext(), getString(R.string.enter_complaint))
                            } else {


                                val params: MutableMap<String, String> = HashMap()
                                params["lan"] = loanID
                                params["msg"] = edtComplatint.text.toString()

                                viewModel.saveServiceComplaint(requireActivity(), params)

                            }
                        }
                    }

                    override fun onPreExecute() {
                        progressDialog.show()
                        //getCurrentLatLong(requireActivity())
                        getLocation();
                    }

                    override fun doInBackground(vararg params: Int?): Boolean? {
                        return null
                    }
                }.execute<Any>()
            }
        }else{
            appPreferences?.putString(Constants.Latitude, "")
            appPreferences?.putString(Constants.Longitude, "")

            if (isNetworkAvailable(requireContext())) {
                if (isEmpty(edtComplatint)) {
                    showToast(requireContext(), getString(R.string.enter_complaint))
                } else {
                    val params: MutableMap<String, String> = HashMap()
                    params["lan"] = loanID
                    params["msg"] = edtComplatint.text.toString()
                    viewModel.saveServiceComplaint(requireActivity(), params)
                }
            }
        }

        /*val manager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            alertMessageNoGps(requireContext(), "Yes")
        } else {
            getCurrentLatLong(requireContext())
            if (isNetworkAvailable(requireContext())) {
                if (isEmpty(edtComplatint)) {
                    showToast(requireContext(), getString(R.string.enter_complaint))
                } else {


                    val params: MutableMap<String, String> = HashMap()
                    params["lan"] = loanID
                    params["msg"] = edtComplatint.text.toString()

                    viewModel.saveServiceComplaint(requireActivity(), params)

                }
            }
        }*/
    }


    private fun getLocation() {
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
        // tvGpsLocation = findViewById(R.id.textView)
        //tvGpsLocation.text = "Latitude: " + location.latitude + " , Longitude: " + location.longitude
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ServiceComplaintDialogFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ServiceComplaintDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnSubmitService -> validation()
            R.id.btnCloseServiceDialog -> dialog?.dismiss()

        }
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        loanID = loanDetailsList[position].loanId
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }
}