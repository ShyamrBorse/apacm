package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.myloan.LoanDetailModel

class SelectLoanTypeAdapter(private val context: Context?, loanDetailsModelList: MutableList<LoanDetailModel>) :
    RecyclerView.Adapter<SelectLoanTypeAdapter.ViewHolder>() {
    lateinit var loanDetailsModelList: MutableList<LoanDetailModel>
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SelectLoanTypeAdapter.ViewHolder {
        // to inflate the layout for each item of recycler view.
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_select_loan_type_history, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectLoanTypeAdapter.ViewHolder, position: Int) {

        var loanDetailModel:LoanDetailModel = loanDetailsModelList.get(holder.adapterPosition)
        holder.radioLoanType.text = loanDetailsModelList.get(position).loanType

        if(loanDetailModel.isLoanTypeSelectedForHistory==false){
            holder.radioLoanType.isChecked= false
        }else{
            holder.radioLoanType.isChecked= true
        }
        holder.radioLoanType.setOnClickListener(View.OnClickListener {

            if(loanDetailModel.isLoanTypeSelectedForHistory==true){
                holder.radioLoanType.isChecked = true
                loanDetailModel.isLoanTypeSelectedForHistory = true
                loanDetailsModelList.set(position,loanDetailModel)
                for((i,model) in loanDetailsModelList.withIndex()){
                     if(i!=holder.adapterPosition){
                         model.isLoanTypeSelectedForHistory = false
                         loanDetailsModelList.set(i,model)
                     }
                }
            }else{
                holder.radioLoanType.isChecked = true
                loanDetailModel.isLoanTypeSelectedForHistory = true
                loanDetailsModelList.set(position,loanDetailModel)
                for((i,model) in loanDetailsModelList.withIndex()){
                    if(i!=holder.adapterPosition){
                        model.isLoanTypeSelectedForHistory = false
                        loanDetailsModelList.set(i,model)
                    }
                }
            }
            notifyDataSetChanged()
        })

    }

    override fun getItemCount(): Int {
        // this method is used for showing number of card items in recycler view.
        return loanDetailsModelList.size
    }

    // View holder class for initializing of your views such as TextView and Imageview.
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val radioLoanType: RadioButton


        init {
            radioLoanType = itemView.findViewById(R.id.radioLoanType)
//            tvMarkRed = itemView.findViewById(R.id.tvMarkRed)

        }
    }

    @JvmName("getLoanDetailsModelList1")
    fun getLoanDetailsModelList(): MutableList<LoanDetailModel> {
        return this.loanDetailsModelList
    }
    // Constructor
    init {
        this.loanDetailsModelList = loanDetailsModelList
    }
}