package com.apacfin.apacm.presentation.payment

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.activity.viewModels
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.util.*
import com.apacfin.data.model.payment.PaymentApiKeyResponse
import com.apacfin.data.model.payment.TransactionListResponse
import com.apacfin.data.model.payment.TransactionModel
import com.apacfin.data.model.payment.VpaDetailsModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputLayout
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.no_data_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.json.JSONObject
import timber.log.Timber

@AndroidEntryPoint
class PaymentActivity : AppCompatActivity(), PaymentResultWithDataListener,
    PaymentTransactionAdapter.OnClickListener,
    PaymentTransactionAdapter.OnPaymentSheduleClickListener,
    LocationListener {
    val TAG: String = PaymentActivity::class.toString()
    private val viewModel: PaymentViewModel by viewModels()
    var loading = false
    var apiKey = ""
    var orderId = ""
    var paymentId = ""
    var signatureId = ""
    var emiAmt = ""
    var emiStatic = ""
    var dueAmt = ""
    var emiCharges = ""
    var amt = ""
    var accountNo = ""
    var aCHolderName = ""
    var ifsc = ""
    var branch = ""
    var loanID = ""
    var description = ""
    var appPreferences: AppPreferences? = null
    lateinit var paymentApiKeyResponse: PaymentApiKeyResponse
    lateinit var transationList: List<TransactionModel>
    var adapter: PaymentTransactionAdapter? = null
    lateinit var transactionListResponse: TransactionListResponse
    lateinit var edtOtherAmt : EditText
    var paymentOptionType = ""
    private var lastClickTime:Long = 0
    private var lastClickTimePay:Long = 0
    private lateinit var locationManager: LocationManager

    var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(s: Editable) {
            if (!s.toString().isEmpty()) {
                edtOtherAmt.removeTextChangedListener(this)
                commaSepratedValueWithnumberToText(s.toString(),edtOtherAmt)
                edtOtherAmt.addTextChangedListener(this)
                Log.d(TAG, "afterTextChanged: $s")


                val amountAdd2000 = dueAmt.toInt() + 2000

                if (s.toString().replace(",", "").toInt() > amountAdd2000) {
                    edtOtherAmt.setText(amountAdd2000.toString())
                    showInternetConnectionLost(
                        this@PaymentActivity,
                        "Please Enter Amount less than or Equals to " + " " + amountFormater(
                            amountAdd2000.toDouble()
                        ), getString(R.string.ok)
                    )
                }
            }
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        initView()

        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this@PaymentActivity, {
            loading = it

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
            }
        })

        //-- Handle getPaymentApiKey response
        viewModel.getPaymentApiKey.observe(this@PaymentActivity, {
            paymentApiKeyResponse = it
            //Timber.e("getPaymentApiKey " + paymentApiKeyResponse)

            if (paymentApiKeyResponse.status) {
                apiKey = paymentApiKeyResponse.keyId
                getEmiOptionsAmt(loanID)
            } else {
                showInternetConnectionLost(
                    this,
                    paymentApiKeyResponse.msg,
                    getString(R.string.ok)
                )
            }
        })

        //-- Handle getEmiOptionsAmt response
        viewModel.getEmiOptionsAmt.observe(this@PaymentActivity, {
            paymentApiKeyResponse = it
            //Timber.e("createOrderId " + paymentApiKeyResponse)

            if (paymentApiKeyResponse.status) {
                emiAmt = paymentApiKeyResponse.emiAmount
                emiStatic = paymentApiKeyResponse.emiStatic
                dueAmt = paymentApiKeyResponse.emiWithCharges
                emiCharges = paymentApiKeyResponse.charges

                if (paymentApiKeyResponse.vpaAccountDetails != null) {
                    accountNo = paymentApiKeyResponse.vpaAccountDetails.accountNo
                    aCHolderName = paymentApiKeyResponse.vpaAccountDetails.accountHolderName
                    ifsc = paymentApiKeyResponse.vpaAccountDetails.ifscCode
                    branch = paymentApiKeyResponse.vpaAccountDetails.branchName
                }

                initBottomsheetForPayment(paymentApiKeyResponse.vpaAccountDetails)

            } else {
                showInternetConnectionLost(
                    this,
                    paymentApiKeyResponse.msg,
                    getString(R.string.ok)
                )
            }

        })



        //-- Handle createOrderId response
        viewModel.createOrderId.observe(this, {
           // if (this@PaymentActivity.lifecycle.currentState == Lifecycle.State.RESUMED) {
                paymentApiKeyResponse = it

                // Timber.e("createOrderId " + paymentApiKeyResponse)

                if (paymentApiKeyResponse.status) {
                    orderId = paymentApiKeyResponse.orderId
                    startPayment(orderId)
                } else {
                    showInternetConnectionLost(
                        this,
                        paymentApiKeyResponse.msg,
                        getString(R.string.ok)
                    )
                }
           // }
        })
        //-- Handle getupdatePaymentStatus response
        viewModel.getupdatePaymentStatus.observe(this@PaymentActivity, {
            paymentApiKeyResponse = it
            // Timber.e("getupdatePaymentStatus " + paymentApiKeyResponse)

            if (paymentApiKeyResponse.status) {
                paymentTransactionList()
            } else {
                showInternetConnectionLost(
                    this,
                    paymentApiKeyResponse.msg,
                    getString(R.string.ok)
                )
            }
        })

        //-- Handle paymentTransactionList response
        viewModel.paymentTransactionList.observe(this@PaymentActivity, {
            transactionListResponse = it
            // Timber.e("transactionListResponse " + transactionListResponse)

            if (transactionListResponse.status) {
                transationList = transactionListResponse.transactionList
                if (transationList.size > 0) {
                    setAdapter(transationList)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }
            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                    this,
                    transactionListResponse.msg,
                    getString(R.string.ok)
                )
            }
        })
        /*
        * To ensure faster loading of the Checkout form,
        * call this method as early as possible in your checkout flow
        * */
        Checkout.preload(applicationContext)

    }

    private fun initView() {
        loanID = intent.getStringExtra(Constants.LoanID).toString()

        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_baseline_arrow_back_24)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.title = getString(R.string.payment)
        appPreferences = AppPreferences.getAppPreferences(this)
        rcyclerViewTransaction!!.layoutManager = LinearLayoutManager(this)

        rcyclerViewTransaction.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                Log.d("scroll", "scrolling")
                if (dy > 0 && btnPayNowBottom.getVisibility() == View.VISIBLE) {
                    btnPayNowBottom.visibility = View.INVISIBLE
                } else if (dy < 0 && btnPayNowBottom.getVisibility() != View.VISIBLE) {
                    btnPayNowBottom.visibility = View.VISIBLE
                }
            }
        })
        setVisibility(View.GONE, View.GONE)

        btnPayNowBottom. setOnClickListener {
            if (SystemClock.elapsedRealtime() - lastClickTimePay > 5000){
                lastClickTimePay = SystemClock.elapsedRealtime();
                getEmiOptionsAmt(loanID)
            }
        }
        getPaymentApiKey()
        paymentTransactionList()
    }

    private fun getPaymentApiKey() {
        if (isNetworkAvailable(this)) {
            viewModel.getPaymentApiKey(this)
        }
    }

    private fun getEmiOptionsAmt(id: String) {
        if (isNetworkAvailable(this)) {
            viewModel.getEmiOptionsAmt(this, id)
        }
    }

    private fun createOrderId(amt: String) {
        if (isNetworkAvailable(this)) {

            //Timber.e("amtInPaisa1 : " + amt.toInt())
            //Timber.e("amtInPaisa2 : " + (amt.toInt() * 100))

            val amtInPaisa = (amt.toInt() * 100)
            //Timber.e("amtInPaisa : " + amtInPaisa)
            val params: MutableMap<String, String> = HashMap()
            params["loanID"] = loanID
            params["emiAmount"] = amtInPaisa.toString()
            params["paymentOptionType"] = paymentOptionType
            viewModel.createOrderId(this, params)
        }
    }



    private fun updatePaymentStatus() {
        if (isNetworkAvailable(this)) {
            Timber.e("updatePaymentStatus paymentId " + paymentId + ", orderId + " + orderId + ", signatureId " + signatureId)
            val params: MutableMap<String, String> = HashMap()
            params["paymentId"] = paymentId
            params["orderId"] = orderId
            params["signatureId"] = signatureId
            viewModel.updatePaymentStatus(this, params)
        }
    }

    private fun paymentTransactionList() {
        if (isNetworkAvailable(this)) {
            viewModel.paymentTransactionList(this)
        }
    }

    private fun setAdapter(loanDetailsList: List<TransactionModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        adapter = PaymentTransactionAdapter(
            this,
            loanDetailsList,
            this, this
        )
        rcyclerViewTransaction!!.adapter = adapter
        adapter!!.notifyDataSetChanged()

    }

    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewTransaction.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData
    }

    private fun initBottomsheetForPayment(vpaAccountDetails: VpaDetailsModel) {

        val modalbottomsheet: View =
            layoutInflater.inflate(R.layout.payment_type_radio_group, null)

        val bottomShitDialog = BottomSheetDialog(this@PaymentActivity)
        bottomShitDialog.setContentView(modalbottomsheet)
        bottomShitDialog.setCanceledOnTouchOutside(false)
        bottomShitDialog.setCancelable(false)
        bottomShitDialog.show()

        val radioGroup = modalbottomsheet.findViewById<RadioGroup>(R.id.payment_type_radio_group)
        val rdbtn_one_month_emi: RadioButton
        val rdbtn_due_charges: RadioButton
        val rdbtn_other_amt: RadioButton
        rdbtn_one_month_emi = modalbottomsheet.findViewById(R.id.rdbtn_one_month_emi)
        rdbtn_due_charges = modalbottomsheet.findViewById(R.id.rdbtn_due_charges)
        rdbtn_other_amt = modalbottomsheet.findViewById(R.id.rdbtn_other_amt)
        val tvTotalAmountCharges= modalbottomsheet.findViewById<TextView>(R.id.tvTotalAmountCharges)
        val tfOtherAmt = modalbottomsheet.findViewById<TextInputLayout>(R.id.tfOtherAmt)

        rdbtn_one_month_emi.text =
            " " + getString(R.string.emi_due) + " " + amountFormater(emiStatic.toDouble())
        rdbtn_due_charges.text =
            " " + getString(R.string.total_payment_due) + " " +amountFormater(dueAmt.toDouble())
        rdbtn_one_month_emi.tag = "emiStatic"
        rdbtn_due_charges.tag = "dueAmt"
        rdbtn_other_amt.tag = "otherAmt"


        radioGroup.setOnCheckedChangeListener { group: RadioGroup?, checkedId: Int ->
            if (rdbtn_one_month_emi.isChecked) {
                description = getString(R.string.emi_due)
                tfOtherAmt.visibility = View.GONE
                tvTotalAmountCharges.visibility=View.GONE

            }
            if (rdbtn_due_charges.isChecked) {
                description = getString(R.string.total_payment_due)
                tfOtherAmt.visibility = View.GONE
                tvTotalAmountCharges.visibility=View.VISIBLE
            }
            if (rdbtn_other_amt.isChecked) {
                description = getString(R.string.other_amount)
                tfOtherAmt.visibility = View.VISIBLE
                tvTotalAmountCharges.visibility=View.GONE
            }
        }
        val btnPayNow = modalbottomsheet.findViewById<Button>(R.id.btnPayNow)
        val btnCancel = modalbottomsheet.findViewById<Button>(R.id.btnCancel)
        val txtSubTitle = modalbottomsheet.findViewById<TextView>(R.id.txtSubTitle)
        val txtNeftRtgs = modalbottomsheet.findViewById<TextView>(R.id.txtNeftRtgs)
        val txtAccountNo = modalbottomsheet.findViewById<TextView>(R.id.txtAccountNo)
        val txtACHolderName = modalbottomsheet.findViewById<TextView>(R.id.txtACHolderName)
        val txtIfsc = modalbottomsheet.findViewById<TextView>(R.id.txtIfsc)
        val txtBranch = modalbottomsheet.findViewById<TextView>(R.id.txtBranch)
        val ivNeftRtgsClose = modalbottomsheet.findViewById<ImageView>(R.id.ivNeftRtgsClose)
        val cardNeftRtgs = modalbottomsheet.findViewById<CardView>(R.id.cardNeftRtgs)

        edtOtherAmt = modalbottomsheet.findViewById<EditText>(R.id.edtOtherAmt)
        edtOtherAmt.addTextChangedListener(textWatcher)
        txtSubTitle.text = getString(R.string.pay_for)+ " "+loanID
        tvTotalAmountCharges.text=""+getString(R.string.emi_total)+" "  + amountFormater(emiAmt.toDouble())  +"  &  "  + getString(R.string.emi_charges)+ " " +amountFormater(emiCharges.toDouble())

        txtAccountNo.text ="" + accountNo
        txtACHolderName.text ="" + aCHolderName
        txtIfsc.text ="" + ifsc
        txtBranch.text ="" + branch

        btnCancel.setOnClickListener {
            bottomShitDialog.cancel()
        }
        txtNeftRtgs.setOnClickListener { v: View? ->
            if (cardNeftRtgs.visibility == View.VISIBLE) {
                cardNeftRtgs.visibility = View.GONE
            } else {
                cardNeftRtgs.visibility = View.VISIBLE
            }
        }

        ivNeftRtgsClose.setOnClickListener { v: View? ->
            cardNeftRtgs.visibility = View.GONE
        }
        btnPayNow.setOnClickListener { v: View? ->

            if (SystemClock.elapsedRealtime() - lastClickTime > 5000){
                lastClickTime = SystemClock.elapsedRealtime()

                val selectedId = radioGroup.checkedRadioButtonId
                // Timber.e("btnDone selectedId :   $selectedId")
                val radioButton = radioGroup.findViewById<RadioButton>(selectedId)

                if (selectedId == -1) {

                    showInternetConnectionLost(
                        this@PaymentActivity,
                        getString(R.string.select_payment_option),
                        getString(R.string.ok)
                    )
                } else {
                    //  Timber.e("btnDone : "+ radioButton.tag.toString() + "," + radioButton.text)

                    if (radioButton.tag.toString().equals("emiStatic")) {
                        amt = emiStatic
                        paymentOptionType = radioButton.tag.toString()
                    } else if (radioButton.tag.toString().equals("dueAmt")) {
                        amt = dueAmt
                        paymentOptionType = radioButton.tag.toString()
                    } else if (radioButton.tag.toString().equals("otherAmt")) {
                        // Timber.e("otherAmt otherAmt : "+edtOtherAmt.text.toString().trim().isNullOrEmpty())
                        if (!edtOtherAmt.text.toString().trim().isNullOrEmpty()) {
                            if (edtOtherAmt.text.toString().replace(",".toRegex(), "").toInt() < 100) {
                                amt=""
                                showInternetConnectionLost(
                                    this, getString(R.string.amt_min_msg)+" "+amountFormater("100".toDouble()),
                                    getString(R.string.ok)
                                )

                            } else {
                                amt = edtOtherAmt.text.toString().replace(",".toRegex(), "")
                                paymentOptionType = radioButton.tag.toString()
                            }
                        } else {
                            showInternetConnectionLost(
                                this,
                                getString(R.string.enter_amt),
                                getString(R.string.ok)
                            )
                        }
                    }

                    if (ContextCompat.checkSelfPermission(
                            this, Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        val manager =
                            getSystemService(Context.LOCATION_SERVICE) as LocationManager
                        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            alertMessageNoGps(this@PaymentActivity, "Yes")
                        } else {
                            var progressDialog = showProgressDialog(this@PaymentActivity)
                            progressDialog.show()
                            val appPreferences = AppPreferences(this@PaymentActivity)

                            object : AsyncTaskCoroutine<Int?, Boolean?>() {
                                override fun onPostExecute(@Nullable result: Boolean?) {
                                    progressDialog.dismiss()
                                    Log.d("Latitude Longitude", appPreferences?.getString(Constants.Latitude, "").toString())
                                    Log.d("Latitude Longitude", appPreferences?.getString(Constants.Longitude, "").toString())

                                    if (!amt.isNullOrEmpty()) {
                                        createOrderId(amt)
                                        bottomShitDialog.dismiss()
                                    }
                                }

                                override fun onPreExecute() {
                                    progressDialog.show()
                                    //getCurrentLatLong(requireActivity())
                                    getLocation();
                                }

                                override fun doInBackground(vararg params: Int?): Boolean? {
                                    return null
                                }
                            }.execute<Any>()

                        }
                    }else{
                        appPreferences?.putString(Constants.Latitude, "")
                        appPreferences?.putString(Constants.Longitude, "")

                        if (!amt.isNullOrEmpty()) {
                            createOrderId(amt)
                            bottomShitDialog.dismiss()
                        }
                    }

                }

            }


        }

    }


    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    private fun startPayment(orderId: String) {
        Timber.e(TAG + " startPayment orderId: " + orderId)
        /*
        *  You need to pass current activity in order to let Razorpay create CheckoutActivity
        * */
        val activity: Activity = this
        val co = Checkout()

        co.setKeyID(apiKey)
        try {
            val options = JSONObject()
            options.put("name", getString(R.string.apac_name))
            options.put("description", description)
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://apacfin.com/wp-content/uploads/2019/06/logo-1.jpg")
            options.put("theme.color", "#1A237E")
            options.put("currency", "INR")
            options.put("order_id", orderId)
            options.put("amount", amt)//pass amount in currency subunits

            val retryObj = JSONObject()
            retryObj.put("enabled", true)
            retryObj.put("max_count", 4)
            options.put("retry", retryObj)

            val prefill = JSONObject()
            //prefill.put("email","gaurav.kumar@example.com")
            prefill.put("contact", appPreferences!!.getString(Constants.MobileNo, ""))

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Timber.e(TAG + " Error in payment: " + e.message)
            e.printStackTrace()
        }
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?, paymentData: PaymentData) {
        try {
            Timber.e("Payment Successful: $razorpayPaymentId ${paymentData.data} ${paymentData.orderId} ${paymentData.paymentId} ")
            orderId = paymentData.orderId
            paymentId = paymentData.paymentId
            signatureId = paymentData.signature
            updatePaymentStatus()
            showAlert(1, orderId, getString(R.string.payment_Success))
        } catch (e: java.lang.Exception) {
            Timber.e(TAG, "Exception in onPaymentSuccess", e)
        }

    }

    override fun onPaymentError(errorCode: Int, response: String?, paymentData: PaymentData?) {
        try {
            Timber.e("Payment failed: $errorCode $response ${paymentData?.data} ${paymentData?.orderId} ${paymentData?.paymentId}")
            //orderId = paymentData!!.orderId
            paymentId = ""
            signatureId = ""
            updatePaymentStatus()
            showAlert(2, orderId, getString(R.string.payment_fail))
        } catch (e: java.lang.Exception) {
            Timber.e(TAG + " Exception in onPaymentError", e)
        }
    }

    override fun onLeadClick(position: Int) {

    }

    override fun onPaymentSheduleClickListener(position: Int) {
        orderId = transationList.get(position).orderId
        paymentId = ""
        signatureId = ""
        updatePaymentStatus()
    }

    private fun showAlert(status: Int, id: String, title: String) {
        playSound(this)
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_layout)
        val iv_payment_success = dialog.findViewById(R.id.iv_payment_success) as LottieAnimationView
        val tvPaymentMsg = dialog.findViewById(R.id.tvPaymentSuccess) as TextView
        val txttransID = dialog.findViewById(R.id.tvPaymentTransId) as TextView
        tvPaymentMsg.text = title
        txttransID.text = "" + id
        if (status == 1) {
            tvPaymentMsg.setTextColor(resources.getColor(R.color.green))
            iv_payment_success.setAnimation("succesfull_payment.json")

        } else if (status == 2) {
            tvPaymentMsg.setTextColor(resources.getColor(R.color.red))
            iv_payment_success.setAnimation("payment_failed.json")
        }
        val yesBtn = dialog.findViewById(R.id.btnDialogOkay) as Button
        //val noBtn = dialog.findViewById(R.id.noBtn) as TextView
        yesBtn.setOnClickListener {
            dialog.dismiss()
        }
        //  noBtn.setOnClickListener { dialog.dismiss() }
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels

        dialog.window?.setLayout(width - 80, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()

    }

    override fun onDestroy() {
        super.onDestroy()
       // viewModel.createOrderId.removeObservers(this@PaymentActivity)
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
    }

}