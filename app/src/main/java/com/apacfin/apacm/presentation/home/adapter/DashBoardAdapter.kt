package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.apacfin.apacm.R
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.presentation.document.ListOfEsignLinksAdapter
import com.apacfin.apacm.presentation.home.DashBoardModel
import com.apacfin.apacm.presentation.referral.ReferralDialogFragment
import com.apacfin.apacm.presentation.registrationprocess.newuser.NewUserDialogFragment
import com.apacfin.apacm.presentation.registrationprocess.otp.OtpDialogFragment
import com.apacfin.data.model.esign.ListOfEsignLinksModel
import kotlinx.android.synthetic.main.activity_registration.*

class DashBoardAdapter(context: Context,
    dashboardModelArrayList: List<DashBoardModel>,
    onDashBoardMenuListener: OnDashBoardMenuListener) : RecyclerView.Adapter<DashBoardAdapter.ViewHolder>() {
    var context: Context
    private lateinit var onDashBoardMenuListener: OnDashBoardMenuListener
    private var dashboardModelArrayList: List<DashBoardModel>

    init {
        this.dashboardModelArrayList = dashboardModelArrayList
        this.onDashBoardMenuListener = onDashBoardMenuListener
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_dashboard_details, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashBoardAdapter.ViewHolder, position: Int) {
        // to set data to textview and imageview of each card layout
        val model: DashBoardModel = dashboardModelArrayList[position]
        holder.tvGenerateVCard.setText(model.card_name)
        holder.img_generatedvcard.setImageResource(model.card_image)
        holder.cardBottNav.setOnClickListener {
            onDashBoardMenuListener.onDashBoardMenuListener(context,model)
        }

    }

    interface OnDashBoardMenuListener {
        fun onDashBoardMenuListener(context: Context,model: DashBoardModel)
    }

    override fun getItemCount(): Int {
        return dashboardModelArrayList.size
    }

    // View holder class for initializing of your views such as TextView and Imageview.
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img_generatedvcard: ImageView
        val tvGenerateVCard: TextView
        val cardBottNav: CardView

        init {
            tvGenerateVCard = itemView.findViewById(R.id.tvGenerateVCard)
            img_generatedvcard = itemView.findViewById(R.id.img_generatedvcard)
            cardBottNav = itemView.findViewById(R.id.cardBottNav)
        }
    }

}





