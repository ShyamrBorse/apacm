package com.apacfin.apacm.presentation.myloan

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.payment.PaymentActivity
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_my_loan.*
import kotlinx.android.synthetic.main.no_data_layout.*
import timber.log.Timber


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MyLoanFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class MyLoanFragment : Fragment(), MyLoanAdapter.OnClickListener,
    MyLoanAdapter.OnStatmentClickListener, MyLoanAdapter.OnPaymentSheduleClickListener {

    val TAG = MyLoanFragment::class.java.simpleName
    private val viewModel: LoanDetailsViewModel by viewModels()
    var loading = false
    lateinit var loanDetailsModelResponse: LoanDetailsModelResponse
    var appPreferences: AppPreferences? = null
    var adapter: MyLoanAdapter? = null
    lateinit var loanDetailsList: List<LoanDetailModel>
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_loan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner) {
            loading = it
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
            }
        }

        //-- Handle getLoanDetails response
        viewModel.getLoanDetails.observe(viewLifecycleOwner) {
            loanDetailsModelResponse = it
            Timber.e("loanDetailsModelResponse " + loanDetailsModelResponse)

            if (loanDetailsModelResponse.status) {
                loanDetailsList = loanDetailsModelResponse.loanDetails
                if (loanDetailsList.size > 0) {
                    setAdapter(loanDetailsList)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }
            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                    requireContext(),
                    loanDetailsModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        }

    }

    private fun setAdapter(loanDetailsList: List<LoanDetailModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        this.adapter = MyLoanAdapter(
            requireContext(),
            loanDetailsList,
            this, this, this
        )
        rcyclerViewMyLoan!!.adapter = adapter
        this.adapter!!.notifyDataSetChanged()

    }


    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        rcyclerViewMyLoan!!.layoutManager = LinearLayoutManager(requireContext())
        setVisibility(View.GONE, View.GONE)
        getLoanList()
    }

    private fun getLoanList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getLoanDetails(requireActivity())
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MyLoanFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MyLoanFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onLeadClick(position: Int) {

    }

    override fun onStatmentClickListener(position: Int) {
        startActivity(
            Intent(activity, PaymentActivity::class.java).putExtra(
                Constants.LoanID,
                loanDetailsList.get(position).loanId
            )
        )
    }

    override fun onPaymentSheduleClickListener(position: Int) {
        val bundle = Bundle()
        bundle.putString(Constants.LoanID, loanDetailsList.get(position).loanId)
        findNavController().navigate(R.id.frgStatement, bundle)
    }

    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewMyLoan.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData
    }
}