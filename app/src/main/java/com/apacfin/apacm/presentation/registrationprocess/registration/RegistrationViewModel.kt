package com.apacfin.apacm.presentation.registrationprocess.registration

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.apacfin.apacm.R
import com.apacfin.data.model.profile.AppDetailModel
import com.apacfin.data.model.registration.GetDetailsFromUCIC
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.RegistrationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : HomeViewModel.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@HiltViewModel
class RegistrationViewModel @Inject constructor(
    private val repository: RegistrationRepository
) : ViewModel() {

    val _getRegistrationResponse: MutableLiveData<RegistrationModel> = MutableLiveData()
    val _getmobileVerification: MutableLiveData<RegistrationModel> = MutableLiveData()
    val _setMpinResponse: MutableLiveData<RegistrationModel> = MutableLiveData()
    val _mPinLoginResponse: MutableLiveData<RegistrationModel> = MutableLiveData()
    val _newUser: MutableLiveData<RegistrationModel> = MutableLiveData()
    val _getAppDetailsResponse: MutableLiveData<AppDetailModel> = MutableLiveData()
    val _updateUserTokenResponse: MutableLiveData<RegistrationModel> = MutableLiveData()

    val _getDetailsFromUCICResponse: MutableLiveData<GetDetailsFromUCIC> = MutableLiveData()
    val _getUCICResponse: MutableLiveData<GetDetailsFromUCIC> = MutableLiveData()
    val _getOTPResponse: MutableLiveData<GetDetailsFromUCIC> = MutableLiveData()
    val _validateOTPResponse: MutableLiveData<GetDetailsFromUCIC> = MutableLiveData()
    val _validateOTPnSendUCICResponse: MutableLiveData<GetDetailsFromUCIC> = MutableLiveData()


    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //getRegistration responce
    val getRegistration: LiveData<RegistrationModel> get() = _getRegistrationResponse

    //getRegistration responce
    val getmobileVerification: LiveData<RegistrationModel> get() = _getmobileVerification

    //validateOTPnSendUCIC responce
    val setMpinResponse: LiveData<RegistrationModel> get() = _setMpinResponse

    //GetDetailsFromUCIC responce
    val getDetailsFromUCICResponse: LiveData<GetDetailsFromUCIC> get() = _getDetailsFromUCICResponse

    //getUCIC responce
    val getUCICResponse: LiveData<GetDetailsFromUCIC> get() = _getUCICResponse

    //getOTP responce
    val getOTPResponse: LiveData<GetDetailsFromUCIC> get() = _getOTPResponse

    //validateOTP responce
    val validateOTPResponse: LiveData<GetDetailsFromUCIC> get() = _validateOTPResponse

    //validateOTPnSendUCIC responce
    val validateOTPnSendUCICResponse: LiveData<GetDetailsFromUCIC> get() = _validateOTPnSendUCICResponse



    //mPinLoginResponse responce
    val mPinLoginResponse: LiveData<RegistrationModel> get() = _mPinLoginResponse

    //validateOTPnSendUCIC responce
    val newUser: LiveData<RegistrationModel> get() = _newUser

    //getAppDetails responce
    val getAppDetailsResponse: LiveData<AppDetailModel> get() = _getAppDetailsResponse

//updateUserTokenResponse responce
    val updateUserTokenResponse: LiveData<RegistrationModel> get() = _updateUserTokenResponse


    //getRegistration api
    fun getRegistration(context:Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getRegistration(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse"  + response.value.msg)
                _getRegistrationResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___validateOTP"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf() ,context.getString(R.string.tecnical_error),
                    false)
                _getRegistrationResponse.postValue(registrationModel)
            }
        }

    }

    //mobileVerification api
    fun mobileVerification(context:Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.mobileVerification(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___mobileVerification"  + response.value.msg)
                _getmobileVerification.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___validateOTP" + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf() ,context.getString(R.string.tecnical_error),
                    false)
                _getmobileVerification.postValue(registrationModel)
            }
        }
    }

    //getDetailsFromUCIC api
    fun getDetailsFromUCIC(params: String) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getDetailsFromUCIC(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse"  + response.value.msg)
                _getDetailsFromUCICResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse"  + response.errorBody)

            }
        }

    }

    //getUCIC api
    fun getUCIC(params: String) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getUCIC(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___getUCICResponse"  + response.value.msg)
                _getUCICResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___getUCICResponse"  + response.errorBody)

            }
        }

    }

    //getOTP api
    fun getOTP(params: String) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getOTP(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___getOTPResponse"  + response.value.msg)
                _getOTPResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___getOTPResponse"  + response.errorBody)

            }
        }

    }

    //validateOTP api
    fun validateOTP(params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.validateOTP(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___validateOTP"  + response.value.msg)
                _validateOTPResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___validateOTP"  + response.errorBody)

            }
        }
    }

    //validateOTPnSendUCIC api
    fun validateOTPnSendUCIC(params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.validateOTPnSendUCIC(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___validateOTPnSendUCIC" + response.value.msg)
                _validateOTPnSendUCICResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___validateOTPnSendUCIC"  + response.errorBody)

            }
        }

    }

    //setMpin api
    fun setMpin(context: Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.setMpin(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___setMpin"  + response.value.msg)
                _setMpinResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___setMpin"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf() ,context.getString(R.string.tecnical_error),
                    false)
                _setMpinResponse.postValue(registrationModel)
            }
        }

    }

    //mPinLogin api
    fun mPinLogin(context: Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.mPinLogin(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("___mPinLogin"  + response.value.msg)
                _mPinLoginResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___setMpin"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf() ,context.getString(R.string.tecnical_error),
                    false)
                _mPinLoginResponse.postValue(registrationModel)
            }
        }

    }

    //newUser api
    fun newUser(context: Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.newUser(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("newUser" + response.value.msg)
                _newUser.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___setMpin"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf() ,context.getString(R.string.tecnical_error),
                    false)
                _newUser.postValue(registrationModel)
            }
        }

    }
    //getAppDetails api
    fun getAppDetails(context: Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        Timber.e("getAppDetails: "+params)
        when (val response = repository.getAppDetails(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)

                _getAppDetailsResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___setMpin"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var appDetailModel =  AppDetailModel(false,
                    "",context.getString(R.string.tecnical_error),false)
                _getAppDetailsResponse.postValue(appDetailModel)
            }
        }

    }

    //updateUserToken api
    fun updateUserToken(context: Context,params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.updateUserToken(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)

                _updateUserTokenResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("updateUserTokenRespo"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var registrationModel =  RegistrationModel("", "", "", "", "", false,
                    "", arrayListOf() ,context.getString(R.string.tecnical_error),
                    false)
                _updateUserTokenResponse.postValue(registrationModel)
        }
    }

    }

}