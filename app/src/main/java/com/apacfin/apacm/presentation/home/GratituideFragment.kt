package com.apacfin.apacm.presentation.gratitude

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.apacfin.apacm.R
import com.apacfin.apacm.util.GlideApp
import kotlinx.android.synthetic.main.fragement_gratitude_dialog.*
import kotlinx.android.synthetic.main.slider_item.view.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
class GratituideFragment : DialogFragment(),View.OnClickListener {

    private var param1: String? = null
    private var param2: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


    }

    override fun onClick(view: View?) {

        when (view?.getId()) {
            R.id.btn_close_dialog -> dialog?.dismiss()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(true)
        }
        return inflater.inflate(R.layout.fragement_gratitude_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        //dialog?.window?.setLayout(width - 80, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog?.window?.setLayout(width - 80, height-250)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }
    private fun initView() {

        btn_close_dialog.setOnClickListener(this)
        iv_gratitude.setOnClickListener(this)

        GlideApp.with(iv_gratitude).load(param1).error(R.drawable.banner).into(iv_gratitude);

    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireActivity(), theme) {
            override fun onBackPressed() {
                // On backpress, do your stuff here.
                dialog?.dismiss()
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment GratituideFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            GratituideFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}








