package com.apacfin.apacm.presentation.language

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.profile.ProfileViewModel
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.ui.SplashActivity
import com.apacfin.apacm.util.*
import com.apacfin.data.model.language.LanguageModel
import com.apacfin.data.model.language.LanguageModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.apacfin.data.prefrerce.Constants.fromProfile
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_language.*
import kotlinx.android.synthetic.main.no_data_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber
import java.util.*

@AndroidEntryPoint
class LanguageActivity : AppCompatActivity(), View.OnClickListener,
    LanguageAdapter.OnClickListener,
    LocationListener {
    val TAG = LanguageActivity::class.java.simpleName
    private val viewModel: ProfileViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    lateinit var languageModelResponse: LanguageModelResponse
    var adapter: LanguageAdapter? = null
    private var mLanguageCode = ""
    var appPreferences: AppPreferences? = null
    var isLogin = false
    var fromView = 0

    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)

        initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, {
            loading = it;
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()

                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })
        //-- Handle getRegistration response

        viewModel.setLangForUser.observe(this, {
            if (this.lifecycle.currentState == Lifecycle.State.RESUMED) {
                registrationModel = it
                Timber.e("setLangForUser " + registrationModel)
                if (registrationModel.status) {
                    setLanguageFinal()
                } else {
                    showInternetConnectionLost(
                        this,
                        registrationModel.msg,
                        getString(R.string.ok)
                    )
                }
            }
        })

        //-- Handle getListLang response
        viewModel.getListLang.observe(this, {
            languageModelResponse = it
            Timber.e("languageModel " + languageModelResponse)

            if (languageModelResponse.status) {
                if (languageModelResponse.languages.size > 0) {
                    setAdapter(languageModelResponse.languages)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }
            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                    this,
                    languageModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })
    }

    fun setAdapter(languages: List<LanguageModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        adapter = LanguageAdapter(
            this,
            languages,
            this@LanguageActivity
        )
        rcyclerViewLanguage!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun initView() {
        toolbar.title = resources.getString(R.string.select_language)
        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_baseline_arrow_back_24)
        appPreferences = AppPreferences.getAppPreferences(this@LanguageActivity)
        fromView = intent.getIntExtra(Constants.FromView, 0)
        //  isLogin = appPreferences.getBoolean(Constants.IsLogin, false)
        rcyclerViewLanguage!!.layoutManager = LinearLayoutManager(this)
        setVisibility(View.GONE, View.GONE)
        btnContinue!!.setOnClickListener(this)
        btnCloseLang!!.setOnClickListener(this)
        if (isNetworkAvailable(this)) {
            viewModel.getListLang(this)
        }
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnContinue -> setLanguage()
            R.id.btnCloseLang -> finish()
        }
    }

//    override fun attachBaseContext(base: Context) {
//        super.attachBaseContext(LocaleHelper.onAttach(base))
//    }

    private fun setLanguage() {

        if (ContextCompat.checkSelfPermission(
                this@LanguageActivity, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val manager =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                alertMessageNoGps(this@LanguageActivity, "Yes")
            } else {
                var progressDialog = showProgressDialog(this@LanguageActivity)

                object : AsyncTaskCoroutine<Int?, Boolean?>() {
                    override fun onPostExecute(@Nullable result: Boolean?) {
                        progressDialog.dismiss()

                        if (isNetworkAvailable(this@LanguageActivity)) {
                            if (LanguageAdapter.selectedPosition === -1) {
                                showInternetConnectionLost(
                                    this@LanguageActivity,
                                    getString(R.string.select_language),
                                    getString(R.string.ok)
                                )

                            } else {
                                mLanguageCode =
                                    LanguageAdapter.languageModels[LanguageAdapter.selectedPosition].code
                                Timber.e("setLanguage: $mLanguageCode")
                                if (fromView == fromProfile) {
                                    val params: MutableMap<String, String> = HashMap()
                                    params["langID"] = mLanguageCode
                                    viewModel.setLangForUser(this@LanguageActivity, params)
                                } else {
                                    setLanguageFinal()
                                }
                            }
                        }
                    }

                    override fun onPreExecute() {
                        progressDialog.show()
                        //getCurrentLatLong(requireActivity())
                        getLocation();
                    }

                    override fun doInBackground(vararg params: Int?): Boolean? {
                        return null
                    }
                }.execute<Any>()

            }
        } else {
            appPreferences?.putString(Constants.Latitude, "")
            appPreferences?.putString(Constants.Longitude, "")

            if (isNetworkAvailable(this)) {
                if (LanguageAdapter.selectedPosition === -1) {
                    showInternetConnectionLost(
                        this,
                        getString(R.string.select_language),
                        getString(R.string.ok)
                    )

                } else {
                    mLanguageCode =
                        LanguageAdapter.languageModels[LanguageAdapter.selectedPosition].code
                    Timber.e("setLanguage: $mLanguageCode")
                    if (fromView == fromProfile) {
                        val params: MutableMap<String, String> = HashMap()
                        params["langID"] = mLanguageCode
                        viewModel.setLangForUser(this, params)
                    } else {
                        setLanguageFinal()
                    }
                }
            }
        }
        /*val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            alertMessageNoGps(this@LanguageActivity, "Yes")
        } else {
            getCurrentLatLong(this@LanguageActivity)
            if (isNetworkAvailable(this)) {
                if (LanguageAdapter.selectedPosition === -1) {
                    showInternetConnectionLost(
                        this,
                        getString(R.string.select_language),
                        getString(R.string.ok)
                    )

                } else {
                    mLanguageCode =
                        LanguageAdapter.languageModels[LanguageAdapter.selectedPosition].code
                    Timber.e("setLanguage: $mLanguageCode")
                    if (fromView == fromProfile) {
                        val params: MutableMap<String, String> = HashMap()
                        params["langID"] = mLanguageCode
                        viewModel.setLangForUser(this, params)
                    } else {
                        setLanguageFinal()
                    }
                }
            }
        }*/
    }

    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
        // tvGpsLocation = findViewById(R.id.textView)
        //tvGpsLocation.text = "Latitude: " + location.latitude + " , Longitude: " + location.longitude
    }
    private fun setLanguageFinal() {

        LocaleHelper.setLocale(this@LanguageActivity, mLanguageCode)
        // appPreferences?.putBoolean(Constants.IsLanguageSet, true)
        appPreferences?.putString(Constants.SetLanguage, mLanguageCode)
        recreate()
        startActivity(
            Intent(
                this,
                SplashActivity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        )
        finish()
    }

    override fun onLeadClick(position: Int) {}

    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewLanguage.visibility = isVisibleRecy
        lnyBottomLang.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData

    }

    companion object {
        private val TAG = LanguageActivity::class.java.simpleName
    }
}