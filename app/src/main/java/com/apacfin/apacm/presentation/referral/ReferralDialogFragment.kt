package com.apacfin.apacm.presentation.referral

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.profile.ProfileViewModel
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.util.*
import com.apacfin.data.model.referal.LoanTypeModel
import com.apacfin.data.model.referal.LoanTypeModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_referral_dialog.*
import timber.log.Timber

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ReferralDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ReferralDialogFragment : DialogFragment(), View.OnClickListener,
    AdapterView.OnItemSelectedListener,
    LocationListener {


    val TAG = ReferralDialogFragment::class.java.simpleName
    private val viewModel: ProfileViewModel by viewModels()
    var loading = false
    lateinit var loanTypeModelResponse: LoanTypeModelResponse
    lateinit var registrationModel: RegistrationModel
    private var param1: String? = null
    private var param2: String? = null

    // var appPreferences: AppPreferences? = null
    var loanType: String? = null
    lateinit var loanTypeList: List<LoanTypeModel>
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2
    var appPreferences: AppPreferences? = null

    var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(s: Editable) {
            if (!s.toString().isEmpty()) {
                edtLoanAmount.removeTextChangedListener(this)
                commaSepratedValueWithnumberToText(s.toString(), edtLoanAmount)
                edtLoanAmount.addTextChangedListener(this)
                Log.d(TAG, "afterTextChanged: $s")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
        }
// Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_referral_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appPreferences = AppPreferences.getAppPreferences(requireActivity())
        initView()

//-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(this, Observer {
            loading = it;
            Timber.e( "SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
// showProgressDialog(this,false)
// Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

//-- Handle saveReferral response
        viewModel.saveReferralResponse.observe(this, {
            registrationModel = it
            Timber.e("saveReferralResponse " + registrationModel)

            if (registrationModel.status) {
                showAlertMessageSingleClick(
                    requireContext(),
                    getString(R.string.ok),
                    registrationModel.msg
                ) { alertDialog, which ->
                    alertDialog.dismiss()
                    dialog?.dismiss()
                }
            } else {
                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
// showSnackBar(this, getDetailsFromUCIC.msg) }
//startActivity(Intent(this, MainActivity::class.java))
            }
        })

//-- Handle getLoanType response
        viewModel.getLoanTypeResponse.observe(viewLifecycleOwner, {
            loanTypeModelResponse = it
            Timber.e("loanTypeModelResponse " + loanTypeModelResponse)

            if (loanTypeModelResponse.status) {

                loanTypeList = loanTypeModelResponse.loanType
                setSpinnerData(loanTypeList)
            } else {
                showInternetConnectionLost(
                    requireContext(),
                    loanTypeModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })

    }
    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout(width - 80, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
    private fun initView() {
// appPreferences = AppPreferences.getAppPreferences(requireContext())
        tvCustCareEmailRef.text = Constants.ContactUsEmail
        btnCloseRefDialog.setOnClickListener(this)
        btnSubmitReferal.setOnClickListener(this)
        edtLoanAmount.addTextChangedListener(textWatcher)
// getLoanTypeList()
    }

    private fun getLoanTypeList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getLoanType(requireActivity())

        }
    }
    private fun setSpinnerData(loanDetailsList: List<LoanTypeModel>) {
        loanType = loanTypeList[0].code
        var lonList : ArrayList<String?> = arrayListOf()
        for(item in loanDetailsList){
            lonList.add(item.codeValueName)
        }

        spLoanType!!.setOnItemSelectedListener(this)

// Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, lonList)
// Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        with(spLoanType)
        {
            adapter = aa
            setSelection(0, false)
            onItemSelectedListener = this@ReferralDialogFragment
            prompt = context.getString(R.string.select_loan)
            setPopupBackgroundResource(R.color.material_grey_600)

        }


    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireActivity(), theme) {
            override fun onBackPressed() {
// On backpress, do your stuff here.
                dialog?.dismiss()
            }
        }
    }
    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnSubmitReferal -> validation()
            R.id.btnCloseRefDialog -> dialog?.dismiss()

        }
    }
    private fun validation() {
        var amt = edtLoanAmount.text.toString().replace(",".toRegex(), "")


        if (ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val manager =
                requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                alertMessageNoGps(requireActivity(), "Yes")
            } else {
                var progressDialog = showProgressDialog(requireActivity())


                object : AsyncTaskCoroutine<Int?, Boolean?>() {
                    override fun onPostExecute(@Nullable result: Boolean?) {
                        progressDialog.dismiss()

                        if (isNetworkAvailable(requireContext())) {
                            if (isEmpty(edtReferredName)) {
                                tfReferredName.error = resources.getString(R.string.er_referred_name)
                            } else if (isValidPhoneNumber(edtMobileNo) == false) {
                                tfMobileNo.error = resources.getString(R.string.er_enter_mobile)
                                tfReferredName.error = null
                            } else if ((!isEmpty(edtAlternetMobileNo)) && (isValidPhoneNumber(
                                    edtAlternetMobileNo
                                ) == false)
                            ) {
                                tfAlternetMobileNo.error = resources.getString(R.string.er_enter_mobile)
                                tfReferredName.error = null
                                tfMobileNo.error = null
                            }/*else if (loanType.toString().isNullOrEmpty()) {
showToast(requireContext(),resources.getString(R.string.er_type_of_loan))
}*/ else if (isEmpty(edtLoanAmount)) {
                                tfLoanAmount.error = resources.getString(R.string.er_loan_amount)
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                            } else if (amt.toDouble() < 350000) {

                                tfLoanAmount.error = resources.getString(R.string.er_less_loan_amount) + " " + resources.getString(
                                    R.string.Rs
                                ) + "3,49,999"
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                            } else if (isEmpty(edtNatureOfBusiness)) {
                                tfNatureOfBusiness.error = resources.getString(R.string.er_natureOfBusiness)
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                                tfLoanAmount.error = null
                            } else if (edtNatureOfBusiness.text.toString().length <= 6) {
                                tfNatureOfBusiness.error = resources.getString(R.string.er_validnatureOfBusiness)
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                                tfLoanAmount.error = null
                            } else if (isEmpty(edtAddress)) {
                                edtAddress.error = resources.getString(R.string.address)
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                                tfLoanAmount.error = null
                                tfNatureOfBusiness.error = null
                                edtNatureOfBusiness.error = null
                            } else if (isEmpty(edtPincode)) {
                                tfPincode.error = resources.getString(R.string.er_enter_pincode)
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                                tfLoanAmount.error = null
                                edtNatureOfBusiness.error = null
                                tfNatureOfBusiness.error = null
                                tfAddress.error = null
                                edtAddress.error = null
                            } else if (edtPincode.text.toString().length != 6) {
                                tfPincode.error = resources.getString(R.string.er_pincode)
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                                tfLoanAmount.error = null
                                edtNatureOfBusiness.error = null
                                tfNatureOfBusiness.error = null
                                tfAddress.error = null
                                edtAddress.error = null
                            } else {
                                tfReferredName.error = null
                                tfMobileNo.error = null
                                tfAlternetMobileNo.error = null
                                tfLoanAmount.error = null
                                edtNatureOfBusiness.error = null
                                edtAddress.error = null
                                tfAddress.error = null
                                tfPincode.error = null

                                val params: MutableMap<String, String> = HashMap()
                                params["refName"] = edtReferredName.text.toString()
                                params["refMobile"] = edtMobileNo.text.toString()
                                params["loanType"] = loanType.toString()
                                params["loanAmt"] =
                                    edtLoanAmount.text.toString().replace(",".toRegex(), "")
                                params["address"] = edtAddress.text.toString()
                                params["pincode"] = edtPincode.text.toString()
                                params["natureOfBusiness"] = edtNatureOfBusiness.text.toString()
                                params["alternateMobile"] = edtAlternetMobileNo.text.toString()

                                viewModel.saveReferral(requireActivity(), params)

                            }
                        }
                    }

                    override fun onPreExecute() {
                        progressDialog.show()
                        //getCurrentLatLong(requireActivity())
                        getLocation()
                    }

                    override fun doInBackground(vararg params: Int?): Boolean? {
                        return null
                    }
                }.execute<Any>()
            }
        } else {
            appPreferences?.putString(Constants.Latitude, "")
            appPreferences?.putString(Constants.Longitude, "")

            if (isNetworkAvailable(requireContext())) {
                if (isEmpty(edtReferredName)) {
                    tfReferredName.error = resources.getString(R.string.er_referred_name)
                } else if (isValidPhoneNumber(edtMobileNo) == false) {
                    tfMobileNo.error = resources.getString(R.string.er_enter_mobile)
                    tfReferredName.error = null
                } else if ((!isEmpty(edtAlternetMobileNo)) && (isValidPhoneNumber(
                        edtAlternetMobileNo
                    ) == false)
                ) {
                    tfAlternetMobileNo.error = resources.getString(R.string.er_enter_mobile)
                    tfReferredName.error = null
                    tfMobileNo.error = null
                }/*else if (loanType.toString().isNullOrEmpty()) {
showToast(requireContext(),resources.getString(R.string.er_type_of_loan))
}*/ else if (isEmpty(edtLoanAmount)) {
                    tfLoanAmount.error = resources.getString(R.string.er_loan_amount)
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                } else if (amt.toDouble() < 350000) {

                    tfLoanAmount.error = resources.getString(R.string.er_less_loan_amount) + " " + resources.getString(
                        R.string.Rs
                    ) + "3,49,999"
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                } else if (isEmpty(edtNatureOfBusiness)) {
                    tfNatureOfBusiness.error = resources.getString(R.string.er_natureOfBusiness)
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                    tfLoanAmount.error = null
                } else if (edtNatureOfBusiness.text.toString().length <= 6) {
                    tfNatureOfBusiness.error = resources.getString(R.string.er_validnatureOfBusiness)
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                    tfLoanAmount.error = null
                } else if (isEmpty(edtAddress)) {
                    edtAddress.error = resources.getString(R.string.address)
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                    tfLoanAmount.error = null
                    tfNatureOfBusiness.error = null
                    edtNatureOfBusiness.error = null
                } else if (isEmpty(edtPincode)) {
                    tfPincode.error = resources.getString(R.string.er_enter_pincode)
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                    tfLoanAmount.error = null
                    edtNatureOfBusiness.error = null
                    tfNatureOfBusiness.error = null
                    tfAddress.error = null
                    edtAddress.error = null
                } else if (edtPincode.text.toString().length != 6) {
                    tfPincode.error = resources.getString(R.string.er_pincode)
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                    tfLoanAmount.error = null
                    edtNatureOfBusiness.error = null
                    tfNatureOfBusiness.error = null
                    tfAddress.error = null
                    edtAddress.error = null
                } else {
                    tfReferredName.error = null
                    tfMobileNo.error = null
                    tfAlternetMobileNo.error = null
                    tfLoanAmount.error = null
                    edtNatureOfBusiness.error = null
                    edtAddress.error = null
                    tfAddress.error = null
                    tfPincode.error = null

                    val params: MutableMap<String, String> = HashMap()
                    params["refName"] = edtReferredName.text.toString()
                    params["refMobile"] = edtMobileNo.text.toString()
                    params["loanType"] = loanType.toString()
                    params["loanAmt"] = edtLoanAmount.text.toString().replace(",".toRegex(), "")
                    params["address"] = edtAddress.text.toString()
                    params["pincode"] = edtPincode.text.toString()
                    params["natureOfBusiness"] = edtNatureOfBusiness.text.toString()
                    params["alternateMobile"] = edtAlternetMobileNo.text.toString()

                    viewModel.saveReferral(requireActivity(), params)

                }
            }
        }

        /*val manager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            alertMessageNoGps(requireContext(), "Yes")
        } else {
            getCurrentLatLong(requireContext())
            if (isNetworkAvailable(requireContext())) {
                if (isEmpty(edtReferredName)) {
                    tfReferredName.setError(resources.getString(R.string.er_referred_name))
                } else if (isValidPhoneNumber(edtMobileNo) == false) {
                    tfMobileNo.setError(resources.getString(R.string.er_enter_mobile))
                    tfReferredName.setError(null)
                } else if ((!isEmpty(edtAlternetMobileNo)) && (isValidPhoneNumber(
                        edtAlternetMobileNo
                    ) == false)
                ) {
                    tfAlternetMobileNo.setError(resources.getString(R.string.er_enter_mobile))
                    tfReferredName.setError(null)
                    tfMobileNo.setError(null)
                }*//*else if (loanType.toString().isNullOrEmpty()) {
showToast(requireContext(),resources.getString(R.string.er_type_of_loan))
}*//* else if (isEmpty(edtLoanAmount)) {
                    tfLoanAmount.setError(resources.getString(R.string.er_loan_amount))
                    tfReferredName.setError(null)
                    tfMobileNo.setError(null)
                    tfAlternetMobileNo.setError(null)
                } else if (amt.toDouble() < 350000) {

                    tfLoanAmount.setError(
                        resources.getString(R.string.er_less_loan_amount) + " " + resources.getString(
                            R.string.Rs
                        ) + "3,49,999"
                    )
                    tfReferredName.setError(null)
                tfMobileNo.setError(null)
                tfAlternetMobileNo.setError(null)
            }else if (isEmpty(edtNatureOfBusiness)) {
                tfNatureOfBusiness.setError(resources.getString(R.string.er_natureOfBusiness))
                tfReferredName.setError(null)
                tfMobileNo.setError(null)
                tfAlternetMobileNo.setError(null)
                tfLoanAmount.setError(null)
            }else if(edtNatureOfBusiness.text.toString().length<=6){
                tfNatureOfBusiness.setError(resources.getString(R.string.er_validnatureOfBusiness))
                tfReferredName.setError(null)
                tfMobileNo.setError(null)
                tfAlternetMobileNo.setError(null)
                tfLoanAmount.setError(null)
            }else if (isEmpty(edtAddress)) {
                edtAddress.setError(resources.getString(R.string.address))
                tfReferredName.setError(null)
                tfMobileNo.setError(null)
                tfAlternetMobileNo.setError(null)
                tfLoanAmount.setError(null)
                tfNatureOfBusiness.setError(null)
                edtNatureOfBusiness.setError(null)
            }else if (isEmpty(edtPincode)) {
                tfPincode.setError(resources.getString(R.string.er_enter_pincode))
                tfReferredName.setError(null)
                tfMobileNo.setError(null)
                tfAlternetMobileNo.setError(null)
                tfLoanAmount.setError(null)
                edtNatureOfBusiness.setError(null)
                tfNatureOfBusiness.setError(null)
                tfAddress.setError(null)
                edtAddress.setError(null)
            }else if (edtPincode.text.toString().length!=6) {
                tfPincode.setError(resources.getString(R.string.er_pincode))
                tfReferredName.setError(null)
                tfMobileNo.setError(null)
                tfAlternetMobileNo.setError(null)
                tfLoanAmount.setError(null)
                edtNatureOfBusiness.setError(null)
                tfNatureOfBusiness.setError(null)
                tfAddress.setError(null)
                edtAddress.setError(null)
            } else {
                tfReferredName.setError(null)
                tfMobileNo.setError(null)
                tfAlternetMobileNo.setError(null)
                tfLoanAmount.setError(null)
                edtNatureOfBusiness.setError(null)
                edtAddress.setError(null)
                tfAddress.setError(null)
                tfPincode.setError(null)

                val params: MutableMap<String, String> = HashMap()
                params["refName"] = edtReferredName.text.toString()
                params["refMobile"] = edtMobileNo.text.toString()
                params["loanType"] = loanType.toString()
                params["loanAmt"] = edtLoanAmount.text.toString().replace(",".toRegex(), "")
                params["address"] = edtAddress.text.toString()
                params["pincode"] = edtPincode.text.toString()
                params["natureOfBusiness"] = edtNatureOfBusiness.text.toString()
                params["alternateMobile"] = edtAlternetMobileNo.text.toString()

                viewModel.saveReferral(requireActivity(),params)

            }
            }
        }*/
    }

    private fun getLocation() {
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location.latitude.toFloat()
        val longitudeToSet: Float = location.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
        // tvGpsLocation = findViewById(R.id.textView)
        //tvGpsLocation.text = "Latitude: " + location.latitude + " , Longitude: " + location.longitude
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {

        Timber.e(TAG + " Selected : " + loanTypeList[position].code)
        loanType = loanTypeList[position].code
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ReferralDialogFragment.
         */
// TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ReferralDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}