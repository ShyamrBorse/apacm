package com.apacfin.apacm.presentation.registrationprocess.mpinlogin

import `in`.aabhasjindal.otptextview.OTPListener
import android.Manifest
import android.Manifest.permission.CALL_PHONE
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Paint
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.apacfin.apacm.BuildConfig
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.home.HomeActivity
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.presentation.registrationprocess.splash.registration.RegistrationActivity
import com.apacfin.apacm.util.*
import com.apacfin.data.model.profile.AppDetailModel
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.apacfin.data.prefrerce.Constants.IsLogin
import com.apacfin.data.prefrerce.Constants.IsRegistred
import com.apacfin.data.prefrerce.Constants.SetLanguage
import com.apacfin.data.prefrerce.Constants.Token
import com.apacfin.data.prefrerce.Constants.Ucic
import com.apacfin.data.prefrerce.Constants.gratitudeFlag
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login_mpin.*
import kotlinx.android.synthetic.main.fragment_set_mpin_dialog.*
import timber.log.Timber
import java.util.HashMap

@AndroidEntryPoint
class LoginMpinActivity : AppCompatActivity(), View.OnClickListener {

    val TAG = LoginMpinActivity::class.java.simpleName
    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    lateinit var appDetailModel: AppDetailModel
    var strPin = ""
    var name = ""
    var mobileNo = ""
    var dob = ""
    lateinit var appPreferences: AppPreferences
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    var permission  = arrayOf(Manifest.permission.READ_PHONE_STATE, /*Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.MANAGE_EXTERNAL_STORAGE,*/ Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private val PERMISSION_REQUEST_CODE = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_mpin)

        initView()

        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, Observer {
            loading = it;
            Timber.e( "SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()
                // showProgressDialog(this)
                // progressDialog!!.show()
            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getDetailsFromUCIC response
        viewModel.mPinLoginResponse.observe(this, {
            registrationModel = it
            Timber.e( "mPinLoginResponse " + registrationModel)

            if (registrationModel.status) {

                  appPreferences?.putString(Token,registrationModel.token)
                  appPreferences?.putString(Ucic,registrationModel.ucic)
                  appPreferences?.putString(SetLanguage,registrationModel.lang)
                  appPreferences?.putString(gratitudeFlag,registrationModel.gratitudeFlag)
                appPreferences?.putBoolean(Constants.isDayFirstLogin, false)
                appPreferences?.putBoolean(IsLogin,true)
                edtLoginPin?.showSuccess()
                startActivity(Intent(this@LoginMpinActivity, HomeActivity::class.java))

                finish()
                if (isNetworkAvailable(this)) {

                    val params: MutableMap<String, String> = HashMap()
                    params["fcmToken"] = appPreferences?.getString(Constants.FirebaseToken,"").toString()

                    viewModel.updateUserToken(this,params)
                }
            } else {
                /*showInternetConnectionLost(
                    this,
                    registrationModel.msg,
                    getString(R.string.ok)
                )*/
                edtLoginPin.showError()
                showAlertMessageSingleClick(
                    this,
                    getString(R.string.ok),
                    registrationModel.msg
                ) { dialog, which ->
                    edtLoginPin.resetState()
                    edtLoginPin.otp=""
                }

            }
        })
        //-- Handle updateUserTokenResponse response
        viewModel.updateUserTokenResponse.observe(this, {
            registrationModel = it
            Timber.e("updateUserTokenResponse " + registrationModel)



        })

        //-- Handle getAppDetails response
        viewModel.getAppDetailsResponse.observe(this, {
            appDetailModel = it
            Timber.e("getAppDetails " + appDetailModel)
            if (appDetailModel.status) {
                appPreferences?.putString(Constants.AppVersion, appDetailModel.version)

                if (appDetailModel.forceUpdate){
                    val dialog = MaterialAlertDialogBuilder(this, R.style.MyMaterialAlertDialog).create()
                    dialog.setTitle(getString(R.string.update_title))
                    //dialog.setIcon(R.drawable.ic_new_logo_foreground)
                    dialog.setMessage(getString(R.string.update_msg))
                    dialog.setCancelable(false)

                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.update)) { _, _ ->

                        try {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.apacfin.apacm")))
                        } catch (e: ActivityNotFoundException) {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.apacfin.apacm")))
                        }
                    }
                    dialog.create()
                    dialog.show()
                }
            }

        })
    }
    /*override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }*/

    override fun onResume() {
        super.onResume()
        getAppDetails()
    }
    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(this@LoginMpinActivity)!!
        Timber.e("getFirebaseToken : " + appPreferences!!.getString(Constants.FirebaseToken,""))
        name = appPreferences!!.getString(Constants.Name,"").toString()
        mobileNo = appPreferences!!.getString(Constants.MobileNo,"").toString()
        dob = appPreferences!!.getString(Constants.DOB,"").toString()
        appPreferences!!.putString(Token,"")

        txtWelcome.text = resources.getString(R.string.welcome) +" "+ appPreferences!!.getString(Constants.Name,"")
        txtForgotPin.setPaintFlags(txtForgotPin.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        txtForgotPin.setOnClickListener(this)
        ivCall.setOnClickListener(this)

        edtLoginPin?.requestFocusOTP()
        edtLoginPin.setOtpListener(object : OTPListener {
            override fun onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            override fun onOTPComplete(otp: String) {

                val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    edtLoginPin.setOTP("")
                    alertMessageNoGps(this@LoginMpinActivity, "Yes")
                } else {
                    if (com.apacfin.apacm.util.checkPermission(this@LoginMpinActivity) == false) {
                        requestPermission()
                    }else{
                        //getCurrentLatLong(this@LoginMpinActivity)
                        strPin = otp
                        validation(strPin)
                    }

                }


            }
        })

    }

    fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val locationPermissionRequest = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result: Map<String, Boolean?>? ->
            }
            locationPermissionRequest.launch(permission)
            Timber.e("requestPermission: IN" + " " + Environment.isExternalStorageManager()
            )

        } else {
            //below android 11
            ActivityCompat.requestPermissions(
                this@LoginMpinActivity, permission, PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Timber.e("onRequestPermissionsResult: "+requestCode+" : "+grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty()) {
                //getCurrentLatLong(this@LoginMpinActivity)
                //strPin = otp
                validation(strPin)
            }
        }
    }
    private fun getAppDetails() {

        if (isNetworkAvailable(this)) {
            val versionName: String = BuildConfig.VERSION_NAME
            val params: MutableMap<String, String> = HashMap()
            params["appVersion"] = versionName
            Timber.e(TAG + " appVer : " + versionName)
            viewModel.getAppDetails(this,params)
        }
    }
    override fun onClick(view: View?) {
        when (view?.getId()) {


            R.id.txtForgotPin ->  {
                /*val transaction: FragmentTransaction =  supportFragmentManager.beginTransaction()
                SetMpinDialogFragment().show(transaction, "")*/
                appPreferences!!.putBoolean(IsLogin, false)
                appPreferences!!.putBoolean(IsRegistred, false)
                appPreferences!!.putString(Token,"")
                 startActivity(Intent(this, RegistrationActivity::class.java).putExtra(Constants.FromView, 0))
                 finish()
            }

            R.id.ivCall ->  {
                if (isSIMInserted(this)) {
                   callDialer(this,Constants.Contact1)
                }
                /*if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(callIntent);
                } else {

                    //You already have permission
                    try {
                        startActivity(callIntent)
                    } catch (e: SecurityException) {
                        e.printStackTrace()
                    }
                }*/
            }

        }
    }

    private fun validation(pin: String) {
        //getCurrentLatLong(this@LoginMpinActivity)
        getDeviceId(this@LoginMpinActivity)
        if (isNetworkAvailable(this)) {
            if (pin.isNullOrBlank()) {
                showInternetConnectionLost(
                    this,
                    resources.getString(R.string.er_enter_otp),
                     resources.getString(R.string.ok)
                )
            } else {
                    Timber.e(TAG+"encryptPassword : "+encryptPassword(pin))
                    val params: MutableMap<String, String> = HashMap()
                    params["mobileNo"] = mobileNo
                    params["dob"] = dob
                    params["mPin"] = encryptPassword(pin).replace("\n","")
                    viewModel.mPinLogin(this, params)

            }
        }
    }
}