package com.apacfin.apacm.presentation.statement

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.amountFormater
import com.apacfin.data.model.myloan.StatementModel


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : StatementAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class StatementAdapter(
    context: Context, listOfChargesModel: List<StatementModel>,
    fromView: Int,
    onClickListener: OnClickListener
) : RecyclerView.Adapter<StatementAdapter.ViewHolder?>() {
    var context: Context
    var fromView: Int

    private val mOnClickListener: OnClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_statement, parent, false)
        return ViewHolder(view, mOnClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listOfChargesModel: StatementModel = listOfChargesModels[position]

        if (fromView==2){
            holder.tvPaymentMode.visibility =View.GONE
            holder.tvPaymentModeLable.visibility =View.GONE
            holder.tvPaidOnLable.text = context.getString(R.string.upcoming_emi)
            holder.tvPaidAmountLable.text = context.getString(R.string.emi_amount)
        }
        holder.tvPaidOn.text = "" + listOfChargesModel.paidOn
        holder.tvPaidAmount.text =  "" + amountFormater(listOfChargesModel.paidAmount.toDouble())
        holder.tvPaymentMode.text = "" + listOfChargesModel.paymentMode

    }

    override fun getItemCount(): Int {
        return listOfChargesModels.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(itemView: View, onClickListener: OnClickListener) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener ,
        OnStatusClickListener {
        var tvPaidOn: TextView
        var tvPaidAmount: TextView
        var tvPaymentMode: TextView
        var tvPaidOnLable: TextView
        var tvPaidAmountLable: TextView
        var tvPaymentModeLable: TextView
        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {
            //Log.d(TAG, "onClick: " + selectedPosition)
            mOnClickListener.onLeadClick(selectedPosition)
        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            tvPaidOn = itemView.findViewById(R.id.tvPaidOn)
            tvPaidAmount = itemView.findViewById(R.id.tvPaidAmount)
            tvPaymentMode = itemView.findViewById(R.id.tvPaymentMode)
            tvPaidOnLable = itemView.findViewById(R.id.tvPaidOnLable)
            tvPaidAmountLable = itemView.findViewById(R.id.tvPaidAmountLable)
            tvPaymentModeLable = itemView.findViewById(R.id.tvPaymentModeLable)

            mOnClickListener = onClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = StatementAdapter::class.java.simpleName
        lateinit var listOfChargesModels: List<StatementModel>
        var selectedPosition = -1
    }

    init {
        listOfChargesModels = listOfChargesModel
        mOnClickListener = onClickListener
        this.context = context
        this.fromView = fromView
    }
}
