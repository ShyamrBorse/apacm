package com.apacfin.apacm.presentation.payment

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apacfin.apacm.R
import com.apacfin.data.model.payment.PaymentApiKeyResponse
import com.apacfin.data.model.payment.TransactionListResponse
import com.apacfin.data.model.payment.VpaDetailsModel
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.PaymentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : PaymentViewModel.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */
@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val repository: PaymentRepository
) : ViewModel() {

    private val _getPaymentApiKeyResponse: MutableLiveData<PaymentApiKeyResponse> = MutableLiveData()
    private val _createOrderIdResponse: MutableLiveData<PaymentApiKeyResponse> = MutableLiveData()
    private val _getEmiOptionsAmtResponse: MutableLiveData<PaymentApiKeyResponse> = MutableLiveData()
    private val _getupdatePaymentStatusResponse: MutableLiveData<PaymentApiKeyResponse> = MutableLiveData()
    private val _paymentTransactionListResponse: MutableLiveData<TransactionListResponse> = MutableLiveData()

    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //getPaymentApiKey responce
    val getPaymentApiKey: LiveData<PaymentApiKeyResponse> get() = _getPaymentApiKeyResponse

    //createOrderId responce
    val createOrderId: LiveData<PaymentApiKeyResponse> get() = _createOrderIdResponse

    //getEmiOptionsAmt responce
    val getEmiOptionsAmt: LiveData<PaymentApiKeyResponse> get() = _getEmiOptionsAmtResponse

     //getupdatePaymentStatus responce
    val getupdatePaymentStatus: LiveData<PaymentApiKeyResponse> get() = _getupdatePaymentStatusResponse

    //paymentTransactionList responce
    val paymentTransactionList: LiveData<TransactionListResponse> get() = _paymentTransactionListResponse

    //getPaymentApiKey api
    fun getPaymentApiKey(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getPaymentApiKey()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getPaymentApiKeyResponse" + response.value.msg)
                _getPaymentApiKeyResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_setLangForUserResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                val paymentApiKeyResponse =  PaymentApiKeyResponse("", "","","","","","","",
                    context.getString(R.string.tecnical_error),
                    false, VpaDetailsModel("","","","")
                )
                _getPaymentApiKeyResponse.postValue(paymentApiKeyResponse)
            }
        }

    }

    //createOrderId api
    fun createOrderId(context: Context, params: MutableMap<String, String>) = GlobalScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.createOrderId(params)) {
            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_createOrderId " + response.value.msg)
                response.value.run {
                    _createOrderIdResponse.postValue(response.value)
                }
            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_createOrderId : "  + response.errorCode+ response.isNetworkError+ response.errorBody)
                val paymentApiKeyResponse =  PaymentApiKeyResponse("", "","", "","","", "",
                   "", context.getString(R.string.tecnical_error),
                    false,VpaDetailsModel("","","",""))
                _createOrderIdResponse.postValue(paymentApiKeyResponse)
            }
        }

    }

    //getEmiOptionsAmt api
    fun getEmiOptionsAmt(context: Context, id: String) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getEmiOptionsAmt(id)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getTypeOfPaymentResponse" + response.value.msg)
                _getEmiOptionsAmtResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getTypeOfPaymentResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                val paymentApiKeyResponse =  PaymentApiKeyResponse("", "","", "","","", "",
                    "",context.getString(R.string.tecnical_error),
                    false,VpaDetailsModel("","","",""))
                _getEmiOptionsAmtResponse.postValue(paymentApiKeyResponse)
            }
        }

    }

    //updatePaymentStatus api
    fun updatePaymentStatus(context: Context, params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.updatePaymentStatus(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getupdatePaymentStatusResponse" + response.value.msg)
                _getupdatePaymentStatusResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getupdatePaymentStatusResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                val paymentApiKeyResponse =  PaymentApiKeyResponse("", "","", "","","", "",
                   "", context.getString(R.string.tecnical_error),
                    false,VpaDetailsModel("","","",""))
                _getupdatePaymentStatusResponse.postValue(paymentApiKeyResponse)
            }
        }

    }

    //paymentTransactionList api
    fun paymentTransactionList(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.paymentTransactionList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_paymentTransactionListResponse" + response.value.msg)
                _paymentTransactionListResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_paymentTransactionListResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                val paymentApiKeyResponse =  TransactionListResponse(
                    context.getString(R.string.tecnical_error),false,arrayListOf()
                    )
                _paymentTransactionListResponse.postValue(paymentApiKeyResponse)
            }
        }

    }

}