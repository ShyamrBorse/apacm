package com.apacfin.apacm.presentation.registrationprocess.splash.registration

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.otp.OtpDialogFragment
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import kotlinx.android.synthetic.main.activity_registration.*
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build.VERSION
import android.os.Environment
import android.provider.Settings

import android.widget.Toast

import androidx.core.app.ActivityCompat

import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.widget.DatePicker
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import com.apacfin.apacm.BuildConfig
import com.apacfin.apacm.presentation.language.LanguageActivity
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.LoginMpinActivity
import com.apacfin.apacm.presentation.registrationprocess.newuser.NewUserDialogFragment
import com.apacfin.data.model.profile.AppDetailModel
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.Constants.IsLogin
import com.apacfin.data.prefrerce.Constants.SimMobNo
import com.apacfin.data.prefrerce.Constants.dateFormat
import com.apacfin.data.prefrerce.Constants.fromRegistation
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_registration.edtDob
import kotlinx.android.synthetic.main.activity_registration.edtMobileNo
import kotlinx.android.synthetic.main.activity_registration.tfDob
import kotlinx.android.synthetic.main.activity_registration.tfMobileNo
import kotlinx.android.synthetic.main.custom_phone_select.*
import kotlinx.android.synthetic.main.fragment_new_user_dialog.*
import java.text.SimpleDateFormat
import java.util.*
import com.apacfin.apacm.util.LocaleHelper
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.CredentialsApi
import com.google.android.gms.auth.api.credentials.HintRequest


@AndroidEntryPoint
class RegistrationActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = RegistrationActivity::class.java.simpleName
    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    lateinit var appDetailModel: AppDetailModel
    var name = ""
    var mobileNo = ""
    var dob = ""
    var refNo = ""

    //   var isGetUCIC = false
    var appPreferences: AppPreferences? = null
    var permission  = arrayOf(Manifest.permission.READ_PHONE_STATE, /*Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.MANAGE_EXTERNAL_STORAGE,*/ Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

    var cal = Calendar.getInstance()

    // Arbitrary number to identify the request for crednetials
    private val PERMISSION_REQUEST_CODE = 200
    private val iRequestCodePhoneNumber = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        Timber.e("checkPermission"+(checkPermission(this)))

    if (checkPermission(this) == false) {
            requestPermission()
        }else{
            doOperation()
        }
         initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, Observer {
            loading = it;
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()
                // showProgressDialog(this)
                // progressDialog!!.show()
            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })


        //-- Handle getRegistration response
        viewModel.getRegistration.observe(this, {
            registrationModel = it
            Timber.e("getRegistration " + registrationModel)

            if (registrationModel.status) {
                refNo = registrationModel.refNo
                name = registrationModel.name

                if (registrationModel.newUserFlag) {
                    NewUserDialogFragment.newInstance(
                        edtMobileNo.text.toString(),
                        edtDob.text.toString()
                    ).apply {
                        show(supportFragmentManager, tag)

                    }
                } else {
                    val bundle = Bundle()
                    // bundle.putString(Constants.Ucic, edtUCIC.text.toString())
                    bundle.putString(Constants.Name, name)
                    bundle.putString(Constants.MobileNo, edtMobileNo.text.toString())
                    bundle.putString(Constants.DOB, edtDob.text.toString())
                    bundle.putString(Constants.RefNo, refNo)
                    bundle.putString(Constants.Fname, "")
                    bundle.putString(Constants.Lname, "")
                    bundle.putString(Constants.Pincode, "")
                    bundle.putBoolean(Constants.FromView, false)

                    OtpDialogFragment().apply {
                        show(supportFragmentManager, tag)
                        arguments = bundle
                    }
                }


            } else {
                showInternetConnectionLost(
                    this,
                    registrationModel.msg,
                    getString(R.string.ok)
                )
                //  showSnackBar(this, getDetailsFromUCIC.msg) }
                //startActivity(Intent(this, MainActivity::class.java))
            }
        })

        //-- Handle getAppDetails response
        viewModel.getAppDetailsResponse.observe(this, {
            appDetailModel = it
            Timber.e("getAppDetails " + appDetailModel)
            if (appDetailModel.status) {
                appPreferences?.putString(Constants.AppVersion, appDetailModel.version)

                if (appDetailModel.forceUpdate){
                    val dialog = MaterialAlertDialogBuilder(this, R.style.MyMaterialAlertDialog).create()
                    dialog.setTitle(getString(R.string.update_title))
                    //dialog.setIcon(R.drawable.ic_new_logo_foreground)
                    dialog.setMessage(getString(R.string.update_msg))
                    dialog.setCancelable(false)
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.update)) { _, _ ->

                        try {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
                        } catch (e: ActivityNotFoundException) {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
                        }
                    }

                    dialog.show()
                }
            }

        })
    }

    private fun SelectPhoneNoDialog(phone: String) {
        Timber.e("Phone phone: " + phone)
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.custom_phone_select, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        //show dialog
        val mAlertDialog = mBuilder.show()
        val tvMobileNo: TextView? = mAlertDialog.findViewById(R.id.tvMobileNo)
        val tvNonOfAbove: TextView? = mAlertDialog.findViewById(R.id.tvNonOfAbove)
        // if button is clicked, close the custom dialog
        tvMobileNo?.text = phone.takeLast(10)
        tvMobileNo?.setOnClickListener(View.OnClickListener {
            mAlertDialog.dismiss()
            edtMobileNo.setText(tvMobileNo.text.toString())
            appPreferences?.putString(SimMobNo, tvMobileNo.text.toString())
            // Toast.makeText(applicationContext, "Dismissed..!!", Toast.LENGTH_SHORT).show()
        })
        tvNonOfAbove?.setOnClickListener(View.OnClickListener {
            mAlertDialog.dismiss()

        })

    }

    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(this@RegistrationActivity)
        appPreferences!!.putString(Constants.Token,"")
        if (appPreferences!!.getBoolean(IsLogin, false)) {
            startActivity(Intent(this, LoginMpinActivity::class.java))
            finish()
        }else{
            getFirebaseToken(this)

        }



        btnGetOTP.setOnClickListener(this)
        edtDob.setOnClickListener(this)
        txtLang.setOnClickListener(this)
       // edtMobileNo.addTextChangedListener(mTextWatcher)

        disableCopyPasteOperations(edtMobileNo)
        //disableCopyPasteOperations(edtDob)

        edtMobileNo.filterTouchesWhenObscured = true
        edtDob.filterTouchesWhenObscured = true
        btnGetOTP.filterTouchesWhenObscured = true
        txtLang.filterTouchesWhenObscured = true
        // create an OnDateSetListener
        var dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                //val myFormat = "dd-MMM-yyyy" // mention the format you need
                val sdf = SimpleDateFormat(dateFormat, Locale.US)
                edtDob.setText(sdf.format(cal.getTime()))
            }
        }
        edtDob.setOnClickListener {

            val datePickerDialog = DatePickerDialog(
                this@RegistrationActivity, R.style.SpinnerDatePickerDialog,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )

            //following line to restrict future date selection
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis())
            datePickerDialog.show()
        }


    }

    override fun onResume() {
        super.onResume()

        getAppDetails()
    }
    private fun getAppDetails() {

        if (isNetworkAvailable(this)) {
            val versionName: String = BuildConfig.VERSION_NAME
            val params: MutableMap<String, String> = HashMap()
            params["appVersion"] = versionName
            Timber.e(TAG + " appVer : " + versionName)
            viewModel.getAppDetails(this,params)
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View?) {

        when (view?.getId()) {

            R.id.btnGetOTP -> {

                checkValidationGetOpt()

            }
            R.id.txtLang -> {

                startActivity(
                    Intent(
                        this,
                        LanguageActivity::class.java
                    ).putExtra(Constants.FromView, fromRegistation)
                )


            }

        }

    }

    /*private fun setVisibility(
        ucicIsVisible: Int,
        mobileIsVisible: Int,
        verifyUCICVisible: Int,
        getOTPVisible: Int,
        otpMsgVisible: Int,
        changeMobileNumberVisible: Int,
        knownUcicIsVisible: Int,
        getUcicIsVisible: Int,
        backGetUCICIsVisible: Int,
        isUCICVerify: Boolean
    ) {
        tfUCIC.visibility = ucicIsVisible
        tfMobileNo.visibility = mobileIsVisible
        btnVerifyUCIC.visibility = verifyUCICVisible
        btnGetOTP.visibility = getOTPVisible
        txtOtpMsg.visibility = otpMsgVisible
        txtChangeMobileNumber.visibility = changeMobileNumberVisible
        txtKnownUcic.visibility = knownUcicIsVisible
        txtGetUcic.visibility = getUcicIsVisible
        ivBackGetUCIC.visibility = backGetUCICIsVisible

        if (isUCICVerify) {
            txtOtpMsg.text = resources.getString(R.string.mobile_verification_msg)
            edtUCIC.isEnabled = false
            //edtMobileNo.isEnabled = false
        } else {
            txtOtpMsg.text = resources.getString(R.string.mobile_verification_msg)
            edtMobileNo.isEnabled = true
            //edtUCIC.isEnabled = true
        }
        *//*if(isGetUCIC){
            txtOtpMsg.text = resources.getString(R.string.you_will_get_otp)
            txtChangeMobileNumber.visibility = View.GONE
            tfUCIC.visibility = View.GONE
        }else{
            txtOtpMsg.text = resources.getString(R.string.mobile_verification_msg)
            txtChangeMobileNumber.visibility = View.VISIBLE
            tfUCIC.visibility = View.VISIBLE
        }*//*


    }*/

    //  create a textWatcher member
  /*  private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {}

        @RequiresApi(Build.VERSION_CODES.O)
        override fun afterTextChanged(editable: Editable) {
            // check Fields For Empty Values

            // checkValidation()
        }
    }*/

    @RequiresApi(Build.VERSION_CODES.O)
    /*private fun checkValidation() {

        if (isNetworkAvailable(this)) {

            // if (isGetUCIC) {

            *//*if (isValidPhoneNumber(edtMobileNo)) {
                tfMobileNo.error = null
                mobileNo = edtMobileNo.text.toString()
                viewModel.getUCIC(mobileNo)

            } else {
                tfMobileNo.setError(resources.getString(R.string.er_enter_mobile))
            }*//*
            // } else {

            if (isEmpty(edtUCIC)) {
                tfUCIC.setError(resources.getString(R.string.er_enter_ucic))
            } else {
                tfUCIC.error = null
                mobileNo = edtUCIC.text.toString()
                viewModel.getDetailsFromUCIC(mobileNo)
            }
            *//* if (validationGetUCICDetails()) {
                 tfUCIC.error = null
                 mobileNo = edtUCIC.text.toString()
                 viewModel.getDetailsFromUCIC(mobileNo)

                 // setButtonValidation(this, btnVerifyUCIC, true)
             } else {
                 //setButtonValidation(this, btnVerifyUCIC, false)
             }*//*

            //  }
        } *//*else {
            showInternetConnectionLost(
                this,
                getString(R.string.no_internet),
                getString(R.string.no_internet_msg),
                getString(
                    R.string.ok
                )
            )
        }*//*


    }*/

    private fun checkValidationGetOpt() {
        //getCurrentLatLong(this@RegistrationActivity)
        getDeviceId(this@RegistrationActivity)
        if (isNetworkAvailable(this)) {
            //if (isGetUCIC) {
            if (!isValidPhoneNumber(edtMobileNo)) {

                tfMobileNo.setError(resources.getString(R.string.er_enter_mobile))


            } else if (isEmpty(edtDob)) {

                tfDob.setError(resources.getString(R.string.er_enter_dob))

            } else {
                tfMobileNo.error = null
                tfDob.error = null
                mobileNo = edtMobileNo.text.toString()
                dob = edtDob.text.toString()
                val params: MutableMap<String, String> = HashMap()
                params["mobileNo"] = mobileNo
                params["dob"] = dob

                viewModel.getRegistration(this,params)
            }

            /*} else {
                if (isEmpty(edtUCIC)) {
                    tfUCIC.setError(resources.getString(R.string.er_enter_ucic))
                } else {
                    tfUCIC.error = null
                    mobileNo = edtUCIC.text.toString()
                    viewModel.getOTP(mobileNo)
                }

            }*/
        } /*else {
            showInternetConnectionLost(
                this,
                getString(R.string.no_internet),
                getString(R.string.no_internet_msg),
                getString(
                    R.string.ok
                )
            )
        }*/


    }

    private fun getPhone(): String {
        val phoneMgr = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            Timber.e("phoneMgr : " + isSIMInserted(this))

        if (isSIMInserted(this)) {
            if (phoneMgr.line1Number.isNullOrEmpty()) {
                return ""
            } else {
                return if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_PHONE_STATE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ""
                } else phoneMgr.line1Number
            }
        }else{
            return ""
        }
    }

    @SuppressLint("LongLogTag")
    private fun requestHint() {
        Timber.e("requestHint")
        val hintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()
        val intent = Credentials.getClient(this).getHintPickerIntent(hintRequest)
        //val intent = Auth.CredentialsApi.getHintPickerIntent(hintRequest)

        try {
            Timber.e("requestHint try")
            startIntentSenderForResult(intent.intentSender,
                iRequestCodePhoneNumber, null, 0, 0, 0)
        } catch (e: Exception) {
            Timber.e("requestHint Error In getting Message", e.message.toString())
        }

    }



   /* override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHandler.onRequestPermissionsResult(requestCode, permissions,
            grantResults)
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
       // permissionHandler.onActivityResult(requestCode)
        Timber.e("onActivityResult: "+requestCode+" : "+resultCode)
        if (requestCode == iRequestCodePhoneNumber && resultCode == Activity.RESULT_OK) {
            val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY)
            val phoneNumber = credential?.id?.takeLast(10)
        Timber.e("phoneNumber "+phoneNumber)

            // *** Do something with the phone number here ***
            edtMobileNo.setText(phoneNumber)
            appPreferences?.putString(SimMobNo, phoneNumber)
        } else if (
            requestCode == iRequestCodePhoneNumber &&
            resultCode == CredentialsApi.ACTIVITY_RESULT_NO_HINTS_AVAILABLE
        ) {
            // *** No phone numbers available ***
            Toast.makeText(this, "No phone numbers found", Toast.LENGTH_LONG).show()
        }
    }
    /*override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }*/

    fun requestPermission() {
        if (VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val locationPermissionRequest = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result: Map<String?, Boolean?>? ->
            }
            locationPermissionRequest.launch(permission)
            Timber.e("requestPermission: IN" + " " + Environment.isExternalStorageManager()
            )
           /* if (Environment.isExternalStorageManager() == false) {
                try {
                    val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                    intent.addCategory("android.intent.category.DEFAULT")
                    intent.data =
                        Uri.parse(String.format("package:%s", applicationContext.packageName))
                    startActivityForResult(intent, 2296)
                } catch (e: java.lang.Exception) {
                    val intent = Intent()
                    intent.action = Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
                    startActivityForResult(intent, 2296)
                }
            }*/
        } else {
            //below android 11
            ActivityCompat.requestPermissions(
                this@RegistrationActivity, permission, PERMISSION_REQUEST_CODE
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Timber.e("onRequestPermissionsResult: "+requestCode+" : "+grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty()) {
                doOperation()
               /* var isGrant = true
                for (element in grantResults) {
                    if (element == PackageManager.PERMISSION_DENIED) {
                        isGrant = false
                    }
                }
                Timber.e("isGrant: "+isGrant)

                if (isGrant) {
                    doOperation()
                } else {
                    var someDenied = false
                    for (permission in permissions) {
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                permission
                            )
                        )  {
                            if (ActivityCompat.checkSelfPermission(
                                    this,
                                    permission
                                ) == PackageManager.PERMISSION_DENIED
                            ) {
                                someDenied = true
                            }
                        }
                    }
                    if (someDenied) {
                        settingActivityOpen()
                    }else{
                        showDialogOK { _: DialogInterface?, which: Int ->
                            when (which) {
                                DialogInterface.BUTTON_POSITIVE -> requestPermission()
                                DialogInterface.BUTTON_NEGATIVE -> { }
                            }
                        }
                    }
                }*/
            }
        }
    }

    private fun doOperation() {
        //Toast.makeText(this, "Successfully granted", Toast.LENGTH_LONG).show()
        requestHint()
        //getCurrentLatLong(this@RegistrationActivity)
        getDeviceId(this@RegistrationActivity)
    }

    private fun settingActivityOpen() {
        Toast.makeText(
            this,
            "Go to settings and enable permissions",
            Toast.LENGTH_LONG
        ).show()
        val i = Intent()
        i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        i.addCategory(Intent.CATEGORY_DEFAULT)
        i.data = Uri.parse("package:$packageName")
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        startActivity(i)
    }
    private fun showDialogOK(okListener: DialogInterface.OnClickListener) {
        MaterialAlertDialogBuilder(this)
            .setMessage("All Permission required for this app")
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }
}