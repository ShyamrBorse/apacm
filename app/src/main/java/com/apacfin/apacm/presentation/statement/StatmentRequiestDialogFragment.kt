package com.apacfin.apacm.presentation.statement

import android.app.DatePickerDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.myloan.LoanDetailsViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_statment.*
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.*
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.btnProceed
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.rbtnLast1Year
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.rbtnLast3Month
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.rbtnLast6Month
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.rbtnLastMonth
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.tvFromDate
import kotlinx.android.synthetic.main.fragment_statment_requiest_dialog.tvToDate
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StatmentRequiestDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class StatmentRequiestDialogFragment : DialogFragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    val TAG = StatmentRequiestDialogFragment::class.java.simpleName
    private val viewModel: LoanDetailsViewModel by viewModels()
    var loading = false
    lateinit var loanDetailsModelResponse: LoanDetailsModelResponse
    lateinit var loanDetailsList: List<LoanDetailModel>
    lateinit var registrationModel: RegistrationModel
    private var param1: String? = null
    private var param2: String? = null
    var appPreferences: AppPreferences? = null

    var loanID = ""
    var fromDate = ""
    var toDate = ""
    var cal = Calendar.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statment_requiest_dialog, container, false)
    }
    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout(width - 80, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initView()



        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(this, Observer {
            loading = it;
            Timber.e( "SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })
        //-- Handle getLoanDetails response
        viewModel.getLoanDetails.observe(viewLifecycleOwner, {
            loanDetailsModelResponse = it
            Timber.e("loanDetailsModelResponse " + loanDetailsModelResponse)

            if (loanDetailsModelResponse.status) {

                loanDetailsList = loanDetailsModelResponse.loanDetails
                setSpinnerData(loanDetailsList)
            } else {
                showInternetConnectionLost(
                    requireContext(),
                    loanDetailsModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })
        //-- Handle getLoanDetails response
        viewModel.saveAccountStatementRequestResponse.observe(viewLifecycleOwner, {
            registrationModel = it
            Timber.e("accountStatementModelResponse " + registrationModel)

            if (registrationModel.status) {

                showAlertMessageSingleClick(
                    requireContext(),
                    getString(R.string.ok),
                    registrationModel.msg
                ) { alertDialog, which ->
                    alertDialog.dismiss()
                    dialog?.dismiss()
                }

            } else {
                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
            }
        })

    }

    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        getLoanList()
        tvFromDate.setOnClickListener(this)
        tvToDate.setOnClickListener(this)
        btnCloseStatementDialog.setOnClickListener(this)
        btnProceed.setOnClickListener(this)
        rbtnLastMonth.setOnClickListener(this)
        rbtnLast3Month.setOnClickListener(this)
        rbtnLast6Month.setOnClickListener(this)
        rbtnLast1Year.setOnClickListener(this)
    }
    private fun getLoanList() {
        if (isNetworkAvailable(requireContext())) {
             viewModel.getLoanDetails(requireActivity())
        }
    }
    private fun setSpinnerData(loanDetailsList: List<LoanDetailModel>) {
        if (loanDetailsList.size>0){
            loanID = loanDetailsList[0].loanId
            var lonList : ArrayList<String?> = arrayListOf()
            for(item in loanDetailsList){
                lonList.add(item.loanId)
            }
            spAccount!!.setOnItemSelectedListener(this)

            // Create an ArrayAdapter using a simple spinner layout and languages array
            val aa = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, lonList)
            // Set layout to use when the list of choices appear
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            with(spAccount)
            {
                adapter = aa
                setSelection(0, false)
                onItemSelectedListener = this@StatmentRequiestDialogFragment
                prompt = context.getString(R.string.select_account)
                setPopupBackgroundResource(R.color.material_grey_600)

            }
        }

        

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireActivity(), theme) {
            override fun onBackPressed() {
                // On backpress, do your stuff here.
                dialog?.dismiss()
            }
        }
    }
    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnProceed -> validation()
            R.id.btnCloseStatementDialog -> dialog?.dismiss()

            R.id.rbtnLastMonth -> {
                rgTwo.clearCheck()
                tvFromDate.text = ""
                tvToDate.text = ""
                //System.out.println("date before 1 months : " + getDate(1))

            }

            R.id.rbtnLast3Month -> {
                rgTwo.clearCheck()
                tvFromDate.text = ""
                tvToDate.text = ""
                System.out.println("date before 3 months : " + getDate(3))
            }

            R.id.rbtnLast6Month -> {
                rgOne.clearCheck()
                tvFromDate.text = ""
                tvToDate.text = ""
                System.out.println("date before 6 months : " + getDate(6))
            }

            R.id.rbtnLast1Year -> {
                rgOne.clearCheck()
                tvFromDate.text = ""
                tvToDate.text = ""
                System.out.println("date before 12 months : " + getDate(12))
            }

            R.id.tvFromDate -> {
                fromDate = ""

                rgOne.clearCheck()
                rgTwo.clearCheck()
                val datePickerDialog = DatePickerDialog(
                    requireContext(), R.style.SpinnerDatePickerDialog,
                    fromDateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                )

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis())
                datePickerDialog.show()
            }
            R.id.tvToDate -> {
                toDate = ""
                rgOne.clearCheck()
                rgTwo.clearCheck()
                val datePickerDialog = DatePickerDialog(
                    requireContext(), R.style.SpinnerDatePickerDialog,
                    toDateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                )

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis())
                datePickerDialog.show()
            }
        }
    }

    private fun validation() {

        if (rbtnLastMonth.isChecked) {
            fromDate = getDate(1).toString()
            toDate = getCurrentDate().toString()
        }
        if (rbtnLast3Month.isChecked) {
            fromDate = getDate(3).toString()
            toDate = getCurrentDate().toString()
        }

        if (rbtnLast6Month.isChecked) {
            fromDate = getDate(6).toString()
            toDate = getCurrentDate().toString()
        }

        if (rbtnLast1Year.isChecked) {
            fromDate = getDate(12).toString()
            toDate = getCurrentDate().toString()
        }
        Timber.e(TAG+"Date : "+fromDate+", "+toDate)
        if (fromDate.isNullOrEmpty() || toDate.isNullOrEmpty()) {
            showToast(requireContext(), resources.getString(R.string.er_statement_date))
        } else {
            if (getDateDiff(fromDate,toDate)){
                if (isNetworkAvailable(requireContext())) {
                    val params: MutableMap<String, String> = HashMap()
                    params["lan"] = loanID
                    params["fromDate"] = fromDate
                    params["toDate"] = toDate

                    viewModel.saveAccountStatementRequest(requireActivity(),params)

                }
            }else{
                showInternetConnectionLost(
                    requireContext(),
                    resources.getString(R.string.er_date_diff),
                    resources.getString(R.string.ok)
                )
            }


        }
    }

    // create an OnDateSetListener
    var fromDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(
            view: DatePicker, year: Int, monthOfYear: Int,
            dayOfMonth: Int
        ) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //val myFormat = "dd-MMM-yyyy" // mention the format you need
            val sdf = SimpleDateFormat(Constants.dateFormat, Locale.US)
            tvFromDate.setText(sdf.format(cal.getTime()))
            fromDate= tvFromDate.text.toString()
        }
    }

    // create an OnDateSetListener
    var toDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(
            view: DatePicker, year: Int, monthOfYear: Int,
            dayOfMonth: Int
        ) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //  val myFormat = "dd-MMM-yyyy" // mention the format you need
            val sdf = SimpleDateFormat(Constants.dateFormat, Locale.US)
            tvToDate.setText(sdf.format(cal.getTime()))
            toDate = tvToDate.text.toString()
        }
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment StatmentRequiestDialogFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            StatmentRequiestDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {

        Timber.e(TAG+" Selected : "+loanDetailsList[position].loanId)
        loanID = loanDetailsList[position].loanId
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }
}