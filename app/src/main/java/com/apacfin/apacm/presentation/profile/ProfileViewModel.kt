package com.apacfin.apacm.presentation.profile

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apacfin.apacm.R
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.data.model.businesscard.BusinessCardResponse
import com.apacfin.data.model.home.BranchDetailsModel
import com.apacfin.data.model.home.SliderModel
import com.apacfin.data.model.language.LanguageModelResponse
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.referal.LoanTypeModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.ProfileRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : ProfileViewModel.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: ProfileRepository
) : ViewModel() {

    private val _setLangForUserResponse: MutableLiveData<RegistrationModel> = MutableLiveData()
    private val _getListLangResponse: MutableLiveData<LanguageModelResponse> = MutableLiveData()
    private val _getLogoutResponse: MutableLiveData<RegistrationModel> = MutableLiveData()
    private val _saveServiceComplaintResponse: MutableLiveData<RegistrationModel> =
        MutableLiveData()
    private val _saveReferralResponse: MutableLiveData<RegistrationModel> = MutableLiveData()
    private val _getLoanTypeResponse: MutableLiveData<LoanTypeModelResponse> = MutableLiveData()
    private val _getLoanDetailsResponse: MutableLiveData<LoanDetailsModelResponse> =
        MutableLiveData()
    private val _sliderListResponse: MutableLiveData<SliderModel> = MutableLiveData()
    private val _businessDetailsListResponse: MutableLiveData<BusinessCardResponse> =
        MutableLiveData()
    private val _businessCardListResponse: MutableLiveData<BusinessCardResponse> =
        MutableLiveData()
private val _saveUpdateBusinessDetailsResponse: MutableLiveData<BusinessCardResponse> =
        MutableLiveData()

    val _getBranchDetailsResponse: MutableLiveData<BranchDetailsModel> = MutableLiveData()

    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //setLangForUser responce
    val setLangForUser: LiveData<RegistrationModel> get() = _setLangForUserResponse


    //getListLang responce
    val getListLang: LiveData<LanguageModelResponse> get() = _getListLangResponse

    //getLogout responce
    val getLogoutResponse: LiveData<RegistrationModel> get() = _getLogoutResponse


    //saveServiceComplaint responce
    val saveServiceComplaintResponse: LiveData<RegistrationModel> get() = _saveServiceComplaintResponse

    //saveServiceComplaint responce
    val saveReferralResponse: LiveData<RegistrationModel> get() = _saveReferralResponse

    //getLoanTypeResponse responce
    val getLoanTypeResponse: LiveData<LoanTypeModelResponse> get() = _getLoanTypeResponse

    //getLoanDetails responce
    val getLoanDetails: LiveData<LoanDetailsModelResponse> get() = _getLoanDetailsResponse

    //sliderList responce
    val sliderListResponse: LiveData<SliderModel> get() = _sliderListResponse

    //_businessDetailsList responce
    val businessDetailsListResponse: LiveData<BusinessCardResponse> get() = _businessDetailsListResponse

    //_businessCardList responce
    val businessCardListResponse: LiveData<BusinessCardResponse> get() = _businessCardListResponse

    //_businessCardList responce
    val saveUpdateBusinessDetailsResponse: LiveData<BusinessCardResponse> get() = _saveUpdateBusinessDetailsResponse
    val getBranchDetailsResponse: LiveData<BranchDetailsModel> get() = _getBranchDetailsResponse
    //setLangForUser api
    fun setLangForUser(context: Context, params: MutableMap<String, String>) =
        viewModelScope.launch {
            _dataLoading.postValue(true)
            when (val response = repository.setLangForUser(params)) {

                is Resource.Success -> {
                    _dataLoading.postValue(false)
                    Timber.e("_setLangForUserResponse" + response.value.msg)
                    _setLangForUserResponse.postValue(response.value)

                }


                is Resource.Failure -> {
                    _dataLoading.postValue(false)
                    Timber.e("_setLangForUserResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                    val registrationModel = RegistrationModel(
                        "", "", "", "", "", false,
                        "", arrayListOf(),context.getString(R.string.tecnical_error),
                        false
                    )
                    _setLangForUserResponse.postValue(registrationModel)
                }
            }

        }

    //getListLang api
    fun getListLang(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getListLang()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getListLangResponse" + response.value.msg)
                _getListLangResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getListLangResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                val languageModelResponse = LanguageModelResponse(
                    arrayListOf(), context.getString(R.string.tecnical_error),
                    false
                )
                _getListLangResponse.postValue(languageModelResponse)
            }
        }

    }

    //logout api
    fun logout(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.logout()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("logout" + response.value.msg)
                _getLogoutResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("logout" + response.errorCode + response.isNetworkError + response.errorBody)
                val registrationModel = RegistrationModel(
                    "", "", "", "", "", false,
                    "", arrayListOf(), context.getString(R.string.tecnical_error),
                    false
                )
                _getLogoutResponse.postValue(registrationModel)
            }
        }

    }


    //saveServiceComplaint api
    fun saveServiceComplaint(context: Context, params: MutableMap<String, String>) =
        viewModelScope.launch {
            _dataLoading.postValue(true)
            when (val response = repository.saveServiceComplaint(params)) {

                is Resource.Success -> {
                    _dataLoading.postValue(false)
                    _saveServiceComplaintResponse.postValue(response.value)

                }

                is Resource.Failure -> {
                    _dataLoading.postValue(false)
                    Timber.e("saveServiceComplaint" + response.errorCode + response.isNetworkError + response.errorBody)
                    val registrationModel = RegistrationModel(
                        "", "", "", "", "", false,
                        "",arrayListOf(), context.getString(R.string.tecnical_error),
                        false
                    )
                    _saveServiceComplaintResponse.postValue(registrationModel)
                }
            }

        }


    //saveReferral api
    fun saveReferral(context: Context, params: MutableMap<String, String>) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.saveReferral(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                _saveReferralResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_saveReferralResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                val registrationModel = RegistrationModel(
                    "", "", "", "", "", false,
                    "",arrayListOf(), context.getString(R.string.tecnical_error),
                    false
                )
                _saveReferralResponse.postValue(registrationModel)
            }
        }

    }

    //getLoanType api
    fun getLoanType(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getLoanType()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                _getLoanTypeResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_saveReferralResponse" + response.errorBody)
                val loanTypeModelResponse = LoanTypeModelResponse(
                    arrayListOf(), context.getString(R.string.tecnical_error),
                    false
                )
                _getLoanTypeResponse.postValue(loanTypeModelResponse)
            }
        }

    }

    //getMyLoan api
    fun getMyLoans(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getMyLoans()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getMyLoanRespons" + response)
                _getLoanDetailsResponse.postValue(response.value)

            }


            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getLoanDetailsRespons" + response.errorCode + response.isNetworkError + response.errorBody)
                var loanDetailsModelResponse = LoanDetailsModelResponse(
                    arrayListOf(), context.getString(R.string.tecnical_error),
                    false
                )
                _getLoanDetailsResponse.postValue(loanDetailsModelResponse)
            }
        }

    }


    //getTemplateList api
    fun getTemplateList(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getTemplateList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_sliderListResponse" + response.value.msg)
                _sliderListResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = SliderModel(
                    context.getString(R.string.tecnical_error),
                    arrayListOf(),
                    false
                )
                _sliderListResponse.postValue(sliderModel)
            }
        }

    }

    //getBusinessDetailsList api
    fun getBusinessDetailsList(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getBusinessDetailsList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_businessDetailsListResponse" + response.value.msg)
                _businessDetailsListResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = BusinessCardResponse(
                    context.getString(R.string.tecnical_error), false,
                    BusinessCardModel(
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                         "",
                        "",
                        ""
                    )
                )
                _businessDetailsListResponse.postValue(sliderModel)
            }
        }

    }

//getBusinessCardList api
   /* fun getBusinessCardList(context: Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getBusinessCardList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_businessCardListResponse" + response.value.msg)
                _businessCardListResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = BusinessCardResponse(
                    context.getString(R.string.tecnical_error), false,
                    BusinessCardModel(
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    )
                )
                _businessCardListResponse.postValue(sliderModel)
            }
        }

    }*/

    //saveUpdateBusinessDetailsList api
    fun saveUpdateBusinessDetailsList(context: Context,params:  BusinessCardModel ) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.saveUpdateBusinessDetailsList(params)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("saveUpdateBusinessDetailsList" + response.value.msg)
                _saveUpdateBusinessDetailsResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("___loginResponse" + response.errorCode + response.isNetworkError + response.errorBody)
                var sliderModel = BusinessCardResponse(
                    context.getString(R.string.tecnical_error), false,
                    BusinessCardModel(
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    )
                )
                _saveUpdateBusinessDetailsResponse.postValue(sliderModel)
            }
        }

    }

    fun getBranchDetails(context: Context,lanID:  String ) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getBranchDetails(lanID)) {
            is Resource.Success -> {
                _dataLoading.postValue(false)
                //Timber.e("getBranchDetails"  + response.value.msg)
                _getBranchDetailsResponse.postValue(response.value)
            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                //Timber.e("___loginResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var branchDetailsModel =  BranchDetailsModel("","","","",context.getString(R.string.tecnical_error),false)
                _getBranchDetailsResponse.postValue(branchDetailsModel)
            }
        }

    }

}