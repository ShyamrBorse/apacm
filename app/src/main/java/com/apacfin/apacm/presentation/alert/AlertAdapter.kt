package com.apacfin.apacm.presentation.alert

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.alert.AlertModel
import com.apacfin.data.model.listofchaarges.ListOfChargesModel


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class AlertAdapter(
    context: Context, languageModels1: List<AlertModel>
) : RecyclerView.Adapter<AlertAdapter.ViewHolder?>() {
    var context: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_alert, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val languageModel: AlertModel = languageModels[position]


         holder.tvAlertTital.text = "" + languageModel.title
         holder.tvAlertMsg.text = "" + languageModel.message
         holder.tvAlertDate.text = "" + languageModel.date

    }

    override fun getItemCount(): Int {
        return languageModels.size
    }



    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView)  {
        var tvAlertTital: TextView
        var tvAlertMsg: TextView
        var tvAlertDate: TextView




        init {
            tvAlertTital = itemView.findViewById(R.id.tvAlertTital)
            tvAlertMsg = itemView.findViewById(R.id.tvAlertMsg)
            tvAlertDate = itemView.findViewById(R.id.tvAlertDate)

        }

    }

    companion object {
        private val TAG = AlertAdapter::class.java.simpleName
        lateinit var languageModels: List<AlertModel>

    }

    init {
        languageModels = languageModels1
        this.context = context
    }
}
