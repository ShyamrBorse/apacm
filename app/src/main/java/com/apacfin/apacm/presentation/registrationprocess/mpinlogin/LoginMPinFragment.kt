package com.apacfin.apacm.presentation.registrationprocess.mpinlogin

import `in`.aabhasjindal.otptextview.OTPListener
import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Paint
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.home.HomeActivityNew
import com.apacfin.apacm.presentation.registrationprocess.interfaces.LoadFragmentListener
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.profile.AppDetailModel
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login_mpin.*
import timber.log.Timber
import kotlin.collections.HashMap
import kotlin.collections.MutableMap
import kotlin.collections.set


@AndroidEntryPoint
class LoginMPinFragment : Fragment(), View.OnClickListener, LocationListener {

    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    lateinit var appDetailModel: AppDetailModel
    var strPin = ""
    var name = ""
    var mobileNo = ""
    var dob = ""
    var appPreferences: AppPreferences? = null
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_mpin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appPreferences = AppPreferences.getAppPreferences(requireActivity())

        initView()


        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(requireActivity(), Observer {
            loading = it;
            Timber.e( "SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()
                // showProgressDialog(this)
                // progressDialog!!.show()
            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getDetailsFromUCIC response
        viewModel.mPinLoginResponse.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                registrationModel = it
                Timber.e("mPinLoginResponse " + registrationModel)

                if (registrationModel.status) {

                    appPreferences?.putString(Constants.Token, registrationModel.token)
                    appPreferences?.putString(Constants.Ucic, registrationModel.ucic)
                    appPreferences?.putString(Constants.SetLanguage, registrationModel.lang)
                    appPreferences?.putString(Constants.gratitudeFlag, registrationModel.gratitudeFlag)
                    appPreferences?.putBoolean(Constants.isDayFirstLogin, false)
                    appPreferences?.putBoolean(Constants.IsLogin, true)
                    edtLoginPin?.showSuccess()
                    val intent = Intent(requireActivity(), HomeActivityNew::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)


                    if (isNetworkAvailable(requireActivity())) {

                        val params: MutableMap<String, String> = HashMap()
                        params["fcmToken"] =
                            appPreferences?.getString(Constants.FirebaseToken, "").toString()

                        viewModel.updateUserToken(requireActivity(), params)
                    }
                } else {
                    /*showInternetConnectionLost(
                        this,
                        registrationModel.msg,
                        getString(R.string.ok)
                    )*/
                    edtLoginPin.showError()
                    showAlertMessageSingleClick(
                        requireActivity(),
                        getString(R.string.ok),
                        registrationModel.msg
                    ) { dialog, which ->
                        edtLoginPin.resetState()
                        edtLoginPin.otp = ""
                    }
                }
            }
        })
        //-- Handle updateUserTokenResponse response
        viewModel.updateUserTokenResponse.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                registrationModel = it
                Timber.e("updateUserTokenResponse " + registrationModel)
            }
        })


    }


    private fun initView() {
        Timber.e("getFirebaseToken : " + appPreferences!!.getString(Constants.FirebaseToken,""))
        name = appPreferences!!.getString(Constants.Name,"").toString()
        mobileNo = appPreferences!!.getString(Constants.MobileNo,"").toString()
        dob = appPreferences!!.getString(Constants.DOB,"").toString()
        appPreferences!!.putString(Constants.Token,"")

        txtWelcome.text = resources.getString(R.string.welcome) +" "+ appPreferences!!.getString(Constants.Name,"")
        txtForgotPin.setPaintFlags(txtForgotPin.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        txtForgotPin.setOnClickListener(this)
        // ivCall.setOnClickListener(this)

        edtLoginPin?.requestFocusOTP()
        edtLoginPin.setOtpListener(object : OTPListener {
            override fun onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            override fun onOTPComplete(otp: String) {
                if (ContextCompat.checkSelfPermission(
                        requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    val manager =
                        requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        edtLoginPin.setOTP("")
                        alertMessageNoGpss(requireActivity(), "Yes")
                    } else {
                        var progressDialog = showProgressDialog(requireActivity())


                        object : AsyncTaskCoroutine<Int?, Boolean?>() {
                            override fun onPostExecute(@Nullable result: Boolean?) {
                                progressDialog.dismiss()
                                strPin = otp
                                validation(strPin)
                                /*Handler().postDelayed({
                                    progressDialog.dismiss()
                                    Log.d("Latitude Longitude", appPreferences?.getString(Constants.Latitude, "").toString())
                                    Log.d("Latitude Longitude", appPreferences?.getString(Constants.Longitude, "").toString())

                                    strPin = otp
                                    validation(strPin)
                                }, 10000)*/
                            }

                            override fun onPreExecute() {
                                progressDialog.show()
                                //getCurrentLatLong(requireActivity())
                                getLocation();
                            }

                            override fun doInBackground(vararg params: Int?): Boolean? {
                                return null
                            }
                        }.execute<Any>()

                    }
                }else{
                    appPreferences?.putString(Constants.Latitude, "")
                    appPreferences?.putString(Constants.Longitude, "")

                    strPin = otp
                    validation(strPin)
                }

            }
        })

    }

    fun alertMessageNoGpss(context: Context, btn: String) {
        try {
            val dialog = MaterialAlertDialogBuilder(context, R.style.MyMaterialAlertDialog).create()
            dialog.setTitle(" ")
            dialog.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            dialog.setCancelable(false)
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, btn) { _, _ ->
                val enableLocationIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(enableLocationIntent, 123)
            }

            dialog.show()
        } catch (e: Exception) {
            Timber.e("classTag", e.toString())
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123) {
            // Check if the user has enabled location services after returning from the settings screen
                getLocation()
        }
    }
    private fun getLocation() {
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {


            R.id.txtForgotPin ->  {
                /*val transaction: FragmentTransaction =  supportFragmentManager.beginTransaction()
                SetMpinDialogFragment().show(transaction, "")*/
                /*appPreferences!!.putBoolean(Constants.IsLogin, false)
                appPreferences!!.putBoolean(Constants.IsRegistred, false)
                appPreferences!!.putString(Constants.Token,"")*/

                val fr: Fragment = LoginMPinFragment()
                val fc: LoadFragmentListener? = activity as LoadFragmentListener?
                fc?.loadFragListener(requireActivity(), Constants.GOTOREGFrag, null, fr)

                //startActivity(Intent(requireActivity(), RegistrationActivity::class.java).putExtra(Constants.FromView, 0))
                //finish()
            }

            R.id.ivCall ->  {
                if (isSIMInserted(requireActivity())) {
                    callDialer(requireActivity(),Constants.Contact1)
                }
                /*if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(callIntent);
                } else {

                    //You already have permission
                    try {
                        startActivity(callIntent)
                    } catch (e: SecurityException) {
                        e.printStackTrace()
                    }
                }*/
            }

        }
    }


    private fun validation(pin: String) {
        //getCurrentLatLong(requireActivity())
        getDeviceId(requireActivity())
        if (isNetworkAvailable(requireActivity())) {
            if (pin.isNullOrBlank()) {
                showInternetConnectionLost(
                    requireActivity(),
                    resources.getString(R.string.er_enter_otp),
                    resources.getString(R.string.ok)
                )
            } else {
                //Timber.e(TAG+"encryptPassword : "+ encryptPassword(pin))
                val params: MutableMap<String, String> = HashMap()
                params["mobileNo"] = mobileNo
                params["dob"] = dob
                params["mPin"] = encryptPassword(pin).replace("\n","")
                viewModel.mPinLogin(requireActivity(), params)

            }
        }
    }


}