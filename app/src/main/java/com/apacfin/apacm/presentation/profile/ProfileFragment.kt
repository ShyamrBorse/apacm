package com.apacfin.apacm.presentation.profile

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.aboutus.AboutUsDialogFragment
import com.apacfin.apacm.presentation.language.LanguageActivity
import com.apacfin.apacm.presentation.listofcharges.ListOfChargesActivity
import com.apacfin.apacm.presentation.myloan.LoanDetailsViewModel
import com.apacfin.apacm.presentation.referral.ReferralDialogFragment
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.LoginActivityNew
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.LoginMpinActivity
import com.apacfin.apacm.presentation.servicecomplaint.ServiceComplaintDialogFragment
import com.apacfin.apacm.presentation.statement.StatmentRequiestDialogFragment
import com.apacfin.apacm.util.*
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.data.model.businesscard.BusinessCardResponse
import com.apacfin.data.model.home.BranchDetailsModel
import com.apacfin.data.model.home.SliderModel
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import com.jackandphantom.carouselrecyclerview.CarouselRecyclerview
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_referral_dialog.*
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.util.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
val SCREENSHOT_FILE_NAME = "share_screenshot.png"
val CACHE_DIRECTORY = "our_screenshots/"
val IMAGES_FOLDER_NAME = "IMAGES_FOLDER_NAME/"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ProfileFragment : Fragment(), View.OnClickListener, ViewTreeObserver.OnGlobalLayoutListener,
    LocationListener {
    val TAG = ProfileFragment::class.java.simpleName
    private val viewModel: ProfileViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    lateinit var sliderModel: SliderModel
    lateinit var businessCardResponse: BusinessCardResponse
    var listTemplate = listOf<String>()
    lateinit var listBusinessDetailsTemp : BusinessCardModel
    val listBusinessDetails = ArrayList<BusinessCardModel>()
    private var param1: String? = null
    private var param2: String? = null
    var appPreferences: AppPreferences? = null
    var isEdit: Boolean = false
    lateinit var recyclerBusinessCard: CarouselRecyclerview
    //lateinit var recyclerUpdateBusinessCard: CarouselRecyclerview
    lateinit var bottomShitDialog: BottomSheetDialog
    lateinit var bottomShitDialogCard: BottomSheetDialog

    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2

    var permission  = arrayOf(Manifest.permission.READ_PHONE_STATE, /*Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.MANAGE_EXTERNAL_STORAGE,*/ Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private val PERMISSION_REQUEST_CODE = 200
    private val viewDataModel: LoanDetailsViewModel by viewModels()
    lateinit var branchDetailsModel: BranchDetailsModel
    lateinit var loanDetailsModelResponse: LoanDetailsModelResponse

    private var lastClickTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, {
            loading = it
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()

                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle registrationModel response
        viewModel.getLogoutResponse.observe(viewLifecycleOwner) {
            registrationModel = it
            Timber.e("getLogoutResponse $registrationModel")

            if (registrationModel.status) {

                appPreferences!!.putBoolean(Constants.IsLogin, false)
                appPreferences!!.putString(Constants.Token, "")
                activity?.startActivity(Intent(requireContext(), LoginActivityNew::class.java))
                activity?.finish()
                // System.exit(0)

            } else {
                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
            }
        }

        //-- Handle templatelist response
        viewModel.sliderListResponse.observe(viewLifecycleOwner) {
            sliderModel = it
            Timber.e("sliderModel $sliderModel")

            if (sliderModel.status) {
                listTemplate = sliderModel.sliderList
                /* listBusinessDetails.clear()
                 for (item in listTemplate) {
                     listBusinessDetails.add(
                         BusinessCardModel(
                             "",
                             "",
                             "Name",
                             "Mobile",
                             "",
                             "Business Name",
                             "Address",
                             "",
                             "",
                             "",
                             item,
                             "",
                             ""
                         )
                     )
                 }
                 Timber.e("sliderModelisEdit $isEdit")
                 if (isEdit){
                     setTempleteData(recyclerUpdateBusinessCard, listBusinessDetails, false)
                 }else {
                     initBottomsheetForBusinessCards(0, listBusinessDetails)
                 }*/

                listBusinessDetails.clear()

                for (item in listTemplate) {
                    listBusinessDetails.add(

                        BusinessCardModel(
                            listBusinessDetailsTemp.id,
                            listBusinessDetailsTemp.lan,
                            listBusinessDetailsTemp.applicantName,
                            listBusinessDetailsTemp.mobile,
                            listBusinessDetailsTemp.dob,
                            listBusinessDetailsTemp.businessName,
                            listBusinessDetailsTemp.address1,
                            listBusinessDetailsTemp.address2,
                            listBusinessDetailsTemp.city,
                            listBusinessDetailsTemp.pincode,
                            item,
                            listBusinessDetailsTemp.updatedOn,
                            listBusinessDetailsTemp.createdOn
                        )
                    )
                }
                initBottomsheetForBusinessCards( listBusinessDetails)


            } else {
                showInternetConnectionLost(
                    requireContext(),
                    sliderModel.msg,
                    getString(R.string.ok)
                )
            }
        }

        //-- Handle businessDetailsList response
        viewModel.businessDetailsListResponse.observe(viewLifecycleOwner) {
            businessCardResponse = it
            Timber.e("businessDetailsList " + businessCardResponse)

            if (businessCardResponse.status) {
                if (businessCardResponse.sliderList!=null) {
                    listBusinessDetailsTemp=businessCardResponse.sliderList
                    getTemplateList()
                    /*listBusinessDetails.clear()

                    listBusinessDetails.addAll(businessCardResponse.sliderList)
                    for (i in 0 until listBusinessDetails.size) {
                        listBusinessDetails[i].image = selectedTemplate
                    }
                    setTempleteData(recyclerBusinessCard, listBusinessDetails, true)*/
                }

            } else {

                showInternetConnectionLost(
                    requireContext(),
                    businessCardResponse.msg,
                    getString(R.string.ok)
                )
            }
        }


        viewDataModel.getLoanDetails.observe(viewLifecycleOwner, {
            loanDetailsModelResponse = it
            Timber.e("loanDetailsModelResponse " + loanDetailsModelResponse)

            if (loanDetailsModelResponse.status) {
                getBranchDetails(loanDetailsModelResponse.loanDetails.get(0).loanId);
            } else {
                showInternetConnectionLost(
                    requireContext(), loanDetailsModelResponse.msg, getString(R.string.ok)
                )
            }
        })

        viewModel.getBranchDetailsResponse.observe(requireActivity()) {
            //if (this.lifecycle.currentState == Lifecycle.State.RESUMED) {
            branchDetailsModel = it
            //Timber.e("getBranchDetailsResponse " + branchDetailsModel)

            if (branchDetailsModel.status) {
                val messageTemplate = """
            Dear Customer,
        
            You can now visit your branch:
            ${branchDetailsModel.branchAddress}
            or
            contact ${branchDetailsModel.managerMobileNumber} for the issuance of Statement of Accounts.
        """.trimIndent()
                showInternetConnectionLost(
                    requireContext(), messageTemplate, getString(R.string.ok)
                )
            } else {
                showInternetConnectionLost(
                    requireContext(), branchDetailsModel.msg, getString(R.string.ok)
                )
            }
            // }
        }

        //-- Handle businessCardListResponse response
        /*viewModel.businessCardListResponse.observe(viewLifecycleOwner) {
            businessCardResponse = it
            Timber.e("businessCardList $businessCardResponse")

            if (businessCardResponse.status) {
                listBusinessDetails.clear()
                if (businessCardResponse.sliderList!=null) {
                    //listBusinessDetails.addAll(businessCardResponse.sliderList)
                    initBottomsheetForBusinessCards( listBusinessDetails)
                } else {
                    //getTemplateList()
                    getBusinessDetailsList()
                }

            } else {
                showInternetConnectionLost(
                    requireContext(),
                    businessCardResponse.msg,
                    getString(R.string.ok)
                )
            }
        }*/

        //-- Handle saveUpdateBusinessDetailsResponse response
        viewModel.saveUpdateBusinessDetailsResponse.observe(viewLifecycleOwner) {
            businessCardResponse = it
            Timber.e("saveUpdateBusinessDetails $businessCardResponse")

            if (businessCardResponse.status) {
                showAlertMessageSingleClick(
                    requireContext(),
                    getString(R.string.ok),
                    "Details updated successfully"
                ) { alertDialog, which ->
                    alertDialog.dismiss()
                    bottomShitDialogCard?.dismiss()
                    if (isEdit){
                        bottomShitDialog?.dismiss()
                    }

                    getBusinessDetailsList()
                }

            } else {
                showInternetConnectionLost(
                    requireContext(),
                    businessCardResponse.msg,
                    getString(R.string.ok)
                )
            }
        }

    }

    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        tvName.text = " " + appPreferences!!.getString(Constants.Name, "")
        if (appPreferences!!.getString(Constants.Ucic, "").toString().equals("null")) {
            tvUCIC.text = "N/A"
        } else {
            tvUCIC.text = " " + appPreferences!!.getString(Constants.Ucic, "")
        }
        tvProfMobile.text = " " + appPreferences!!.getString(Constants.MobileNo, "")
        tvVersion.text = "Version : " + appPreferences!!.getString(Constants.AppVersion, "")

        tvLanguage.setOnClickListener(this)

        tvBusinessCard.setOnClickListener(this)
        tvInformation.setOnClickListener(this)
        tvListOfCharges.setOnClickListener(this)
        tvRequestStatementAccount.setOnClickListener(this)
        tvServiceComplaint.setOnClickListener(this)
        //tvReferral.setOnClickListener(this)
        tvAboutUs.setOnClickListener(this)
        tvLogout.setOnClickListener(this)
        /*ivFB.setOnClickListener(this)
        ivLD.setOnClickListener(this)*/
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(view: View?) {

        when (view?.id) {
            R.id.tvBusinessCard -> getBusinessDetailsList()//getBusinessCardList()
            R.id.tvLanguage -> startActivity(
                Intent(
                    activity,
                    LanguageActivity::class.java
                ).putExtra(Constants.FromView, Constants.fromProfile)
            )
            R.id.tvListOfCharges -> startActivity(
                Intent(
                    activity,
                    ListOfChargesActivity::class.java
                ).putExtra(Constants.FromView, 0)
            )
            R.id.tvInformation -> startActivity(
                Intent(
                    activity,
                    ListOfChargesActivity::class.java
                ).putExtra(Constants.FromView, 1)
            )
            R.id.tvRequestStatementAccount -> {


                if (SystemClock.elapsedRealtime() - lastClickTime > 3000) {
                    lastClickTime = SystemClock.elapsedRealtime()
                    getLoanList()
                }

                /*        val transaction: FragmentTransaction =
                            requireActivity().supportFragmentManager.beginTransaction()
                        StatmentRequiestDialogFragment().apply {
                            show(transaction, tag)

                        }*/

            }
            R.id.tvServiceComplaint -> {


                val transaction: FragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                ServiceComplaintDialogFragment().apply {
                    show(transaction, tag)

                }

            }
            /*R.id.tvReferral -> {

                val transaction: FragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                ReferralDialogFragment().apply {
                    show(transaction, tag)

                }
            }*/

            R.id.tvAboutUs -> {

                val transaction: FragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                AboutUsDialogFragment().apply {
                    show(transaction, tag)
                }
            }

            R.id.tvLogout -> {
                val dialog =
                    MaterialAlertDialogBuilder(
                        requireContext(),
                        R.style.MyMaterialAlertDialog
                    ).create()
                dialog.setTitle(" ")
                dialog.setIcon(R.drawable.ic_new_logo_foreground)
                dialog.setMessage(getString(R.string.you_want_to_logout))
                dialog.setCancelable(false)
                dialog.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    resources.getString(R.string.logout)
                )
                { _, _ ->

                    if (isNetworkAvailable(requireContext())) {

                        /*val params: MutableMap<String, String> = HashMap()
                        params["mobileNo"] = appPreferences?.getString(Constants.MobileNo,"").toString()
                        params["dob"] = appPreferences?.getString(Constants.DOB,"").toString()*/

                        viewModel.logout(requireActivity())
                    }
                    dialog.dismiss()

                }
                dialog.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    resources.getString(R.string.cancel)
                )
                { _, _ ->
                    dialog.dismiss()
                    dialog.dismiss()
                }
                dialog.show()
            }

            /*  R.id.ivFB -> {
                  openFacebookUrl(requireContext())
              }

              R.id.ivLD -> {
                  openLinkedInUrl(requireContext())
              }*/

        }
    }


    private fun getLoanList() {

        if (isNetworkAvailable(requireContext())) {
            viewDataModel.getLoanDetails(requireActivity())
        }
    }


    private fun getBranchDetails(loadId: String) {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getBranchDetails(requireContext(), loadId)
        }
    }

    private fun getTemplateList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getTemplateList(requireActivity())
        }
    }

    private fun getBusinessDetailsList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getBusinessDetailsList(requireActivity())
        }
    }

    /* private fun getBusinessCardList() {
         if (isNetworkAvailable(requireContext())) {
             viewModel.getBusinessCardList(requireActivity())
         }
     }*/

    private fun saveUpdateBusinessDetailsList( businessCardModel: BusinessCardModel  ) {
        Timber.e("saveUpdateBusinessDetailsList  : $businessCardModel")


        if (ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val manager =
                requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                alertMessageNoGps(requireActivity(), "Yes")
            } else {
                var progressDialog = showProgressDialog(requireActivity())


                object : AsyncTaskCoroutine<Int?, Boolean?>() {
                    override fun onPostExecute(@Nullable result: Boolean?) {
                        progressDialog.dismiss()

                        if (isNetworkAvailable(requireContext())) {
                            viewModel.saveUpdateBusinessDetailsList(requireActivity(), businessCardModel)
                        }
                    }

                    override fun onPreExecute() {
                        progressDialog.show()
                        //getCurrentLatLong(requireActivity())
                        getLocation();
                    }

                    override fun doInBackground(vararg params: Int?): Boolean? {
                        return null
                    }
                }.execute<Any>()
            }
        }else{
            appPreferences?.putString(Constants.Latitude, "")
            appPreferences?.putString(Constants.Longitude, "")

            if (isNetworkAvailable(requireContext())) {
                viewModel.saveUpdateBusinessDetailsList(requireActivity(), businessCardModel)
            }
        }

        /*val manager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            alertMessageNoGps(requireContext(), "Yes")
        } else {
            getCurrentLatLong(requireContext())
            if (isNetworkAvailable(requireContext())) {
                viewModel.saveUpdateBusinessDetailsList(requireActivity(), businessCardModel)
            }
        }*/
    }


    private fun getLocation() {
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
        // tvGpsLocation = findViewById(R.id.textView)
        //tvGpsLocation.text = "Latitude: " + location.latitude + " , Longitude: " + location.longitude
    }

    private fun initBottomsheetForBusinessCards(

        listBusinessDetails: ArrayList<BusinessCardModel>
    ) {
        isEdit = false
        val modalbottomsheet: View = layoutInflater.inflate(R.layout.business_card_layout, null)

        bottomShitDialogCard = BottomSheetDialog(requireActivity())
        bottomShitDialogCard.setContentView(modalbottomsheet)
        bottomShitDialogCard.setCanceledOnTouchOutside(false)
        bottomShitDialogCard.setCancelable(false)
        bottomShitDialogCard.show()

        //val lnyCreatNewCardView = modalbottomsheet.findViewById<LinearLayout>(R.id.lnyCreatNewCardView)
        val lnyBtns = modalbottomsheet.findViewById<LinearLayout>(R.id.lnyBtns)


        // val recyclerNewBusinessCard = modalbottomsheet.findViewById<CarouselRecyclerview>(R.id.recyclerNewBusinessCard)
        val btnSelect = modalbottomsheet.findViewById<Button>(R.id.btnSelect)

        recyclerBusinessCard = modalbottomsheet.findViewById(R.id.recyclerBusinessCard)
        val btnEdit = modalbottomsheet.findViewById<ImageButton>(R.id.btnEdit)
        val btnDownload = modalbottomsheet.findViewById<ImageButton>(R.id.btnDownload)
        val btnShare = modalbottomsheet.findViewById<ImageButton>(R.id.btnShare)
        val txtTitle = modalbottomsheet.findViewById<TextView>(R.id.txtTitle)
        val txtCardMsg = modalbottomsheet.findViewById<TextView>(R.id.txtCardMsg)
        // Timber.e("fromView: "+fromView)
        setTempleteData(recyclerBusinessCard, listBusinessDetails )
        txtTitle.setOnClickListener {
            bottomShitDialogCard.cancel()
        }

        btnSelect.setOnClickListener {

            //selectedTemplate = listTemplate[recyclerBusinessCard.getSelectedPosition()]

            //call api for fetch business detail
            getBusinessDetailsList()
        }

        btnEdit.setOnClickListener {
            isEdit = true
            editBusinessCardDetails(listBusinessDetails[recyclerBusinessCard.getSelectedPosition()])
        }

        btnDownload.setOnClickListener {
            val bitmap = getBitmapFromView(
                getSelectedView(
                    recyclerBusinessCard,
                    recyclerBusinessCard.getSelectedPosition()
                )
            )
            saveImage(requireContext(), bitmap!!, SCREENSHOT_FILE_NAME)

        }
        btnShare.setOnClickListener {
            val bitmap = getBitmapFromView(
                getSelectedView(
                    recyclerBusinessCard,
                    recyclerBusinessCard.getSelectedPosition()
                )
            )
            shareCard(requireContext(), bitmap!!)
        }
    }

    private fun setTempleteData(
        recyclerView: CarouselRecyclerview?,
        listBusinessDetails: ArrayList<BusinessCardModel>
    ) {
        Timber.e("listBusinessDetails   ${listBusinessDetails.size}")
        var adapter1 = BusinessCardAdapter(requireContext(), listBusinessDetails)
        recyclerView?.apply {
            adapter = adapter1
            set3DItem(true)
            setAlpha(true)
            setInfinite(false)
            //setFlat(true)
            setIntervalRatio(0.4f)
            setIsScrollingEnabled(true)
        }
        //val carouselLayoutManager =  recyclerView!!.getCarouselLayoutManager()
        //val currentlyCenterPosition = recyclerView.getSelectedPosition()

        /*if (saveCard) {
            val listBusinessDetailsToSave = ArrayList<BusinessCardModel>()

            recyclerView?.viewTreeObserver?.addOnGlobalLayoutListener {
                Timber.e("convertToBaseCount ${recyclerView.childCount}")
                //for (i in 0 until recyclerView.childCount) {
                for (i in 0 until listBusinessDetails.size) {
                    var businessCardModel = listBusinessDetails[i]
                    Timber.e("convertToBase6411 $businessCardModel")
                    val bitmap = getBitmapFromView(
                        getSelectedView(
                            recyclerView, recyclerView.getSelectedPosition()
                        )
                    )

                    val base64String: String = convertToBase64(bitmap!!)
                    businessCardModel.image = base64String

                   // Timber.e("convertToBase64 $businessCardModel")
                    listBusinessDetailsToSave.add(businessCardModel)

                }
                //saveUpdateBusinessDetailsList(listBusinessDetailsToSave)
                recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        }*/
    }

    fun shareCard(context: Context, screenshotBitmap: Bitmap) {

        val cachePath = File(context.externalCacheDir, CACHE_DIRECTORY)
        cachePath.mkdirs()

        val screenshotFile = File(cachePath, SCREENSHOT_FILE_NAME).also { file ->
            FileOutputStream(file).use { fileOutputStream ->
                screenshotBitmap.compress(
                    Bitmap.CompressFormat.PNG,
                    100,
                    fileOutputStream
                )
            }
        }.apply {
            deleteOnExit()
        }

        val shareImageFileUri: Uri =
            FileProvider.getUriForFile(context, context.packageName + ".provider", screenshotFile)
        val shareMessage = "Your message that should get attached to the shared message."

        // Create the intent
        val intent = Intent(Intent.ACTION_SEND).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            putExtra(Intent.EXTRA_STREAM, shareImageFileUri)
            putExtra(Intent.EXTRA_TEXT, shareMessage)
            type = "image/png"
        }

        // Initialize the share chooser
        val chooserTitle: String = "Share your screenshot!"
        val chooser = Intent.createChooser(intent, chooserTitle)
        val resInfoList: List<ResolveInfo> =
            context.packageManager.queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY)
        for (resolveInfo in resInfoList) {
            val packageName: String = resolveInfo.activityInfo.packageName
            context.grantUriPermission(
                packageName,
                shareImageFileUri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }

        shareResult.launch(chooser)

    }

    private val shareResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            // Optional - called as soon as the user selects an option from the system share dialog
        }

    private fun editBusinessCardDetails(businessCardDataModel: BusinessCardModel) {
        Timber.e("Phone businessCardDataModel: " + businessCardDataModel)

        val modalbottomsheet: View =
            layoutInflater.inflate(R.layout.edit_business_card_details, null)

        bottomShitDialog = BottomSheetDialog(requireActivity())
        bottomShitDialog.setContentView(modalbottomsheet)
        bottomShitDialog.setCanceledOnTouchOutside(false)
        bottomShitDialog.setCancelable(false)
        bottomShitDialog.show()

        bottomShitDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        val bottomSheetBehavior = bottomShitDialog.behavior
        bottomSheetBehavior.isDraggable = false

        //getTemplateList()
        //recyclerUpdateBusinessCard = modalbottomsheet.findViewById(R.id.recyclerUpdateBusinessCard)

        val txtTitle: TextView? = modalbottomsheet.findViewById(R.id.txtTitle)
        val tfCardName: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardName)
        val tfCardMobileNo: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardMobileNo)
        val tfCardBusinessName: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardBusinessName)
        val tfCardAddress1: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardAddress1)
        val tfCardAddress2: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardAddress2)
        val tfCity: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCity)
        val tfPincode: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfPincode)
        val edtCardName: EditText? = modalbottomsheet.findViewById(R.id.edtCardName)
        val edtCardMobileNo: EditText? = modalbottomsheet.findViewById(R.id.edtCardMobileNo)
        val edtCardBusinessName: EditText? = modalbottomsheet.findViewById(R.id.edtCardBusinessName)
        val edtCardAddress1: EditText? = modalbottomsheet.findViewById(R.id.edtCardAddress1)
        val edtCardAddress2: EditText? = modalbottomsheet.findViewById(R.id.edtCardAddress2)
        val edtCity: EditText? = modalbottomsheet.findViewById(R.id.edtCity)
        val edtPincode: EditText? = modalbottomsheet.findViewById(R.id.edtPincode)
        val btnUpdateCard: Button? = modalbottomsheet.findViewById(R.id.btnUpdateCard)



        edtCardName?.setText(businessCardDataModel.applicantName)
        edtCardMobileNo?.setText(businessCardDataModel.mobile)
        edtCardBusinessName?.setText(businessCardDataModel.businessName)
        edtCardAddress1?.setText(businessCardDataModel.address1)
        edtCardAddress2?.setText(businessCardDataModel.address2)
        edtCity?.setText(businessCardDataModel.city)
        edtPincode?.setText(businessCardDataModel.pincode)

        txtTitle?.setOnClickListener {
            bottomShitDialog.dismiss()
        }

        btnUpdateCard?.setOnClickListener {
            if (isNetworkAvailable(requireContext())) {
                if (isEmpty(edtCardName!!)) {
                    tfCardName!!.setError(resources.getString(R.string.er_name))
                }else if (!isValidPhoneNumber(edtCardMobileNo!!)) {
                    tfCardMobileNo!!.setError(resources.getString(R.string.er_enter_mobile))
                }else if (isEmpty(edtCardBusinessName!!)) {
                    tfCardBusinessName!!.setError(resources.getString(R.string.er_b_name))
                }else if (isEmpty(edtCardAddress1!!)) {
                    tfCardAddress1!!.setError(resources.getString(R.string.er_address1))
                }else if (isEmpty(edtCardAddress2!!)) {
                    tfCardAddress2!!.setError(resources.getString(R.string.er_address2))
                }else if (isEmpty(edtCity!!)) {
                    tfCity!!.setError(resources.getString(R.string.er_city))
                }  else if (edtPincode!!.text.toString().length!=6||edtPincode!!.text.toString().contains("000000")) {

                    tfPincode!!.setError(resources.getString(R.string.er_pincode))

                } else {

                    //selectedTemplate = listTemplate[recyclerUpdateBusinessCard.getSelectedPosition()]
                    listBusinessDetails.clear()
                    businessCardDataModel.applicantName = edtCardName?.text.toString()
                    businessCardDataModel.mobile = edtCardMobileNo?.text.toString()
                    businessCardDataModel.businessName = edtCardBusinessName?.text.toString()
                    businessCardDataModel.address1 = edtCardAddress1?.text.toString()
                    businessCardDataModel.address2 = edtCardAddress2?.text.toString()
                    businessCardDataModel.city = edtCity?.text.toString()
                    businessCardDataModel.pincode = edtPincode?.text.toString()
                    //businessCardDataModel.image = selectedTemplate
                    //listBusinessDetails.add(businessCardDataModel)
                    Timber.e("Phone businessCardDataModel11: " + businessCardDataModel)
                    // setTempleteData(recyclerUpdateBusinessCard, listBusinessDetails, true)
                    saveUpdateBusinessDetailsList(businessCardDataModel)

                }
            }


            // bottomShitDialog?.dismiss()



        }

    }

    override fun onGlobalLayout() {

    }
}