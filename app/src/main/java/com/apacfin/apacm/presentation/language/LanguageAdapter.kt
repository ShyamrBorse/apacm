package com.apacfin.apacm.presentation.language

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.language.LanguageModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class LanguageAdapter(
    context: Context, languageModels1: List<LanguageModel>,
    onClickListener: OnClickListener
) : RecyclerView.Adapter<LanguageAdapter.ViewHolder?>() {
    var context: Context

    private val mOnClickListener: OnClickListener
    var appPreferences: AppPreferences? = AppPreferences.getAppPreferences(context)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_language, parent, false)
        return ViewHolder(view, mOnClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val languageModel: LanguageModel = languageModels[position]
        Timber.e(TAG+"onBindViewHolder languageModel getSelected : " + selectedPosition)
        holder.rgbLang.isChecked = position == selectedPosition

        holder.tvLangTital.text = "" + languageModel.codeValue
        holder.tvLangSubTitle.text = "(" + languageModel.codeValueName + ")"
        holder.rgbLang.setOnClickListener { // languageModels.get(position).setSelected(true);
            selectedPosition = holder.adapterPosition
            notifyDataSetChanged()
        }

        holder.rlyLang.setOnClickListener { // languageModels.get(position).setSelected(true);
            selectedPosition = holder.adapterPosition
            notifyDataSetChanged()
        }
        Timber.e(TAG+"onBindViewHol SetLanguage : " + languageModel.code +","+appPreferences?.getString(Constants.SetLanguage, ""))

        //notifyItemChanged(position, pdQuestionsModel);
    }

    override fun getItemCount(): Int {
        return languageModels.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
      
    }
   
    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(itemView: View, onClickListener: OnClickListener) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener ,
        OnStatusClickListener {
        var rlyLang: RelativeLayout
        var tvLangTital: TextView
        var tvLangSubTitle: TextView
        var rgbLang: RadioButton
        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {
            Timber.e(TAG+"onClick: " + selectedPosition)
            mOnClickListener.onLeadClick(selectedPosition)
        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            rlyLang = itemView.findViewById(R.id.rlyLang)
            tvLangTital = itemView.findViewById(R.id.tvLangTital)
            tvLangSubTitle = itemView.findViewById(R.id.tvLangSubTitle)
            rgbLang = itemView.findViewById(R.id.rgbLang)
            mOnClickListener = onClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = LanguageAdapter::class.java.simpleName
        lateinit var languageModels: List<LanguageModel>
        var selectedPosition = -1
    }

    init {
        languageModels = languageModels1
        mOnClickListener = onClickListener
        this.context = context
    }
}
