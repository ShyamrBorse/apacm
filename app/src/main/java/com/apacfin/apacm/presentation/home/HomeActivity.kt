package com.apacfin.apacm.presentation.home

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.apacfin.apacm.R
import androidx.navigation.ui.NavigationUI
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.presentation.registrationprocess.splash.registration.RegistrationActivity
import com.apacfin.apacm.util.LocaleHelper
import com.apacfin.apacm.util.LogOutTimerUtil
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import com.apacfin.apacm.util.LogOutTimerUtil.startLogoutTimer
import com.apacfin.apacm.util.isNetworkAvailable
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import timber.log.Timber
import java.util.HashMap




@AndroidEntryPoint
class HomeActivity : AppCompatActivity(), LogOutTimerUtil.LogOutListener {
    val TAG = HomeActivity::class.java.simpleName


    var appPreferences: AppPreferences? = null
    private var pressedTime: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initView()
    }
    override fun onBackPressed() {
        val idItem: Int = bttm_nav.getSelectedItemId()
        if (R.id.frgHome === idItem) {

            val dialog = MaterialAlertDialogBuilder(this, R.style.MyMaterialAlertDialog).create()
            dialog.setTitle(" ")
            dialog.setIcon(R.drawable.ic_new_logo_foreground)
            dialog.setMessage(getString(R.string.exti_msg))
            dialog.setCancelable(false)
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok)) { _, _ ->
                dialog.dismiss()
                finish()
            }
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE,getString(R.string.cancel),
                { _, _ ->
                    dialog.dismiss()
                })
            dialog.show()

        } else {
            super.onBackPressed() // this line make back action
        }

    }
    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(this@HomeActivity)
        setUpNavigation()

    }

    private fun setUpNavigation() {

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        NavigationUI.setupWithNavController(bttm_nav!!,navHostFragment!!.navController)
       // toolbarMain.inflateMenu(R.menu.option_menu)
       // toolbarMain.title = appPreferences?.getString(Constants.Name,"")
        /*toolbar.setupWithNavController( navController = navHostFragment.navController )
        navHostFragment.navController.addOnDestinationChangedListener { _, destination, _ ->
            toolbar.title = destination.label
        }*/
    }
     /*override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }*/
    override fun onStart() {
        super.onStart()
        startLogoutTimer(this, this)
        Timber.e("OnStart () &&& Starting timer")
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        startLogoutTimer(this, this)
        Timber.e( "User interacting with screen")
    }


    override fun onPause() {
        super.onPause()
        Timber.e( "onPause()")
    }

    override fun onResume() {
        super.onResume()
        Timber.e("onResume()")
    }

    /**
     * Performing idle time logout
     */
    override fun doLogout() {
        // write your stuff here
        Timber.e("doLogout()")
        /*Handler(Looper.getMainLooper()).post {
            // any UI update here
            val dialog = MaterialAlertDialogBuilder(this, R.style.MyMaterialAlertDialog).create()
            dialog.setTitle(" ")
            dialog.setIcon(R.drawable.ic_new_logo_foreground)
            dialog.setMessage(getString(R.string.session))
            dialog.setCancelable(false)
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, resources.getString(R.string.ok))
            { _, _ ->

                if (isNetworkAvailable(this)) {

                    *//* val params: MutableMap<String, String> = HashMap()
                     params["mobileNo"] = appPreferences?.getString(Constants.MobileNo,"").toString()
                     params["dob"] = appPreferences?.getString(Constants.DOB,"").toString()
                     viewModel.logout(params)*//*
                }
                dialog.dismiss()
            }
            dialog.show()
        }*/
    }
}