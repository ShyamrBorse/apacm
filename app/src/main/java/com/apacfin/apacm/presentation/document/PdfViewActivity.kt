package com.apacfin.apacm.presentation.document

import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.myloan.LoanDetailsViewModel
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.document.DocViewModelResponse
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.github.barteksc.pdfviewer.util.FitPolicy
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_pdf_view.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber
import java.lang.Byte.decode
import java.util.*

@AndroidEntryPoint
class PdfViewActivity : AppCompatActivity() {

    val TAG = PdfViewActivity::class.java.simpleName
    private val viewModel: LoanDetailsViewModel by viewModels()
    var loading = false
    lateinit var docViewModelResponse: DocViewModelResponse
    var appPreferences: AppPreferences? = null
    var docID:Int=0
    var loanID:String=""
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_view)
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        initView()
        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, {
            loading = it
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getLoanDetails response
        viewModel.getDocumentResponse.observe(this, {
            docViewModelResponse = it
           // Timber.e("docViewModelResponse " + docViewModelResponse)

            if (docViewModelResponse.status) {

                storePdfandOpen(docViewModelResponse.document.file)
                toolbar.title = docViewModelResponse.document.name
            } else {
                showInternetConnectionLost(
                    this,
                    docViewModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })
    }

    private fun initView() {
        docID = intent.getIntExtra(Constants.DocID,0)
        loanID = intent.getStringExtra(Constants.LoanID).toString()

        getDocument(docID,loanID)
        Timber.e(TAG+" docID: "+docID+" loanID: "+loanID )
        appPreferences = AppPreferences.getAppPreferences(this)

        toolbar.navigationIcon = ResourcesCompat.getDrawable(resources,R.drawable.ic_baseline_arrow_back_24,null)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        //toolbar.title = resources.getString(R.string.a_c_statment)


    }

    private fun getDocument(docID:Int,loanID:String) {
        if (isNetworkAvailable(this)) {
              viewModel.getDocument(this,docID.toString(),loanID)
        }
    }


    private fun storePdfandOpen(base: String?) {
        /*val myDirectory = File( Environment.getExternalStorageDirectory(), Constants.directoryName + "/ApacmDoc")
        if (!myDirectory.exists()) {
            myDirectory.mkdirs()
        }
        val generator = Random()
        var n = 10000
        n = generator.nextInt(n)

        val fname = "apacdoc-$n.pdf"
        val file = File(myDirectory, fname)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            val pdfAsBytes: ByteArray = Base64.decode(base, 0)
            out.write(pdfAsBytes)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }*/
        val decodedBytes = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
           Base64.getDecoder().decode(base)
        } else {

            android.util.Base64.decode(base, android.util.Base64.NO_WRAP)
        }

        Timber.e(TAG+"decodedBytes: "+decodedBytes)
        // pdfView.fromFile(file)
            pdfView.fromBytes(decodedBytes)
        //pdfView.fromBytes(base)
            .password(null)
            .defaultPage(0)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .fitEachPage(true)
            .pageFling(true)
            .pageFitPolicy(FitPolicy.BOTH)
            .enableAntialiasing(true)
            .enableAnnotationRendering(true)
            .spacing(1)
            .load()
    }

}