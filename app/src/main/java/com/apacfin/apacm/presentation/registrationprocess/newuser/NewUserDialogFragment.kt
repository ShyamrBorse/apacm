package com.apacfin.apacm.presentation.registrationprocess.newuser

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.otp.OtpDialogFragment
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.isEmpty
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_new_user_dialog.*
import timber.log.Timber
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_MOBILENO = "param1"
private const val ARG_DOB = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [NewUserDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class NewUserDialogFragment : DialogFragment() {

    val TAG = NewUserDialogFragment::class.java.simpleName
    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    private var mobileNo: String? = null
    private var dob: String? = null
    private var name: String = ""
    private var refNo: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mobileNo = it.getString(ARG_MOBILENO)
            dob = it.getString(ARG_DOB)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE);
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_user_dialog, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edtMobileNo.setText(mobileNo)
        edtDob.setText(dob)
        ivCloseDialog.setOnClickListener {
            dialog?.dismiss()
        }
        btnSubmit.setOnClickListener {
            validation()
        }
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(this, Observer {
            loading = it;
            Timber.e( "SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle newUser response
        viewModel.newUser.observe(this, {
            registrationModel = it
            Timber.e("newUser " + registrationModel)

            if (registrationModel.status) {
                refNo = registrationModel.refNo
                name = registrationModel.name
                val transaction: FragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                val bundle = Bundle()

                bundle.putString(Constants.Name, name)
                bundle.putString(Constants.MobileNo, mobileNo)
                bundle.putString(Constants.DOB, dob)
                bundle.putString(Constants.RefNo, refNo)
                bundle.putString(Constants.Fname, edtFname.text.toString())
                bundle.putString(Constants.Lname, edtLname.text.toString())
                bundle.putString(Constants.Pincode, edtPincode.text.toString())
                bundle.putBoolean(Constants.FromView, true)
                OtpDialogFragment().apply {
                    show(transaction, tag)
                    arguments = bundle
                }


            } else {
                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
                //  showSnackBar(this, getDetailsFromUCIC.msg) }
                //startActivity(Intent(this, MainActivity::class.java))
            }
        })

    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout(width - 80, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
    private fun validation() {
        if (isNetworkAvailable(requireContext())) {
            if (isEmpty(edtFname)) {
                tfFname.setError(resources.getString(R.string.er_first_name))
            }else if (isEmpty(edtFname)) {
                tfLname.setError(resources.getString(R.string.er_last_name))
            }else if (isEmpty(edtDob)) {

                tfDob.setError(resources.getString(R.string.er_enter_dob))

            }else if (isEmpty(edtPincode)) {

                tfPincode.setError(resources.getString(R.string.er_pincode))

            } else {


                val params: MutableMap<String, String> = HashMap()
                params["fName"] = edtFname.text.toString()
                params["lName"] = edtLname.text.toString()
                params["pinCode"] = edtPincode.text.toString()
                params["mobileNo"] = mobileNo.toString()
                params["dob"] = dob.toString()
                viewModel.newUser(requireActivity(),params)

            }
        }
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NewUserDialogFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NewUserDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_MOBILENO, param1)
                    putString(ARG_DOB, param2)
                }
            }
    }
}