package com.apacfin.apacm.presentation.alert

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.apacfin.apacm.R
import com.apacfin.data.model.alert.AlertModelResponse
import com.apacfin.data.model.home.BannerModel
import com.apacfin.data.model.home.SliderModel
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.AlertRepository
import com.apacfin.domain.repository.HomeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : HomeViewModel.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@HiltViewModel
class AlertViewModel @Inject constructor(
    private val repository: AlertRepository
) : ViewModel() {

    val _getAlertListResponse: MutableLiveData<AlertModelResponse> = MutableLiveData()

    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //sliderList responce
    val getAlertListResponse: LiveData<AlertModelResponse> get() = _getAlertListResponse


    //getAlertList api
    fun getAlertList(context:Context) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getAlertList()) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("_getAlertListResponse"  + response.value.msg)
                _getAlertListResponse.postValue(response.value)

            }

            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("_getAlertListResponse"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var bannerModel =  AlertModelResponse(
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false)
                _getAlertListResponse.postValue(bannerModel)
            }
        }

    }





}