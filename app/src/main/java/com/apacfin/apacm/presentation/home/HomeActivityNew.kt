package com.apacfin.apacm.presentation.home

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.profile.BusinessCardAdapter
import com.apacfin.apacm.presentation.profile.CACHE_DIRECTORY
import com.apacfin.apacm.presentation.profile.SCREENSHOT_FILE_NAME
import com.apacfin.apacm.presentation.referral.ReferralDialogFragment
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.util.*
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.data.model.businesscard.BusinessCardResponse
import com.apacfin.data.model.home.BranchDetailsModel
import com.apacfin.data.model.home.SliderModel
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import com.jackandphantom.carouselrecyclerview.CarouselRecyclerview
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home_new.*
import kotlinx.android.synthetic.main.activity_login_mpin.*
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream

@AndroidEntryPoint
class HomeActivityNew : AppCompatActivity(), View.OnClickListener, MenuItemClickListener,
    LocationListener {

    private val viewModel: HomeViewModel by viewModels()
    lateinit var navController: NavController
    var appPreferences: AppPreferences? = null
    private var pressedTime: Long = 0


    lateinit var businessCardResponse: BusinessCardResponse
    lateinit var listBusinessDetailsTemp: BusinessCardModel
    lateinit var sliderModel: SliderModel
    var listTemplate = listOf<String>()
    val listBusinessDetails = ArrayList<BusinessCardModel>()

    var isEdit: Boolean = false
    lateinit var recyclerBusinessCard: CarouselRecyclerview

    //lateinit var recyclerUpdateBusinessCard: CarouselRecyclerview
    lateinit var bottomShitDialog: BottomSheetDialog
    lateinit var bottomShitDialogCard: BottomSheetDialog
    var loading = false
    lateinit var branchDetailsModel: BranchDetailsModel
    private lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_new)

        initView()
        navController = findNavController(R.id.home_nav_host_fragment)
    }

    fun initView() {

        ivProfile.setOnClickListener(this)
        ivHome.setOnClickListener(this)
        ivAlert.setOnClickListener(this)
//        val navController = findNavController(R.id.home_nav_host_fragment)
//        navController.navigate(R.id.frgHome)


        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, {
            loading = it
            if (loading) {
                progressDialog.show()
            } else {
                progressDialog.dismiss()
            }
        })


        //-- Handle businessDetailsList response
        viewModel.businessDetailsListResponse.observe(this) {
            //if (this.lifecycle.currentState == Lifecycle.State.RESUMED) {
            businessCardResponse = it
            Timber.e("businessDetailsList " + businessCardResponse)

            if (businessCardResponse!=null && businessCardResponse.status) {
                if (businessCardResponse.sliderList != null) {
                    listBusinessDetailsTemp = businessCardResponse.sliderList
                    getTemplateList()
                    /*listBusinessDetails.clear()
                listBusinessDetails.addAll(businessCardResponse.sliderList)
                for (i in 0 until listBusinessDetails.size) {
                    listBusinessDetails[i].image = selectedTemplate
                }
                setTempleteData(recyclerBusinessCard, listBusinessDetails, true)*/
                }else{
                    com.apacfin.apacm.util.showToast(this, "Yet Loan Details Not Assigned!")
                }

            } else {
                showInternetConnectionLost(
                    this,
                    businessCardResponse.msg,
                    getString(R.string.ok)
                )
            }
            //}
        }

        //-- Handle templatelist response
        viewModel.sliderListResponse.observe(this) {
            if (this.lifecycle.currentState == Lifecycle.State.RESUMED) {
                sliderModel = it
                Timber.e("sliderModel $sliderModel")

                if (sliderModel.status) {
                    listTemplate = sliderModel.sliderList

                    listBusinessDetails.clear()

                    for (item in listTemplate) {
                        listBusinessDetails.add(

                            BusinessCardModel(
                                listBusinessDetailsTemp.id,
                                listBusinessDetailsTemp.lan,
                                listBusinessDetailsTemp.applicantName,
                                listBusinessDetailsTemp.mobile,
                                listBusinessDetailsTemp.dob,
                                listBusinessDetailsTemp.businessName,
                                listBusinessDetailsTemp.address1,
                                listBusinessDetailsTemp.address2,
                                listBusinessDetailsTemp.city,
                                listBusinessDetailsTemp.pincode,
                                item,
                                listBusinessDetailsTemp.updatedOn,
                                listBusinessDetailsTemp.createdOn
                            )
                        )
                    }
                    initBottomsheetForBusinessCards(listBusinessDetails)


                } else {
                    showInternetConnectionLost(
                        this,
                        sliderModel.msg,
                        getString(R.string.ok)
                    )
                }
            }
        }

        //-- Handle saveUpdateBusinessDetailsResponse response
        viewModel.saveUpdateBusinessDetailsResponse.observe(this) {
            //if (this.lifecycle.currentState == Lifecycle.State.RESUMED) {
            businessCardResponse = it
            Timber.e("saveUpdateBusinessDetails $businessCardResponse")

            if (businessCardResponse.status) {
                showAlertMessageSingleClick(
                    this,
                    getString(R.string.ok),
                    "Details updated successfully"
                ) { alertDialog, which ->
                    alertDialog.dismiss()
                    bottomShitDialogCard?.dismiss()
                    if (isEdit) {
                        bottomShitDialog?.dismiss()
                    }

                    getBusinessDetailsList()
                }

            } else {
                showInternetConnectionLost(
                    this,
                    businessCardResponse.msg,
                    getString(R.string.ok)
                )
            }
            //}
        }

        //-- Handle get loan emi history response

        viewModel.getBranchDetailsResponse.observe(this, {
            //if (this.lifecycle.currentState == Lifecycle.State.RESUMED) {
            branchDetailsModel = it
            //Timber.e("getBranchDetailsResponse " + branchDetailsModel)

            if (branchDetailsModel.status == true) {
                branchDetailsDialog(branchDetailsModel)
            } else {
                showInternetConnectionLost(
                    this,
                    branchDetailsModel.msg.toString(),
                    getString(R.string.ok)
                )
            }
            // }
        })

    }

    override fun onBackPressed() {
        if ((navController.currentBackStackEntry?.destination as FragmentNavigator.Destination).className.equals(
                "com.apacfin.apacm.presentation.home.HomeFragmentNew"
            )
        ) {
            val dialog = MaterialAlertDialogBuilder(this, R.style.MyMaterialAlertDialog).create()
            dialog.setTitle(" ")
            dialog.setIcon(R.drawable.ic_new_logo_foreground)
            dialog.setMessage(getString(R.string.exti_msg))
            dialog.setCancelable(false)
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok)) { _, _ ->
                dialog.dismiss()
                super.onBackPressed()
            }
            dialog.setButton(
                DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel),
                { _, _ ->
                    dialog.dismiss()
                })
            dialog.show()
        } else if ((navController.currentBackStackEntry?.destination as FragmentNavigator.Destination).className.equals(
                "com.apacfin.apacm.presentation.profile.ProfileFragment"
            )
        ) {
            navController.backQueue.clear()
            navController.navigate(R.id.frgHomeNew)
        } else if ((navController.currentBackStackEntry?.destination as FragmentNavigator.Destination).className.equals(
                "com.apacfin.apacm.presentation.alert.AlertFragment"
            )
        ) {
            navController.backQueue.clear()
            navController.navigate(R.id.frgHomeNew)
        } else if ((navController.currentBackStackEntry?.destination as FragmentNavigator.Destination).className.equals(
                "com.apacfin.apacm.presentation.document.DocumentFragment"
            )
        ) {
            navController.backQueue.clear()
            navController.navigate(R.id.frgHomeNew)
        } else if ((navController.currentBackStackEntry?.destination as FragmentNavigator.Destination).className.equals(
                "com.apacfin.apacm.presentation.myloan.MyLoanFragment"
            )
        ) {
            navController.backQueue.clear()
            navController.navigate(R.id.frgHomeNew)
        } else if (((navController.currentBackStackEntry?.destination as FragmentNavigator.Destination).className.equals(
                "com.apacfin.apacm.presentation.statement.StatmentFragment"
            ))
        ) {
            navController.backQueue.clear()
            navController.navigate(R.id.frgMyLoan)
        }
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.ivProfile -> {
                //val navController = findNavController(R.id.home_nav_host_fragment)
                navController.navigate(R.id.frgProfile)
            }
            R.id.ivHome -> {
                navController.navigate(R.id.frgHomeNew)
            }

            R.id.ivAlert -> {
                navController.navigate(R.id.frgAlert)
            }
        }
    }

    override fun onDashboardMenuItemClickListener(
        context: Context,
        dashBoardModel: DashBoardModel, loanDetailsList: List<LoanDetailModel>, loanId: String
    ) {
        if (dashBoardModel.card_id.equals(Constants.ReferandEarn)) {
            val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            ReferralDialogFragment().apply {
                show(transaction, tag)
            }
        }



        if (dashBoardModel.card_id.equals(Constants.PayEMI)) {
            navController.navigate(R.id.frgMyLoan)
        }
        if (dashBoardModel.card_id.equals(Constants.Documents)) {
            navController.navigate(R.id.frgDocument)
        }
        if (dashBoardModel.card_id.equals(Constants.LoanViewDetails)) {
            navController.navigate(R.id.frgMyLoan)
        }

        if (dashBoardModel.card_id.equals(Constants.GenerateVCard)) {
            getBusinessDetailsList()
        }

        if (dashBoardModel.card_id.equals(Constants.BranchConnect)) {
            if(loanId!=null && (!loanId.equals(""))) {
                getBranchDetails(loanId)
            }else{
                com.apacfin.apacm.util.showToast(context, "Yet Loan Details Not Assigned!")
            }
        }
    }

    private fun getBranchDetails(loadId: String) {
        if (isNetworkAvailable(this)) {
            //viewModel.sliderList(requireActivity())
            viewModel.getBranchDetails(this, loadId)
        }
    }

    private fun getBusinessDetailsList() {
        if (isNetworkAvailable(this)) {
            viewModel.getBusinessDetailsList(this)
        }
    }

    private fun getTemplateList() {
        if (isNetworkAvailable(this)) {
            viewModel.getTemplateList(this)
        }
    }

    private fun initBottomsheetForBusinessCards(

        listBusinessDetails: ArrayList<BusinessCardModel>
    ) {
        isEdit = false
        val modalbottomsheet: View = layoutInflater.inflate(R.layout.business_card_layout, null)

        bottomShitDialogCard = BottomSheetDialog(this)
        bottomShitDialogCard.setContentView(modalbottomsheet)
        bottomShitDialogCard.setCanceledOnTouchOutside(false)
        bottomShitDialogCard.setCancelable(false)
        bottomShitDialogCard.show()

        //val lnyCreatNewCardView = modalbottomsheet.findViewById<LinearLayout>(R.id.lnyCreatNewCardView)
        val lnyBtns = modalbottomsheet.findViewById<LinearLayout>(R.id.lnyBtns)


        // val recyclerNewBusinessCard = modalbottomsheet.findViewById<CarouselRecyclerview>(R.id.recyclerNewBusinessCard)
        val btnSelect = modalbottomsheet.findViewById<Button>(R.id.btnSelect)

        recyclerBusinessCard = modalbottomsheet.findViewById(R.id.recyclerBusinessCard)
        val btnEdit = modalbottomsheet.findViewById<ImageButton>(R.id.btnEdit)
        val btnDownload = modalbottomsheet.findViewById<ImageButton>(R.id.btnDownload)
        val btnShare = modalbottomsheet.findViewById<ImageButton>(R.id.btnShare)
        val txtTitle = modalbottomsheet.findViewById<TextView>(R.id.txtTitle)
        val txtCardMsg = modalbottomsheet.findViewById<TextView>(R.id.txtCardMsg)
        // Timber.e("fromView: "+fromView)
        setTempleteData(recyclerBusinessCard, listBusinessDetails)
        txtTitle.setOnClickListener {
            bottomShitDialogCard.cancel()
        }

        btnSelect.setOnClickListener {

            //selectedTemplate = listTemplate[recyclerBusinessCard.getSelectedPosition()]

            //call api for fetch business detail
            getBusinessDetailsList()
        }

        btnEdit.setOnClickListener {
            isEdit = true
            editBusinessCardDetails(listBusinessDetails[recyclerBusinessCard.getSelectedPosition()])
        }

        btnDownload.setOnClickListener {
            val bitmap = getBitmapFromView(
                getSelectedView(
                    recyclerBusinessCard,
                    recyclerBusinessCard.getSelectedPosition()
                )
            )
            saveImage(this, bitmap!!, SCREENSHOT_FILE_NAME)

        }
        btnShare.setOnClickListener {
            val bitmap = getBitmapFromView(
                getSelectedView(
                    recyclerBusinessCard,
                    recyclerBusinessCard.getSelectedPosition()
                )
            )
            shareCard(this, bitmap!!)
        }
    }

    private fun setTempleteData(
        recyclerView: CarouselRecyclerview?,
        listBusinessDetails: ArrayList<BusinessCardModel>
    ) {
        Timber.e("listBusinessDetails   ${listBusinessDetails.size}")
        var adapter1 = BusinessCardAdapter(this, listBusinessDetails)
        recyclerView?.apply {
            adapter = adapter1
            set3DItem(true)
            setAlpha(true)
            setInfinite(false)
            //setFlat(true)
            setIntervalRatio(0.4f)
            setIsScrollingEnabled(true)
        }
    }

    private fun editBusinessCardDetails(businessCardDataModel: BusinessCardModel) {
        Timber.e("Phone businessCardDataModel: " + businessCardDataModel)

        val modalbottomsheet: View =
            layoutInflater.inflate(R.layout.edit_business_card_details, null)

        bottomShitDialog = BottomSheetDialog(this)
        bottomShitDialog.setContentView(modalbottomsheet)
        bottomShitDialog.setCanceledOnTouchOutside(false)
        bottomShitDialog.setCancelable(false)
        bottomShitDialog.show()

        bottomShitDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        val bottomSheetBehavior = bottomShitDialog.behavior
        bottomSheetBehavior.isDraggable = false

        //getTemplateList()
        //recyclerUpdateBusinessCard = modalbottomsheet.findViewById(R.id.recyclerUpdateBusinessCard)

        val txtTitle: TextView? = modalbottomsheet.findViewById(R.id.txtTitle)
        val tfCardName: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardName)
        val tfCardMobileNo: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardMobileNo)
        val tfCardBusinessName: TextInputLayout? =
            modalbottomsheet.findViewById(R.id.tfCardBusinessName)
        val tfCardAddress1: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardAddress1)
        val tfCardAddress2: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCardAddress2)
        val tfCity: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfCity)
        val tfPincode: TextInputLayout? = modalbottomsheet.findViewById(R.id.tfPincode)
        val edtCardName: EditText? = modalbottomsheet.findViewById(R.id.edtCardName)
        val edtCardMobileNo: EditText? = modalbottomsheet.findViewById(R.id.edtCardMobileNo)
        val edtCardBusinessName: EditText? = modalbottomsheet.findViewById(R.id.edtCardBusinessName)
        val edtCardAddress1: EditText? = modalbottomsheet.findViewById(R.id.edtCardAddress1)
        val edtCardAddress2: EditText? = modalbottomsheet.findViewById(R.id.edtCardAddress2)
        val edtCity: EditText? = modalbottomsheet.findViewById(R.id.edtCity)
        val edtPincode: EditText? = modalbottomsheet.findViewById(R.id.edtPincode)
        val btnUpdateCard: Button? = modalbottomsheet.findViewById(R.id.btnUpdateCard)



        edtCardName?.setText(businessCardDataModel.applicantName)
        edtCardMobileNo?.setText(businessCardDataModel.mobile)
        edtCardBusinessName?.setText(businessCardDataModel.businessName)
        edtCardAddress1?.setText(businessCardDataModel.address1)
        edtCardAddress2?.setText(businessCardDataModel.address2)
        edtCity?.setText(businessCardDataModel.city)
        edtPincode?.setText(businessCardDataModel.pincode)

        txtTitle?.setOnClickListener {
            bottomShitDialog.dismiss()
        }

        btnUpdateCard?.setOnClickListener {
            if (isNetworkAvailable(this)) {
                if (isEmpty(edtCardName!!)) {
                    tfCardName!!.setError(resources.getString(R.string.er_name))
                } else if (!isValidPhoneNumber(edtCardMobileNo!!)) {
                    tfCardMobileNo!!.setError(resources.getString(R.string.er_enter_mobile))
                } else if (isEmpty(edtCardBusinessName!!)) {
                    tfCardBusinessName!!.setError(resources.getString(R.string.er_b_name))
                } else if (isEmpty(edtCardAddress1!!)) {
                    tfCardAddress1!!.setError(resources.getString(R.string.er_address1))
                } else if (isEmpty(edtCardAddress2!!)) {
                    tfCardAddress2!!.setError(resources.getString(R.string.er_address2))
                } else if (isEmpty(edtCity!!)) {
                    tfCity!!.setError(resources.getString(R.string.er_city))
                } else if (edtPincode!!.text.toString().length != 6 || edtPincode!!.text.toString()
                        .contains("000000")
                ) {

                    tfPincode!!.setError(resources.getString(R.string.er_pincode))

                } else {

                    //selectedTemplate = listTemplate[recyclerUpdateBusinessCard.getSelectedPosition()]
                    listBusinessDetails.clear()
                    businessCardDataModel.applicantName = edtCardName?.text.toString()
                    businessCardDataModel.mobile = edtCardMobileNo?.text.toString()
                    businessCardDataModel.businessName = edtCardBusinessName?.text.toString()
                    businessCardDataModel.address1 = edtCardAddress1?.text.toString()
                    businessCardDataModel.address2 = edtCardAddress2?.text.toString()
                    businessCardDataModel.city = edtCity?.text.toString()
                    businessCardDataModel.pincode = edtPincode?.text.toString()
                    //businessCardDataModel.image = selectedTemplate
                    //listBusinessDetails.add(businessCardDataModel)
                    Timber.e("Phone businessCardDataModel11: " + businessCardDataModel)
                    // setTempleteData(recyclerUpdateBusinessCard, listBusinessDetails, true)
                    saveUpdateBusinessDetailsList(businessCardDataModel)

                }
            }
            // bottomShitDialog?.dismiss()
        }
    }

    private fun saveUpdateBusinessDetailsList(businessCardModel: BusinessCardModel) {
        Timber.e("saveUpdateBusinessDetailsList  : $businessCardModel")

        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val manager =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                alertMessageNoGps(this@HomeActivityNew, "Yes")
            } else {
                var progressDialog = showProgressDialog(this@HomeActivityNew)


                object : AsyncTaskCoroutine<Int?, Boolean?>() {
                    override fun onPostExecute(@Nullable result: Boolean?) {
                        progressDialog.dismiss()
                        Log.d("Latitude Longitude", appPreferences?.getString(Constants.Latitude, "").toString())
                        Log.d("Latitude Longitude", appPreferences?.getString(Constants.Longitude, "").toString())

                        //getCurrentLatLong(requireActivity())
                        if (isNetworkAvailable(this@HomeActivityNew)) {
                            viewModel.saveUpdateBusinessDetailsList(this@HomeActivityNew, businessCardModel)
                        }
                    }

                    override fun onPreExecute() {
                        progressDialog.show()
                        //getCurrentLatLong(requireActivity())
                        getLocation();
                    }

                    override fun doInBackground(vararg params: Int?): Boolean? {
                        return null
                    }
                }.execute<Any>()

            }
        }else{
            appPreferences?.putString(Constants.Latitude, "")
            appPreferences?.putString(Constants.Longitude, "")

            //getCurrentLatLong(requireActivity())
            if (isNetworkAvailable(this@HomeActivityNew)) {
                viewModel.saveUpdateBusinessDetailsList(this@HomeActivityNew, businessCardModel)
            }
        }
    }

    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    fun shareCard(context: Context, screenshotBitmap: Bitmap) {

        val cachePath = File(context.externalCacheDir, CACHE_DIRECTORY)
        cachePath.mkdirs()

        val screenshotFile = File(cachePath, SCREENSHOT_FILE_NAME).also { file ->
            FileOutputStream(file).use { fileOutputStream ->
                screenshotBitmap.compress(
                    Bitmap.CompressFormat.PNG,
                    100,
                    fileOutputStream
                )
            }
        }.apply {
            deleteOnExit()
        }

        val shareImageFileUri: Uri =
            FileProvider.getUriForFile(context, context.packageName + ".provider", screenshotFile)
        val shareMessage = "Your message that should get attached to the shared message."

        // Create the intent
        val intent = Intent(Intent.ACTION_SEND).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            putExtra(Intent.EXTRA_STREAM, shareImageFileUri)
            putExtra(Intent.EXTRA_TEXT, shareMessage)
            type = "image/png"
        }

        // Initialize the share chooser
        val chooserTitle: String = "Share your screenshot!"
        val chooser = Intent.createChooser(intent, chooserTitle)
        val resInfoList: List<ResolveInfo> =
            context.packageManager.queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY)
        for (resolveInfo in resInfoList) {
            val packageName: String = resolveInfo.activityInfo.packageName
            context.grantUriPermission(
                packageName,
                shareImageFileUri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }

        shareResult.launch(chooser)

    }

    private val shareResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            // Optional - called as soon as the user selects an option from the system share dialog
        }

    fun branchDetailsDialog(branchDetailsModel: BranchDetailsModel) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.dialog_branch_connect, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        //show dialog
        val mAlertDialog = mBuilder.show()

        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val tvBranchName: TextView? = mAlertDialog.findViewById(R.id.tvBranchName)
        val tvBranchAddress: TextView? = mAlertDialog.findViewById(R.id.tvBranchAddress)
        val tvManagerName: TextView? = mAlertDialog.findViewById(R.id.tvManagerName)
        val tvMobileNum: TextView? = mAlertDialog.findViewById(R.id.tvMobileNum)
        val btnOkBranchDetails: Button? = mAlertDialog.findViewById(R.id.btnOkBranchDetails)

        tvBranchName?.text = branchDetailsModel.branchName
        tvBranchAddress?.text = branchDetailsModel.branchAddress
        tvManagerName?.text = branchDetailsModel.activeManagerName
        tvMobileNum?.text = branchDetailsModel.managerMobileNumber

        btnOkBranchDetails?.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
    }

}