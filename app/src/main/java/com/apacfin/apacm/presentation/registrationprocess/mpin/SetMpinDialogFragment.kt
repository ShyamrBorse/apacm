package com.apacfin.apacm.presentation.registrationprocess.mpin

import `in`.aabhasjindal.otptextview.OTPListener
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.LoginMpinActivity
import com.apacfin.data.prefrerce.Constants
import kotlinx.android.synthetic.main.fragment_set_mpin_dialog.*
import kotlinx.android.synthetic.main.fragment_set_mpin_dialog.ivCloseDialog
import android.content.Intent
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.registration.RegistrationModel
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_otp_dialog.*
import kotlinx.android.synthetic.main.fragment_set_mpin_dialog.tvWelcomeName
import timber.log.Timber
import java.util.HashMap
import com.apacfin.data.prefrerce.AppPreferences
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlin.properties.Delegates

/**
 * A simple [Fragment] subclass.
 * Use the [SetMpinDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class SetMpinDialogFragment : DialogFragment(), View.OnClickListener {
    val TAG = SetMpinDialogFragment::class.java.simpleName
    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    var name = ""
    var mobileNo = ""
    var dob = ""
    var fName = ""
    var lName = ""
    var pincode = ""
    var fromView = false


    var appPreferences: AppPreferences? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE);
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_set_mpin_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout(width - 70, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initVeiw()

        edtSetPin?.requestFocusOTP()
        edtSetPin.setOtpListener(object : OTPListener {
            override fun onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            override fun onOTPComplete(otp: String) {
                edtSetPin?.showSuccess()

            }
        })
        edtConfirmPin.setOtpListener(object : OTPListener {
            override fun onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            override fun onOTPComplete(otp: String) {
                edtConfirmPin?.showSuccess()
                // fired when user has entered the OTP fully.
                //setButtonValidation(requireContext(), btnSubmit, true)

                //Toast.makeText(activity, "The OTP is $otp", Toast.LENGTH_SHORT).show()
            }
        })

        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(this, Observer {
            loading = it;
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle setMpin response
        viewModel.setMpinResponse.observe(this, {
            registrationModel = it
            Timber.e("setMpinResponse " + registrationModel)

            if (registrationModel.status) {

                appPreferences?.putString(Constants.Name, name)
                appPreferences?.putString(Constants.MobileNo, mobileNo)
                appPreferences?.putString(Constants.DOB, dob)

                val dialog = MaterialAlertDialogBuilder(
                    requireContext(),
                    R.style.MyMaterialAlertDialog
                ).create()
                dialog.setTitle(" ")
                dialog.setIcon(R.drawable.ic_new_logo_foreground)
                //dialog.setMessage(getDetailsFromUCIC.msg)
                dialog.setMessage(resources.getString(R.string.mpin_set_success))
                dialog.setCancelable(false)
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, resources.getString(R.string.ok))
                { _, _ ->
                    dialog.dismiss()
                    appPreferences!!.putBoolean(Constants.IsRegistred, true)
                    activity?.startActivity(Intent(activity, LoginMpinActivity::class.java))
                    activity?.finish()
                }

                dialog.show()
            } else {
                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
            }
        })

    }

    private fun initVeiw() {

        appPreferences = AppPreferences.getAppPreferences(requireContext())


        arguments?.let {
            name = getArguments()?.getString(Constants.Name, "").toString()
            mobileNo = getArguments()?.getString(Constants.MobileNo, "").toString()
            dob = getArguments()?.getString(Constants.DOB, "").toString()
            fName = getArguments()?.getString(Constants.Fname, "").toString()
            lName = getArguments()?.getString(Constants.Lname, "").toString()
            pincode = getArguments()?.getString(Constants.Pincode, "").toString()
            fromView = requireArguments().getBoolean(Constants.FromView, false)
        }
        tvWelcomeName.text = " " + name
        Timber.e(name + ", " + mobileNo + ", " + dob)
        //tvUCIC.text = "***"+ucic
        dialog?.setCancelable(false)
        ivCloseDialog.setOnClickListener(this)
        btnSubmit.setOnClickListener(this)
        // ivShowSetPin.setOnClickListener(this)


    }

    override fun onClick(view: View?) {

        when (view?.getId()) {
            R.id.ivCloseDialog -> dialog?.dismiss()
            R.id.btnSubmit -> {
                validation()
            }


        }
    }

    /*fun ShowHidePass() {
        Timber.e("ivShowSetPin getTag : "+ivShowSetPin.getTag())
        //Show Password
        if (ivShowSetPin.getTag().toString().lowercase().equals(resources.getString(R.string.show).lowercase())) {
            ivShowSetPin.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_visibility_off_24))
            ivShowSetPin.setTag(R.string.hide)
            edtSetPin.
        }

        //Hide Password
        if (ivShowSetPin.getTag().toString().lowercase().equals(resources.getString(R.string.hide).lowercase())) {
            ivShowSetPin.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_visibility_24))
            ivShowSetPin.setTag(R.string.show)
        }
    }*/

    private fun validation() {
        if (isNetworkAvailable(requireContext())) {
            var strSetPin = edtSetPin.otp
            var strConfirmPin = edtConfirmPin.otp

            if (strSetPin.isNullOrBlank()) {
                showInternetConnectionLost(
                    requireContext(),
                    requireContext().resources.getString(R.string.er_set_pin),
                    requireContext().resources.getString(R.string.ok)
                )
            } else if (strConfirmPin.isNullOrBlank()) {
                showInternetConnectionLost(
                    requireContext(),
                    requireContext().resources.getString(R.string.er_confirm_pin),
                    requireContext().resources.getString(R.string.ok)
                )
            } else if (!strSetPin.equals(strConfirmPin)) {

                edtSetPin.showError()
                edtConfirmPin.showError()
                showAlertMessageSingleClick(
                    requireContext(),
                    getString(R.string.ok),
                    requireContext().resources.getString(R.string.er_set_confirm_pin)
                ) { dialog, which ->
                    edtSetPin.resetState()
                    edtSetPin.otp=""
                    edtConfirmPin.resetState()
                    edtConfirmPin.otp=""
                }
            } else {

                val params: MutableMap<String, String> = HashMap()
                params["mobileNo"] = mobileNo
                params["mpin"] = encryptPassword(strSetPin).replace("\n","")
                params["dob"] = dob
                params["langID"] = appPreferences?.getString(Constants.SetLanguage, "").toString()
                params["newUserFlag"] = fromView.toString()
                params["fName"] = fName
                params["lName"] = lName
                params["pinCode"] = pincode
                viewModel.setMpin(requireActivity(),params)
            }
        }
    }
}