package com.apacfin.apacm.presentation.firebase

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.apacfin.apacm.R
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber
import java.util.concurrent.atomic.AtomicInteger
import android.app.PendingIntent

import android.content.Intent


import android.graphics.BitmapFactory

import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.LoginActivityNew


//
// Created by Suyog Dorlikar on 11/1/2021.
//
class MyFirebaseMessagingService : FirebaseMessagingService() {

    val TAG: String = MyFirebaseMessagingService::class.java.simpleName
    var CHANNEL_ID = "my_channel_01" // The id of the channel.

    //var name: CharSequence = resources.getString(R.string.app_name) // The user-visible name of the channel.


    var importance = NotificationManager.IMPORTANCE_HIGH
    var mChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        NotificationChannel(CHANNEL_ID, "app_name", importance)
    } else {

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Timber.e(TAG + " onMessageReceived : ${remoteMessage}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Timber.e(TAG + "onMessageReceived Message Data : " + remoteMessage.data)
            Timber.e(TAG + "onMessageReceived Message Data url : " + remoteMessage.data.get("url"))

            setNotification(remoteMessage.data.get("title"), remoteMessage.data.get("message"), remoteMessage.data.get("url"))

        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Timber.e(TAG + "onMessageReceived Notification Body: ${it.body}")
            //Message Services handle notification

            setNotification(remoteMessage.from, it.body,"")

        }

    }

    private fun setNotification(from: String?, body: String?, imgUrl: String?) {
        var notificationBuilder = NotificationCompat.Builder(this)

        Timber.e(TAG + "setNotification imgUrl : " + imgUrl)
        //var imgUrl = "https://res.cloudinary.com/demo/image/upload/v1312461204/sample.jpg"

        val icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_new_logo_foreground)
        if (imgUrl.isNullOrEmpty()) {

            notificationBuilder.setStyle(NotificationCompat.BigTextStyle()
                .bigText(body))
        } else {
            val futureTarget = Glide.with(this)
                .asBitmap()
                .load(imgUrl)
                .submit()

            val bitmap = futureTarget.get()

            Glide.with(this).clear(futureTarget)

            notificationBuilder.setStyle(NotificationCompat.BigPictureStyle()
                .bigPicture(bitmap))
        }
        val notification = notificationBuilder
            .setContentTitle(from)
            .setContentText(body)
            .setChannelId(CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_new_logo_foreground)
            .setLargeIcon(icon)
            // .addAction(R.drawable.ic_baseline_call_24, "Turn OFF driving mode", pIntentlogin)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setOngoing(false)
            .setOnlyAlertOnce(true)
            .setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .build()

        val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel as NotificationChannel)
        }
        val notificationIntent = Intent(this, LoginActivityNew::class.java)
        notificationIntent.flags =
            (Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val intent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_MUTABLE)
        notification.contentIntent = intent
        notification.flags = notification.flags or Notification.FLAG_AUTO_CANCEL
        mNotificationManager.notify(1, notification)
    }

    internal object NotificationID {
        private val c: AtomicInteger = AtomicInteger(100)
        val iD: Int
            get() = c.incrementAndGet()
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }


}