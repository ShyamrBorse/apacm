package com.apacfin.apacm.presentation.registrationprocess.mpinlogin

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import com.apacfin.apacm.BuildConfig
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.gratitude.GratituideFragment
import com.apacfin.apacm.presentation.home.HomeViewModel
import com.apacfin.apacm.presentation.home.adapter.SliderAdapter
import com.apacfin.apacm.presentation.language.LanguageActivity
import com.apacfin.apacm.presentation.registrationprocess.interfaces.LoadFragmentListener
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.home.SliderModel
import com.apacfin.data.model.profile.AppDetailModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login_new.*
import timber.log.Timber
import java.util.*
import kotlin.collections.HashMap


@AndroidEntryPoint
class LoginActivityNew : AppCompatActivity(), LoadFragmentListener, LocationListener {

    private val viewModel: RegistrationViewModel by viewModels()
    private val PERMISSION_REQUEST_CODE = 200
    private val iRequestCodePhoneNumber = 100
    var appPreferences: AppPreferences? = null
    private lateinit var locationManager: LocationManager
    var permission  = arrayOf(Manifest.permission.READ_PHONE_STATE, /*Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.MANAGE_EXTERNAL_STORAGE,*/ Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    lateinit var appDetailModel: AppDetailModel
    private val homeviewModel: HomeViewModel by viewModels()
    lateinit var sliderModel: SliderModel
    var list = mutableListOf<String>()
    var loading = false
    var currentPageSlider = 0
    var timer: Timer? = null
    val DELAY_MS: Long = 700 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 5000
    val handler = Handler()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_new)

        appPreferences = AppPreferences.getAppPreferences(this)
        appPreferences?.putBoolean("NumberNotFound", false)
        if (com.apacfin.apacm.util.checkPermission(this) == false) {
            requestPermission()
        }else{
            doOperation()
        }

        setFragment()
        getSliderList()


        /*After setting the adapter use the timer */
        val Update = Runnable {
            Timber.e("currentPageFlashSale: " + currentPageSlider + " : " + list.size)
            if (currentPageSlider == list.size - 1) {
                currentPageSlider = 0
            }
            login_viewpagerSlider.setCurrentItem(currentPageSlider++, true)
        }

        timer = Timer() // This will create a new Thread

        timer!!.schedule(object : TimerTask() {
            // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)

        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(this)
        homeviewModel.loading.observe(this, {
            loading = it
            if (loading) {
                progressDialog.show()
            } else {
                progressDialog.dismiss()
            }
        })


        //-- Handle sliderList response
        homeviewModel.sliderListResponse.observe(this, {
            //if (this.lifecycle.currentState == Lifecycle.State.RESUMED) {
            sliderModel = it
            Timber.e("sliderModel " + sliderModel)

            if (sliderModel.status) {
                list.addAll(sliderModel.sliderList)
                setAdapter(list)
            } else {
                showInternetConnectionLost(
                    this,
                    sliderModel.msg,
                    getString(R.string.ok)
                )
            }
            //}
        })


        txtLang.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(
                    this,
                    LanguageActivity::class.java
                ).putExtra(Constants.FromView, Constants.fromRegistation)
            )
        })
    }

    private fun setAdapter(list: MutableList<String>) {

        val adapter = SliderAdapter(this, list)
        login_viewpagerSlider.adapter = adapter
        login_viewpagerSlider.setClipToPadding(false)
        login_viewpagerSlider.setPadding(10, 0, 10, 0)
        login_indicator.attachToPager(login_viewpagerSlider)
    }


    private fun getSliderList() {
        if (isNetworkAvailable(this)) {
            homeviewModel.sliderList(this)
        }

    }

    fun setFragment(){
        var registrationFragment:RegistrationFragment = RegistrationFragment()
        var loginMPinFragment:LoginMPinFragment = LoginMPinFragment()
        var transaction:FragmentTransaction = getSupportFragmentManager().beginTransaction()

        if (appPreferences!!.getBoolean(Constants.IsLogin, false)) {
            transaction.replace(R.id.nav_host_login_fragment, loginMPinFragment)
        }else{
            appPreferences!!.putString(Constants.Token,"")
            getFirebaseToken(this)
            transaction.replace(R.id.nav_host_login_fragment, registrationFragment)
        }
        transaction.addToBackStack(null)
        transaction.commit()
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }


    private fun doOperation() {
        //Toast.makeText(this, "Successfully granted", Toast.LENGTH_LONG).show()
        //requestHint()
        //getCurrentLatLong(this@LoginActivityNew)
        if (ContextCompat.checkSelfPermission(
                this@LoginActivityNew, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            getLocation();
        }
        getDeviceId(this@LoginActivityNew)
    }

    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
    }

    fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val locationPermissionRequest = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result: Map<String, Boolean?>? ->
            }
            locationPermissionRequest.launch(permission)
            Timber.e("requestPermission: IN" + " " + Environment.isExternalStorageManager()
            )

        } else {
            //below android 11
            ActivityCompat.requestPermissions(
                this@LoginActivityNew, permission, PERMISSION_REQUEST_CODE
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Timber.e("onRequestPermissionsResult: "+requestCode+" : "+grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty()) {
                doOperation()
            }
        }
    }



    private fun settingActivityOpen() {
        Toast.makeText(
            this,
            "Go to settings and enable permissions",
            Toast.LENGTH_LONG
        ).show()
        val i = Intent()
        i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        i.addCategory(Intent.CATEGORY_DEFAULT)
        i.data = Uri.parse("package:$packageName")
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        startActivity(i)
    }
    private fun showDialogOK(okListener: DialogInterface.OnClickListener) {
        MaterialAlertDialogBuilder(this)
            .setMessage("All Permission required for this app")
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }

    override fun loadFragListener(
        context: Context,
        type: String,
        bundle: Bundle?,
        fragment: Fragment
    ) {
        val transaction:FragmentTransaction = getSupportFragmentManager().beginTransaction()
        if(type.equals(Constants.VerifyOTPFrg)) {
            val verifyOtpFragment: VerifyOtpFragment = VerifyOtpFragment()
            verifyOtpFragment.arguments = bundle
            transaction.replace(R.id.nav_host_login_fragment, verifyOtpFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }else if(type.equals(Constants.ResetMPINFrag)) {
            val setupMPinFragment: SetupMPinFragment = SetupMPinFragment()
            setupMPinFragment.arguments = bundle
            transaction.replace(R.id.nav_host_login_fragment, setupMPinFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }else if(type.equals(Constants.LoginMPINFrag)) {
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            val loginMPinFragment: LoginMPinFragment = LoginMPinFragment()
            loginMPinFragment.arguments = bundle
            transaction.replace(R.id.nav_host_login_fragment, loginMPinFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }else if(type.equals(Constants.GOTOREGFrag)) {
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            val registrationFragment: RegistrationFragment = RegistrationFragment()
            transaction.replace(R.id.nav_host_login_fragment, registrationFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }


}