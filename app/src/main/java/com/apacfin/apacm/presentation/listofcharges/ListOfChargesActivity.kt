package com.apacfin.apacm.presentation.listofcharges

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.util.LocaleHelper
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.listofchaarges.ListOfChargesModel
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_list_of_charges.*
import kotlinx.android.synthetic.main.fragment_alert.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.no_data_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber
import java.util.ArrayList
import java.util.HashMap

@AndroidEntryPoint
class ListOfChargesActivity : AppCompatActivity(), View.OnClickListener,
    ChargesAdapter.OnClickListener {
    val TAG = ListOfChargesActivity::class.java.simpleName
    private val viewModel: ListOfChargesViewModel by viewModels()
    var loading = false
    var from = 0

    var adapter: ChargesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_charges)


        initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, {
            loading = it;
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle sliderList response
        viewModel.getListOfCharges.observe(this, {

            Timber.e("sliderModel " + it)

            if (it.status) {

                if (it.charges.size > 0) {

                    setAdapter(it.charges)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }

            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                    this,
                    it.msg,
                    getString(R.string.ok)
                )
            }
        })
    }

    private fun setAdapter(list: List<ListOfChargesModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        adapter = ChargesAdapter(
            this,
            list,
            this@ListOfChargesActivity
        )
        rcyclerViewCharges!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun initView() {

        from = intent.getIntExtra(Constants.FromView, 0)
        if (from == 0) {
            toolbar.title = resources.getString(R.string.list_of_charges)
        } else {
            toolbar.title = resources.getString(R.string.information)
        }
        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_baseline_arrow_back_24)
        rcyclerViewCharges!!.layoutManager = LinearLayoutManager(this)
        setVisibility(View.GONE, View.GONE)
        if (isNetworkAvailable(this)) {
            if (from == 0) {
                viewModel.getListOfCharges(this, "charges")
            } else {
                viewModel.getListOfCharges(this, "info")
            }
        }


        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewCharges.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData

    }

    override fun onClick(p0: View?) {

    }

    override fun onLeadClick(position: Int) {

    }

    /*override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }*/
}