package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.GlideApp
import com.apacfin.data.model.home.TestimonialModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.bumptech.glide.request.RequestOptions
import com.devs.readmoreoption.ReadMoreOption
import kotlinx.android.synthetic.main.slider_item.view.*


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : TestimonialAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class TestimonialAdapter(
    context: Context, listOfChargesModel: List<TestimonialModel>
) : RecyclerView.Adapter<TestimonialAdapter.ViewHolder?>() {
    var context: Context
    var appPreferences: AppPreferences? =AppPreferences.getAppPreferences(context)
    private var readMoreOption: ReadMoreOption
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_testimonial, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listOfChargesModel: TestimonialModel = listOfChargesModels[position]

        /*holder.ivTestimonial.load(listOfChargesModel?.imageUrl) {
            setHeader("imei", appPreferences!!.getString(Constants.ImeiNo, "")!!)
            setHeader("appVersion", appPreferences!!.getString(Constants.AppVersion, "")!!)
            crossfade(true)
            crossfade(500)
            placeholder(R.drawable.logo_circle)
            error(R.drawable.logo_circle)
           transformations(CircleCropTransformation())
        }*/
        GlideApp.with(holder.ivTestimonial).load(listOfChargesModel?.imageUrl).error(R.drawable.logo_circle).apply(
            RequestOptions.circleCropTransform()).into(holder.ivTestimonial);
       holder.tvTestimonialName.text = "" + listOfChargesModel.userName
        //holder.tvTestimonial.text = "" + listOfChargesModel.testimonial
        readMoreOption.addReadMoreTo(
            holder.tvTestimonial,
            Html.fromHtml(listOfChargesModel.comment)
        )
    }

    override fun getItemCount(): Int {
        return listOfChargesModels.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var ivTestimonial: ImageView
        var tvTestimonialName: TextView
        var tvTestimonial: TextView



        init {
            ivTestimonial = itemView.findViewById(R.id.ivTestimonial)
            tvTestimonialName = itemView.findViewById(R.id.tvTestimonialName)
            tvTestimonial = itemView.findViewById(R.id.tvTestimonial)


        }

    }

    companion object {
        private val TAG = TestimonialAdapter::class.java.simpleName
        lateinit var listOfChargesModels: List<TestimonialModel>
        var selectedPosition = -1
    }

    init {
        readMoreOption = ReadMoreOption.Builder(context)
             .moreLabelColor(context.resources.getColor(R.color.Orange))
             .lessLabelColor(context.resources.getColor(R.color.purple_500))
            .build()
        listOfChargesModels = listOfChargesModel
        this.context = context
    }
}
