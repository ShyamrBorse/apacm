package com.apacfin.apacm.presentation.document

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.amountFormater
import com.apacfin.data.model.esign.EsignLoanDetailsModel
import com.apacfin.data.model.myloan.LoanDetailModel
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : SignPendingDocAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class SignPendingDocAdapter(
    context: Context, loanDetailModel: List<EsignLoanDetailsModel>,
    onClickListener: OnClickListener,
    onStatmentClickListener: OnStatmentClickListener,
    onPaymentSheduleClickListener: OnPaymentSheduleClickListener,typeTest:Int
) : RecyclerView.Adapter<SignPendingDocAdapter.ViewHolder?>() {
    var context: Context
    var type:Int = typeTest


    private val mOnClickListener: OnClickListener
    private var mOnStatmentClickListener: OnStatmentClickListener
    private var mOnPaymentSheduleClickListener: OnPaymentSheduleClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_sign_pending_loan_details, parent, false)
        return ViewHolder(
            view,
            mOnClickListener,
            mOnStatmentClickListener,
            mOnPaymentSheduleClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: EsignLoanDetailsModel = loanDetailModelData[position]

        holder.txtLoanID.text = "" + loanDetailModel.loanID
        holder.txtApplicant.text = "" + loanDetailModel.applicantName

        if (!loanDetailModel.sanctionAmount.isNullOrEmpty()) {
            holder.txtTotalAmount.text =
                "" + amountFormater(loanDetailModel.sanctionAmount.toDouble())
            //holder.txtPrincipleAmount.text =  "" + amountFormater(loanDetailModel.sanctionAmount.toInt())
        }
        if (type == 1) {
            holder.tvStatus.setVisibility(View.VISIBLE);

            val fadeIn = AlphaAnimation(0.0f, 1.0f)
            val fadeOut = AlphaAnimation(1.0f, 0.0f)

            holder.tvStatus.startAnimation(fadeIn)
            holder.tvStatus.startAnimation(fadeOut)

            holder.tvStatus.startAnimation(fadeIn)
            holder.tvStatus.startAnimation(fadeOut)

            fadeIn.duration = 200
            fadeOut.duration = 500
            fadeOut.startOffset = 500 + fadeIn.startOffset + 500
            fadeIn.setRepeatCount(Animation.INFINITE)
            fadeOut.setRepeatCount(Animation.INFINITE)
        }

        holder.tvPaymentShedule.setOnClickListener {
            mOnPaymentSheduleClickListener.onPaymentSheduleClickListener(
                position
            )
        }

    }

    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }

    interface OnStatmentClickListener {
        fun onStatmentClickListener(position: Int)
    }

    interface OnPaymentSheduleClickListener {
        fun onPaymentSheduleClickListener(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View,
        onClickListener: OnClickListener,
        onStatmentClickListener: OnStatmentClickListener,
        onPaymentSheduleClickListener: OnPaymentSheduleClickListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var txtLoanID: TextView = itemView.findViewById(R.id.txtLoanID)
        var txtApplicant: TextView = itemView.findViewById(R.id.txtApplicant)
        var tvStatus: TextView = itemView.findViewById(R.id.tvStatus)

        var txtTotalAmount: TextView = itemView.findViewById(R.id.txtTotalAmount)
        //  var txtPrincipleAmount: TextView

        var tvPaymentShedule: TextView = itemView.findViewById(R.id.tvPaymentShedule)
        var mOnClickListener: OnClickListener = onClickListener

        override fun onClick(view: View) {

        }


        override fun onLeadStatusClick(position: Int) {}

        init {

            //txtPrincipleAmount = itemView.findViewById(R.id.txtPrincipleAmount)

            mOnStatmentClickListener = onStatmentClickListener
            mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = SignPendingDocAdapter::class.java.simpleName
        lateinit var loanDetailModelData: List<EsignLoanDetailsModel>

    }

    init {
        loanDetailModelData = loanDetailModel
        mOnClickListener = onClickListener
        mOnStatmentClickListener = onStatmentClickListener
        mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
        this.context = context
    }
}
