package com.apacfin.apacm.presentation.myloan

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.amountFormater
import com.apacfin.data.model.myloan.ApplicantListModel
import com.apacfin.data.model.myloan.LoanDetailModel
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class MyLoanAdapter(
    context: Context, loanDetailModel: List<LoanDetailModel>,
    onClickListener: OnClickListener,
    onStatmentClickListener: OnStatmentClickListener,
    onPaymentSheduleClickListener: OnPaymentSheduleClickListener
) : RecyclerView.Adapter<MyLoanAdapter.ViewHolder?>() {
    var context: Context

    private val mOnClickListener: OnClickListener
    private var mOnStatmentClickListener: OnStatmentClickListener
    private var mOnPaymentSheduleClickListener: OnPaymentSheduleClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_my_loan_details, parent, false)
        return ViewHolder(
            view,
            mOnClickListener,
            mOnStatmentClickListener,
            mOnPaymentSheduleClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: LoanDetailModel = loanDetailModelData[position]


        holder.txtLoanID.text = "" + loanDetailModel.loanId
        holder.txtApplicant.text = "" + loanDetailModel.applicant
        holder.txtLoanType.text = "" + loanDetailModel.loanType
        holder.txtStatus.text = "" + loanDetailModel.loanStatus
        holder.txtTotalAmount.text = ""+amountFormater(loanDetailModel.totalAmount.toDouble())
        holder.txtPrincipleAmount.text =  "" + amountFormater(loanDetailModel.principleAmount.toDouble())
        holder.txtDisbursedAmount.text =  "" + amountFormater(loanDetailModel.disbursedAmount.toDouble())
        holder.txtBalanceTenure.text = "" + loanDetailModel.balanceTenure+" " +context.getString(R.string.months)
        holder.txtUpcomingEmi.text = "" + loanDetailModel.dueDate
        holder.txtEMIAmount.text = "" + amountFormater(loanDetailModel.emiAmount.toDouble())
        holder.txtLastEMIDate.text = "" + loanDetailModel.lastEMIDate
        if(loanDetailModel.applicantDetails.size ==1){
            holder.txtCoApplicants.text = "N/A"
        }
        if(loanDetailModel.applicantDetails.size >1){
            holder.txtCoApplicants.text = "" + loanDetailModel.applicantDetails.get(1).applicantName
        }


        if (loanDetailModel.loanStatus.trim().equals("DISBURSED")){
            holder.tvPayEmi.visibility = View.VISIBLE
        }else{
            holder.tvPayEmi.visibility = View.GONE
        }

        holder.tvPayEmi.setOnClickListener {
            mOnStatmentClickListener.onStatmentClickListener(
                position
            )
        }
        holder.txtCoApplicantsView.setOnClickListener {
        showList(context,loanDetailModel.applicantDetails)
        }
        holder.tvPaymentShedule.setOnClickListener {
            mOnPaymentSheduleClickListener.onPaymentSheduleClickListener(
                position
            )
        }
        holder.relyExpand.setOnClickListener {
            if (holder.lnyDetails.getVisibility() === View.GONE) {
                // it's collapsed - expand it
                holder.lnyDetails.setVisibility(View.VISIBLE)
                holder.ivExpand.setImageResource(R.drawable.chevron_up)
            } else {
                // it's expanded - collapse it
                holder.lnyDetails.setVisibility(View.GONE)
                holder.ivExpand.setImageResource(R.drawable.chevron_down)
            }
        }
    }

    private fun showList(context: Context, applicantDetails: List<ApplicantListModel>)  {
      //  var adapter: ApplicantListAdapter? = null
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.contain_applicant_list)
            val btnDialogOkay = dialog.findViewById(R.id.btnDialogOkay) as Button
            val rcyclerViewApplicantList = dialog.findViewById(R.id.rcyclerViewApplicantList) as RecyclerView
        rcyclerViewApplicantList!!.layoutManager = LinearLayoutManager(context)
        var adapter = ApplicantListAdapter(
            context,
            applicantDetails
        )
        rcyclerViewApplicantList!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
        btnDialogOkay.setOnClickListener {
                dialog.dismiss()
            }
            //  noBtn.setOnClickListener { dialog.dismiss() }
            val displayMetrics = DisplayMetrics()
           // this.context.applicationContext.windowManager.defaultDisplay.getMetrics(displayMetrics)

            val width = displayMetrics.widthPixels

          // dialog.window?.setLayout(width - 80, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

        }


    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }

    interface OnStatmentClickListener {
        fun onStatmentClickListener(position: Int)
    }

    interface OnPaymentSheduleClickListener {
        fun onPaymentSheduleClickListener(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View,
        onClickListener: OnClickListener,
        onStatmentClickListener: OnStatmentClickListener,
        onPaymentSheduleClickListener: OnPaymentSheduleClickListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var txtLoanID: TextView
        var txtApplicant: TextView
        var txtLoanType: TextView
        var txtStatus: TextView
        var txtTotalAmount: TextView
        var txtPrincipleAmount: TextView
        var txtDisbursedAmount: TextView
        var txtBalanceTenure: TextView
        var txtEMIAmount: TextView
        var txtLastEMIDate: TextView
        var txtCoApplicants: TextView
        var txtCoApplicantsView: TextView
        var tvPayEmi: TextView
        var tvPaymentShedule: TextView
        var txtUpcomingEmi: TextView

        var lnyDetails: LinearLayout
        var ivExpand: ImageView
        var relyExpand: RelativeLayout

        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {

        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            txtLoanID = itemView.findViewById(R.id.txtLoanID)
            txtApplicant = itemView.findViewById(R.id.txtApplicant)
            txtLoanType = itemView.findViewById(R.id.txtLoanType)
            txtStatus = itemView.findViewById(R.id.txtStatus)
            txtTotalAmount = itemView.findViewById(R.id.txtTotalAmount)
            txtPrincipleAmount = itemView.findViewById(R.id.txtPrincipleAmount)
            txtDisbursedAmount = itemView.findViewById(R.id.txtDisbursedAmount)
            txtBalanceTenure = itemView.findViewById(R.id.txtBalanceTenure)
            txtEMIAmount = itemView.findViewById(R.id.txtEMIAmount)
            txtLastEMIDate = itemView.findViewById(R.id.txtLastEMIDate)
            txtCoApplicants = itemView.findViewById(R.id.txtCoApplicants)
            txtCoApplicantsView = itemView.findViewById(R.id.txtCoApplicantsView)
            tvPayEmi = itemView.findViewById(R.id.tvPayEmi)
            tvPaymentShedule = itemView.findViewById(R.id.tvPaymentShedule)
            txtUpcomingEmi = itemView.findViewById(R.id.txtUpcomingEmi)


            lnyDetails = itemView.findViewById(R.id.lnyDetails)
            ivExpand = itemView.findViewById(R.id.ivExpand)
            relyExpand = itemView.findViewById(R.id.relyExpand)

            mOnClickListener = onClickListener
            mOnStatmentClickListener = onStatmentClickListener
            mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = MyLoanAdapter::class.java.simpleName
        lateinit var loanDetailModelData: List<LoanDetailModel>

    }

    init {
        loanDetailModelData = loanDetailModel
        mOnClickListener = onClickListener
        mOnStatmentClickListener = onStatmentClickListener
        mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
        this.context = context
    }
}
