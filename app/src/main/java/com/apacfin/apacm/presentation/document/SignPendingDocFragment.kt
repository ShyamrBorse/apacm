package com.apacfin.apacm.presentation.document

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.util.*
import com.apacfin.data.model.esign.EsignLoanDetailsModel
import com.apacfin.data.model.esign.EsignLoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login_mpin.*
import kotlinx.android.synthetic.main.fragment_all_doc.*
import kotlinx.android.synthetic.main.no_data_layout.*
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : DocumentMainAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@AndroidEntryPoint
class SignPendingDocFragment : Fragment() ,SignPendingDocAdapter.OnClickListener, SignPendingDocAdapter.OnStatmentClickListener,
    SignPendingDocAdapter.OnPaymentSheduleClickListener, LocationListener {
    val TAG = SignPendingDocFragment::class.java.simpleName
    private val viewModel: DocumentViewModel by viewModels()
    var loading = false
    private lateinit var documentModelResponse: EsignLoanDetailsModelResponse
    lateinit var registrationModel: RegistrationModel
    lateinit var refId: String
    var appPreferences: AppPreferences? = null
    var adapter: SignPendingDocAdapter? = null
    lateinit var loanDetailModel: List<EsignLoanDetailsModel>
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2
    private var type : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_all_doc, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, {
            loading = it
            Timber.e("SmileyFaceCanvassing $loading $it")
              if (loading) {
                progressDialog.show()
            } else progressDialog.dismiss()
        })

        //-- Handle getLoanDetails response
        viewModel.getSignatureDocument.observe(viewLifecycleOwner, {
            documentModelResponse = it
            Timber.e("documentModelResponse $documentModelResponse")

            if (documentModelResponse.status) {
                loanDetailModel = documentModelResponse.loanDetails
                type = documentModelResponse.type?:0
                if (loanDetailModel.isNotEmpty()) {
                    setAdapter(loanDetailModel)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }
            } else {
                setVisibility(View.GONE, View.VISIBLE)

            }
        })

        //-- Handle initiateEsign response
        viewModel.initiateEsign.observe(viewLifecycleOwner, {
            registrationModel = it
            Timber.e("initiateEsign $registrationModel")

            if (registrationModel.status) {
                if (this::refId.isInitialized){
                    startActivity(Intent(
                        activity,
                        SignPendingDetailActivity::class.java
                    ).putExtra(Constants.RefNo,refId).putExtra(Constants.Type,type))
                }
            } else {

                showInternetConnectionLost(
                    requireContext(),
                    registrationModel.msg,
                    getString(R.string.ok)
                )
            }
        })

        tvGotoHome.setOnClickListener {
            findNavController().navigate(R.id.frgHome)
        }
    }


    private fun setAdapter(loanDetailsList: List<EsignLoanDetailsModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        SignPendingDocAdapter(
            requireContext(),
            loanDetailsList,
            this,
            this,
            this,
                    type).also { adapter = it }
        rcyclerViewAllDoc!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        rcyclerViewAllDoc!!.layoutManager = LinearLayoutManager(requireContext())
        setVisibility(View.GONE, View.GONE)
        getSignDocList()
    }
    private fun getSignDocList() {
        if (isNetworkAvailable(requireContext())) {
           /* val params: MutableMap<String, String> = HashMap()
            params["mobileNo"] = appPreferences?.getString(Constants.MobileNo,"").toString()
            params["dob"] = appPreferences?.getString(Constants.DOB,"").toString()*/

            viewModel.getSignatureDocument(requireActivity())

        }
    }
    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewAllDoc.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData
        tvGotoHome.visibility = isVisibleNoData

    }

    override fun onLeadClick(position: Int) {

    }

    override fun onStatmentClickListener(position: Int) {

    }

    override fun onPaymentSheduleClickListener(position: Int) {

        if (ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val manager =
                requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                alertMessageNoGps(requireActivity(), "Yes")
            } else {
                var progressDialog = showProgressDialog(requireActivity())


                object : AsyncTaskCoroutine<Int?, Boolean?>() {
                    override fun onPostExecute(@Nullable result: Boolean?) {
                        progressDialog.dismiss()

                        if (isNetworkAvailable(requireContext())) {
                            refId = loanDetailModel[position].refId
                            viewModel.initiateEsign(requireActivity(), refId,type)
                        }
                    }

                    override fun onPreExecute() {
                        progressDialog.show()
                        //getCurrentLatLong(requireActivity())
                        getLocation();
                    }

                    override fun doInBackground(vararg params: Int?): Boolean? {
                        return null
                    }
                }.execute<Any>()

            }
        }else{
            appPreferences?.putString(Constants.Latitude, "")
            appPreferences?.putString(Constants.Longitude, "")

            if (isNetworkAvailable(requireContext())) {
                refId = loanDetailModel[position].refId
                viewModel.initiateEsign(requireActivity(), refId,type)
            }
        }
        //Log.d(TAG, "checkGPSStatusPermission: Encrypt " + utils.encryptPassword(edPassword.getText().toString()));
        /*val manager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            alertMessageNoGps(requireContext(), "Yes")
        } else {
            getCurrentLatLong(requireContext())
            if (isNetworkAvailable(requireContext())) {
                refId = loanDetailModel[position].refId
                viewModel.initiateEsign(requireActivity(), refId)
            }
        }*/

    }

    private fun getLocation() {
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
        // tvGpsLocation = findViewById(R.id.textView)
        //tvGpsLocation.text = "Latitude: " + location.latitude + " , Longitude: " + location.longitude
    }
}