package com.apacfin.apacm.presentation.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.getDrawable
import com.apacfin.data.model.loanstatus.TimeLineModel
import com.github.vipulasri.timelineview.TimelineView
import kotlinx.android.synthetic.main.item_timeline.view.*

class ExampleTimeLineAdapter(private val mFeedList: List<TimeLineModel>) : RecyclerView.Adapter<ExampleTimeLineAdapter.TimeLineViewHolder>() {

    private lateinit var mLayoutInflater: LayoutInflater

    override fun getItemViewType(position: Int): Int {
        return TimelineView.getTimeLineViewType(position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeLineViewHolder {

        if(!::mLayoutInflater.isInitialized) {
            mLayoutInflater = LayoutInflater.from(parent.context)
        }

        return TimeLineViewHolder(mLayoutInflater.inflate(R.layout.item_timeline, parent, false), viewType)
    }

    override fun onBindViewHolder(holder: TimeLineViewHolder, position: Int) {

        val timeLineModel = mFeedList[position]

        if (timeLineModel.title.equals("Reject")){
            setMarker(holder, R.drawable.ic_marker, R.color.red,TimelineView.LineStyle.NORMAL)
        }else{
        when {
            timeLineModel.stageStatus == false -> {
                setMarker(holder, R.drawable.ic_marker_inactive, R.color.colorGrey500,TimelineView.LineStyle.DASHED)
            }
           /* timeLineModel.stageStatus == false -> {
                setMarker(holder, R.drawable.ic_marker_active, R.color.colorGrey500)
            }*/
            else -> {
                setMarker(holder, R.drawable.ic_marker, R.color.green,TimelineView.LineStyle.NORMAL)
            }
        }
        }

        holder.message.text = timeLineModel.title

       /* holder.timeline.animate()
            .scaleY(1.2f)
        .setDuration(2000)*/
    }

    private fun setMarker(
        holder: TimeLineViewHolder,
        drawableResId: Int,
        colorFilter: Int,
        lineStyle: Int
    ) {
        holder.timeline.lineStyle = lineStyle
        holder.timeline.setStartLineColor(ContextCompat.getColor(holder.itemView.context, colorFilter),holder.itemViewType)
        holder.timeline.setEndLineColor(ContextCompat.getColor(holder.itemView.context, colorFilter),holder.itemViewType)
        holder.timeline.marker = getDrawable(holder.itemView.context, drawableResId, ContextCompat.getColor(holder.itemView.context, colorFilter))
    }

    override fun getItemCount() = mFeedList.size

    inner class TimeLineViewHolder(itemView: View, viewType: Int) : RecyclerView.ViewHolder(itemView) {

        val date = itemView.text_timeline_date
        val message = itemView.text_timeline_title
        val timeline = itemView.timeline

        init {
            timeline.initLine(viewType)
        }
    }

}