package com.apacfin.apacm.presentation.aboutus

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.apacfin.apacm.R
import com.apacfin.apacm.util.*
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import kotlinx.android.synthetic.main.fragment_about_us_dialog.*
import kotlinx.android.synthetic.main.fragment_service_complaint_dialog.*
import timber.log.Timber

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AboutUsDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AboutUsDialogFragment : DialogFragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_us_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout(width - 40, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {
        tvContactusEmail.text = Constants.ContactUsEmail //As discussed with business change email id
        tvContact1.text = Constants.Contact1
        tvContact2.text = Constants.Contact2
        btnCloseAboutDialog.setOnClickListener(this)
        tvContact1.setOnClickListener(this)
        tvContact2.setOnClickListener(this)
        ivFB.setOnClickListener(this)
        ivLD.setOnClickListener(this)

    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireActivity(), theme) {
            override fun onBackPressed() {
                // On backpress, do your stuff here.
                dialog?.dismiss()
            }
        }
    }
    override fun onClick(view: View?) {

        when (view?.getId()) {
            R.id.btnCloseAboutDialog -> dialog?.dismiss()

            R.id.ivFB -> {
                openFacebookUrl(requireContext())
            }

            R.id.ivLD -> {
                openLinkedInUrl(requireContext())
            }
            R.id.tvContact1 -> {
                callDialer(requireContext(), Constants.Contact1)
            }
            R.id.tvContact2 -> {
                callDialer(requireContext(), Constants.Contact2)
            }

        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AboutUsDialogFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AboutUsDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}