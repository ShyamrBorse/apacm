package com.apacfin.apacm.presentation.document

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.document.DocumentModelResponse
import com.apacfin.data.model.document.LoanListForDocModel
import com.apacfin.data.prefrerce.AppPreferences
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_all_doc.*
import kotlinx.android.synthetic.main.no_data_layout.*
import timber.log.Timber

/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : AllDocFragment.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@AndroidEntryPoint
class AllDocFragment : Fragment() ,AllDocumentAdapter.OnClickListener {
    val TAG = AllDocFragment::class.java.simpleName
    private val viewModel: DocumentViewModel by viewModels()
    var loading = false
    lateinit var documentModelResponse: DocumentModelResponse
    var appPreferences: AppPreferences? = null
    var adapter: AllDocumentAdapter? = null
    lateinit var loanListForDocModel: List<LoanListForDocModel>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_all_doc, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, {
            loading = it
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getLoanDetails response
        viewModel.getAllDocument.observe(viewLifecycleOwner, {
            documentModelResponse = it
            Timber.e("documentModelResponse " + documentModelResponse)

            if (documentModelResponse.status) {
                loanListForDocModel = documentModelResponse.data

                if (loanListForDocModel.size > 0) {
                    setAdapter(loanListForDocModel)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }
            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                    requireContext(),
                    documentModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })
    }

    private fun setAdapter(loanListForDocModel: List<LoanListForDocModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        adapter = AllDocumentAdapter(
            requireActivity(),
            loanListForDocModel,
            this
        )
        rcyclerViewAllDoc!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun initView() {
        appPreferences = AppPreferences.getAppPreferences(requireContext())
        rcyclerViewAllDoc!!.layoutManager = LinearLayoutManager(requireContext())
        setVisibility(View.GONE, View.GONE)
        getAllDocList()
    }
    private fun getAllDocList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getAllDocument(requireActivity())
        }
    }
    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewAllDoc.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData
    }

    override fun onLeadClick(position: Int) {
    }
}