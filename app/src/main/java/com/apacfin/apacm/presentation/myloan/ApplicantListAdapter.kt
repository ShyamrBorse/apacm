package com.apacfin.apacm.presentation.myloan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.myloan.ApplicantListModel
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class ApplicantListAdapter(
    context: Context, loanDetailModel: List<ApplicantListModel>
) : RecyclerView.Adapter<ApplicantListAdapter.ViewHolder?>() {
    var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_applicant_list, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: ApplicantListModel = loanDetailModelData[position]

        holder.txtApplName.text = "" + loanDetailModel.applicantName
        holder.txtApplType.text = "" + loanDetailModel.applicantType

        //selectedPosition = holder.adapterPosition

    }

    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }

    interface OnStatmentClickListener {
        fun onStatmentClickListener(position: Int)
    }

    interface OnPaymentSheduleClickListener {
        fun onPaymentSheduleClickListener(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var txtApplName: TextView
        var txtApplType: TextView

        override fun onClick(view: View) {
           // Timber.e(TAG+"onClick: " + selectedPosition)

        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            txtApplName = itemView.findViewById(R.id.txtApplName)
            txtApplType = itemView.findViewById(R.id.txtApplType)



            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = ApplicantListAdapter::class.java.simpleName
        lateinit var loanDetailModelData: List<ApplicantListModel>
        //var selectedPosition = -1
    }

    init {
        loanDetailModelData = loanDetailModel

        this.context = context
    }
}
