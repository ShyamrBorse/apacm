package com.apacfin.apacm.presentation.listofcharges

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.apacfin.apacm.R
import com.apacfin.data.model.alert.AlertModelResponse
import com.apacfin.data.model.home.BannerModel
import com.apacfin.data.model.home.SliderModel
import com.apacfin.data.model.listofchaarges.ListOfChargesModelResponse
import com.apacfin.domain.Resource
import com.apacfin.domain.repository.HomeRepository
import com.apacfin.domain.repository.ListOfChargesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : HomeViewModel.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

@HiltViewModel
class ListOfChargesViewModel @Inject constructor(
    private val repository: ListOfChargesRepository
) : ViewModel() {

    val _getListOfChargesResponse: MutableLiveData<ListOfChargesModelResponse> = MutableLiveData()

    private val _dataLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _dataLoading


    //getListOfCharges responce
    val getListOfCharges: LiveData<ListOfChargesModelResponse> get() = _getListOfChargesResponse

    //getListOfCharges api
    fun getListOfCharges(context:Context,data:String) = viewModelScope.launch {
        _dataLoading.postValue(true)
        when (val response = repository.getListOfCharges(data)) {

            is Resource.Success -> {
                _dataLoading.postValue(false)
                Timber.e("getListOfCharges"+ response.value.msg)
                _getListOfChargesResponse.postValue(response.value)

            }

            /*is Resource.Loading -> {

                *//*setContent {
                    DummyProgress(
                        isDisplayed = viewModel.loading.value
                    )
                }*//*

            }*/
            is Resource.Failure -> {
                _dataLoading.postValue(false)
                Timber.e("getListOfCharges"  + response.errorCode+ response.isNetworkError+ response.errorBody)
                var listOfChargesModelResponse =  ListOfChargesModelResponse(
                    arrayListOf(),context.getString(R.string.tecnical_error),
                    false)
                _getListOfChargesResponse.postValue(listOfChargesModelResponse)
            }
        }

    }




}