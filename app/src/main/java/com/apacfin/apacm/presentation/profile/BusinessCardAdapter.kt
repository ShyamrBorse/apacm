package com.apacfin.apacm.presentation.profile

import android.content.Context
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.GlideApp
import com.apacfin.data.model.businesscard.BusinessCardModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import timber.log.Timber


class BusinessCardAdapter(context: Context, private var list: List<BusinessCardModel>) :
    RecyclerView.Adapter<BusinessCardAdapter.ViewHolder>() {
    var appPreferences: AppPreferences? = AppPreferences.getAppPreferences(context)
    var context = context

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.image)
        val tvName: TextView = itemView.findViewById(R.id.tvName)
        val tvMobile: TextView = itemView.findViewById(R.id.tvMobile)
        val tvBusinessName: TextView = itemView.findViewById(R.id.tvBusinessName)
        val tvAddress: TextView = itemView.findViewById(R.id.tvAddress)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
            .inflate(R.layout.business_card_item_view, parent, false)
        return ViewHolder(inflater)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /*val url = GlideUrl(
                list.get(position).image, LazyHeaders.Builder()
                .addHeader("Authorization", appPreferences?.getString(Constants.Token, "")!!)
                .addHeader("imei", appPreferences!!.getString(Constants.ImeiNo, "")!!)
                .addHeader("appVersion", appPreferences!!.getString(Constants.AppVersion, "")!!)
                .build()
        )
         Glide.with(holder.image).load(url).error(R.drawable.vc01).into(holder.image)*/

        Timber.e("onBindViewHolder ${list.get(position).image}")
        if (!list.get(position).image.isNullOrBlank()) {
            if (list.get(position).image.contains("https://")) {
                GlideApp.with(holder.image).load(list.get(position).image).error(R.drawable.vc01)
                    .into(holder.image)
            } else {
                Glide.with(holder.image)
                    .load(Base64.decode(list.get(position).image, Base64.DEFAULT))
                    .placeholder(R.drawable.vc01)
                    .into(holder.image)
            }
        }


        //
        holder.tvName.text = list.get(position).applicantName
        holder.tvMobile.text = list.get(position).mobile
        holder.tvBusinessName.text = list.get(position).businessName
        /*holder.tvAddress.text =
            list.get(position).address1 + "\n" + list.get(position).address2 + "\n" + list.get(
                position
            ).city + "\n" + list.get(position).pincode*/

        holder.tvAddress.text =
            list.get(position).address1 + ", " + list.get(position).address2 + ", " + list.get(
                position
            ).city + ", " + list.get(position).pincode

    }

    fun updateData(list: List<BusinessCardModel>) {
        this.list = list
        notifyDataSetChanged()
    }
}