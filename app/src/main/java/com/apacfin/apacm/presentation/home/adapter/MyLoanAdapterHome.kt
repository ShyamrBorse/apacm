package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.language.LanguageAdapter
import com.apacfin.data.model.myloan.LoanDetailModel
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class MyLoanAdapterHome(
    context: Context, loanDetailModel: List<LoanDetailModel>,
    onClickListener: OnClickListener,
    onStatmentClickListener: OnStatmentClickListener,
    onPaymentSheduleClickListener: OnPaymentSheduleClickListener
) : RecyclerView.Adapter<MyLoanAdapterHome.ViewHolder?>() {
    var context: Context

    private val mOnClickListener: OnClickListener
    private var mOnStatmentClickListener: OnStatmentClickListener
    private var mOnPaymentSheduleClickListener: OnPaymentSheduleClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_my_loan_details_home, parent, false)
        return ViewHolder(
            view,
            mOnClickListener,
            mOnStatmentClickListener,
            mOnPaymentSheduleClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: LoanDetailModel = loanDetailModelData[position]

        holder.txtLoanID.text = "" + loanDetailModel.loanId
        holder.txtApplicant.text = "" + loanDetailModel.applicant
        holder.txtLoanType.text = "" + loanDetailModel.loanType
        holder.txtStatus.text = "" + loanDetailModel.loanStatus



        holder.cardMain.setOnClickListener {
            mOnClickListener.onLeadClick(position)

        }

    }

    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }

    interface OnStatmentClickListener {
        fun onStatmentClickListener(position: Int)
    }

    interface OnPaymentSheduleClickListener {
        fun onPaymentSheduleClickListener(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View,
        onClickListener: OnClickListener,
        onStatmentClickListener: OnStatmentClickListener,
        onPaymentSheduleClickListener: OnPaymentSheduleClickListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var txtLoanID: TextView
        var txtApplicant: TextView
        var txtLoanType: TextView
        var txtStatus: TextView
        var cardMain: CardView


        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {

        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            txtLoanID = itemView.findViewById(R.id.txtLoanID)
            txtApplicant = itemView.findViewById(R.id.txtApplicant)
            txtLoanType = itemView.findViewById(R.id.txtLoanType)
            txtStatus = itemView.findViewById(R.id.txtStatus)
            cardMain = itemView.findViewById(R.id.cardMain)


            mOnClickListener = onClickListener
            mOnStatmentClickListener = onStatmentClickListener
            mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = MyLoanAdapterHome::class.java.simpleName
        lateinit var loanDetailModelData: List<LoanDetailModel>

    }

    init {
        loanDetailModelData = loanDetailModel
        mOnClickListener = onClickListener
        mOnStatmentClickListener = onStatmentClickListener
        mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
        this.context = context
    }
}
