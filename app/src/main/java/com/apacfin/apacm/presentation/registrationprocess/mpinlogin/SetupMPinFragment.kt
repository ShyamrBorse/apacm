package com.apacfin.apacm.presentation.registrationprocess.mpinlogin

import `in`.aabhasjindal.otptextview.OTPListener
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.interfaces.LoadFragmentListener
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_registration.*
import kotlinx.android.synthetic.main.fragment_setup_mpin.*
import timber.log.Timber
import java.util.HashMap


@AndroidEntryPoint
class SetupMPinFragment : Fragment(), View.OnClickListener {

    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    var name = ""
    var mobileNo = ""
    var dob = ""
    var fName = ""
    var lName = ""
    var pincode = ""
    var fromView = false
    var resetOTP = ""
    var confirmOTP = ""

    var appPreferences: AppPreferences? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setup_mpin, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVeiw()

        edtSetPin?.requestFocusOTP()
        edtSetPin.setOtpListener(object : OTPListener {
            override fun onInteractionListener() {

            }

            override fun onOTPComplete(otp: String) {
                resetOTP = otp
                edtSetPin?.showSuccess()
//                if(confirmOTP.length==4 && resetOTP.length==4){
//                    btnSubmit.setBackgroundResource(R.drawable.btn_orange_curve)
//                }else{
//                    btnSubmit.setBackgroundResource(R.drawable.btn_color)
//                }
            }
        })


        edtConfirmPin.setOtpListener(object : OTPListener {
            override fun onInteractionListener() {

            }

            override fun onOTPComplete(otp: String) {
                confirmOTP = otp
                edtConfirmPin?.showSuccess()
//                if(confirmOTP.length==4 && resetOTP.length==4){
//                    btnSubmit.setBackgroundResource(R.drawable.btn_orange_curve)
//                }else{
//                    btnSubmit.setBackgroundResource(R.drawable.btn_color)
//                }

                // fired when user has entered the OTP fully.
                //setButtonValidation(requireContext(), btnSubmit, true)

                //Toast.makeText(activity, "The OTP is $otp", Toast.LENGTH_SHORT).show()
            }
        })

        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, Observer {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                loading = it;
                Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

                if (loading) {
                    progressDialog.show()

                } else {
                    progressDialog.dismiss()
                    // showProgressDialog(this,false)
                    //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
                }
            }
        })

        //-- Handle setMpin response
        viewModel.setMpinResponse.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                registrationModel = it
                Timber.e("setMpinResponse " + registrationModel)

                if (registrationModel.status) {

                    appPreferences?.putString(Constants.Name, name)
                    appPreferences?.putString(Constants.MobileNo, mobileNo)
                    appPreferences?.putString(Constants.DOB, dob)

                    val dialog = MaterialAlertDialogBuilder(
                        requireContext(),
                        R.style.MyMaterialAlertDialog
                    ).create()
                    dialog.setTitle(" ")
                    dialog.setIcon(R.drawable.ic_new_logo_foreground)
                    //dialog.setMessage(getDetailsFromUCIC.msg)
                    dialog.setMessage(resources.getString(R.string.mpin_set_success))
                    dialog.setCancelable(false)
                    dialog.setButton(
                        DialogInterface.BUTTON_POSITIVE,
                        resources.getString(R.string.ok)
                    )
                    { _, _ ->
                        dialog.dismiss()
                        appPreferences!!.putBoolean(Constants.IsRegistred, true)
                        appPreferences!!.putBoolean(Constants.IsLogin, true)
//                        activity?.startActivity(Intent(activity, LoginMpinActivity::class.java))
//                        activity?.finish()
                        val fr: Fragment = LoginMPinFragment()
                        val fc: LoadFragmentListener? = activity as LoadFragmentListener?
                        fc?.loadFragListener(requireActivity(), Constants.LoginMPINFrag, null, fr)
                    }

                    dialog.show()
                } else {
                    showInternetConnectionLost(
                        requireContext(),
                        registrationModel.msg,
                        getString(R.string.ok)
                    )
                }
            }
        })

    }

    private fun initVeiw() {

        appPreferences = AppPreferences.getAppPreferences(requireContext())


        arguments?.let {
            name = getArguments()?.getString(Constants.Name, "").toString()
            mobileNo = getArguments()?.getString(Constants.MobileNo, "").toString()
            dob = getArguments()?.getString(Constants.DOB, "").toString()
            fName = getArguments()?.getString(Constants.Fname, "").toString()
            lName = getArguments()?.getString(Constants.Lname, "").toString()
            pincode = getArguments()?.getString(Constants.Pincode, "").toString()
            fromView = requireArguments().getBoolean(Constants.FromView, false)
        }
        tvWelcomeName.text = " " + name
        Timber.e(name + ", " + mobileNo + ", " + dob)
        //tvUCIC.text = "***"+ucic
        //dialog?.setCancelable(false)
        //ivCloseDialog.setOnClickListener(this)
        btnSubmit.setOnClickListener(this)
        // ivShowSetPin.setOnClickListener(this)


    }

    override fun onClick(view: View?) {

        when (view?.getId()) {
            //R.id.ivCloseDialog -> dialog?.dismiss()
            R.id.btnSubmit -> {
                validation()
            }
        }
    }

    /*fun ShowHidePass() {
        Timber.e("ivShowSetPin getTag : "+ivShowSetPin.getTag())
        //Show Password
        if (ivShowSetPin.getTag().toString().lowercase().equals(resources.getString(R.string.show).lowercase())) {
            ivShowSetPin.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_visibility_off_24))
            ivShowSetPin.setTag(R.string.hide)
            edtSetPin.
        }

        //Hide Password
        if (ivShowSetPin.getTag().toString().lowercase().equals(resources.getString(R.string.hide).lowercase())) {
            ivShowSetPin.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_visibility_24))
            ivShowSetPin.setTag(R.string.show)
        }
    }*/

    private fun validation() {
        if (isNetworkAvailable(requireContext())) {
            var strSetPin = edtSetPin.otp
            var strConfirmPin = edtConfirmPin.otp

            if (strSetPin.isNullOrBlank()) {
                showInternetConnectionLost(
                    requireContext(),
                    requireContext().resources.getString(R.string.er_set_pin),
                    requireContext().resources.getString(R.string.ok)
                )
            } else if (strConfirmPin.isNullOrBlank()) {
                showInternetConnectionLost(
                    requireContext(),
                    requireContext().resources.getString(R.string.er_confirm_pin),
                    requireContext().resources.getString(R.string.ok)
                )
            } else if (!strSetPin.equals(strConfirmPin)) {

                edtSetPin.showError()
                edtConfirmPin.showError()
                showAlertMessageSingleClick(
                    requireContext(),
                    getString(R.string.ok),
                    requireContext().resources.getString(R.string.er_set_confirm_pin)
                ) { dialog, which ->
                    edtSetPin.resetState()
                    edtSetPin.otp=""
                    edtConfirmPin.resetState()
                    edtConfirmPin.otp=""
                }
            } else {

                val params: MutableMap<String, String> = HashMap()
                params["mobileNo"] = mobileNo
                params["mpin"] = encryptPassword(strSetPin).replace("\n","")
                params["dob"] = dob
                params["langID"] = appPreferences?.getString(Constants.SetLanguage, "").toString()
                params["newUserFlag"] = fromView.toString()
                params["fName"] = fName
                params["lName"] = lName
                params["pinCode"] = pincode
                viewModel.setMpin(requireActivity(),params)
            }
        }
    }
}