package com.apacfin.apacm.presentation.listofcharges

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.listofchaarges.ListOfChargesModel
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class ChargesAdapter(
    context: Context, listOfChargesModel: List<ListOfChargesModel>,
    onClickListener: OnClickListener
) : RecyclerView.Adapter<ChargesAdapter.ViewHolder?>() {
    var context: Context

    private val mOnClickListener: OnClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_charges, parent, false)
        return ViewHolder(view, mOnClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listOfChargesModel: ListOfChargesModel = listOfChargesModels[position]

         holder.tvChargesTital.text = "" + listOfChargesModel.particulars
        holder.tvChargesSubTitle.text = "" + listOfChargesModel.name.replace("\\n","\n")

    }

    override fun getItemCount(): Int {
        return listOfChargesModels.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(itemView: View, onClickListener: OnClickListener) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener ,
        OnStatusClickListener {
        var tvChargesTital: TextView
        var tvChargesSubTitle: TextView
        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {
            Timber.e(TAG+"onClick: " + selectedPosition)
            mOnClickListener.onLeadClick(selectedPosition)
        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            tvChargesTital = itemView.findViewById(R.id.tvChargesTital)
            tvChargesSubTitle = itemView.findViewById(R.id.tvChargesSubTitle)

            mOnClickListener = onClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = ChargesAdapter::class.java.simpleName
        lateinit var listOfChargesModels: List<ListOfChargesModel>
        var selectedPosition = -1
    }

    init {
        listOfChargesModels = listOfChargesModel
        mOnClickListener = onClickListener
        this.context = context
    }
}
