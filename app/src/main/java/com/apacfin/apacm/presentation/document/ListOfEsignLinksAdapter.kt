package com.apacfin.apacm.presentation.document

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.apacm.util.requestInAppReview
import com.apacfin.data.model.esign.ListOfEsignLinksModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : NumberEmployeeUnderManager.java
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class ListOfEsignLinksAdapter(
    context: Context, loanDetailModel: List<ListOfEsignLinksModel>,
    onClickListener: OnClickListener,
    onStatmentClickListener: OnStatmentClickListener,
    onPaymentSheduleClickListener: OnPaymentSheduleClickListener
) : RecyclerView.Adapter<ListOfEsignLinksAdapter.ViewHolder?>() {
    var context: Context

    private val mOnClickListener: OnClickListener
    private var mOnStatmentClickListener: OnStatmentClickListener
    private var mOnPaymentSheduleClickListener: OnPaymentSheduleClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_list_of_esign, parent, false)
        return ViewHolder(
            view,
            mOnClickListener,
            mOnStatmentClickListener,
            mOnPaymentSheduleClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: ListOfEsignLinksModel = loanDetailModelData[position]


        holder.txtApplicantName.text = "" + loanDetailModel.applicantName
        holder.txtApplicantType.text = "" + loanDetailModel.applicantType
        holder.txtApplicantMobile.text = "" + loanDetailModel.mobileNo


        holder.btnEsign.setOnClickListener {
            mOnPaymentSheduleClickListener.onPaymentSheduleClickListener(
                position
            )
        }

        if (loanDetailModel.esignStatus.equals("C")) {
            holder.txtEsignStatus.visibility = View.VISIBLE
            holder.btnEsign.visibility = View.GONE
            holder.txtEsignStatus.text = context.getString(R.string.completed)
        }else{
            holder.txtEsignStatus.visibility = View.GONE
            holder.btnEsign.visibility = View.VISIBLE
        }

        if (loanDetailModel.esignStatus.equals("C")) {
            if ((AppPreferences.appPreferences?.getBoolean(
                    Constants.InAppReviewDone,
                    false
                )==false)
            ) {
                requestInAppReview(context)
                AppPreferences.appPreferences?.putBoolean(Constants.InAppReviewDone, true)
            }
        }
    }

    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }

    interface OnStatmentClickListener {
        fun onStatmentClickListener(position: Int)
    }

    interface OnPaymentSheduleClickListener {
        fun onPaymentSheduleClickListener(position: Int)
    }


    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View,
        onClickListener: OnClickListener,
        onStatmentClickListener: OnStatmentClickListener,
        onPaymentSheduleClickListener: OnPaymentSheduleClickListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var txtApplicantName: TextView
        var txtApplicantType: TextView
        var txtApplicantMobile: TextView
        var txtEsignStatus: TextView
        var btnEsign: Button

        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {
//            Log.d(TAG, "onClick: " + selectedPosition)
            mOnClickListener.onLeadClick(selectedPosition)
        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            txtApplicantName = itemView.findViewById(R.id.txtApplicantName)
            txtApplicantType = itemView.findViewById(R.id.txtApplicantType)
            txtApplicantMobile = itemView.findViewById(R.id.txtApplicantMobile)
            txtEsignStatus = itemView.findViewById(R.id.txtEsignStatus)
            btnEsign = itemView.findViewById(R.id.btnEsign)

            mOnClickListener = onClickListener
            mOnStatmentClickListener = onStatmentClickListener
            mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = ListOfEsignLinksAdapter::class.java.simpleName
        lateinit var loanDetailModelData: List<ListOfEsignLinksModel>
        var selectedPosition = -1
    }

    init {
        loanDetailModelData = loanDetailModel
        mOnClickListener = onClickListener
        mOnStatmentClickListener = onStatmentClickListener
        mOnPaymentSheduleClickListener = onPaymentSheduleClickListener
        this.context = context
    }
}
