package com.apacfin.apacm.presentation.home

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.apacfin.data.model.myloan.LoanDetailModel

interface MenuItemClickListener {
    fun onDashboardMenuItemClickListener(context: Context, dashBoardModel: DashBoardModel,loanDetailsList: List<LoanDetailModel>,loanId:String)
}