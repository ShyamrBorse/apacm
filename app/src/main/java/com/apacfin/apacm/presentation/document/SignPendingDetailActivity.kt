package com.apacfin.apacm.presentation.document

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.mpinlogin.AsyncTaskCoroutine
import com.apacfin.apacm.util.alertMessageNoGps
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.esign.ListOfEsignLinksModel
import com.apacfin.data.model.esign.ListOfEsignLinksModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_sign_pending_detail.*
import kotlinx.android.synthetic.main.no_data_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber

@AndroidEntryPoint
class SignPendingDetailActivity : AppCompatActivity(), ListOfEsignLinksAdapter.OnClickListener,
    ListOfEsignLinksAdapter.OnPaymentSheduleClickListener,
    ListOfEsignLinksAdapter.OnStatmentClickListener,
    LocationListener {

    val tag: String = SignPendingDetailActivity::class.java.simpleName
    private val viewModel: DocumentViewModel by viewModels()
    var loading = false
    lateinit var listOfEsignLinksModelResponse: ListOfEsignLinksModelResponse
    lateinit var registrationModel: RegistrationModel
    var appPreferences: AppPreferences? = null
    var adapter: ListOfEsignLinksAdapter? = null
    lateinit var listOfEsignLinksModel: List<ListOfEsignLinksModel>
    lateinit var refID: String

    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2
    private var type:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_pending_detail)
        initView()

        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, {
            loading = it
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()
            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getLoanDetails response
        viewModel.getListOfEsignLinks.observe(this, {
            listOfEsignLinksModelResponse = it
            Timber.e("listOfEsignLinksModelResponse " + listOfEsignLinksModelResponse)

            if (listOfEsignLinksModelResponse.status) {
                listOfEsignLinksModel = listOfEsignLinksModelResponse.applicantsList

                if (listOfEsignLinksModel.size > 0) {
                    setAdapter(listOfEsignLinksModel)
                } else {
                    setVisibility(View.GONE, View.VISIBLE)
                }
            } else {
                setVisibility(View.GONE, View.VISIBLE)
                showInternetConnectionLost(
                   this,
                    listOfEsignLinksModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })

        //-- Handle esignInitialise response
        viewModel.esignInitialise.observe(this, {
            registrationModel = it
            Timber.e("esignInitialise " + registrationModel)

        })
    }
    private fun initView() {
        toolbar.title = getString(R.string.esign_process)
        toolbar.navigationIcon = ResourcesCompat.getDrawable(resources,R.drawable.ic_baseline_arrow_back_24,null)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        refID = intent.getStringExtra(Constants.RefNo).toString()
        type = intent.getIntExtra(Constants.Type,0)

        appPreferences = AppPreferences.getAppPreferences(this)
        rcyclerViewListOfLinks!!.layoutManager = LinearLayoutManager(this)
        setVisibility(View.GONE, View.GONE)


    }

    private fun getListOfEsignLinks(loanID: String,type:Int?) {

        if (isNetworkAvailable(this)) {

            viewModel.getListOfEsignLinks(this,loanID,type)

        }
    }
    private fun setVisibility(isVisibleRecy: Int, isVisibleNoData: Int) {
        rcyclerViewListOfLinks.visibility = isVisibleRecy
        iv_no_data_view.visibility = isVisibleNoData
    }

    private fun setAdapter(loanDetailsList: List<ListOfEsignLinksModel>) {
        setVisibility(View.VISIBLE, View.GONE)
        adapter = ListOfEsignLinksAdapter(
            this,
            loanDetailsList,
            this,this,this
        )
        rcyclerViewListOfLinks!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    /*override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }*/

    override fun onLeadClick(position: Int) {

    }

    override fun onPaymentSheduleClickListener(position: Int) {


        if (ContextCompat.checkSelfPermission(
                this@SignPendingDetailActivity, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val manager =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                alertMessageNoGps(this@SignPendingDetailActivity, "Yes")
            } else {
                var progressDialog = showProgressDialog(this@SignPendingDetailActivity)


                object : AsyncTaskCoroutine<Int?, Boolean?>() {
                    override fun onPostExecute(@Nullable result: Boolean?) {
                        progressDialog.dismiss()

                        if (isNetworkAvailable(this@SignPendingDetailActivity)) {

                            val params: MutableMap<String, String> = HashMap()
                            params["refNo"] = refID
                            params["applicantId"] = listOfEsignLinksModel.get(position).applicantId
                            viewModel.esignInitialise(this@SignPendingDetailActivity, params)

                            /*val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(listOfEsignLinksModel.get(position).esignLisnk))
                            startActivity(browserIntent)*/

                            //Using Custom Tab
                            val builder = CustomTabsIntent.Builder()
                            val customBuilder = builder.build()

                            //  builder.setColorScheme(R.color.purple_500)

                            builder.setToolbarColor(
                                ContextCompat.getColor(
                                    this@SignPendingDetailActivity,
                                    R.color.purple_500
                                )
                            )

                            customBuilder.launchUrl(
                                this@SignPendingDetailActivity,
                                Uri.parse(listOfEsignLinksModel.get(position).esignLisnk)
                            )
                        }
                    }

                    override fun onPreExecute() {
                        progressDialog.show()
                        //getCurrentLatLong(requireActivity())
                        getLocation();
                    }

                    override fun doInBackground(vararg params: Int?): Boolean? {
                        return null
                    }
                }.execute<Any>()

            }
        } else {
            appPreferences?.putString(Constants.Latitude, "")
            appPreferences?.putString(Constants.Longitude, "")

            if (isNetworkAvailable(this)) {

                val params: MutableMap<String, String> = HashMap()
                params["refNo"] = refID
                params["applicantId"] = listOfEsignLinksModel.get(position).applicantId
                viewModel.esignInitialise(this, params)

                /*val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(listOfEsignLinksModel.get(position).esignLisnk))
                startActivity(browserIntent)*/

                //Using Custom Tab
                val builder = CustomTabsIntent.Builder()
                val customBuilder = builder.build()

                //  builder.setColorScheme(R.color.purple_500)

                builder.setToolbarColor(
                    ContextCompat.getColor(
                        this@SignPendingDetailActivity,
                        R.color.purple_500
                    )
                )

                customBuilder.launchUrl(
                    this@SignPendingDetailActivity,
                    Uri.parse(listOfEsignLinksModel.get(position).esignLisnk)
                )
            }
        }


        /*val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            alertMessageNoGps(this@SignPendingDetailActivity, "Yes")
        } else {
            getCurrentLatLong(this@SignPendingDetailActivity)
        if (isNetworkAvailable(this)) {

            val params: MutableMap<String, String> = HashMap()
            params["refNo"] = refID
            params["applicantId"] = listOfEsignLinksModel.get(position).applicantId
            viewModel.esignInitialise(this, params)

            *//*val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(listOfEsignLinksModel.get(position).esignLisnk))
            startActivity(browserIntent)*//*

            //Using Custom Tab
            val builder = CustomTabsIntent.Builder()
            val customBuilder = builder.build()

            //  builder.setColorScheme(R.color.purple_500)

            builder.setToolbarColor(
                ContextCompat.getColor(
                    this@SignPendingDetailActivity,
                    R.color.purple_500
                )
            )

            customBuilder.launchUrl(
                this@SignPendingDetailActivity,
                Uri.parse(listOfEsignLinksModel.get(position).esignLisnk)
            )
        }
        }*/

    }


    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this);
    }

    override fun onLocationChanged(location: Location) {
        val latitudeToSet: Float = location!!.latitude.toFloat()
        val longitudeToSet: Float = location!!.longitude.toFloat()
        appPreferences?.putString(Constants.Latitude, latitudeToSet.toString())
        appPreferences?.putString(Constants.Longitude, longitudeToSet.toString())
        // tvGpsLocation = findViewById(R.id.textView)
        //tvGpsLocation.text = "Latitude: " + location.latitude + " , Longitude: " + location.longitude
    }

    override fun onStatmentClickListener(position: Int) {

    }

    override fun onResume() {
        super.onResume()
        Timber.e("onResume", "onResume")
        getListOfEsignLinks(refID,type)
    }
}