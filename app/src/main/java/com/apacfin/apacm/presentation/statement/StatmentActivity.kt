package com.apacfin.apacm.presentation.statement

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.myloan.LoanDetailsViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.myloan.AccountStatementModelResponse
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.StatementModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_statment.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber
import java.util.HashMap

@AndroidEntryPoint
class StatmentActivity : AppCompatActivity(), StatementAdapter.OnClickListener {

    val TAG = StatmentActivity::class.java.simpleName
    private val viewModel: LoanDetailsViewModel by viewModels()
    var loading = false
    lateinit var accountStatementModelResponse: AccountStatementModelResponse
    var appPreferences: AppPreferences? = null
    var adapter: StatementAdapter? = null
    lateinit var loanDetails: LoanDetailModel
    lateinit var statementModelList: List<StatementModel>
    var loanID=""
    var fromView=0
    var fromDate=""
    var toDate=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statment)
        initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(this)
        viewModel.loading.observe(this, {
            loading = it;
            Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

            if (loading) {
                progressDialog.show()

            } else {
                progressDialog.dismiss()
                // showProgressDialog(this,false)
                //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
            }
        })

        //-- Handle getLoanDetails response
        viewModel.getAccountStatementResponse.observe(this, {
            accountStatementModelResponse = it
            Timber.e("accountStatementModelResponse " + accountStatementModelResponse)

            if (accountStatementModelResponse.status) {
                loanDetails = accountStatementModelResponse.loanDetails
                statementModelList = accountStatementModelResponse.statements
                setDataAndAdapter(loanDetails,statementModelList)
            } else {
                showInternetConnectionLost(
                    this,
                    accountStatementModelResponse.msg,
                    getString(R.string.ok)
                )
            }
        })

    }
    /*override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }*/
    private fun setDataAndAdapter(
        loanDetails: LoanDetailModel,
        statementModelList: List<StatementModel>
    ) {

        tvLoanID.text = loanDetails.loanId
        tvEMIAmount.text =  ""+amountFormater(loanDetails.emiAmount.toDouble()).toString()
        tvLoanType.text = loanDetails.loanType
        tvDueDate.text = loanDetails.dueDate

        adapter = StatementAdapter(
           this,
            statementModelList,fromView,
            this
        )
        rcyclerViewStatementDtl!!.adapter = adapter
        adapter!!.notifyDataSetChanged()

    }
    private fun initView() {
        fromView = intent.getIntExtra(Constants.FromView, 0)
        loanID = intent.getStringExtra(Constants.LoanID).toString()
        fromDate = intent.getStringExtra(Constants.FromDate).toString()
        toDate = intent.getStringExtra(Constants.ToDate).toString()

        Timber.e(TAG+" loanID: "+loanID )
        appPreferences = AppPreferences.getAppPreferences(this)
        rcyclerViewStatementDtl!!.layoutManager = LinearLayoutManager(this)

        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_baseline_arrow_back_24)
        toolbar.setNavigationOnClickListener { onBackPressed() }
         if (fromView == 1) {
            toolbar.title = resources.getString(R.string.a_c_statment)
             getStatementList(loanID)
        } else {
            toolbar.title = resources.getString(R.string.payment_shedule)
             getSheduleList(loanID)
        }

    }
    private fun getSheduleList(loanID:String) {
        if (isNetworkAvailable(this)) {
            /*val params: MutableMap<String, String> = HashMap()
            params["mobileNo"] = appPreferences?.getString(Constants.MobileNo,"").toString()
            params["dob"] = appPreferences?.getString(Constants.DOB,"").toString()*/
            viewModel.getPaymentShedule(loanID)
        }
    }
    private fun getStatementList(loanID:String) {
        if (isNetworkAvailable(this)) {
            val params: MutableMap<String, String> = HashMap()
            /*params["mobileNo"] = appPreferences?.getString(Constants.MobileNo,"").toString()
            params["dob"] = appPreferences?.getString(Constants.DOB,"").toString()*/
            params["fromDate"] = fromDate
            params["endDate"] = toDate

            viewModel.getAccountStatement(loanID,params)

        }
    }

    override fun onLeadClick(position: Int) {

    }
}