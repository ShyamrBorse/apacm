package com.apacfin.apacm.presentation.document

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apacfin.apacm.R
import com.apacfin.data.model.document.DocumentListModel
import com.apacfin.data.model.document.LoanListForDocModel
import com.apacfin.data.model.myloan.LoanDetailModel
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : DocumentMainAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class AllDocumentAdapter(
    context: Context, loanDetailModel: List<LoanListForDocModel>,
    onClickListener: OnClickListener
) : RecyclerView.Adapter<AllDocumentAdapter.ViewHolder?>() {
    var context: Context
    var adapter: DocumentGridAdapter? = null
    private val mOnClickListener: OnClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.contain_single_doc_main, parent, false)
        return ViewHolder(
            view,
            mOnClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loanDetailModel: LoanDetailModel = loanDetailModelData[position].loanDetails

       Timber.e(TAG+ " loanDetailModel  : " + loanDetailModel)


        holder.txtLoanID.text = "" + loanDetailModel.loanId
        holder.txtLoanType.text = "" + loanDetailModel.loanType

        holder.relyExpand.setOnClickListener {
            if (holder.rcyclerViewDocExpanded.getVisibility() === View.GONE) {
                // it's collapsed - expand it
                holder.rcyclerViewDocExpanded.setVisibility(View.VISIBLE)
                holder.ivExpand.setImageResource(R.drawable.chevron_up)

                val documentListModel:List<DocumentListModel> = loanDetailModelData[position].documents
                holder.rcyclerViewDocExpanded!!.layoutManager = GridLayoutManager(context,3)
                adapter = DocumentGridAdapter(context,documentListModel,loanDetailModelData[position].loanDetails.loanId)
                holder.rcyclerViewDocExpanded!!.adapter = adapter
                adapter!!.notifyDataSetChanged()
            } else {
                // it's expanded - collapse it
                holder.rcyclerViewDocExpanded.setVisibility(View.GONE)
                holder.ivExpand.setImageResource(R.drawable.chevron_down)
            }
        }
        Timber.e(TAG+ " loanDetailModelTest  : " + loanDetailModelData[position].loanDetails.loanId+" size:" + loanDetailModelData[position].documents.size)

    }

    override fun getItemCount(): Int {
        return loanDetailModelData.size
    }

    interface OnClickListener {
        fun onLeadClick(position: Int)
    }




    interface OnStatusClickListener {
        fun onLeadStatusClick(position: Int)
    }

    inner class ViewHolder(
        itemView: View,
        onClickListener: OnClickListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener,
        OnStatusClickListener {
        var txtLoanID: TextView
        var txtLoanType: TextView
        var rcyclerViewDocExpanded: RecyclerView
        var relyExpand: RelativeLayout
        var ivExpand: ImageView

        var mOnClickListener: OnClickListener

        override fun onClick(view: View) {

        }


        override fun onLeadStatusClick(position: Int) {}

        init {
            txtLoanID = itemView.findViewById(R.id.txtLoanID)
            txtLoanType = itemView.findViewById(R.id.txtLoanType)

            rcyclerViewDocExpanded = itemView.findViewById(R.id.rcyclerViewDocExpanded)
            ivExpand = itemView.findViewById(R.id.ivExpand)
            relyExpand = itemView.findViewById(R.id.relyExpand)

            mOnClickListener = onClickListener

            itemView.setOnClickListener(this)

        }

    }

    companion object {
        private val TAG = AllDocumentAdapter::class.java.simpleName
        lateinit var loanDetailModelData: List<LoanListForDocModel>

    }

    init {
        loanDetailModelData = loanDetailModel
        mOnClickListener = onClickListener

        this.context = context
    }
}
