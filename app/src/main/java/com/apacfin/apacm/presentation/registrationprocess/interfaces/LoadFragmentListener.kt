package com.apacfin.apacm.presentation.registrationprocess.interfaces

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment

interface LoadFragmentListener {
    fun loadFragListener(context:Context, type:String, bundle: Bundle?, fragment:Fragment)
}