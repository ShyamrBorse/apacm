package com.apacfin.apacm.presentation.home

import android.app.ProgressDialog.show
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.*
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.gratitude.GratituideFragment
import com.apacfin.apacm.presentation.home.adapter.*
import com.apacfin.apacm.presentation.referral.ReferralDialogFragment
import com.apacfin.apacm.util.isNetworkAvailable
import com.apacfin.apacm.util.showInternetConnectionLost
import com.apacfin.apacm.util.showProgressDialog
import com.apacfin.data.model.home.*
import com.apacfin.data.model.loanstatus.TimeLineModel
import com.apacfin.data.model.myloan.LoanDetailModel
import com.apacfin.data.model.myloan.LoanDetailsModelResponse
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home_new.*
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*


@AndroidEntryPoint
class HomeFragmentNew : Fragment(), DashBoardAdapter.OnDashBoardMenuListener,
    MyLoanDetailsAdapter.OnMyLoadViewDetailsListener,
    BannerAdapter.OnClickListener, PaymentHistoryAdapter.OnPaymentHistoryListener {
    val TAG: String = HomeFragmentNew::class.java.simpleName
    private val viewModel: HomeViewModel by viewModels()
    var loading = false
    lateinit var sliderModel: SliderModel
    lateinit var bannerModel: BannerModelResponse
    lateinit var loanDetailsModelResponse: LoanDetailsModelResponse
    lateinit var testimonialModelResponse: TestimonialModelResponse
    lateinit var registrationModel: RegistrationModel
    lateinit var paymentHistoryResponse: PaymentHistoryResponse

    //    lateinit var itemsList : ArrayList<DashBoardModel>
//    var adapter: MyLoanAdapterHome? = null
    var adapter: MyLoanAdapterHome? = null
    var madapter: MyLoanDetailsAdapter? = null
    var adapterTestimonial: TestimonialAdapter? = null
    var adapterBanner: BannerAdapter? = null
    var loanDetailsList: List<LoanDetailModel> = ArrayList<LoanDetailModel>()
    lateinit var testimonialList: List<TestimonialModel>
    lateinit var bannerList: List<BannerModel>
    private var mDataList = ArrayList<TimeLineModel>()
    private var loanId: String = ""

    private var param1: String? = null
    private var param2: String? = null
    var timer: Timer? = null
    val DELAY_MS: Long = 700 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 5000
    val handler = Handler()

    var list = mutableListOf<String>()
    var appPreferences: AppPreferences? = null
    var currentPageSlider = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    //new flow

    private val itemsList = ArrayList<DashBoardModel>()
    private var paymentHistoryList = ArrayList<EmiPayHistoryDetailModel>()
    private lateinit var dashboardAdapter: DashBoardAdapter
    private lateinit var paymentHistoryAdapter: PaymentHistoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_new, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        newUIInitView()
        //-- Handle progressDialog loading
        val progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, {
            loading = it
            if (loading) {
                progressDialog.show()
            } else {
                progressDialog.dismiss()
            }
        })


        //-- Handle getBanner response
        viewModel.getBannerResponse.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                bannerModel = it
                Timber.e("getBanner " + bannerModel)
                if (bannerModel.status) {
                    if (!bannerModel.banners.isNullOrEmpty()) {
                        bannerList = bannerModel.banners
                        setAdapterBanner(bannerList)
                        rcyclerViewBanner.visibility = View.VISIBLE
                    }
                } else {
                    rcyclerViewBanner.visibility = View.GONE
                    showInternetConnectionLost(
                        requireContext(),
                        bannerModel.msg,
                        getString(R.string.ok)
                    )
                }
            }
        })

        //-- Handle getLoanDetails response
        viewModel.getLoanDetails.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                loanDetailsModelResponse = it
                Timber.e("loanDetailsModelResponse " + loanDetailsModelResponse)

                if (loanDetailsModelResponse.status) {
                    loanDetailsList = loanDetailsModelResponse.loanDetails
//                setAdapterLoan(loanDetailsList)  changes by shweta
                    setAdapterLoanDetails(loanDetailsList)
                    if(loanDetailsList.size>0) {
                        loanId = loanDetailsList.get(0).loanId
                        if (!loanId.equals("")) {
                            getPaymentHistory(loanId)
                        }
                    }else{
                        tvNoRecordFound.visibility = View.VISIBLE
                        rcyclerPaymentHistory.visibility = View.GONE
                    }

                } else {
                    showInternetConnectionLost(
                        requireContext(),
                        loanDetailsModelResponse.msg,
                        getString(R.string.ok)
                    )
                }
            }
        })


        //-- Handle getLoanStageDetails response
        viewModel.getLoanStageDetails.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                registrationModel = it
                Timber.e("getLoanStageDetails " + registrationModel)

                if (registrationModel.status) {
                    if (!registrationModel.loanStageData.isEmpty()) {

                        mDataList = registrationModel.loanStageData
                        //initRecyclerView(mDataList)

                        val transaction: FragmentTransaction =
                            requireActivity().supportFragmentManager.beginTransaction()
                        LoanStatusDialogFragment.newInstance(
                            loanId,
                            mDataList
                        ).apply {
                            show(transaction, tag)

                        }
                    }


                } else {
                    showInternetConnectionLost(
                        requireContext(),
                        registrationModel.msg,
                        getString(R.string.ok)
                    )
                }
            }
        })

        //-- Handle get loan emi history response
        viewModel.getPaymentHistoryResponse.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                paymentHistoryResponse = it
                Timber.e("getLoanLoanEmiHistoryDetails " + paymentHistoryResponse)

                if (paymentHistoryResponse.status == true) {
                    tvNoRecordFound.visibility = View.GONE
                    rcyclerPaymentHistory.visibility = View.VISIBLE

                    paymentHistoryList = paymentHistoryResponse.emiDetailList
                    setAdapterForPaymentHistory()
//                    val transaction: FragmentTransaction =
//                        requireActivity().supportFragmentManager.beginTransaction()
//                    LoanStatusDialogFragment.newInstance(
//                        loanId,
//                        mDataList
//                    ).apply {
//                        show(transaction, tag)
//
//                    }

                } else {
                    tvNoRecordFound.visibility = View.VISIBLE
                    rcyclerPaymentHistory.visibility = View.GONE
//                showInternetConnectionLost(
//                    requireContext(),
//                    paymentHistoryResponse.msg.toString(),
//                    getString(R.string.ok)
//                )
                }
            }
        })

        //-- Handle getGratitude response
        viewModel.getGratitude.observe(viewLifecycleOwner, {
            sliderModel = it
            Timber.e("getGratitude " + sliderModel)

            if (sliderModel.status) {
                if (!sliderModel.sliderList.isEmpty()) {
                    val transaction: FragmentTransaction =
                        requireActivity().supportFragmentManager.beginTransaction()
                    GratituideFragment.newInstance(sliderModel.sliderList.get(0), "").apply {
                        show(transaction, tag)
                    }

                    appPreferences?.putBoolean(Constants.isDayFirstLogin, true)
                    //  appPreferences?.putString(LoginDate, getCurrentDate())
                }


            } else {
                showInternetConnectionLost(
                    requireContext(),
                    sliderModel.msg,
                    getString(R.string.ok)
                )
            }
        })


        viewModel.getKnowledgeSeries.observe(viewLifecycleOwner, {
            sliderModel = it
            Timber.e("sliderModel " + sliderModel)

            if (sliderModel.status) {
                rlHomeViewBanner.visibility = View.VISIBLE
                list.addAll(sliderModel.sliderList)
                setAdapter(list)
            } else {
                rlHomeViewBanner.visibility = View.GONE

                showInternetConnectionLost(
                    requireContext(),
                    sliderModel.msg,
                    getString(R.string.ok)
                )
            }
        })
    }


    private fun setAdapter(list: MutableList<String>) {

        val adapter = SliderAdapter(requireActivity(), list)
        home_viewpagerSlider.adapter = adapter
        home_viewpagerSlider.setClipToPadding(false)
        home_viewpagerSlider.setPadding(10, 0, 10, 0)
        home_indicator.attachToPager(home_viewpagerSlider)
    }

    fun setAdapterForPaymentHistory() {
        rcyclerPaymentHistory!!.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        paymentHistoryAdapter = PaymentHistoryAdapter(requireActivity(), paymentHistoryList, this)
        val linearPaymentHistoryLayoutManager = LinearLayoutManager(context)
        rcyclerPaymentHistory.layoutManager = linearPaymentHistoryLayoutManager
        rcyclerPaymentHistory.adapter = paymentHistoryAdapter
        rcyclerPaymentHistory!!.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        if(paymentHistoryList!=null && paymentHistoryList.size>0){
            tvNoRecordFound.visibility = View.GONE
            rcyclerPaymentHistory.visibility = View.VISIBLE
        }else{
            tvNoRecordFound.visibility = View.VISIBLE
            rcyclerPaymentHistory.visibility = View.GONE
        }
    }

    fun newUIInitView() {

        val snapHelper: SnapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(rcyclerViewDashBoard)
        itemsList.add(
            DashBoardModel(
                resources.getString(R.string.genearte_v_card),
                R.drawable.generatevcard,
                Constants.GenerateVCard
            )
        )
        itemsList.add(
            DashBoardModel(
                resources.getString(R.string.branch_connect),
                R.drawable.branchconnect,
                Constants.BranchConnect
            )
        )
        itemsList.add(
            DashBoardModel(resources.getString(R.string.pay_emi),
                R.drawable.payemi,Constants.PayEMI))
        itemsList.add(
            DashBoardModel(
                resources.getString(R.string.refer_and_earn),
                R.drawable.refer_and_earn,
                Constants.ReferandEarn
            )
        )
        itemsList.add(
            DashBoardModel(
                resources.getString(R.string.loan_tracker),
                R.drawable.loantracker,
                Constants.LoanTracker
            )
        )
        itemsList.add(
            DashBoardModel(resources.getString(R.string.documents), R.drawable.documents,
                Constants.Documents))
        dashboardAdapter = DashBoardAdapter(
            requireActivity(),
            itemsList, this
        )
        val linearLayoutManager = GridLayoutManager(context, 3)
        rcyclerViewDashBoard.layoutManager = linearLayoutManager
        rcyclerViewDashBoard.adapter = dashboardAdapter
    }


    private fun setAdapterLoanDetails(loanDetailsList: List<LoanDetailModel>) {

        rcyclerViewLoanDetails!!.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        madapter = MyLoanDetailsAdapter(
            this, rcyclerViewLoanDetails,
            requireActivity(),
            loanDetailsList
        )
        rcyclerViewLoanDetails!!.adapter = madapter
        madapter!!.notifyDataSetChanged()


        var lastVisibleItemPosition: Int = 0

        rcyclerViewLoanDetails.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val myLayoutManager: LinearLayoutManager =
                    rcyclerViewLoanDetails!!.layoutManager as LinearLayoutManager
                //val findLastVisibleItemPosition = myLayoutManager.findLastVisibleItemPosition()
                val findFirstCompletelyVisibleItemPosition =
                    myLayoutManager.findFirstCompletelyVisibleItemPosition()
                //val findLastCompletelyVisibleItemPosition = myLayoutManager.findLastCompletelyVisibleItemPosition()
                //val findFirstVisibleItemPosition = myLayoutManager.findFirstVisibleItemPosition()
                if (findFirstCompletelyVisibleItemPosition != -1) {
                    getPaymentHistory(loanDetailsList.get(findFirstCompletelyVisibleItemPosition).loanId)
                    loanId = loanDetailsList.get(findFirstCompletelyVisibleItemPosition).loanId
                }

                lastVisibleItemPosition = myLayoutManager.findLastCompletelyVisibleItemPosition();
            }
        })
        rcyclerViewLoanDetails.scrollToPosition(lastVisibleItemPosition)
    }


    private fun setAdapterBanner(bannerModelList: List<BannerModel>) {

        rcyclerViewBanner!!.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
//        val snapHelper: SnapHelper = PagerSnapHelper()
//        snapHelper.attachToRecyclerView(rcyclerViewBanner)

        adapterBanner = BannerAdapter(rcyclerViewBanner, requireContext(), bannerModelList, this)
        rcyclerViewBanner!!.adapter = adapterBanner
        adapterBanner!!.notifyDataSetChanged()

    }

    private fun initView() {

        // cardBottonNav.startAnimation(translateAnimation)
        loanDetailsList = ArrayList<LoanDetailModel>()
        getSliderList()
        // getTestimonials()
        getKnowledgeSeries()
        getLoanList()

        appPreferences = AppPreferences.getAppPreferences(requireContext())
        if (appPreferences?.getString(Constants.gratitudeFlag, "")
                .equals("0") || appPreferences?.getString(Constants.gratitudeFlag, "").equals("1")
        ) {
            //getCurrentDayLogin(requireContext())

            if (!appPreferences?.getBoolean(Constants.isDayFirstLogin, false)!!) {
                getGratitude()
            }
        }


    }
    private fun getGratitude() {
        if (isNetworkAvailable(requireContext())) {

            val params: MutableMap<String, String> = HashMap()
            params["appVersion"] = appPreferences?.getString(Constants.gratitudeFlag, "").toString()
            viewModel.getGratitude(requireActivity(), params)

        }
    }

    private fun getSliderList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getBanner(requireActivity())
        }
    }

    private fun getPaymentHistory(loadId: String) {
        if (isNetworkAvailable(requireContext())) {
            //viewModel.sliderList(requireActivity())
            viewModel.getPaymentHistory(requireActivity(), loadId)
        }
    }


    private fun getLoanList() {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getMyLoans(requireActivity())
        }
    }

    private fun getLoanStatus(loanId: String) {
        if (isNetworkAvailable(requireContext())) {
            viewModel.getLoanStageDetails(requireActivity(), loanId)
        }
    }

    override fun onPause() {
        super.onPause()
        Timber.e("aa onPause")
        handler.removeCallbacksAndMessages(null)
        timer?.cancel()
    }

    override fun onMyLoadViewDetailsListener(context: Context, loanDetailModel: LoanDetailModel) {
        var dashBoardModel: DashBoardModel = DashBoardModel("LoanViewDetails", R.drawable.apac_icon,Constants.LoanViewDetails)
        val dashBoardMenuItemClick: MenuItemClickListener? = context as MenuItemClickListener?
        dashBoardMenuItemClick?.onDashboardMenuItemClickListener(
            context,
            dashBoardModel,
            loanDetailsList, loanId
        )
    }

    override fun onBannerClick(position: Int) {
        Timber.e(TAG + "onBannerClick: " + bannerList.size)
        if (bannerList[position].action.equals("ESIGN")) { //ESIGN/EMI
            findNavController().navigate(R.id.frgDocument)
        }
        if (bannerList[position].action.equals("EMI")) {
            findNavController().navigate(R.id.frgMyLoan)
        }
        if (bannerList[position].action.equals("ReferAndErn")) {
            val transaction: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            ReferralDialogFragment().apply {
                show(transaction, tag)
            }
        }
    }

    override fun onDashBoardMenuListener(context: Context, model: DashBoardModel) {
        if (model.card_id.equals(Constants.LoanTracker)) {
            if (loanDetailsList != null && loanDetailsList.size > 0) {

                //SelectLoanIdForLoanTrack(context, loanDetailsList)
                getLoanStatus(loanId)
            }else{
                com.apacfin.apacm.util.showToast(context, "Yet Loan Details Not Assigned!")
            }
        } else {
            val dashBoardMenuItemClick: MenuItemClickListener? = context as MenuItemClickListener?
            dashBoardMenuItemClick?.onDashboardMenuItemClickListener(
                context,
                model,
                loanDetailsList, loanId
            )
        }
    }

    private fun SelectLoanIdForLoanTrack(context: Context, loanDetailsList: List<LoanDetailModel>) {
        //Inflate the dialog with custom view
        val mDialogView =
            LayoutInflater.from(context).inflate(R.layout.choose_loan_for_tracker, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        //show dialog
        val mAlertDialog = mBuilder.show()
        val rvRadioGrpSelectLoan: RecyclerView? =
            mAlertDialog.findViewById(R.id.rvRadioGrpSelectLoan)
        val btnViewLoanTrackerDetails: Button? =
            mAlertDialog.findViewById(R.id.btnViewLoanTrackerDetails)
        val ivClose: ImageView? = mAlertDialog.findViewById(R.id.ivClose)

        var temploanDetailsModelList: MutableList<LoanDetailModel> =
            mutableListOf<LoanDetailModel>()
        for ((i, model) in loanDetailsList.withIndex()) {
            if (i == 0)
                model.isLoanTypeSelectedForHistory = true
            else
                model.isLoanTypeSelectedForHistory = false
            temploanDetailsModelList.add(model)
        }

        var selectLoanTypeAdapter = SelectLoanTypeAdapter(context, temploanDetailsModelList)
        val linearPaymentHistoryLayoutManager = LinearLayoutManager(context)
        rvRadioGrpSelectLoan?.layoutManager = linearPaymentHistoryLayoutManager
        rvRadioGrpSelectLoan?.adapter = selectLoanTypeAdapter
        rvRadioGrpSelectLoan!!.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


        ivClose?.setOnClickListener(View.OnClickListener {
            mAlertDialog.dismiss()
        })

        btnViewLoanTrackerDetails?.setOnClickListener {
            mAlertDialog.dismiss()
            var temploanDetailsModelLists: MutableList<LoanDetailModel> =
                selectLoanTypeAdapter.getLoanDetailsModelList()
            for (model in temploanDetailsModelLists) {
                if (model.isLoanTypeSelectedForHistory == true)
                    loanId = model.loanId
            }
            if (!loanId.equals("")) {
                getLoanStatus(loanId)
            } else {
                com.apacfin.apacm.util.showToast(context, "Please select loan type!")
            }
        }

    }

    override fun onPaymentHistoryListener(context: Context, model: EmiPayHistoryDetailModel) {

        //Inflate the dialog with custom view
        val mDialogView =
            LayoutInflater.from(context).inflate(R.layout.dialog_payment_history, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        //show dialog
        val mAlertDialog = mBuilder.show()

        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvEmiDate: TextView? = mAlertDialog.findViewById(R.id.tvEmiDate)
        val tvEmiAmount: TextView? = mAlertDialog.findViewById(R.id.tvEmiAmount)
        val tvLoanId: TextView? = mAlertDialog.findViewById(R.id.tvLoanId)
        val ivCustPaidEmiAmt: TextView? = mAlertDialog.findViewById(R.id.ivCustPaidEmiAmt)
        val btnOkPaymentDetails: Button? = mAlertDialog.findViewById(R.id.btnOkPaymentDetails)

        tvLoanId?.text = loanId
        tvEmiDate?.text = model.emiDate

        val formatter = DecimalFormat("#,###,###")
        val formattedEmiAmt: String = formatter.format(model.emiAmount?.toInt())
        val formattedPaidAmt: String = formatter.format(model.customerPaidAmount?.toInt())
        tvEmiAmount?.text = "\u20B9 " + formattedEmiAmt
        ivCustPaidEmiAmt?.text = "\u20B9 " + formattedPaidAmt

        btnOkPaymentDetails?.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    private fun getKnowledgeSeries() {
        if (isNetworkAvailable(requireContext())) {
            //viewModel.sliderList(requireActivity())
            viewModel.getKnowledgeSeries(requireActivity(), "knowledgeSeries")

        }
    }

}