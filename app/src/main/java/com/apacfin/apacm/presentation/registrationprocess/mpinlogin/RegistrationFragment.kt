package com.apacfin.apacm.presentation.registrationprocess.mpinlogin

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.observe
import com.apacfin.apacm.BuildConfig
import com.apacfin.apacm.R
import com.apacfin.apacm.presentation.registrationprocess.interfaces.LoadFragmentListener
import com.apacfin.apacm.presentation.registrationprocess.newuser.NewUserDialogFragment
import com.apacfin.apacm.presentation.registrationprocess.registration.RegistrationViewModel
import com.apacfin.apacm.util.*
import com.apacfin.data.model.profile.AppDetailModel
import com.apacfin.data.model.registration.RegistrationModel
import com.apacfin.data.prefrerce.AppPreferences
import com.apacfin.data.prefrerce.Constants
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.CredentialsApi
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_registration.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


@AndroidEntryPoint
class RegistrationFragment : Fragment(), View.OnClickListener {

    private val viewModel: RegistrationViewModel by viewModels()
    var loading = false
    lateinit var registrationModel: RegistrationModel
    lateinit var appDetailModel: AppDetailModel
    var name = ""
    var mobileNo = ""
    var dob = ""
    var refNo = ""

    //   var isGetUCIC = false
    var appPreferences: AppPreferences? = null
    var permission  = arrayOf(Manifest.permission.READ_PHONE_STATE, /*Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.MANAGE_EXTERNAL_STORAGE,*/ Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

    var cal = Calendar.getInstance()

    private val iRequestCodePhoneNumber = 100

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Toast.makeText(requireContext(),"Register",Toast.LENGTH_SHORT).show()
        appPreferences = AppPreferences.getAppPreferences(requireActivity())
        initView()
        //-- Handle progressDialog loading
        var progressDialog = showProgressDialog(requireActivity())
        viewModel.loading.observe(viewLifecycleOwner, {
            progressDialog.dismiss()
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                loading = it;
                Timber.e("SmileyFaceCanvasloading " + loading + " " + it)

                if (loading) {
                    progressDialog.show()
                    // showProgressDialog(this)
                    // progressDialog!!.show()
                } else {
                    progressDialog.dismiss()
                    // showProgressDialog(this,false)
                    //  Handler().postDelayed(Runnable { progressDialog.dismiss() }, 5000)
                }
            }
        })


        //-- Handle getRegistration response
        viewModel.getRegistration.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                registrationModel = it
                Timber.e("getRegistration " + registrationModel)

                if (registrationModel.status) {
                    refNo = registrationModel.refNo
                    name = registrationModel.name

                    if (registrationModel.newUserFlag) {
                        NewUserDialogFragment.newInstance(
                            edtMobileNo.text.toString(),
                            edtDob.text.toString()
                        ).apply {
                            //show(supportFragmentManager, tag)
                        }
                    } else {

                        val bundle = Bundle()
                        // bundle.putString(Constants.Ucic, edtUCIC.text.toString())
                        bundle.putString(Constants.Name, name)
                        bundle.putString(Constants.MobileNo, edtMobileNo.text.toString())
                        bundle.putString(Constants.DOB, edtDob.text.toString())
                        bundle.putString(Constants.RefNo, refNo)
                        bundle.putString(Constants.Fname, "")
                        bundle.putString(Constants.Lname, "")
                        bundle.putString(Constants.Pincode, "")
                        bundle.putBoolean(Constants.FromView, false)
                        //viewModel.loadFragment(requireActivity(),bundle)

                        val fr: Fragment = VerifyOtpFragment()
                        val fc: LoadFragmentListener? = activity as LoadFragmentListener?
                        fc?.loadFragListener(requireActivity(), Constants.VerifyOTPFrg, bundle, fr)
//                    OtpDialogFragment().apply {
//                        //show(supportFragmentManager, tag)
//                        arguments = bundle
//                    }
                    }


                } else {
                    showInternetConnectionLost(
                        requireActivity(),
                        registrationModel.msg,
                        getString(R.string.ok)
                    )
                    //  showSnackBar(this, getDetailsFromUCIC.msg) }
                    //startActivity(Intent(this, MainActivity::class.java))
                }
            }
        })


        //-- Handle getAppDetails response
        viewModel.getAppDetailsResponse.observe(viewLifecycleOwner, {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                appDetailModel = it
                Timber.e("getAppDetails " + appDetailModel)
                if (appDetailModel.status) {
                    appPreferences?.putString(Constants.AppVersion, appDetailModel.version)

                    if (appDetailModel.forceUpdate) {
                        val dialog = MaterialAlertDialogBuilder(
                            requireActivity(),
                            R.style.MyMaterialAlertDialog
                        ).create()
                        dialog.setTitle(getString(R.string.update_title))
                        //dialog.setIcon(R.drawable.ic_new_logo_foreground)
                        dialog.setMessage(getString(R.string.update_msg))
                        dialog.setCancelable(false)

                        dialog.setButton(
                            DialogInterface.BUTTON_POSITIVE,
                            getString(R.string.update)
                        ) { _, _ ->

                            try {
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id=com.apacfin.apacm")
                                    )
                                )
                            } catch (e: ActivityNotFoundException) {
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("https://play.google.com/store/apps/details?id=com.apacfin.apacm")
                                    )
                                )
                            }
                        }
                        dialog.create()
                        dialog.show()
                    }else{
                        checkValidationGetOpt()
                    }
                }
            }

        })

    }

    private fun initView() {
        btnGetOTP.setOnClickListener(this)
        edtDob.setOnClickListener(this)
        // edtMobileNo.addTextChangedListener(mTextWatcher)
        requestHint()
        disableCopyPasteOperations(edtMobileNo)
        //disableCopyPasteOperations(edtDob)

        edtMobileNo.filterTouchesWhenObscured = true
        edtDob.filterTouchesWhenObscured = true
        btnGetOTP.filterTouchesWhenObscured = true
        // create an OnDateSetListener
        var dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                //val myFormat = "dd-MMM-yyyy" // mention the format you need
                val sdf = SimpleDateFormat(Constants.dateFormat, Locale.US)
                edtDob.setText(sdf.format(cal.getTime()))

//                if((!edtDob.text.toString().equals("")) && (!edtMobileNo.text.toString().equals("")) && (edtMobileNo.text.toString().length==10)){
//                    btnGetOTP.setBackgroundResource(R.drawable.btn_orange_curve)
//                }
            }
        }
        edtDob.setOnClickListener {

            val datePickerDialog = DatePickerDialog(
                requireActivity(), R.style.SpinnerDatePickerDialog,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )

            //following line to restrict future date selection
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis())
            datePickerDialog.show()
        }

        edtMobileNo.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
//                if((!edtDob.text.toString().equals("")) && (!s.toString().equals("")) && (s.toString().length==10)){
//                    btnGetOTP.setBackgroundResource(R.drawable.btn_orange_curve)
//                }else{
//                    btnGetOTP.setBackgroundResource(R.drawable.btn_color)
//                }
            }
        })


    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.btnGetOTP -> {

                if (!isValidPhoneNumber(edtMobileNo)) {
                    tfMobileNo.setError(resources.getString(R.string.er_enter_mobile))
                } else if (isEmpty(edtDob)) {
                    tfDob.setError(resources.getString(R.string.er_enter_dob))
                } else {
                    tfMobileNo.error = null
                    tfDob.error = null
                    getAppDetails()
                }

            }
        }

    }

    private fun getAppDetails() {

        if (isNetworkAvailable(requireActivity())) {
            val versionName: String = BuildConfig.VERSION_NAME
            val params: MutableMap<String, String> = java.util.HashMap()
            params["appVersion"] = versionName
            viewModel.getAppDetails(requireActivity(),params)
        }
    }

    private fun checkValidationGetOpt() {
        //getCurrentLatLong(requireActivity())
        //getDeviceId(requireActivity())
        if (isNetworkAvailable(requireActivity())) {
            //if (isGetUCIC) {
            if (!isValidPhoneNumber(edtMobileNo)) {

                tfMobileNo.setError(resources.getString(R.string.er_enter_mobile))


            } else if (isEmpty(edtDob)) {

                tfDob.setError(resources.getString(R.string.er_enter_dob))

            } else {
                tfMobileNo.error = null
                tfDob.error = null
                mobileNo = edtMobileNo.text.toString()
                dob = edtDob.text.toString()
                val params: MutableMap<String, String> = HashMap()
                params["mobileNo"] = mobileNo
                params["dob"] = dob

                viewModel.getRegistration(requireActivity(),params)
            }
        }


    }

    @SuppressLint("LongLogTag")
    private fun requestHint() {
        Timber.e("requestHint")
        val hintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()
        val intent = Credentials.getClient(requireActivity()).getHintPickerIntent(hintRequest)
        //val intent = Auth.CredentialsApi.getHintPickerIntent(hintRequest)

        try {
            Timber.e("requestHint try")
            startIntentSenderForResult(intent.intentSender,
                iRequestCodePhoneNumber, null, 0, 0, 0,null)
        } catch (e: Exception) {
            Timber.e("requestHint Error In getting Message", e.message.toString())
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // permissionHandler.onActivityResult(requestCode)
        Timber.e("onActivityResult: "+requestCode+" : "+resultCode)
        if (requestCode == iRequestCodePhoneNumber && resultCode == Activity.RESULT_OK) {
            val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY)
            val phoneNumber = credential?.id?.takeLast(10)
            Timber.e("phoneNumber "+phoneNumber)

            // *** Do something with the phone number here ***
            edtMobileNo.setText(phoneNumber)
            appPreferences?.putString(Constants.SimMobNo, phoneNumber)
        } else if (
            requestCode == iRequestCodePhoneNumber &&
            resultCode == CredentialsApi.ACTIVITY_RESULT_NO_HINTS_AVAILABLE
        ) {
            // *** No phone numbers available ***
            if(appPreferences?.getBoolean("NumberNotFound", false)==false){
                Toast.makeText(requireActivity(), "No phone numbers found", Toast.LENGTH_LONG).show()
            }
            appPreferences?.putBoolean("NumberNotFound", true)
        }
    }

}