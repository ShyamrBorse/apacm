package com.apacfin.apacm.presentation.home.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.apacfin.apacm.R
import com.apacfin.apacm.util.GlideApp
import kotlinx.android.synthetic.main.slider_item.view.*
import timber.log.Timber


/**
 *
 * @author SHYAM BORSE
 *
 *         © Copyright APAC Financial Services
 *
 *         File Name : SliderAdapter.kt
 *
 *         Modification History
 *
 *         16-Oct-2020 Shyam Borse : Initial version
 *                               01-Jul-2021 First Last : Fix issue with getting reportee details method
 */

class SliderAdapter(
    private val context: FragmentActivity?,
    flashSaleSlider: List<String>
) : PagerAdapter() {


    private var inflater: LayoutInflater? = null
    private val sliderData = flashSaleSlider

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view === `object`
    }

    override fun getCount(): Int {

        return sliderData?.size!!
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater!!.inflate(R.layout.slider_item, null)

        Timber.e( "instantiateItem: "+sliderData?.get(position))
        /*view.ivTopSlider.load(sliderData?.get(position)) {
            crossfade(true)
            placeholder(R.drawable.banner)
            error(R.drawable.banner)
            //transformations(CircleCropTransformation())
        }*/
        GlideApp.with( view.ivTopSlider).load(sliderData?.get(position)).error(R.drawable.banner).into( view.ivTopSlider)
        val vp = container as ViewPager
        vp.addView(view, 0)


        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}